#include "Driver.hh"

/**
 * Pomocnicza funkcja konwertująca.
 * @param QString
 * @return bool Zwraca false <=> gdy str=="0".
 */
static bool QStringNum2Bool(QString str)
{
  if(str=="0")
    return false;
  else
    return true;
}

/**
 * Pomocnicza funkcja konwertująca.
 * @param Deg liczba w stopniach.
 * @return Zwraca wartość HEX odpowiadającą liczie stopni dla serw Dynamixel.
 */
static uint16_t DegToHex(double Deg)
{
  return static_cast<uint16_t>(floor(0x3ff*Deg/300));
}

/**
 * Pomocnicza f-cja transformująca.
 * @param d liczba do transformacji.
 * @param min dolna granica przedziału liczby wynikowej.
 * @param max górna granica przedziału liczby wynikowej.
 * @return liczba 16-sto bitowa, bez znaku. 
 */
static uint16_t Norm2hex(double d, uint16_t min, uint16_t max)
{
  if (max>min)
    return min+floor(d*(max-min));
  else
    return max+floor(d*(min-max));
}

Driver::Driver()
{
  // Przygotowanie obsługi XML 
  QDomDocument doc("DriverConfig");
  QFile file(DRIVER_CONFIG_XML);
  if(!file.open(QIODevice::ReadOnly))
  {
    std::cerr<<"Driver: Driver(): error: can't open DRIVER_CONFIG_XML file"<<std::endl;
    exit(-1);
  }
  if(!doc.setContent(&file))
  {
    file.close();
    std::cerr<<"Driver: Driver(): error: can't set XML file content"<<std::endl;
    exit(-2);
  }
  file.close();

  QDomElement root=doc.documentElement();
  if(root.tagName()!="DriverConfig")
  {
    std::cerr<<"Driver: Driver(): error: can't find root in DRIVER_CONFIG_XML file "<<std::endl;
    exit(-3);
  }

  // Parametry do pobrania z pliku XML
  std::string RightCamDev;
  std::string LeftCamDev;
  std::string SerialDev;
  bool ServosDebug;

  //Odczyt parametrów
  QDomNode n=root.firstChild();
  QDomElement e=n.toElement();
  if(e.tagName()=="configs")
  {
    LeftCamDev=e.attribute("LeftCamDev","").toStdString();
    RightCamDev=e.attribute("RightCamDev","").toStdString();
    SerialDev=e.attribute("SerialDev","").toStdString();
    ServosDebug=QStringNum2Bool(e.attribute("ServosDebug",""));
    debug=QStringNum2Bool(e.attribute("DriverSignalsDebug",""));
  }
  else
  {
    std::cerr<<"Driver: Driver(): error: Driver/DriverConfig.xml wrong construction"<<std::endl;
    exit(-4);
  }

  //Inicjalizacja modułu
  InitServoConfigs();
  RightCam.CamInit(RightCamDev);
  LeftCam.CamInit(LeftCamDev);
  DynServos.InitDynamixelModule(&ServoConfigs,ServosDebug,SerialDev);
}

DrvServoConfig GetConf(const QDomElement & e, unsigned int id, 
    std::map <Servo::ServoId, QString> & ServoIdOnQString)
{
  double _minPosDeg=0; // wartosci poczatkowe, na wypadek...
  double _maxPosDeg=0; // ... gdyby serwa nie zdefiniowano w pliku
  uint16_t _minPosHex=0;
  uint16_t _maxPosHex=0;
  uint16_t _minVelHex=0;
  uint16_t _maxVelHex=0;
  int _ret_lev=0;

  QDomNode n = e.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName()=="Servo" and 
	  e.attribute("ServoId","")==ServoIdOnQString[static_cast<Servo::ServoId>(id)])
      {
	_minPosDeg=e.attribute( "minPosDeg", "" ).toDouble();
	_maxPosDeg=e.attribute( "maxPosDeg", "" ).toDouble();

	_minPosHex=DegToHex(_minPosDeg);
	_maxPosHex=DegToHex(_maxPosDeg);

	unsigned long int tmp;
	sscanf(e.attribute("minVelHex","").toStdString().c_str(),"%lx",&tmp);
	_minVelHex=static_cast<uint16_t>(tmp);
	sscanf(e.attribute("maxVelHex","").toStdString().c_str(),"%lx",&tmp);
	_maxVelHex=static_cast<uint16_t>(tmp);

	_ret_lev=e.attribute( "RetLev", "" ).toInt();
      }
    }
    n = n.nextSibling();
  }

  return DrvServoConfig(_minPosDeg,_maxPosDeg,
      _minPosHex,_maxPosHex,_minVelHex,_maxVelHex,_ret_lev);
}

void Driver::InitServoConfigs()
{
  ServoConfigs.reserve(Servo::TOTAL);

  // Przygotowanie obsługi XML 
  QDomDocument doc("DrvServoConfig");
  QFile file(DRIVER_SERVO_CONF_XML);
  if(!file.open(QIODevice::ReadOnly))
  {
    std::cerr<<"Driver: InitServoConfigs(): error: can't open DRV_SERVO_CONF_XML file"<<std::endl;
    exit(-1);
  }

  if(!doc.setContent(&file))
  {
    file.close();
    std::cerr<<"Driver: InitServoConfigs(): error: can't set XML file content"<<std::endl;
    exit(-2);
  }
  file.close();

  QDomElement root=doc.documentElement();
  if(root.tagName()!="DrvServoConfig")
  {
    std::cerr<<"Driver: InitServoConfigs(): error: can't find root in DRV_SERVO_CONF_XML file "<<std::endl;
    exit(-3);
  }

  // deklaracja mapy do tlumaczenia informacji z pliku xml
  std::map <Servo::ServoId,QString> ServoIdOnQString;
  ServoIdOnQString[Servo::HeadV]="HeadV";
  ServoIdOnQString[Servo::HeadH]="HeadH";
  ServoIdOnQString[Servo::HeadR]="HeadR";
  ServoIdOnQString[Servo::LeftEyeV]="LeftEyeV";
  ServoIdOnQString[Servo::LeftEyeH]="LeftEyeH";
  ServoIdOnQString[Servo::RightEyeV]="RightEyeV";
  ServoIdOnQString[Servo::RightEyeH]="RightEyeH";
  ServoIdOnQString[Servo::BotRightLips]="BotRightLips";
  ServoIdOnQString[Servo::BotLeftLips]="BotLeftLips";
  ServoIdOnQString[Servo::TopRightLips]="TopRightLips";
  ServoIdOnQString[Servo::TopLeftLips]="TopLeftLips";
  ServoIdOnQString[Servo::LeftEyeLidV]="LeftEyeLidV";
  ServoIdOnQString[Servo::LeftEyeLidR]="LeftEyeLidR";
  ServoIdOnQString[Servo::RightEyeLidV]="RightEyeLidV";
  ServoIdOnQString[Servo::RightEyeLidR]="RightEyeLidR";

  //pobieranie konfiguracji z pliku xml
  for (unsigned int i=0; i<Servo::TOTAL; i++)
    ServoConfigs.push_back(GetConf(root,i,ServoIdOnQString));

  /**
   * @note 
   * Wskazówki odnoścnie tworzenia pliku konfiguracji napędów
   * znajdują się w pliku "DrvServoConfig.readme".
   */
}

void Driver::CamsControl(char camID, double destPosV, double destPosH)
{
  // Na prośbę Mateusza, kontrola otrzymywanej pozycji:
  {
    bool bad_dest=false;
    if(destPosV>1) { destPosV=1; bad_dest=true; }
    if(destPosV<0 and destPosV!=-1) { destPosV=0; bad_dest=true; }
    if(destPosH>1) { destPosH=1; bad_dest=true; }
    if(destPosH<0 and destPosH!=-1) { destPosH=0; bad_dest=true; }

    if(bad_dest)
      std::cerr<<"Driver: CamsControl(): warning: given position not in <0,1>, auto-repaired"<<std::endl;
  }

  int ActPosH;
  int ActPosV;
  int NewPosH;
  int NewPosV;

  if(camID=='r') // prawa kamerka
  {
    ActPosH=RightCam.getPosH();
    ActPosV=RightCam.getPosV();

    if(destPosV<0) NewPosV=ActPosV;
    else NewPosV=floor((ServoConfigs[Servo::RightEyeV].minPosDeg+destPosV
	  *(ServoConfigs[Servo::RightEyeV].maxPosDeg
	    -ServoConfigs[Servo::RightEyeV].minPosDeg)));

    if(destPosH<0) NewPosH=ActPosH;
    else NewPosH=floor((ServoConfigs[Servo::RightEyeH].minPosDeg+destPosH
	  *(ServoConfigs[Servo::RightEyeH].maxPosDeg
	    -ServoConfigs[Servo::RightEyeH].minPosDeg)));
  }
  else // lewa kamerka
  {
    ActPosH=LeftCam.getPosH();
    ActPosV=LeftCam.getPosV();

    if(destPosV<0) NewPosV=ActPosV;
    else NewPosV=floor((ServoConfigs[Servo::LeftEyeV].minPosDeg+destPosV
	  *(ServoConfigs[Servo::LeftEyeV].maxPosDeg
	    -ServoConfigs[Servo::LeftEyeV].minPosDeg)));

    if(destPosH<0) NewPosH=ActPosH;
    else NewPosH=floor((ServoConfigs[Servo::LeftEyeH].minPosDeg+destPosH
	  *(ServoConfigs[Servo::LeftEyeH].maxPosDeg
	    -ServoConfigs[Servo::LeftEyeH].minPosDeg)));
  }

  int Vchange=NewPosV-ActPosV;
  int Hchange=NewPosH-ActPosH;

  if(debug)
    std::cerr<<"Driver: CamsControl(): sending"<<" camID: "<<camID<<", ActPosH: "<<ActPosH<<", NewPosH: "<<NewPosH<<", ActPosV: "<<ActPosV<<", NewPosV: "<<NewPosV<<"."<<std::endl;

  if(camID=='r') // prawa kamerka
    RightCam.MoveVH(Vchange,Hchange);
  else // camID==2, lewa
    LeftCam.MoveVH(Vchange,Hchange);
}

void Driver::ServoControl(unsigned int ID, double destPos, double Vel)
{
  // Na prośbę Mateusza, kontrola otrzymywanej pozycji:
  {
    bool bad_dest=false;
    if(destPos>1) { destPos=1; bad_dest=true; }
    if(destPos<0) { destPos=0; bad_dest=true; }
    if(bad_dest)
      std::cerr<<"Driver: ServoControl(): warning: given position not in <0,1>, auto-repaired"<<std::endl;
  }

  uint16_t LastVel=DynServos.LastVelocities[ID];
  uint16_t LastPos=DynServos.LastPositions[ID];
  uint16_t NewVel=Norm2hex(Vel,
      ServoConfigs.at(ID).minVelHex,
      ServoConfigs.at(ID).maxVelHex);
  uint16_t NewPos=Norm2hex(destPos,
      ServoConfigs.at(ID).minPosHex,
      ServoConfigs.at(ID).maxPosHex);

  if(debug)
    std::cerr<<"Driver: ServoControl(): sending"<<" ServoID: "<<ID<<", LastPos: "<<LastPos<<", NewPos: "<<NewPos<<", LastVel: "<<LastVel<<", NewVel: "<<NewVel<<"."<<std::endl;

  if(LastVel!=Vel)
    DynServos.SetSpeed(ID,NewVel);

  if(LastPos!=destPos)
    DynServos.SetPos(ID,NewPos);  
}

void Driver::receiveFrame(FrameCam frame)
{
  switch( static_cast<unsigned int> (frame.ID) )
  {
    case Servo::LeftEyeV:
    case Servo::LeftEyeH:
      CamsControl('l',frame.destPosV,frame.destPosH); 
      break;
    case Servo::RightEyeV:
    case Servo::RightEyeH:
      CamsControl('r',frame.destPosV,frame.destPosH); 
      break;
  }
}

void Driver::receiveFrame(Frame frame)
{
  ServoControl(frame.ID,frame.destPos,frame.vel);
}

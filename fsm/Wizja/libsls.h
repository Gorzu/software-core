#ifndef LIBSLS_H_
#define LIBSLS_H_

#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QtXml>
#include <iostream>
#include <QString>
#include "uartLinux.h"
#include "qmath.h"
#include "fft.h" 

//Library profiling
#define OSLINUX
#define GNUPLOTOUTPUT
#define STANDALONE
#define CONSOLOUTPUT
//#define SOUNDRECORDING

#undef GNUPLOTOUTPUT
#undef CONSOLOUTPUT
#undef STANDALONE

#ifdef SOUNDRECORDING
#include <sndfile.h>
#include <time.h>
#define STREFACZASOWA (+1)
#endif

#define SLS_CONFIG_XML "cfg/SLSConfig.xml"

//#############################################################################
// Data/device  structures
//#############################################################################
typedef struct {
	double *ch1;
	double *ch2;
	UWord16 ch1Size;
	UWord16 ch2Size;
	UWord16 ptr;
	UWord16 n;
	UWord8 v;
} data_str;

typedef struct {
	double *Wn; /* twiddle factor  */
	double *X1; /* ch1 signal */
	double *X2; /* ch2 signal */
	double *AC1; /* autocorrelation signal of ch1 */
	double *AC2; /* autocorrelation signal of ch2 */
	double *E1; /* envelope of ch1 */
	double *E2; /* envelope of ch2 */
	double *P1; /* power spectrum of ch1 */
	double *P2; /* power spectrum of ch2 */
	double *phi1; /* phase spectrum of ch1 */
	double *phi2; /* phase spectrum of ch2 */
	double *CC; /* cross-correlation of ch1 and ch2 */
	double *ECC; /* cross-correlation envelope */
	double *PCC; /* cross-correlation power spectrum */
	double CCmax; /* cross-correlation max value */
	double P1Tot; /* ch1 total spectrum power */
	double P2Tot; /* ch2 total spectrum power */
	double delay; /* time delay */
	double angle; /* sound source position angle */
	UWord16 imax; /* cross-correlation max value index */
	UWord16 size;
	UWord16 ptr;
	UWord16 n;
	UWord8 v;
} work_str;

typedef struct {
	data_str *work_ptr; /* pointer to workspace buffer */
	data_str *load_ptr; /* pointer to load buffer  */
	data_str data1; /* data buffer 1 */
	data_str data2; /* data buffer 2 */
	QMutex qMutex_getData; /* get data mutex */
	QWaitCondition qCond_getData; /* get data condition variable */
	QMutex qMutex_dataReady; /* data ready mutex */
	QWaitCondition qCond_dataReady; /* data ready condition variable */
	QMutex qMutex_dev; /* device mutex */

	int fd; /* tty file descriptor */
	int state; /* frame decoder state */
	int err; /* error flag */
	int timeout; /* timeout counter */
	Boolean getData; /* get data flag */
	Boolean dataReady; /* data ready flag */
} comm_dev_str;

//#############################################################################
// Low-pass data filter 
//#############################################################################


class SLSFilter {
public:
	SLSFilter(int historyDepth);
	~SLSFilter();
	void addResult(double angle, double correlation, double sigTotPower);
	//	void addResultNull(double sigTotPower);
private:
	//variables
	double* _historyAngle;
	double* _historyPower;
	double* _historyCorr;
	int* _checked;
	int _historyDepth;
	int _idx;
	int _idxSearch;

	//methods
	double normalizePower(double power);
	double normalizeCorrelation(double cc);

	//experiential values
	double _angleSpread;
	double _initialThreshold;
	double _mainThreshold;
	double _powerThresholdLower;
	double _powerThresholdUpper;
	double _corrThresholdLower;
	double _corrThresholdUpper;
	int _maxAttempts;
	int _shift;
	int _expiration;
	int _minSampleThreshold;
};

//#############################################################################
// Device handling class -- user-space driver
//#############################################################################

class ThreadServiceRutine: public QThread {
	//Q_OBJECT
public:
	void setCommDev(comm_dev_str *ptr);
	void run();
private:
	//variables
	comm_dev_str* _dev; // Pointer to communication device structure
	//methods
	int MY_log2(UWord32 x);
};

//#############################################################################
// Main class representing Sound Localization Subsystem
//#############################################################################

class SoundLocalizationSubsystem: public QThread {
Q_OBJECT
public:
	//SoundLocalizationSubsystem(char* deviceName /*TODO: przemyslec wymagane dane*/);//minimal initialization
	SoundLocalizationSubsystem();//minimal initialization
	~SoundLocalizationSubsystem();
	void run();
#ifdef GNUPLOTOUTPUT
	/* gnuplot output variables & methods*/
	FILE *pipegnuplot;
	void setGnuplotPipe(FILE* pipe);
#endif

#ifdef OSLINUX
	void setFifo(int fd);
#endif
public slots:
	void haltSoundLocalization(bool halt);

private:
	//variables
	ThreadServiceRutine TSR;
	SLSFilter filter;
	comm_dev_str commDev; //Communication device structure
	work_str work; // sound samples and computation products structure
	std::string _soundDevName; // name of the audio device
	int _fd; // fifo descriptor 

	volatile bool _halt;// externaly changed flag
	double _micDist; // microphone distance [cm]
	double _sampleFreq; // sample fequency [Hz]
	double _sampleTime; // sample time [s]
	double _soundSpeed; // sound speed [m/s]
	volatile bool _record;// recording flag

#ifdef SOUNDRECORDING
	double *_sndBuffer;
	SNDFILE *_sndfile ;
	SF_INFO _sfinfo ;
	bool _recordActive;
	int _recordedFrames;
	time_t _rawtime;
	tm * _parsedtime;
	char _sndFileName[36];
#endif

	//methods
	//SoundLocalizationSubsystem(); // need futher initialization
	void measureInit();

};

//##################################################################
// Sound source structure
//##################################################################
typedef struct {
	QMutex qMutex;
	double angle; // founded sounds range: (-Pi/2,Pi/2) or {Pi} if not found //TODO: zmienić na stopnie
	bool found; // idicate availability of founded sound source 
	bool isLonely; // not connected with any face
	int expiration;
} SoundSource;

#endif /*LIBSLS_H_*/

#ifndef HEAD_GL_UTILS_H
#define HEAD_GL_UTILS_H

/* Struktura sluzaca do przekazywania informacji o bledach pojawiajacych sie
   w trakcie dzialania programu. */
struct exception
{
    // Rodzaje bledow:
    enum errorValue
    {
        // Ostrzeznia nie powinny powodowac nieprawidlowosci w dzialaniu
        // modulu:

        // Ostrzezenie.
        warning,
        // Ostrzezenie dotyczace nieprawidlowych danych przekazywanych
        // z innych modulow.
        slotWarning,
        // Ostrzezenie dotyczace nieprawidlowych danych wprowadzanych
        // przez uzytkownika aplikacji.
        userWarning,

        // Bledy moga powodowac nieprawidlowosci w dzialaniu modulu:

        // Blad.
        error,
        // Blad dotyczacy nieprawidlowych danych przekazywanych
        // z innych modulow.
        slotError,
        // Blad dotyczacy nieprawidlowych danych wprowadzanych
        // przez uzytkownika aplikacji.
        userError,


        // Blad potencjalnie zagrazajacy funkcjonowaniu calej aplikacji:
        criticalError
    };
    // Ilosc bledow.
    static int errorCount;
    // Numer tego bledu.
    int nr;
    // Znaczenie bledu;
    errorValue value;
    // Nazwa modulu i funkcji z ktorej pochodzi blad.
    QString source;
    // Komunikat bledu.
    QString msg;


    exception(errorValue value, QString source, QString msg=""):
    nr(errorCount), value(value), source(source), msg(msg)
    {errorCount++;}
};

/* Konfiguracja pojedynczego serwa. */
struct ServoConfig
{
    // Maksymalna wartosc pozycji w stopniach.
    double maxPos;
    // Minimalna wartosc pozycji w stopniach.
    double minPos;
    // Maksymalna wartosc predkosci w stopniach na milisekunde.
    double maxVel;
    // Minimalna wartosc predkosci w stopniach na milisekunde.
    double minVel;

    ServoConfig(double _minPos, double _maxPos, double _minVel,
                double _maxVel)
    {
        maxPos = _maxPos;
        minPos = _minPos;
        maxVel = _maxVel;
        minVel = _minVel;
    }

    ServoConfig()
    {
        maxPos = 360;
        minPos = 0;
        maxVel = 0.684;
        minVel = .0;
    }
};

struct bezierCoef
{
    float a;
    float b;
    float c;
    float d;
    bezierCoef(float a, float b, float c, float d): a(a), b(b), c(c), d(d){}
};

struct vertex
{
    double x, y, z;
    vertex(double xa, double ya, double za)
    {
        x = xa;
        z = za;
        y = ya;
    }
};

struct face
{
    int v1, v2, v3;
    face(int v1a, int v2a, int v3a)
    {
        v1 = v1a;
        v2 = v2a;
        v3 = v3a;
    }
};

struct material
{
    float ambient[4];
    float diffuse[4];
    material(float r, float g, float b)
    {
      ambient[0] = r;
      ambient[1] = g;
      ambient[2] = b;
      ambient[4] = 1.0;
    }
    void setDiffuse(float r, float g, float b)
    {
      diffuse[0] = r;
      diffuse[1] = g;
      diffuse[2] = b;
      diffuse[3] = 1.0;
    }
};

struct facePart
{
    static const QString facePartName[11];
    enum facePartId
    {
      Head,
      LeftEye,
      LeftEyeLid,
      RightEye,
      RightEyeLid,
      Lips,
      Camera,
      upperLipsPt1,
      upperLipsPt2,
      lowerLipsPt1,
      lowerLipsPt2, 
      elemCount
    };
};

struct facePartCoor
{
    float x;
    float y;
    float z;
    float xRot;
    float yRot;
    float zRot;

    facePartCoor()
    {
        x = 0;
        y = 0;
        z = 0;
        xRot = 0;
        zRot = 0;
        yRot = 0;
    }

    facePartCoor(float x_, float y_, float z_, float xRot_, float yRot_, float zRot_)
    {
        x = x_;
        y = y_;
        z = z_;
        xRot = xRot_;
        zRot = zRot_;
        yRot = yRot_;
    }

    void set(float x_, float y_, float z_, float xRot_, float yRot_, float zRot_)
    {
        x = x_;
        y = y_;
        z = z_;
        xRot = xRot_;
        zRot = zRot_;
        yRot = yRot_;
    }
};

#endif // HEAD_GL_UTILS_H

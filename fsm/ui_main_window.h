/********************************************************************************
** Form generated from reading UI file 'main_window.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAIN_WINDOW_H
#define UI_MAIN_WINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QStatusBar>
#include <QtGui/QTextEdit>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionLoadFSM;
    QAction *actionLoadBElements;
    QWidget *centralwidget;
    QWidget *visWidget;
    QTextEdit *infoWindow;
    QGroupBox *FollowSpeedBox;
    QLabel *EyesSpeedLabel;
    QDoubleSpinBox *EyesSpeedSpinBox;
    QLabel *HeadSpeedLabel;
    QDoubleSpinBox *HeadSpeedSpinBox;
    QGroupBox *FSMControlBox;
    QPushButton *StartFSMButton;
    QPushButton *StopFSMButton;
    QGroupBox *StartFromStateBox;
    QComboBox *CurrentStateComboBox;
    QLabel *CurrentStateLabel;
    QLabel *PreviousStateLabel;
    QComboBox *PreviousStateComboBox;
    QGroupBox *HeadSimulationBox;
    QPushButton *headSendBtn;
    QCheckBox *headExistBox;
    QSlider *headHorizontalSlider;
    QSlider *headVerticalSlider;
    QSlider *soundVerticalSlider;
    QSlider *soundHorizontalSlider;
    QCheckBox *NewTargetBox;
    QCheckBox *CrowdBox;
    QCheckBox *OtherSpeakingBox;
    QCheckBox *TargetSpeakingBox;
    QPushButton *ResetEyesButton;
    QCheckBox *VisEnCheckBox;
    QMenuBar *menubar;
    QMenu *menuLoadFSM;
    QMenu *menuLoadBElements;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(973, 526);
        actionLoadFSM = new QAction(MainWindow);
        actionLoadFSM->setObjectName(QString::fromUtf8("actionLoadFSM"));
        actionLoadBElements = new QAction(MainWindow);
        actionLoadBElements->setObjectName(QString::fromUtf8("actionLoadBElements"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        visWidget = new QWidget(centralwidget);
        visWidget->setObjectName(QString::fromUtf8("visWidget"));
        visWidget->setGeometry(QRect(20, 20, 400, 400));
        infoWindow = new QTextEdit(centralwidget);
        infoWindow->setObjectName(QString::fromUtf8("infoWindow"));
        infoWindow->setGeometry(QRect(680, 20, 241, 481));
        FollowSpeedBox = new QGroupBox(centralwidget);
        FollowSpeedBox->setObjectName(QString::fromUtf8("FollowSpeedBox"));
        FollowSpeedBox->setGeometry(QRect(30, 430, 161, 81));
        EyesSpeedLabel = new QLabel(FollowSpeedBox);
        EyesSpeedLabel->setObjectName(QString::fromUtf8("EyesSpeedLabel"));
        EyesSpeedLabel->setGeometry(QRect(10, 22, 61, 16));
        EyesSpeedSpinBox = new QDoubleSpinBox(FollowSpeedBox);
        EyesSpeedSpinBox->setObjectName(QString::fromUtf8("EyesSpeedSpinBox"));
        EyesSpeedSpinBox->setGeometry(QRect(80, 20, 62, 22));
        EyesSpeedSpinBox->setDecimals(3);
        EyesSpeedSpinBox->setMaximum(1);
        EyesSpeedSpinBox->setSingleStep(0.01);
        EyesSpeedSpinBox->setValue(0.1);
        HeadSpeedLabel = new QLabel(FollowSpeedBox);
        HeadSpeedLabel->setObjectName(QString::fromUtf8("HeadSpeedLabel"));
        HeadSpeedLabel->setGeometry(QRect(10, 52, 61, 16));
        HeadSpeedSpinBox = new QDoubleSpinBox(FollowSpeedBox);
        HeadSpeedSpinBox->setObjectName(QString::fromUtf8("HeadSpeedSpinBox"));
        HeadSpeedSpinBox->setGeometry(QRect(80, 50, 62, 22));
        HeadSpeedSpinBox->setDecimals(3);
        HeadSpeedSpinBox->setMaximum(1);
        HeadSpeedSpinBox->setSingleStep(0.01);
        HeadSpeedSpinBox->setValue(0.1);
        FSMControlBox = new QGroupBox(centralwidget);
        FSMControlBox->setObjectName(QString::fromUtf8("FSMControlBox"));
        FSMControlBox->setGeometry(QRect(440, 20, 231, 191));
        StartFSMButton = new QPushButton(FSMControlBox);
        StartFSMButton->setObjectName(QString::fromUtf8("StartFSMButton"));
        StartFSMButton->setGeometry(QRect(20, 150, 75, 23));
        StopFSMButton = new QPushButton(FSMControlBox);
        StopFSMButton->setObjectName(QString::fromUtf8("StopFSMButton"));
        StopFSMButton->setGeometry(QRect(130, 150, 71, 23));
        StartFromStateBox = new QGroupBox(FSMControlBox);
        StartFromStateBox->setObjectName(QString::fromUtf8("StartFromStateBox"));
        StartFromStateBox->setGeometry(QRect(10, 20, 211, 121));
        StartFromStateBox->setCheckable(true);
        StartFromStateBox->setChecked(false);
        CurrentStateComboBox = new QComboBox(StartFromStateBox);
        CurrentStateComboBox->setObjectName(QString::fromUtf8("CurrentStateComboBox"));
        CurrentStateComboBox->setGeometry(QRect(10, 40, 191, 22));
        CurrentStateLabel = new QLabel(StartFromStateBox);
        CurrentStateLabel->setObjectName(QString::fromUtf8("CurrentStateLabel"));
        CurrentStateLabel->setGeometry(QRect(70, 20, 71, 16));
        PreviousStateLabel = new QLabel(StartFromStateBox);
        PreviousStateLabel->setObjectName(QString::fromUtf8("PreviousStateLabel"));
        PreviousStateLabel->setGeometry(QRect(70, 70, 71, 16));
        PreviousStateComboBox = new QComboBox(StartFromStateBox);
        PreviousStateComboBox->setObjectName(QString::fromUtf8("PreviousStateComboBox"));
        PreviousStateComboBox->setGeometry(QRect(10, 90, 191, 22));
        HeadSimulationBox = new QGroupBox(centralwidget);
        HeadSimulationBox->setObjectName(QString::fromUtf8("HeadSimulationBox"));
        HeadSimulationBox->setGeometry(QRect(440, 220, 151, 251));
        headSendBtn = new QPushButton(HeadSimulationBox);
        headSendBtn->setObjectName(QString::fromUtf8("headSendBtn"));
        headSendBtn->setGeometry(QRect(20, 220, 121, 23));
        headExistBox = new QCheckBox(HeadSimulationBox);
        headExistBox->setObjectName(QString::fromUtf8("headExistBox"));
        headExistBox->setEnabled(true);
        headExistBox->setGeometry(QRect(20, 20, 72, 18));
        headExistBox->setChecked(false);
        headHorizontalSlider = new QSlider(HeadSimulationBox);
        headHorizontalSlider->setObjectName(QString::fromUtf8("headHorizontalSlider"));
        headHorizontalSlider->setGeometry(QRect(10, 40, 91, 21));
        headHorizontalSlider->setMinimum(-90);
        headHorizontalSlider->setMaximum(90);
        headHorizontalSlider->setValue(0);
        headHorizontalSlider->setSliderPosition(0);
        headHorizontalSlider->setOrientation(Qt::Horizontal);
        headVerticalSlider = new QSlider(HeadSimulationBox);
        headVerticalSlider->setObjectName(QString::fromUtf8("headVerticalSlider"));
        headVerticalSlider->setGeometry(QRect(110, 10, 21, 81));
        headVerticalSlider->setMinimum(-60);
        headVerticalSlider->setMaximum(60);
        headVerticalSlider->setValue(0);
        headVerticalSlider->setSliderPosition(0);
        headVerticalSlider->setOrientation(Qt::Vertical);
        soundVerticalSlider = new QSlider(HeadSimulationBox);
        soundVerticalSlider->setObjectName(QString::fromUtf8("soundVerticalSlider"));
        soundVerticalSlider->setGeometry(QRect(110, 130, 21, 81));
        soundVerticalSlider->setMinimum(-60);
        soundVerticalSlider->setMaximum(60);
        soundVerticalSlider->setValue(0);
        soundVerticalSlider->setSliderPosition(0);
        soundVerticalSlider->setOrientation(Qt::Vertical);
        soundHorizontalSlider = new QSlider(HeadSimulationBox);
        soundHorizontalSlider->setObjectName(QString::fromUtf8("soundHorizontalSlider"));
        soundHorizontalSlider->setGeometry(QRect(10, 140, 91, 21));
        soundHorizontalSlider->setMinimum(-90);
        soundHorizontalSlider->setMaximum(90);
        soundHorizontalSlider->setValue(0);
        soundHorizontalSlider->setSliderPosition(0);
        soundHorizontalSlider->setOrientation(Qt::Horizontal);
        NewTargetBox = new QCheckBox(HeadSimulationBox);
        NewTargetBox->setObjectName(QString::fromUtf8("NewTargetBox"));
        NewTargetBox->setEnabled(true);
        NewTargetBox->setGeometry(QRect(20, 70, 72, 18));
        NewTargetBox->setChecked(false);
        CrowdBox = new QCheckBox(HeadSimulationBox);
        CrowdBox->setObjectName(QString::fromUtf8("CrowdBox"));
        CrowdBox->setEnabled(true);
        CrowdBox->setGeometry(QRect(20, 90, 72, 18));
        CrowdBox->setChecked(false);
        OtherSpeakingBox = new QCheckBox(HeadSimulationBox);
        OtherSpeakingBox->setObjectName(QString::fromUtf8("OtherSpeakingBox"));
        OtherSpeakingBox->setEnabled(true);
        OtherSpeakingBox->setGeometry(QRect(20, 190, 72, 18));
        OtherSpeakingBox->setChecked(false);
        TargetSpeakingBox = new QCheckBox(HeadSimulationBox);
        TargetSpeakingBox->setObjectName(QString::fromUtf8("TargetSpeakingBox"));
        TargetSpeakingBox->setEnabled(true);
        TargetSpeakingBox->setGeometry(QRect(20, 170, 72, 18));
        TargetSpeakingBox->setChecked(false);
        ResetEyesButton = new QPushButton(centralwidget);
        ResetEyesButton->setObjectName(QString::fromUtf8("ResetEyesButton"));
        ResetEyesButton->setGeometry(QRect(350, 430, 92, 28));
        VisEnCheckBox = new QCheckBox(centralwidget);
        VisEnCheckBox->setObjectName(QString::fromUtf8("VisEnCheckBox"));
        VisEnCheckBox->setGeometry(QRect(180, 430, 161, 23));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 973, 25));
        menuLoadFSM = new QMenu(menubar);
        menuLoadFSM->setObjectName(QString::fromUtf8("menuLoadFSM"));
        menuLoadBElements = new QMenu(menubar);
        menuLoadBElements->setObjectName(QString::fromUtf8("menuLoadBElements"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuLoadFSM->menuAction());
        menubar->addAction(menuLoadBElements->menuAction());
        menuLoadFSM->addAction(actionLoadFSM);
        menuLoadBElements->addAction(actionLoadBElements);

        retranslateUi(MainWindow);
        QObject::connect(StopFSMButton, SIGNAL(clicked()), MainWindow, SLOT(StopFSMButton()));
        QObject::connect(ResetEyesButton, SIGNAL(clicked()), MainWindow, SLOT(ResetEyesButton()));
        QObject::connect(VisEnCheckBox, SIGNAL(clicked(bool)), MainWindow, SLOT(VisEnable(bool)));

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "\"Samuel Project. FSM.\"", 0, QApplication::UnicodeUTF8));
        actionLoadFSM->setText(QApplication::translate("MainWindow", "LoadFSM", 0, QApplication::UnicodeUTF8));
        actionLoadBElements->setText(QApplication::translate("MainWindow", "LoadBElements", 0, QApplication::UnicodeUTF8));
        FollowSpeedBox->setTitle(QApplication::translate("MainWindow", "FollowSpeed", 0, QApplication::UnicodeUTF8));
        EyesSpeedLabel->setText(QApplication::translate("MainWindow", "EyesSpeed", 0, QApplication::UnicodeUTF8));
        HeadSpeedLabel->setText(QApplication::translate("MainWindow", "HeadSpeed", 0, QApplication::UnicodeUTF8));
        FSMControlBox->setTitle(QApplication::translate("MainWindow", "FSM Control", 0, QApplication::UnicodeUTF8));
        StartFSMButton->setText(QApplication::translate("MainWindow", "Start FSM", 0, QApplication::UnicodeUTF8));
        StopFSMButton->setText(QApplication::translate("MainWindow", "Stop FSM", 0, QApplication::UnicodeUTF8));
        StartFromStateBox->setTitle(QApplication::translate("MainWindow", "StartFromState", 0, QApplication::UnicodeUTF8));
        CurrentStateLabel->setText(QApplication::translate("MainWindow", "Current State", 0, QApplication::UnicodeUTF8));
        PreviousStateLabel->setText(QApplication::translate("MainWindow", "Previous State", 0, QApplication::UnicodeUTF8));
        HeadSimulationBox->setTitle(QApplication::translate("MainWindow", "Head Simulation", 0, QApplication::UnicodeUTF8));
        headSendBtn->setText(QApplication::translate("MainWindow", "Send Head", 0, QApplication::UnicodeUTF8));
        headExistBox->setText(QApplication::translate("MainWindow", "Detected?", 0, QApplication::UnicodeUTF8));
        NewTargetBox->setText(QApplication::translate("MainWindow", "NewTarget", 0, QApplication::UnicodeUTF8));
        CrowdBox->setText(QApplication::translate("MainWindow", "Crowd", 0, QApplication::UnicodeUTF8));
        OtherSpeakingBox->setText(QApplication::translate("MainWindow", "Other", 0, QApplication::UnicodeUTF8));
        TargetSpeakingBox->setText(QApplication::translate("MainWindow", "Target", 0, QApplication::UnicodeUTF8));
        ResetEyesButton->setText(QApplication::translate("MainWindow", "Reset Eyes", 0, QApplication::UnicodeUTF8));
        VisEnCheckBox->setText(QApplication::translate("MainWindow", "Enable Visualisation", 0, QApplication::UnicodeUTF8));
        menuLoadFSM->setTitle(QApplication::translate("MainWindow", "LoadFSM", 0, QApplication::UnicodeUTF8));
        menuLoadBElements->setTitle(QApplication::translate("MainWindow", "LoadBElements", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAIN_WINDOW_H

#ifndef WYSYLACZIPL_H
#define WYSYLACZIPL_H

#include <QObject>
#include <QThread>
#include "facedetector.hh"

class WysylaczIPL : public QThread
{
public:
    WysylaczIPL();
    void setFaceDetector(FaceDetector* oko);
    void run();
    FaceDetector* okoWskaznik;
    //void quit(char* msg, int retval);
    int is_data_ready;
    int serversock, clientsock;
};

#endif // WYSYLACZIPL_H

#include "XmlRW.h"

bool QString2Bool(const QString & str)
{
  if(str=="true")
    return true;
  else
    return false;
}

QString bool2QString(bool b)
{
  if(b)
    return "true";
  else
    return "false";
}

QDomElement servoToNode(QDomDocument &d, SimpleMove &SM,
    unsigned int i)
{
  /**
   * @par TODO:
   * Trzeba się zastanowić, czy nie lepiej przekazywać
   * poniższą mape przez referencje do tej f-cji...
   * (szkoda, żeby miała być tworzona i kasowana
   * za każdym razem kiedy jest wywoływna...)
   */

  map <Servo::ServoId,QString> ServoIdOnQString;
  ServoIdOnQString[Servo::HeadV]="HeadV";
  ServoIdOnQString[Servo::HeadH]="HeadH";
  ServoIdOnQString[Servo::HeadR]="HeadR";
  ServoIdOnQString[Servo::LeftEyeV]="LeftEyeV";
  ServoIdOnQString[Servo::LeftEyeH]="LeftEyeH";
  ServoIdOnQString[Servo::RightEyeV]="RightEyeV";
  ServoIdOnQString[Servo::RightEyeH]="RightEyeH";
  ServoIdOnQString[Servo::BotRightLips]="BotRightLips";
  ServoIdOnQString[Servo::BotLeftLips]="BotLeftLips";
  ServoIdOnQString[Servo::TopRightLips]="TopRightLips";
  ServoIdOnQString[Servo::TopLeftLips]="TopLeftLips";
  ServoIdOnQString[Servo::LeftEyeLidV]="LeftEyeLidV";
  ServoIdOnQString[Servo::LeftEyeLidR]="LeftEyeLidR";
  ServoIdOnQString[Servo::RightEyeLidV]="RightEyeLidV";
  ServoIdOnQString[Servo::RightEyeLidR]="RightEyeLidR";
  ServoIdOnQString[Servo::OverrideEyes]="OverrideEyes";
  ServoIdOnQString[Servo::NeckV]="NeckV";

  QDomElement cn = d.createElement( "servo" );
  cn.setAttribute("vel",SM.servoVelocity[i]);
  cn.setAttribute("pos",SM.servoPosition[i]);
  cn.setAttribute("name",
      ServoIdOnQString[static_cast<Servo::ServoId>(i)]);

  return cn;
}

QDomElement SimpleMoveToNode(QDomDocument &d, SimpleMove &SM)
{
  QDomElement cn = d.createElement( "SimpleMove" );
  cn.setAttribute("normalizedTime",SM.normalizedTime);
  for(vector< bool >::size_type i = 0;
      i < SM.isSet.size(); ++i)
    if(SM.isSet.at(i)==true)
      cn.appendChild(servoToNode(d,SM,i));

  return cn;
}

QDomElement SimpleMovesToNode(QDomDocument &d,std::vector<SimpleMove> SM)
{
  QDomElement cn = d.createElement( "SimpleMoves" );
  for(vector< SimpleMove >::size_type i = 0;
      i < SM.size(); ++i)
    cn.appendChild(SimpleMoveToNode(d,SM.at(i)));
  return cn;
} 

QDomElement soundFileNameToNode(QDomDocument &d,SpeechSet SS)
{
  QDomElement cn = d.createElement( "soundFileName" );
  cn.setAttribute( "name", SS.soundFileName );

  return cn;
} 

QDomElement SpeechSetToNode(QDomDocument &d, 
    map<QString,SpeechSet>::const_iterator it) 
{
  QDomElement cn = d.createElement("SpeechSet");
  cn.setAttribute( "name", it->first);
  cn.appendChild(soundFileNameToNode(d,it->second));
  cn.appendChild(SimpleMovesToNode(d,it->second.simpleMoves));
  return cn;
}

QDomElement SpeechesToNode(QDomDocument &d, 
    const map<QString, SpeechSet> &speeches)
{
  QDomElement cn = d.createElement("speeches");
  for(map<QString,SpeechSet>::const_iterator it = speeches.begin(); 
      it != speeches.end(); ++it)
    cn.appendChild(SpeechSetToNode(d,it));
  return cn;
}  

QDomElement MovesSetToNode(QDomDocument &d, 
    map<QString,MovesSet>::const_iterator it) 
{
  QDomElement cn = d.createElement("MovesSet");
  cn.setAttribute( "name", it->first);
  cn.appendChild(SimpleMovesToNode(d,it->second.simpleMoves));
  return cn;
}

QDomElement movesToNode(QDomDocument &d, 
    const map<QString, MovesSet> &moves)
{
  QDomElement cn = d.createElement("moves");
  for(map<QString,MovesSet>::const_iterator it = moves.begin(); 
      it != moves.end(); ++it)
    cn.appendChild(MovesSetToNode(d,it));
  return cn;
}  

QDomElement emotionsToNode(QDomDocument &d, 
    const map<QString, MovesSet> &emotions)
{
  QDomElement cn = d.createElement("emotions");
  for(map<QString,MovesSet>::const_iterator it = emotions.begin(); 
      it != emotions.end(); ++it)
    cn.appendChild(MovesSetToNode(d,it));
  return cn;
}  

void WriteBElementsToFile(BElements &be, QString filename)
{
  QDomDocument doc;
  QDomElement root = doc.createElement("BElements");
  doc.appendChild( root );

  root.appendChild(SpeechesToNode(doc,be.speeches));
  root.appendChild(movesToNode(doc,be.moves));
  root.appendChild(emotionsToNode(doc,be.emotions));

  QFile file(filename);
  if( !file.open(QIODevice::WriteOnly) )
  {
    cerr<<"XmlRW: WriteBElementsToFile(): error: can't open "
					  << filename.toStdString()<<endl;
    exit(-1);
  }

  QTextStream ts( &file );
  ts << "<?xml version=\"1.0\"?>" << endl;
  ts << doc.toString();
  file.close();
}

void InServo(QDomElement &tag,SimpleMove &SM)
{

  /**
   * @par TODO:
   * Trzeba się zastanowić, czy nie lepiej przekazywać
   * poniższą mape przez referencje do tej f-cji...
   * (szkoda, żeby miała być tworzona i kasowana
   * za każdym razem kiedy jest wywoływna...)
   */

  map <QString, Servo::ServoId> QStringOnServoId;

  QStringOnServoId["HeadV"]=Servo::HeadV;
  QStringOnServoId["HeadH"]=Servo::HeadH;
  QStringOnServoId["HeadR"]=Servo::HeadR;
  QStringOnServoId["LeftEyeV"]=Servo::LeftEyeV;
  QStringOnServoId["LeftEyeH"]=Servo::LeftEyeH;
  QStringOnServoId["RightEyeV"]=Servo::RightEyeV;
  QStringOnServoId["RightEyeH"]=Servo::RightEyeH;
  QStringOnServoId["BotRightLips"]=Servo::BotRightLips;
  QStringOnServoId["BotLeftLips"]=Servo::BotLeftLips;
  QStringOnServoId["TopRightLips"]=Servo::TopRightLips;
  QStringOnServoId["TopLeftLips"]=Servo::TopLeftLips;
  QStringOnServoId["LeftEyeLidV"]=Servo::LeftEyeLidV;
  QStringOnServoId["LeftEyeLidR"]=Servo::LeftEyeLidR;
  QStringOnServoId["RightEyeLidV"]=Servo::RightEyeLidV;
  QStringOnServoId["RightEyeLidR"]=Servo::RightEyeLidR;
  QStringOnServoId["OverrideEyes"]=Servo::OverrideEyes;
  QStringOnServoId["NeckV"]=Servo::NeckV;

  Servo::ServoId _ServoId=QStringOnServoId[tag.attribute("name","")];
  double _pos = tag.attribute("pos","").toDouble();
  double _vel = tag.attribute("vel","").toDouble();

  SM.setElement(_ServoId,_pos,_vel);
  /*
  cerr<<"      Ustawiono Servo w SimpleMove (id="
    <<tag.attribute("name","").toStdString()
    <<", pos="<<_pos<<", vel="<<_vel<<")"<<endl;
    */
}

void InSimpleMove(QDomElement &root, SpeechSet &SS)
{
  SimpleMove newSM; //nowy Simple Move
  newSM.normalizedTime=root.attribute("normalizedTime","").toDouble();

  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "servo" )
	InServo(e,newSM);
    }
    n = n.nextSibling();
  }
  SS.simpleMoves.push_back(newSM);
  /*
  cerr<<"   Dodano nowy SimpleMove (normalizedTime="<<newSM.normalizedTime
    <<") do wektora SimpleMoves mapy speeches"<<endl;
    */
}

void InSimpleMove(QDomElement &root, MovesSet &MS)
{
  SimpleMove newSM; //nowy Simple Move
  newSM.normalizedTime=root.attribute("normalizedTime","").toDouble();

  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "servo" )
	InServo(e,newSM);
    }
    n = n.nextSibling();
  }
  MS.simpleMoves.push_back(newSM);
  /*
  cerr<<"   Dodano nowy SimpleMove (normalizedTime="<<newSM.normalizedTime
    <<") do wektora SimpleMoves mapy moves"<<endl;
    */
}

void InSimpleMoves(QDomElement &root, MovesSet & MS)
{
  //cerr<<"   w SimpleMoves"<<endl;
  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "SimpleMove" )
	InSimpleMove(e,MS);
    }
    n = n.nextSibling();
  }
}

void InSpeechSet(QDomElement & root, BElements &ToRet)
{
  SpeechSet newSS; // New Speech Set
  newSS.name = root.attribute( "name", "" );

  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "soundFileName" )
      {
	newSS.soundFileName=e.attribute("name","");
	/*
	cerr<<"   Dodano soundFileName="<<newSS.soundFileName.toStdString()
	  <<" do SpeechSet'u o name="<<newSS.name.toStdString()<<endl;
	  */
      }
      else
	if( e.tagName() == "SimpleMoves" )
	  InSimpleMoves(e,newSS);
    }    
    n = n.nextSibling();
  }
  ToRet.speeches[newSS.name]=newSS;
  /*
  cerr<<"Dodano nowy SpeechSet o nazwie name="<<newSS.name.toStdString()
    <<" do mapy speeches"<<endl;
    */
}

void InSpeeches(QDomElement & root, BElements &ToRet)
{
  //cerr<<"w speeches"<<endl;
  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "SpeechSet" )
	InSpeechSet(e,ToRet);
    }
    n = n.nextSibling();
  }
}

void InMovesSet(QDomElement & root, map<QString,MovesSet> &ActMap)
{
  MovesSet newMS; // New Moves Set
  newMS.name = root.attribute( "name", "" );

  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "SimpleMoves" )
	InSimpleMoves(e,newMS);
    }    
    n = n.nextSibling();
  }
  ActMap[newMS.name]=newMS;
  /*
  cerr<<"Dodano nowy MovesSet o name="
    <<newMS.name.toStdString()<<" do aktualnej map"<<endl;
    */
}

void InMovesOrEmotions(QDomElement & root, map<QString,MovesSet> &ActMap)
{
  QDomNode n = root.firstChild();
  while( !n.isNull() )
  {
    QDomElement e = n.toElement();
    if( !e.isNull() )
    {
      if( e.tagName() == "MovesSet" )
	InMovesSet(e,ActMap);
    }
    n = n.nextSibling();
  }
}

BElements GetBElementsFromFile(QString filename)
{
  BElements ToRet; // zwracana struktura

  // przygotowanie dokumentu
  QDomDocument doc("BElements");
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
  {
    cerr<<"XmlRW: GetBElementsFromFile(): error: can't open file"<<endl;
    exit(-1);
  }
  if(!doc.setContent(&file))
  {
    file.close();
    cerr<<"XmlRW: GetBElementsFromFile(): error: can't set content"<<endl;
    exit(-2);
  }
  file.close();

  // sprawdzenie root'a
  QDomElement root = doc.documentElement();
  if( root.tagName() != "BElements" )
  {
    cerr<<"XmlRW: GetBElementsFromFile(): error: wrong root"<<endl;
    exit(-3);
  }

  // czytanie danych
  QDomNode Root = root.firstChild();
  while( !Root.isNull() )
  {
    QDomElement w0 = Root.toElement();
    if( !w0.isNull() )
    {
      if( w0.tagName() == "speeches" )
	InSpeeches(w0,ToRet);
      else
      {
	if(w0.tagName() == "moves")
	{
	  //cerr<<"w moves"<<endl;
	  InMovesOrEmotions(w0,ToRet.moves);
	}
	else
	{
	  if(w0.tagName() == "emotions")
	  {
	    //cerr<<"w emotions"<<endl;
	    InMovesOrEmotions(w0,ToRet.emotions);
	  }
	}
      }
    }
    Root = Root.nextSibling();
  }
  return ToRet;
}

QDomElement speechElemToNode(QDomDocument &d, vector<Elem> &Elems,
    unsigned int k)
{
  QDomElement cn = d.createElement( "speechElem" );
  cn.setAttribute( "name", Elems.at(k).name);
  cn.setAttribute( "probability", Elems.at(k).probability);
  cn.setAttribute( "totalTime",static_cast<qlonglong>(Elems.at(k).totalTime));
  return cn;
} 

QDomElement moveElemToNode(QDomDocument &d, vector<Elem> &Elems,
    unsigned int k)
{
  QDomElement cn = d.createElement( "moveElem" );
  cn.setAttribute( "name", Elems.at(k).name);
  cn.setAttribute( "probability", Elems.at(k).probability);
  cn.setAttribute( "totalTime",static_cast<qlonglong>(Elems.at(k).totalTime));
  return cn;
} 

QDomElement emotionElemToNode(QDomDocument &d, vector<Elem> &Elems,
    unsigned int k)
{
  QDomElement cn = d.createElement( "emotionElem" );
  cn.setAttribute( "name", Elems.at(k).name);
  cn.setAttribute( "probability", Elems.at(k).probability);
  cn.setAttribute( "totalTime",static_cast<qlonglong>(Elems.at(k).totalTime));
  return cn;
} 

QDomElement speechElemsToNode(QDomDocument &d, vector<Elem> &Elems)
{
  QDomElement cn = d.createElement( "speechElems" );
  for(unsigned int k=0; k<Elems.size();k++)
    cn.appendChild(speechElemToNode(d,Elems,k));
  return cn;
} 

QDomElement moveElemsToNode(QDomDocument &d,vector<Elem> &Elems)
{
  QDomElement cn = d.createElement( "moveElems" );
  for(unsigned int k=0; k<Elems.size();k++)
    cn.appendChild(moveElemToNode(d,Elems,k));
  return cn;
} 

QDomElement emotionElemsToNode(QDomDocument &d, vector<Elem> &Elems)
{
  QDomElement cn = d.createElement( "emotionElems" );
  for(unsigned int k=0; k<Elems.size();k++)
    cn.appendChild(emotionElemToNode(d,Elems,k));
  return cn;
} 

QDomElement totalTimeToNode(QDomDocument &d, long tT)
{
  QDomElement cn = d.createElement( "totalTime" );
  cn.setAttribute("value",static_cast<qlonglong>(tT));
  return cn;
}

QDomElement FrameToNode(QDomDocument &d,vector<BFrame> &list,unsigned int j)
{
  QDomElement cn = d.createElement( "frame" );
  cn.appendChild( speechElemsToNode(d,list.at(j).speechElems));
  cn.appendChild(   moveElemsToNode(d,list.at(j).moveElems));
  cn.appendChild(emotionElemsToNode(d,list.at(j).emotionElems));
  cn.appendChild(   totalTimeToNode(d,list.at(j).totalTime));
  return cn;
} 

QDomElement entryStateToNode(QDomDocument &d,vector<QString> &States,
    unsigned int m)
{
  QDomElement cn = d.createElement( "state" );
  cn.setAttribute( "name", States.at(m));
  return cn;
} 

QDomElement entryStatesToNode(QDomDocument &d,vector<QString> &States)
{
  QDomElement cn = d.createElement( "entryStates" );
  for(unsigned int m=0; m<States.size();m++)
    cn.appendChild(entryStateToNode(d,States,m));
  return cn;
} 

QDomElement conditionToNode(QDomDocument &d,vector<QString> &cond,
    unsigned int m)
{
  QDomElement cn = d.createElement( "condition" );
  cn.setAttribute( "name", cond.at(m));
  return cn;
} 

QDomElement conditionsToNode(QDomDocument &d,vector<QString> &conds)
{
  QDomElement cn = d.createElement( "conditions" );
  for(unsigned int m=0; m<conds.size();m++)
    cn.appendChild(conditionToNode(d,conds,m));
  return cn;
} 

QDomElement exitStateToNode(QDomDocument &d,vector<NameProb> &States,
    unsigned int m)
{
  QDomElement cn = d.createElement( "state" );
  cn.setAttribute( "name", States.at(m).name);
  cn.setAttribute( "probability", States.at(m).probability);
  return cn;
} 

QDomElement exitStatesToNode(QDomDocument &d,std::vector<NameProb> &States)
{
  QDomElement cn = d.createElement( "exitStates" );
  for(unsigned int m=0; m<States.size();m++)
    cn.appendChild(exitStateToNode(d,States,m));
  return cn;
} 

QDomElement RelationToNode(QDomDocument &d,vector <Relation> &rlist,
    unsigned int l)
{
  QDomElement cn = d.createElement( "relation" );
  cn.appendChild(entryStatesToNode(d,rlist.at(l).entryStates));
  cn.appendChild(conditionsToNode(d,rlist.at(l).conditions));
  cn.appendChild(exitStatesToNode(d,rlist.at(l).exitStates));
  return cn;
}

QDomElement BFrameListToNode(QDomDocument &d,vector <BFrame> list)
{
  QDomElement cn = d.createElement( "frames" );
  for(unsigned int j=0; j<list.size();j++)
    cn.appendChild(FrameToNode(d,list,j));
  return cn;
} 

QDomElement RelationsToNode(QDomDocument &d, vector <Relation> rlist)
{
  QDomElement cn = d.createElement( "relations" );
  for(unsigned int l=0; l<rlist.size();l++)
    cn.appendChild(RelationToNode(d,rlist,l));
  return cn;
} 

QDomElement visionInterruptToNode(QDomDocument &d, bool vI)
{
  QDomElement cn = d.createElement( "visionInterrupt" );
  cn.setAttribute("value",bool2QString(vI));
  return cn;
} 

QDomElement soundInterruptToNode(QDomDocument &d, bool sI)
{
  QDomElement cn = d.createElement( "soundInterrupt" );
  cn.setAttribute("value",bool2QString(sI));
  return cn;
} 


QDomElement StateToNode(QDomDocument &d, map<QString,State>::const_iterator it)
{
  QDomElement cn = d.createElement( "state" );
  cn.setAttribute( "nazwa", it->first);
  cn.appendChild(BFrameListToNode(d,it->second.BFrameList));
  cn.appendChild( RelationsToNode(d,it->second.relations));
  cn.appendChild( visionInterruptToNode(d,it->second.visionInterrupt));
  cn.appendChild( soundInterruptToNode(d,it->second.soundInterrupt));
  return cn;
} 

void WriteFSMtoFile(FSM & _FSM, QString filename)
{
  QDomDocument doc;
  QDomElement root = doc.createElement("FSM");
  doc.appendChild( root );

  for(map<QString,State>::const_iterator it = _FSM.states.begin(); 
      it != _FSM.states.end(); ++it)
    root.appendChild(StateToNode(doc,it));
 

  QFile file(filename);
  if( !file.open(QIODevice::WriteOnly) )
  {
    cerr<<"XmlRW: WriteStateToFile(): error: can't open "
				      << filename.toStdString()<<endl;
    exit(-1);
  }

  QTextStream ts( &file );

  ts << "<?xml version=\"1.0\"?>" << endl;
  ts << doc.toString();
  file.close();
}


void InElems(QDomElement &e4, vector<Elem> &someElems,QString whatElem)
{
  Elem someElem_tmp;
  QDomNode in_someElems = e4.firstChild();
  while( !in_someElems.isNull() )
  {
    QDomElement e5= in_someElems.toElement();
    if( !e5.isNull() )
    {
      if( e5.tagName() == whatElem )
      {
	someElem_tmp.name=e5.attribute("name","");
	someElem_tmp.probability=
	  e5.attribute("probability","0").toDouble();
	someElem_tmp.totalTime=
	  e5.attribute("totalTime",0).toLong();
      }
    }
    someElems.push_back(someElem_tmp);
    /*
    cerr<<"      Dodano nowy Elem (name="<<someElem_tmp.name.toStdString()
      <<")"<<endl;
      */
    in_someElems=in_someElems.nextSibling();
  } 
}

void InTotalTime(QDomElement &e4, BFrame &BFrame_tmp)
{
  BFrame_tmp.totalTime=e4.attribute("value","0").toLong();
  /*
  cerr<<"        Dodano totalTime value="<<BFrame_tmp.totalTime<<endl;
  */
}

void InFrame(QDomElement &e3, BFrame &BFrame_tmp)
{
  QDomNode in_frame = e3.firstChild();
  while( !in_frame.isNull() )
  {
    QDomElement e4= in_frame.toElement();
    if( !e4.isNull() )
    {
      if( e4.tagName() == "speechElems" )
	InElems(e4,BFrame_tmp.speechElems,"speechElem");
      else
      {
	if( e4.tagName() == "moveElems" )
	  InElems(e4,BFrame_tmp.moveElems,"moveElem");
	else
	{
	  if(e4.tagName() == "emotionElems" )
	    InElems(e4,BFrame_tmp.emotionElems,"emotionElem");
	  else
	    if(e4.tagName() == "totalTime")
	      InTotalTime(e4,BFrame_tmp);
	}
      }
    }
    in_frame = in_frame.nextSibling();
  }
}

void InFrames(QDomElement &e2, State & State_tmp)
{
  QDomNode in_frames = e2.firstChild();
  while( !in_frames.isNull() )
  {
    BFrame BFrame_tmp;
    QDomElement e3= in_frames.toElement();
    if( !e3.isNull() )
    {
      if( e3.tagName() == "frame" )
	InFrame(e3,BFrame_tmp);
    }
    State_tmp.BFrameList.push_back(BFrame_tmp);
    /*
    cerr<<"   Dodano BFrame "<<BFrame_tmp.id.toStdString()
      <<" do stanu "<<State_tmp.name.toStdString()<<endl; 
      */
    in_frames = in_frames.nextSibling();
  }
}

void InConditionsOrEntryStates(QDomElement &e4,
    vector <QString> &Vec, QString tagname)
{
  QDomNode in_ActNode = e4.firstChild();
  while( !in_ActNode.isNull() )
  {
    QString vec;
    QDomElement e5 = in_ActNode.toElement();
    if( !e5.isNull() )
    {
      if( e5.tagName() == tagname )
      {
	vec=e5.attribute( "name", "" );
      }
    }
    Vec.push_back(vec);
    /*
    cerr<<"      Dodano "<<tagname.toStdString()<<" "
      <<vec.toStdString()
      <<" do wektora conditions/entryStates obecnej relacji"<<endl;
      */
    in_ActNode = in_ActNode.nextSibling();
  }
}

void InExitStates(QDomElement &e4,vector <NameProb> &exitStates)
{
  QDomNode in_exitStates = e4.firstChild();
  while( !in_exitStates.isNull() )
  {
    NameProb ExitState;
    QDomElement e5 = in_exitStates.toElement();
    if( !e5.isNull() )
    {
      if( e5.tagName() == "state" )
      {
	ExitState.name=e5.attribute( "name", "" );
	ExitState.probability=e5.attribute( "probability", "0" ).toDouble();
      }
    }
    exitStates.push_back(ExitState);
    /*
    cerr<<"      Dodano NameProb ExitState o nazwie "
      <<ExitState.name.toStdString()<<" i prob="
      <<ExitState.probability<<endl;
      */
    in_exitStates = in_exitStates.nextSibling();
  }
}

void InRelation(QDomElement &e3, Relation &Relation_tmp)
{
  QDomNode in_relation = e3.firstChild();
  while( !in_relation.isNull() )
  {
    QDomElement e4 = in_relation.toElement();
    if( !e4.isNull() )
    {
      if( e4.tagName() == "entryStates" )
	InConditionsOrEntryStates(e4,Relation_tmp.entryStates,"state");
      else
      {
	if( e4.tagName() == "conditions" )
	  InConditionsOrEntryStates(e4,Relation_tmp.conditions ,"condition");
	else
	  if( e4.tagName() == "exitStates" )
	    InExitStates(e4,Relation_tmp.exitStates);
      }
    }
    in_relation = in_relation.nextSibling();
  }
}

void InRelations(QDomElement &e2, State & State_tmp)
{
  QDomNode in_relations = e2.firstChild();
  while( !in_relations.isNull() )
  {
    Relation Relation_tmp;

    QDomElement e3 = in_relations.toElement();
    if( !e3.isNull() )
    {
      if( e3.tagName() == "relation" )
	InRelation(e3,Relation_tmp);
    }
    State_tmp.relations.push_back(Relation_tmp);
    /*
    cerr<<"  Dodano kolejną relację do stanu "
      <<State_tmp.name.toStdString()<<endl;
      */
    in_relations = in_relations.nextSibling();
  }

}

void InState(QDomElement &e, State & State_tmp)
{
  QDomNode in_state = e.firstChild();
  while(!in_state.isNull())
  {

    QDomElement e2= in_state.toElement();
    if( !e2.isNull() )
    {
      if(e2.tagName() == "frames" )
	InFrames(e2,State_tmp);
      else
      {
	if(e2.tagName()=="relations")
	  InRelations(e2,State_tmp);
	else
	{
	  if(e2.tagName()=="visionInterrupt")
	    State_tmp.visionInterrupt=QString2Bool(e2.attribute("value",""));
	  else
	    if(e2.tagName()=="soundInterrupt")
	      State_tmp.soundInterrupt=QString2Bool(e2.attribute("value",""));
	}
      }
    }
    in_state = in_state.nextSibling();
  }
}

FSM GetFSMfromFile(QString filename)
{
  FSM FSMtoRet;

  // przygotowanie dokumentu
  QDomDocument doc("FSM");
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
  {
    cerr<<"XmlRW: GetStatesFromFile(): error: can't open file"<<endl;
    exit(-1);
  }
  if(!doc.setContent(&file))
  {
    file.close();
    cerr<<"XmlRW: GetStatesFromFile(): error: can't set content"<<endl;
    exit(-2);
  }
  file.close();

  // sprawdzenie root'a
  QDomElement root = doc.documentElement();
  if( root.tagName() != "FSM" )
  {
    cerr<<"XmlRW: GetStatesFromFile(): error: wrong root"<<endl;
    exit(-3);
  }

  // czytanie danych
  QDomNode n = root.firstChild();
  QString StateNameTmp;
  while( !n.isNull() )
  {
    State State_tmp;
    QDomElement e = n.toElement();
    if( !e.isNull() )
      if( e.tagName() == "state" )
      {
	State_tmp.name = e.attribute( "nazwa", "" );
	StateNameTmp=e.attribute("nazwa","");
	InState(e,State_tmp);
      }
    FSMtoRet.states[StateNameTmp]=State_tmp;
    /*
    cerr<<"dodano stan "<<State_tmp.name.toStdString()i
      <<" do mapy FSM"<<endl;
    */
    n = n.nextSibling();
  }

  return FSMtoRet;
}

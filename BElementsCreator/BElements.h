#ifndef BELEMENTS_H
#define BELEMENTS_H	

#include <QString>
#include <QTime>
#include <vector>
#include <map>

#include "moves.h"

struct MovesSet{
	QString name;
	std::vector<SimpleMove> simpleMoves;
};

struct SpeechSet : MovesSet{
	QString soundFileName;
};

struct SpeechQueueElem{
	QString soundFileName;
	QTime executeTime;
	SpeechQueueElem(QString filename, QTime time){
		soundFileName = filename;
		executeTime = time;
	}

};


struct BElements{
	std::map<QString, SpeechSet> speeches;
	std::map<QString, MovesSet> moves;
	std::map<QString, MovesSet> emotions;

};


#endif //BELEMENTS_H	
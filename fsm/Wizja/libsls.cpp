/*****************************************************************************
 *
 * main_fft.c
 *
 * FFT test program 
 *
 * Target: x86
 *
 * Mariusz Janiak
 *
 * Wroclaw 2008
 *
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#include <sys/time.h>
#include <signal.h>
#include <ctype.h>
#include <pthread.h>
#include <vector>
#include "libsls.h"

//#include "klasyfikacyjny.h"


/***************************************************************************** 
 * Constants
 *****************************************************************************/

/* Version */
#define MY_MAJOR        0
#define MY_MINOR        2

#define DEF_FIFO_NAME  "fifo"
#define DEF_DEV_NAME   "/dev/ttyUSB1"

#define FACTOR         1000

#define DEF_N          1024
#define DEF_V          10

#define MAX_DATA       512.0

#define POWER_TRES     0.06
//#define POWER_TRES     0.07
//#define POWER_TRES     0.12

#define SIG_START      0x55
static const char sig_start = 0x55;
#define SIG_ERROR        0xAA

#define FRAME_HEAD     0x5555
#define FRAME_DEVERR   0xAAAA

#define TIMEOUT_US     500000
static const unsigned long timeout_ms = 1500;// increased timeout
#define MAX_TIMEOUT    3
static const int max_timeout = 3; //same as above

#define FI_MIN         0
#define FI_MAX         (DEF_N>>2)

#define MIC_DISTANCE   0.30              /* microphone distance: 30cm */
#define SAMPLE_FREQ    20000.0           /* sample fequency 20kHz*/   
#define SAMPLE_TIME    1.0/(SAMPLE_FREQ) /* sample time */
#define SOUND_SPEED    340               /* sound speed [m/s] */

enum {
	STATE_HEAD,
	STATE_DATASIZE,
	STATE_DATA_CH1,
	STATE_DATA_CH2,
	STATE_ERR,
	STATE_TERMINATE
};
enum {
	ERR_OK, ERR_FRAME, ERR_DEV, ERR_MEM, ERR_SIZE
};

#define MSBLSBWORD(msb,lsb) ((UWord16)(((msb) << 8) | (lsb)))

/*****************************************************************************
 * Global data
 *****************************************************************************/

/*
 typedef struct SoundSource{
 QMutex qMutex;
 double angle; // range: (-Pi/2,Pi/2)
 bool found; // idicate availability of founded sound source
 bool isLonely; // not connected with any face
 int expiration;
 }soundSource; */

SoundSource soundSource;

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

#ifdef CONSOLOUTPUT
int writeFifo(int fd, double angle);
int writeFifo(int fd, double angle, double ccmax, double power);
#endif

/*****************************************************************************
 *  Output filtering class
 * ****************************************************************************/

SLSFilter::SLSFilter(int historyDepth) :
	_historyDepth(historyDepth) {
	if (_historyDepth < 1)
		_historyDepth = 1;

	_historyAngle = (double*) malloc(_historyDepth * sizeof(double));
	_historyPower = (double*) malloc(_historyDepth * sizeof(double));
	_historyCorr = (double*) malloc(_historyDepth * sizeof(double));
	_checked = (int*) malloc(_historyDepth * sizeof(int));
	memset(_historyAngle, 0, _historyDepth * sizeof(double));
	memset(_historyPower, 0, _historyDepth * sizeof(double));
	memset(_historyCorr, 0, _historyDepth * sizeof(double));
	_idx = 0;

	//TODO:dobrać wartości 
	_angleSpread = 0.0873;
	_initialThreshold = 0.08;
	_mainThreshold = 1.5;
	_maxAttempts = 3;
	_powerThresholdLower = 0.05;
	_powerThresholdUpper = 0.36;
	_corrThresholdLower = 80.0;
	_corrThresholdUpper = 320.0;
	_shift = 8;
	_idxSearch = _historyDepth - _shift;
	_expiration = 20;
	_minSampleThreshold = 2;
}

SLSFilter::~SLSFilter() {
	free(_historyAngle);
	free(_historyPower);
	free(_historyCorr);
}

void SLSFilter::addResult(double angle, double correlation, double sigTotPower) {
	//adding new result
	_idx++;
	if (_idx == _historyDepth)
		_idx = 0;
	_idxSearch++;
	if (_idxSearch == _historyDepth)
		_idxSearch = 0;

	_historyAngle[_idx] = angle;
	_historyPower[_idx] = normalizePower(sigTotPower);
	_historyCorr[_idx] = normalizeCorrelation(correlation);

	int oldest;
	if (_idx == _historyDepth - 1)
		oldest = 0;
	else
		oldest = _idx + 1;

	//computing total power of history
	int tmp = 0;
	double totalPower = 0;
	while (tmp < _historyDepth) {
		totalPower += _historyPower[tmp];
		tmp++;
	}

	int pass = 0;
	memset(_checked, 0, _historyDepth * sizeof(int));
	while (pass < _maxAttempts) {//do _maxAttepmts attempts to find sound source based on history

		//looking for potential sound source
		int tmpidx = _idxSearch, tmpsteps = 0, initial = -1;
		double point2check;
		while (tmpsteps < _historyDepth - _shift) {
			if (!_checked[tmpidx]) {
				//_checked[tmpidx] = 1;
				if ((_historyPower[tmpidx] * _historyCorr[tmpidx]
						> _initialThreshold) && (_historyAngle[tmpidx] != M_PI)) {
					initial = tmpidx;
					point2check = _historyAngle[tmpidx];
					break;
				}
			}
			tmpidx--;
			if (tmpidx == -1)
				tmpidx = _historyDepth - 1;
			tmpsteps++;
		}

		//checking the history for sounds from potential direction
		if (initial != -1) {
			double tmpCorrPow = 0;
			double tmpPow = 0;
			double tmpPowTot = 0;
			double tmpCorrPowTot = 0;
			int tmpCount = 0;
			int tmpidx2 = 0;
			while (tmpidx2 < _historyDepth) {
				if ((_historyAngle[tmpidx2] < point2check + _angleSpread)
						&& (_historyAngle[tmpidx2] > point2check - _angleSpread)) {
					tmpCorrPow += _historyPower[tmpidx2]
							* _historyCorr[tmpidx2];
					tmpPow += _historyPower[tmpidx2];
					_checked[tmpidx2] = 1; //locking double using resault
					tmpCount++;
				}
				tmpPowTot += _historyPower[tmpidx2];
				tmpCorrPowTot += _historyPower[tmpidx2] * _historyCorr[tmpidx2];
				tmpidx2++;
			}
			//if(tmpCorrPow/tmpPow > _mainThreshold){
			//if(tmpCorrPow > tmpCorrPowTot*0.5){ 
			if ((tmpPow > tmpPowTot * 0.5) && (tmpCount > _minSampleThreshold)) {
				soundSource.qMutex.lock();
				soundSource.angle = point2check * 180 / M_PI;//switch to deg
				soundSource.found = 1;
				soundSource.isLonely = 1;
				soundSource.expiration = _expiration;
				soundSource.qMutex.unlock();
				break; //source found -- end
			}
		} else {
			//if not found any potential sound source end
			break;
		}

		pass++;
	}

	soundSource.qMutex.lock();
	soundSource.expiration--;
	if ((soundSource.expiration < 0) && (soundSource.found == 1)) {
		soundSource.found = 0;
		soundSource.isLonely = 0;
		soundSource.angle = M_PI;
	}
	soundSource.qMutex.unlock();
}

double SLSFilter::normalizePower(double power) {
	//simple normalization based on S-function
	if (power < _powerThresholdLower) {
		return 0;
	} else {
		if (power < _powerThresholdUpper) {
			return (power - _powerThresholdLower) / (_powerThresholdUpper
					- _powerThresholdLower);
		} else {
			return 1;
		}
	}
}

double SLSFilter::normalizeCorrelation(double cc) {
	//simple normalization based on S-function
	if (cc < _corrThresholdLower) {
		return 0;
	} else {
		if (cc < _corrThresholdUpper) {
			return (cc - _corrThresholdLower) / (_corrThresholdUpper
					- _corrThresholdLower);
		} else {
			return 1;
		}
	}
}

/******************************************************************************
 Thread Service Rutine Class
 ****************************************************************************/

/* Device setting method */
void ThreadServiceRutine::setCommDev(comm_dev_str *ptr) {
	_dev = ptr;
}

/* Thread Service Rutine */
void ThreadServiceRutine::run() {
	//printf("SoundLocalizationSubsystem::ThreadServiceRutine::Started\n");
	int state = 1;
	if (_dev == NULL) {
		//communication device not set
		printf(
				"SoundLocalizationSubsystem::ThreadServiceRutine::ERROR::Communication device not set\n");
		QThread::exit(ERR_DEV);
		state = 0;
	}
	double *newdata = 0;
	UWord8 msb, lsb;
	Boolean fLSB = TRUE;
	UWord16 data;
	int tmp;

	//printf("Create data thread\n");
	while (state) {
		_dev->qMutex_getData.lock();
		if (!_dev->getData) {
			_dev->qCond_getData.wait(&_dev->qMutex_getData);
			/* LSB first */
			fLSB = TRUE;
			/* Recive frame head */
			_dev->state = STATE_HEAD;
			/* Set data pointer */
			_dev->load_ptr->ptr = 0;
		}
		_dev->qMutex_getData.unlock();
		/* Recieve data */
		if (fLSB) {
			UL_rx(_dev->fd, &lsb);
			fLSB = FALSE;
		} else {
			UL_rx(_dev->fd, &msb);
			fLSB = TRUE;
			/* Convert data to 16b */
			data = MSBLSBWORD(msb,lsb);
			/* Proces frame */
			//printf("SoundLocalizationSubsystem::ThreadServiceRutine::HEAD:: msb:0x%x lsb:0x%x\n", msb, lsb);
			switch (_dev->state) {
			case STATE_HEAD:
				//printf("HEAD:: msb:0x%x lsb:0x%x\n",msb,lsb);
				if (data == FRAME_HEAD) {
					_dev->qMutex_dev.lock();
					_dev->state = STATE_DATASIZE;
					_dev->qMutex_dev.unlock();
				} else if (data == FRAME_DEVERR) {
					_dev->qMutex_dev.lock();
					_dev->state = STATE_HEAD;
					_dev->err = ERR_DEV;
					_dev->getData = FALSE;
					_dev->dataReady = TRUE;
					_dev->qMutex_dev.unlock();
					/* Send data ready signal */
					_dev->qMutex_dataReady.lock();
					_dev->qCond_dataReady.wakeAll();
					_dev->qMutex_dataReady.unlock();
				} else {
					//printf("SoundLocalizationSubsystem::ThreadServiceRutine::HEAD:: msb:0x%x lsb:0x%x\n", msb, lsb);
					_dev->qMutex_dev.lock();
					_dev->state = STATE_ERR;
					_dev->err = ERR_FRAME;
					_dev->dataReady = TRUE;
					_dev->qMutex_dev.unlock();
					/* Send data ready signal */
					_dev->qMutex_dataReady.lock();
					_dev->qCond_dataReady.wakeAll();
					_dev->qMutex_dataReady.unlock();
				}
				break;
			case STATE_DATASIZE:
				//printf("DATASIZE:: %u\n",data);
				/* Check data size */
				tmp = MY_log2(data);
				if (tmp < 0) {
					_dev->qMutex_dev.lock();
					_dev->state = STATE_ERR;
					_dev->err = ERR_SIZE;
					_dev->dataReady = TRUE;
					_dev->qMutex_dev.unlock();
					/* Send data ready signal */
					_dev->qMutex_dataReady.lock();
					_dev->qCond_dataReady.wakeAll();
					_dev->qMutex_dataReady.unlock();
					break;
				}
				/* Resize data buffers if needed */
				if (data > _dev->load_ptr->n) {
					/* Resize ch1 */
					if (data > _dev->load_ptr->ch1Size) {
						newdata = (double*) realloc(_dev->load_ptr->ch1, data);
						if (newdata != NULL) {
							_dev->qMutex_dev.lock();
							_dev->load_ptr->ch1 = newdata;
							_dev->load_ptr->ch1Size = data;
							_dev->qMutex_dev.unlock();
						} else {
							_dev->qMutex_dev.lock();
							_dev->state = STATE_ERR;
							_dev->err = ERR_MEM;
							_dev->dataReady = TRUE;
							_dev->qMutex_dev.unlock();
							/* Send data ready signal */
							_dev->qMutex_dataReady.lock();
							_dev->qCond_dataReady.wakeAll();
							_dev->qMutex_dataReady.unlock();
							break;
						}
					}
					/* Resize ch2 */
					if (data > _dev->load_ptr->ch2Size) {
						newdata = (double*) realloc(_dev->load_ptr->ch2, data);
						if (newdata != NULL) {
							_dev->qMutex_dev.lock();
							_dev->load_ptr->ch2 = newdata;
							_dev->load_ptr->ch2Size = data;
							_dev->qMutex_dev.unlock();
						} else {
							_dev->qMutex_dev.lock();
							_dev->state = STATE_ERR;
							_dev->err = ERR_MEM;
							_dev->dataReady = TRUE;
							_dev->qMutex_dev.unlock();
							/* Send data ready signal */
							_dev->qMutex_dataReady.lock();
							_dev->qCond_dataReady.wakeAll();
							_dev->qMutex_dataReady.unlock();
							break;
						}
					}
					_dev->qMutex_dev.lock();
					_dev->load_ptr->n = data;
					_dev->load_ptr->v = tmp;
					_dev->qMutex_dev.unlock();
				} else if (data < _dev->load_ptr->n) {
					_dev->qMutex_dev.lock();
					_dev->load_ptr->n = data;
					_dev->load_ptr->v = tmp;
					_dev->qMutex_dev.unlock();
				}
				_dev->qMutex_dev.lock();
				_dev->state = STATE_DATA_CH1;
				_dev->qMutex_dev.unlock();
				break;
			case STATE_DATA_CH1:
				//printf("DATA CH1:: %u\n",data);
				_dev->qMutex_dev.lock();
				_dev->load_ptr->ch1[_dev->load_ptr->ptr++] = ((double) data
						- MAX_DATA) / MAX_DATA;
				if (_dev->load_ptr->ptr == _dev->load_ptr->n) {
					_dev->state = STATE_DATA_CH2;
					_dev->load_ptr->ptr = 0;
				}
				_dev->qMutex_dev.unlock();
				break;
			case STATE_DATA_CH2:
				//printf("DATA CH2:: %u\n",data);
				_dev->qMutex_dev.lock();
				/* Normalize and save data */
				_dev->load_ptr->ch2[_dev->load_ptr->ptr++] = ((double) data
						- MAX_DATA) / MAX_DATA;
				//	printf("ch1[%i]=%f ch2[%i]=%f\n",dev->load_ptr->ptr-1,dev->load_ptr->ch1[dev->load_ptr->ptr-1],dev->load_ptr->ptr-1,dev->load_ptr->ch2[dev->load_ptr->ptr-1]);
				if (_dev->load_ptr->ptr == _dev->load_ptr->n) {
					_dev->state = STATE_HEAD;
					_dev->timeout = 0;
					_dev->load_ptr->ptr = 0;
					_dev->err = ERR_OK;
					_dev->getData = FALSE;
					_dev->dataReady = TRUE;
					/* Send data ready signal */
					_dev->qMutex_dataReady.lock();
					_dev->qCond_dataReady.wakeAll();
					_dev->qMutex_dataReady.unlock();
				}
				_dev->qMutex_dev.unlock();

				break;
			case STATE_ERR:
				//printf("ERR STATE (data=0x%x)\n",data);
				/* Wait for fream head (sync) */
				if (data == FRAME_HEAD) {
					_dev->qMutex_dev.lock();
					_dev->state = STATE_DATASIZE;
					_dev->qMutex_dev.unlock();
				} else if (data == FRAME_DEVERR) {
					_dev->qMutex_dev.lock();
					_dev->state = STATE_HEAD;
					_dev->err = ERR_DEV;
					_dev->getData = FALSE;
					_dev->dataReady = TRUE;
					_dev->qMutex_dev.unlock();
					/* Send data ready signal */
					_dev->qMutex_dataReady.lock();
					_dev->qCond_dataReady.wakeAll();
					_dev->qMutex_dataReady.unlock();
				}
				break; /* end: STATE_ERR */
			case STATE_TERMINATE:
				printf(
						"SoundLocalizationSubsystem::ThreadServiceRutine::Transmiter terminated\n");
				state = 0;
				QThread::exit(0);
			default:
				printf(
						"SoundLocalizationSubsystem::ThreadServiceRutine::ERROR: TSR_data -- unknown state %i!!!\n",
						_dev->state);
				break;
			}/* end: switch (dev->state) */
		}/*end: else associated with if(fLSB) */
	}/* end: while(1) */
	printf(
			"SoundLocalizationSubsystem::ThreadServiceRutine::ERROR: Terminate transmitter\n");
	QThread::exit(0);
}

int ThreadServiceRutine::MY_log2(UWord32 x) {
	UWord32 i = 0;
	UWord32 tmp = 1;

	if (x == 0)
		return -1;
	while (1) {
		if (x == tmp)
			return i;
		else if (x > tmp) {
			tmp <<= 1;
			i++;
		} else
			return -1;
	}
}

/*######################################################################*/
/* Main rutine of sound localization subsystem							*/
/*######################################################################*/

/**
 * Pomocnicza funkcja konwertująca.
 * @param QString
 * @return bool Zwraca false <=> gdy str=="0".
 */
static bool QStringNum2Bool(QString str) {
	if (str == "0")
		return false;
	else
		return true;
}

SoundLocalizationSubsystem::SoundLocalizationSubsystem() :
	filter(20) {
	TSR.setCommDev(&commDev);// connecting device handling thread with device
	_fd = 0;

	//read XML config
	QDomDocument doc("SLSConfig");
	QFile file(SLS_CONFIG_XML);
	if (!file.open(QIODevice::ReadOnly)) {
		std::cerr
				<< "SoundLocalizationSubsystem::SoundLocalizationSubsystem():: error: can't open SLS_CONFIG_XML file"
				<< std::endl;
		exit(-1);
	}
	if (!doc.setContent(&file)) {
		file.close();
		std::cerr
				<< "SoundLocalizationSubsystem::SoundLocalizationSubsystem():: error: can't set XML file content"
				<< std::endl;
		exit(-2);
	}
	file.close();

	QDomElement root = doc.documentElement();
	if (root.tagName() != "SLSConfig") {
		std::cerr
				<< "SoundLocalizationSubsystem::SoundLocalizationSubsystem():: error: can't find root in SLS_CONFIG_XML file "
				<< std::endl;
		exit(-3);
	}

	//some default parameters
	_sampleFreq = SAMPLE_FREQ;
	_sampleTime = SAMPLE_TIME;
	_micDist = MIC_DISTANCE;
	_soundSpeed = SOUND_SPEED;
	_soundDevName = DEF_DEV_NAME;

	//readout XML
	QDomNode n = root.firstChild();
	QDomElement e = n.toElement();

	if (e.tagName() == "configs") {
		_soundDevName = e.attribute("SerialDev", "").toStdString();
		_micDist = e.attribute("MicDistance", "").toDouble();
		_sampleFreq = e.attribute("SampleFreq", "").toDouble();
		_sampleTime = 1.0 / _sampleFreq;
		_soundSpeed = e.attribute("SoundSpeed", "").toDouble();
		_record = QStringNum2Bool(e.attribute("DataRecording", ""));
	} else {
		std::cerr
				<< "SoundLocalizationSubsystem::SoundLocalizationSubsystem()::error: cfg/SLSConfig.xml wrong construction"
				<< std::endl;
		exit(-4);
	}

	_halt = false;

	/* Init global variables */
	commDev.data1.ch1 = (double*) malloc(DEF_N * sizeof(commDev.data1.ch1[0]));
	commDev.data1.ch2 = (double*) malloc(DEF_N * sizeof(commDev.data1.ch2[0]));
	commDev.data1.ch1Size = DEF_N;
	commDev.data1.ch2Size = DEF_N;
	commDev.data1.ptr = 0;
	commDev.data1.n = DEF_N;
	commDev.data1.v = DEF_V;
	commDev.data2.ch1 = (double*) malloc(DEF_N * sizeof(commDev.data2.ch1[0]));
	commDev.data2.ch2 = (double*) malloc(DEF_N * sizeof(commDev.data2.ch2[0]));
	commDev.data2.ch1Size = DEF_N;
	commDev.data2.ch2Size = DEF_N;
	commDev.data2.ptr = 0;
	commDev.data2.n = DEF_N;
	commDev.data2.v = DEF_V;
	commDev.work_ptr = &commDev.data1;
	commDev.load_ptr = &commDev.data2;
	commDev.state = STATE_HEAD;
	commDev.timeout = 0;
	commDev.getData = FALSE;
	commDev.dataReady = FALSE;
	commDev.err = FALSE;
	work.Wn = (double*) malloc(((1 << (DEF_V - 1)) - 2) * sizeof(work.Wn[0]));
	work.X1 = commDev.work_ptr->ch1;
	work.X2 = commDev.work_ptr->ch2;
	work.AC1 = (double*) malloc(DEF_N * sizeof(work.AC1[0]));
	work.AC2 = (double*) malloc(DEF_N * sizeof(work.AC2[0]));
	work.E1 = (double*) malloc(DEF_N * sizeof(work.E1[0]));
	work.E2 = (double*) malloc(DEF_N * sizeof(work.E2[0]));
	work.P1 = (double*) malloc(DEF_N * sizeof(work.P1[0]));
	work.P2 = (double*) malloc(DEF_N * sizeof(work.P2[0]));
	work.phi1 = (double*) malloc(DEF_N * sizeof(work.phi1[0]));
	work.phi2 = (double*) malloc(DEF_N * sizeof(work.phi2[0]));
	work.CC = (double*) malloc(DEF_N * sizeof(work.CC[0]));
	work.ECC = (double*) malloc(DEF_N * sizeof(work.ECC[0]));
	work.PCC = (double*) malloc(DEF_N * sizeof(work.PCC[0]));
	work.delay = 0;
	work.angle = 0;
	work.size = DEF_N;
	work.ptr = 0;
	work.n = DEF_N;
	work.v = DEF_V;

	soundSource.angle = M_PI;
	soundSource.found = 0;
	soundSource.isLonely = 0;
	soundSource.expiration = 0;

#ifdef SOUNDRECORDING
	//data recording setting
	_sndBuffer = (double*) malloc(DEF_N * 2 * sizeof(double));
	_sfinfo.samplerate = _sampleFreq;
	_sfinfo.channels = 2; //stereo
	_sfinfo.format = (SF_FORMAT_WAV | SF_FORMAT_PCM_16);
	_recordActive = 0;
#endif
}

SoundLocalizationSubsystem::~SoundLocalizationSubsystem() {
	//terminating TSR
	commDev.state = STATE_TERMINATE;
	while (TSR.isRunning()) {
		commDev.state = STATE_TERMINATE;
	};

	//releasing memory
	free(commDev.data1.ch1);
	free(commDev.data1.ch2);
	free(commDev.data2.ch1);
	free(commDev.data2.ch2);
	free(work.Wn);
	free(work.AC1);
	free(work.AC2);
	free(work.E1);
	free(work.E2);
	free(work.P1);
	free(work.P2);
	free(work.phi1);
	free(work.phi2);
	free(work.CC);
	free(work.ECC);
	free(work.PCC);

#ifdef OSLINUX
	//closing device
	if (commDev.fd != 0) {
		close(commDev.fd);
	}
#endif
}

void SoundLocalizationSubsystem::run() {

	int nTimeouts = 0, i;
	int state = 1;

#ifdef CONSOLOUTPUT
	int err;
#endif

#ifdef GNUPLOTOUTPUT
	double filtered; /** calculated direction after filtering RM **/
	double tmpAngle;
#endif

	double tmp;
	int flag = 0;

	/* Open serial port*/
#ifdef OSLINUX
	commDev.fd = UL_open(_soundDevName.c_str(), UL_BOUDRATE_921600, TRUE);//TODO:wymaga portu na windowsa
	if (commDev.fd < 0) {
		printf(
				"SoundLocalizationSubsystem::MainRutine::ERROR::Can't open device: %s\n",
				_soundDevName.c_str());
		QThread::exit(EXIT_FAILURE);
		state = 0;
	}
	/* Flush rx/tx buffers */
	tcflush(commDev.fd, TCIOFLUSH);
#endif

#ifdef OSWINDOWS
	//TODO:wymaga portu na windowsa
#endif

	/* Create data thread */
	//TSR.setCommDev(&commDev); //moved to constructor
	TSR.start();
	if (!TSR.isRunning()) {
		printf(
				"SoundLocalizationSubsystem::MainRutine::ERROR::ThreadServiceRutine creation failed\n");
#ifdef OSLINUX
		UL_close(commDev.fd);
#endif

#ifdef OSWINDOWS
		//TODO:wymaga portu na windowsa
#endif
		QThread::exit(EXIT_FAILURE);
		state = 0;
	}

	/* Compute twiddle factors for FHT*/
	fht_twiddles_double(work.Wn, work.v);

	/* Measure init */
	//measureInit(&commDev);

	while (state) {

		while (_halt) {// _halt -- external turn-off flag
			//TODO:problem jaki stan powinny przyjąć dane wyjściowe jeśli zamrożony
			msleep(25);// sleeping in 25ms delays if halted
		}

		/* Measure init */
		measureInit();

		/* Wait for data */
		commDev.qMutex_dataReady.lock();
		if (!commDev.dataReady) {
			while (!commDev.qCond_dataReady.wait(&commDev.qMutex_dataReady,
					timeout_ms)) {
				printf(
						"SoundLocalizationSubsystem::MainRutine::DEVICE TIMEOUT\n");
				nTimeouts++;
				measureInit();
				if (nTimeouts > max_timeout) {
					printf(
							"SoundLocalizationSubsystem::MainRutine::ERROR::Endding rutine. Device failiure.\n");
					QThread::exit(EXIT_FAILURE);
					state = 0;
					break;
				}
			}
		}
		commDev.qMutex_dataReady.unlock();

		/* Swich load and work buffers */
		commDev.qMutex_dev.lock();
		if (commDev.work_ptr == &commDev.data1) {
			commDev.work_ptr = &commDev.data2;
			commDev.load_ptr = &commDev.data1;
		} else {
			commDev.work_ptr = &commDev.data1;
			commDev.load_ptr = &commDev.data2;
		}
		commDev.qMutex_dev.unlock();

#ifdef SOUNDRECORDING
		// >>>>>>>>>>> recording sound to file <<<<<<<<<<<
		if (_record) {
			if (!_recordActive) {
				//file need to be open
				//generating name
				time(&_rawtime);
				_parsedtime = gmtime(&_rawtime);
				sprintf(_sndFileName, "record/sounds-%d-%d_%2d%02d.wav",
						_parsedtime->tm_mon + 1, _parsedtime->tm_mday,
						(_parsedtime->tm_hour + STREFACZASOWA) % 24,
						_parsedtime->tm_min);
				//opening file
				if ((_sndfile = sf_open(_sndFileName, SFM_WRITE, &_sfinfo))) {
					//file opened
					_recordActive = true;
					_recordedFrames = 0;
				}
			}

			if (_recordActive) {
				//mixing channels
				for (i = 0; i < DEF_N; i++) {
					//TODO: sprawdzić normalizację
					_sndBuffer[i] = (commDev.work_ptr->ch1[i] * MAX_DATA)
							+ MAX_DATA;
					_sndBuffer[i + 1] = (commDev.work_ptr->ch2[i] * MAX_DATA)
							+ MAX_DATA;
				}
				//recording data
				if (sf_write_double(_sndfile, _sndBuffer, _sfinfo.channels
						* DEF_N) != _sfinfo.channels * DEF_N) {
					//if trouble turn-off recording
					_recordActive = false;
					_record = false;
					sf_close(_sndfile);
				} else {
					_recordedFrames++;
					if (_recordedFrames > 6000) {
						//record limited to ~5min
						_recordActive = false;
						sf_close(_sndfile);
					}
				}
			}
		}
#endif

		/* Connect working space with data buffer */
		work.X1 = commDev.work_ptr->ch1;
		work.X2 = commDev.work_ptr->ch2;
		/* Measure init */
		//measureInit(&commDev);
		/* Proces data */
		if (commDev.err == ERR_OK) {
			/* Check data size */
			if (commDev.work_ptr->n != work.n) {
				printf(
						"SoundLocalizationSubsystem::MainRutine::ERROR::Data size changed!!!\n");
				QThread::exit(EXIT_FAILURE);
				state = 0;
				break;
			}

			/* Compute signals envelope */
			envelope_hilbert_fht_double(work.X1, work.E1, work.Wn, work.v);
			envelope_hilbert_fht_double(work.X2, work.E2, work.Wn, work.v);
			work.X1 = work.E1; //TODO: tu skończyłeś grzebać przy filtracji
			work.X2 = work.E2;

			/* FHT transform */
			fht_radix2_double(work.X1, work.Wn, work.v);
			fht_radix2_double(work.X2, work.Wn, work.v);

			/* Compute autocorrelation signal*/
			fht_autocorrelation_core_with_output(work.X1, work.AC1, work.v);
			fht_autocorrelation_core_with_output(work.X1, work.AC2, work.v);
			fht_radix2_double(work.AC1, work.Wn, work.v);
			fht_radix2_double(work.AC2, work.Wn, work.v);

			/* Move negative lags before positive lags */
			for (i = 0; i < work.n / 2; i++) {
				tmp = work.AC1[i];
				work.AC1[i] = work.AC1[i + work.n / 2];
				work.AC1[i + work.n / 2] = tmp;
				tmp = work.AC2[i];
				work.AC2[i] = work.AC2[i + work.n / 2];
				work.AC2[i + work.n / 2] = tmp;
			}

			/* Compute signals power spectrum */
			power_spectrum_core_fht_double(work.X1, work.P1, work.v);
			power_spectrum_core_fht_double(work.X2, work.P2, work.v);
			/* Compute signals phase spectrum */
			phase_core_fht_double(work.X1, work.phi1, work.v);//TODO:wyłączyć
			phase_core_fht_double(work.X2, work.phi2, work.v);
			/* Compute signal total power (without constant factor)*/
			work.P1Tot = work.P2Tot = 0;
			for (i = 1; i < work.n >> 1; i++) {
				work.P1Tot += work.P1[i];
				work.P2Tot += work.P2[i];
			}
			work.P1Tot /= (work.n >> 1) - 1;
			work.P2Tot /= (work.n >> 1) - 1;
			if ((work.P1Tot < POWER_TRES) || (work.P2Tot < POWER_TRES)) {
#ifdef GNUPLOTOUTPUT
				flag++;
				if (flag>4) {
					flag = 0;
					soundSource.qMutex.lock();
					tmpAngle = soundSource.angle;
					soundSource.qMutex.unlock();
					if(tmpAngle == M_PI) {
						fprintf(pipegnuplot, "plot \"-\" with vector title \"Dir\"\n");
						fprintf(pipegnuplot, "0 0 0.01 0.01\n");
						fprintf(pipegnuplot, "end\n");
						fflush(pipegnuplot);
					} else {
						fprintf(pipegnuplot, "plot \"-\" with vector lw 12 title \"Dir\"\n");
						fprintf(pipegnuplot, "0 0 %f %f\n", cos(tmpAngle+M_PI/2), sin(tmpAngle
										+M_PI/2));
						fprintf(pipegnuplot, "end\n");
						fflush(pipegnuplot);
					}
				}
#endif
				filter.addResult(M_PI, 0.0, 0.0);
#ifdef CONSOLOUTPUT
				err = writeFifo(_fd, M_PI);
				if (err) {
					QThread::exit(EXIT_FAILURE);
					state = 0;
					break;
				}
#endif
				continue;
			}

			/* Remove echos from signal */
			for (i = 0; i < work.n; i++) {
				work.X1[i] /= work.AC1[i];
				work.X2[i] /= work.AC2[i];
			}
			/* Compute cross-correlation */
			fht_correlation_PHAT_core_with_output(work.X1, work.X2, work.CC,
					work.v);
			/* Convert CC to time domain */
			fht_radix2_double(work.CC, work.Wn, work.v);
			/* Move negative lags before positive lags */
			for (i = 0; i < work.n / 2; i++) {
				tmp = work.CC[i];
				work.CC[i] = work.CC[i + work.n / 2];
				work.CC[i + work.n / 2] = tmp;
			}
			/* Find max in cross-correlation*/
			work.imax = 0;
			work.CCmax = 0;
			for (i = 0; i < work.n; i++) {
				if (work.CC[i] > work.CCmax) {
					work.CCmax = work.CC[i];
					work.imax = i;
				}
			}
			//printf("###moj klasyfikator: %f ###\n",work.CCmax*(work.P1Tot+work.P2Tot));
			/* Compute time daly */
			work.delay = _sampleTime * (work.imax - work.n / 2);
			/* Compute sound source position angle */
			tmp = work.delay * _soundSpeed / _micDist;
			if ((tmp > -1) && (tmp < 1)) {
				work.angle = asin(tmp);
			} else {
				filter.addResult(M_PI, 0.0, 0.0);
#ifdef CONSOLOUTPUT
				err = writeFifo(_fd, M_PI);
				if (err) {
					QThread::exit(EXIT_FAILURE);
					state = 0;
					break;
				}
#endif
				continue;
			}
			/* Plot sound source position */
			if (work.delay == 0) {
				filter.addResult(M_PI, 0.0, 0.0);
#ifdef CONSOLOUTPUT
				err = writeFifo(_fd, M_PI);
				if (err) {
					QThread::exit(EXIT_FAILURE);
					state = 0;
					break;
				}
#endif
				continue;
			}
			flag = 0;

			filter.addResult(work.angle, work.CCmax, work.P1Tot + work.P2Tot);

			/** Filter calculated angle RM **/
			//filtered = filtruj(work.angle);

#ifdef GNUPLOTOUTPUT
			soundSource.qMutex.lock();
			filtered = soundSource.angle;
			soundSource.qMutex.unlock();
			fprintf(pipegnuplot, "plot \"-\" with vector lw 12 title \"Dir\"\n");
			fprintf(pipegnuplot, "0 0 %f %f\n", cos(filtered+M_PI/2), sin(filtered
							+M_PI/2));
			fprintf(pipegnuplot, "end\n");
			fflush(pipegnuplot);
#endif
			/* Write fifo */
			/** Hopefully write calculated angle here and only here RM **/
			//printf("oryginal = %f[rad]\n", work.angle);
			/*       err = writeFifo(fd,work.angle); */

			//err = writeFifo(_fd, filtered);

#ifdef CONSOLOUTPUT
			err = writeFifo(_fd, work.angle, work.CCmax, work.P1Tot+work.P2Tot);
			if (err) {
				QThread::exit(EXIT_FAILURE);
				state = 0;
				break;
			}
#endif
		} else {
			switch (commDev.err) {
			case ERR_FRAME:
				printf(
						"SoundLocalizationSubsystem::MainRutine::ERROR::FRAME ERROR!!!\n");
				break;
			case ERR_DEV:
				printf(
						"SoundLocalizationSubsystem::MainRutine::ERROR::DEV ERROR!!!\n");
				QThread::exit(EXIT_FAILURE);
				state = 0;
				break;
			case ERR_MEM:
				printf(
						"SoundLocalizationSubsystem::MainRutine::ERROR::MEM ERROR!!!\n");
				QThread::exit(EXIT_FAILURE);
				state = 0;
				break;
			case ERR_SIZE:
				printf(
						"SoundLocalizationSubsystem::MainRutine::ERROR::SIZE ERROR!!!\n");
				QThread::exit(EXIT_FAILURE);
				state = 0;
				break;
			}
			/* Send error signal to device */
#ifdef OSLINUX
			UL_tx(commDev.fd, SIG_ERROR);
			tcflush(commDev.fd, TCOFLUSH);
#endif
#ifdef OSWINDOWS
			//TODO:wymaga portu na windowsa
#endif
		} /*end: } else { */
	}/* end: while(1) */
#ifdef GNUPLOTOUTPUT
	pclose(pipegnuplot);
#endif

	/* Close port */
#ifdef OSLINUX
	UL_close(commDev.fd);
	commDev.fd = 0;
#endif
#ifdef OSWINDOWS

#endif

	QThread::exit(EXIT_SUCCESS);
}
;

void SoundLocalizationSubsystem::measureInit() {

	/* OS depended section - some hardware handling*/
#ifdef OSLINUX
	/* Flush input buffer */
	tcflush(commDev.fd, TCIFLUSH);
	/* Send start signal */
	//UL_tx(dev->fd, SIG_START);
	UL_tx(commDev.fd, sig_start);
	tcflush(commDev.fd, TCOFLUSH);
#endif

#ifdef OSWINDOWS
	//TODO: stworzyć wersje na Windowsowa
#endif

	/* Set flags */
	commDev.qMutex_dev.lock();
	commDev.getData = TRUE;
	commDev.dataReady = FALSE;
	commDev.qMutex_dev.unlock();
	/* Unblock getData mutex */
	commDev.qMutex_getData.lock();
	commDev.qCond_getData.wakeAll();
	commDev.qMutex_getData.unlock();
}

void SoundLocalizationSubsystem::haltSoundLocalization(bool halt) {
	_halt = halt;
}

#ifdef OSLINUX
void SoundLocalizationSubsystem::setFifo(int fd) {
	_fd = fd;
}
#endif

#ifdef GNUPLOTOUTPUT
void SoundLocalizationSubsystem::setGnuplotPipe(FILE* pipe) {
	pipegnuplot = pipe;
}
#endif

/***************************************************************************** 
 * Stand alone version
 *****************************************************************************/

#ifdef STANDALONE

#define PROG_USAGE "USAGE: %s [-d device_name] [-f fifo_name] [-h]\n"

int main(int argc, char *argv[]) {

	FILE *mypipe4 = NULL;
	char *fname = NULL;
	char *dname = NULL;
	int fd;

	/* Proces program arguments */
	{
		int opt;
		opterr = 0;
		while ((opt = getopt(argc, argv, "d:f:h")) != -1) {
			switch (opt) {
				case 'd':
				dname = optarg;//broken
				break;
				case 'f':
				fname = optarg;
				break;
				case 'h':
				printf("\n");
				printf(PROG_USAGE,argv[0]);
				printf("\n");
				exit(EXIT_SUCCESS);
				break;
				case '?':
				fprintf(stderr, "%s: Unknown option -- %c\n", argv[0], optopt);
				fprintf(stderr, PROG_USAGE, argv[0]);
				exit(EXIT_FAILURE);
				break;
				default:
				fprintf(stderr, "%s: Unhandled option -- %c\n", argv[0], opt);
				exit(EXIT_FAILURE);
			}
		}/*end: while ((opt = getopt(argc, argv, "d:f:h")) != -1) */
		if (optind < argc) {
			fprintf(stderr, PROG_USAGE, argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	/* loading default values */
	if (fname == NULL) {
		fname = DEF_FIFO_NAME;
	}
	/*if (_soundDevName == NULL) {
	 _soundDevName = DEF_DEV_NAME;
	 }*/

	SoundLocalizationSubsystem wielkieUcho(dname);

	printf("Waiting for fifo receiver... ");
	fflush(stdout);
	if ((fd = open(fname, O_WRONLY)) < 0) {
		printf("FAILED\n");
		printf("%s: Can't open fifo: %s\n", argv[0], fname);
		perror(argv[0]);
		exit(EXIT_FAILURE);
	}
	printf("OK\n");
	/* My pipe 4 */
	//TODO:linuksowe - wyrzucić/otagować odpowiednio
	if ((mypipe4 = popen("gnuplot", "w")) == NULL) {
		printf("%s: Can't creat pipe to GnuPlot\n", argv[0]);
		perror(argv[0]);
		exit(EXIT_FAILURE);
	}
	fprintf(mypipe4, "set xrange [-1.1:1.1]\n");
	fprintf(mypipe4, "set yrange [0:1.1]\n");
	fprintf(mypipe4, "set xlabel \"x\"\n");
	fprintf(mypipe4, "set ylabel \"y\"\n");
	fprintf(mypipe4, "set size ratio 0.5\n");
	fprintf(mypipe4, "set grid polar\n");

	wielkieUcho.setGnuplotPipe(mypipe4);
	wielkieUcho.setFifo(fd);
	wielkieUcho.start();
	while(1) {};
}

#endif

/*******************************************************************/

#ifdef CONSOLOUTPUT

char gBuf[100];

/* Write fifo *///TODO: napisac wersje na windowsa
int writeFifo(int fd, double angle) {
	int tmp = (int)(angle*FACTOR);
	int len;

	len = sprintf(gBuf, ":%d\n", tmp);
	if (write(fd, gBuf, len) < 0) {
		printf("%s: Write fifo error\n", __FUNCTION__);
		perror(__FUNCTION__);
		return -1;
	}
	if (angle < 3) {
		printf("  sent angle = %f[rad]                          \n", angle);
		fflush(stdout);
	}
	return 0;
}

/* Write fifo *///TODO: napisac wersje na windowsa
int writeFifo(int fd, double angle, double ccmax, double power) {
	int tmp = (int)(angle*FACTOR);
	int len;

	len = sprintf(gBuf, ":%d #%f #%f\n", tmp, ccmax, power);
	if (write(fd, gBuf, len) < 0) {
		printf("%s: Write fifo error\n", __FUNCTION__);
		perror(__FUNCTION__);
		return -1;
	}
	if (angle < 3) {
		printf("  sent angle = %f[rad]                          \n", angle);
		fflush(stdout);
	}
	return 0;
}

#endif

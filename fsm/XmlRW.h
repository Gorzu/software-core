#ifndef XMLRW_HH
#define XMLRW_HH

#include <iostream>
#include <QString>
#include <QtXml>
#include "utils.h"
#include "FSM.h"
#include "BElements.h"

using std::vector;
using std::cerr;
using std::endl;
using std::map;

/**
 * @param filename ścieżka do pliku *xml.
 * @return wczytane dane.
 */
BElements GetBElementsFromFile(QString filename);

/**
 * @param be przekazywana prez referencję struktura
 * danych BElements do zapisania w pliku filename.
 * @param filename ścieżka do pliku *.xml do zapisu.
 */
void WriteBElementsToFile(BElements &be,QString filename);

/**
 * @param filename ścieżka do pliku *xml.
 * @return wczytane dane.
 */
FSM GetFSMfromFile(QString filename);

/**
 * @param FSM przekazywana prez referencję struktura
 * danych do zapisania w pliku filename
 * @param filename ścieżka do pliku *.xml do zapisu.
 */
void WriteFSMtoFile(FSM & _FSM, QString filename);

#endif // XMLRW_HH

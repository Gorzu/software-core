#ifndef FSM_H
#define FSM_H

#include <vector>
#include <map>
#include <QString>


struct NameProb{
	NameProb(QString name, double probability)
		:name(name), probability(probability) {}
	NameProb() {}
	QString name;
	double probability;
};

struct Elem: public NameProb{
	Elem(QString name, double probability, long totalTime)
		:totalTime(totalTime) {
	this->name = name;
	this->probability = probability;
	}
	Elem(){}

	long totalTime;


};

struct Relation{
	std::vector<QString> entryStates;
	std::vector<QString> conditions;
	std::vector<NameProb> exitStates;

	bool inEntryState(QString state){
		for (unsigned int i=0; i < entryStates.size() ;i++){
			if ( (state == entryStates[i]) || (entryStates[i] == "*") )
				return true;
		}
		return false;
	}


};

struct BFrame{
	std::vector<Elem> speechElems;
	std::vector<Elem> moveElems;
	std::vector<Elem> emotionElems;
	long totalTime;

	Elem ChooseElem(const std::vector<Elem>& lista, double r){
		for (unsigned int i=0; i< lista.size(); i++){
			if( r > lista[i].probability)
				r -= lista[i].probability;
			else
				return lista[i];
		}
		//error
	}

	BFrame SingleBFrame(double r1, double r2, double r3){
		BFrame result;
		result.speechElems.push_back( ChooseElem(speechElems, r1) );
		result.moveElems.push_back( ChooseElem(moveElems, r2) );
		result.emotionElems.push_back( ChooseElem(emotionElems, r3) );
		result.totalTime = this->totalTime;
		return result;
	}
};

struct State
{
	QString name;
	std::vector<Relation> relations;
	std::vector<BFrame> BFrameList;
	bool visionInterrupt;
	bool soundInterrupt;

	State(){
		visionInterrupt = false;
		soundInterrupt = false ;
	}


};

struct FSM
{
	std::map<QString, State> states;
};


#endif //FSM_H


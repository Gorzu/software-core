/******************************************************************************
 *
 * fft.c
 *
 * Fast Fourier Transformation module.
 * 
 * Autor: Marisz Janiak
 *
 * Wroclaw 2008
 *
 *****************************************************************************/

#include <stdio.h>
#include <math.h>
#include "qmath.h"
#include "fft.h"

/***************************************************************************** 
 * Constants
 *****************************************************************************/

#define N (1 << v)

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

/***************************************************************************** 
 *  Global variables
 *****************************************************************************/

/***************************************************************************** 
 * Functions implementation
 *****************************************************************************/

/* Generate twiddle factor matrix of 2^(v-1) length */
void fft_twiddles_complex(complex_str Wn[], UWord16 v)
{
  UWord16 i;
  double theta;
    
  theta = 2*M_PI/N;
  /* Loop for each sub DFT */
  for(i=0; i<(1<<(v-1)); i++){ 
    /* Loop for each butterfly */
    Wn[i].re = cos(i*theta);
    Wn[i].im = -sin(i*theta);
  }
}

/* Sort matrix ib bit reverse order */
void bit_reverse_complex(complex_str x[], UWord16 v)
{
  UWord16 i,j,k;
  complex_str tmp;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap */
      tmp  = x[i];
      x[i] = x[j];
      x[j] = tmp;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

/* FFT radix-2 algorithm using complex number representation */
/* Wn -- twiddle factor matrix (length N/2) */
void fft_radix2_complex(complex_str x[], UWord16 v, complex_str Wn[])
{
  UWord16 i,j,k;
  UWord32 le,le2,n2,n1;
  complex_str tmp;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_complex(x,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){ 
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){ 
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
 	n2       = k + le2;
	//printf("i:%u, j:%u, k:%u, n1:%lu, n2:%lu, le:%lu\n",i,j,k,n1,n2,le);
	tmp.re   = x[n2].re*Wn[n1].re - x[n2].im*Wn[n1].im;
	tmp.im   = x[n2].re*Wn[n1].im + x[n2].im*Wn[n1].re;
	x[n2].re = x[k].re - tmp.re;
	x[n2].im = x[k].im - tmp.im;
	x[k].re  = x[k].re + tmp.re;
	x[k].im  = x[k].im + tmp.im;
      }
    }
  }
}

/* Inverse FFT radix-2 algorithm using complex number representation */
/* Wn -- twiddle factor matrix (length N/2) */
void ifft_radix2_complex(complex_str X[], UWord16 v, complex_str Wn[])
{
  UWord16 i,j,k;
  UWord32 le,le2,n2,n1;
  complex_str tmp;

  /* Place the elements of the X array in bit-reversed order */
  bit_reverse_complex(X,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){ 
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){ 
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
	n2       = k + le2; 
	tmp.re   = X[n2].re*Wn[n1].re + X[n2].im*Wn[n1].im;
	tmp.im   = -X[n2].re*Wn[n1].im + X[n2].im*Wn[n1].re;
	X[n2].re = X[k].re - tmp.re;
	X[n2].im = X[k].im - tmp.im;
	X[k].re  = X[k].re + tmp.re;
	X[k].im  = X[k].im + tmp.im;
      }
    }
  }
  /* Scale output by N*/
  for(k=0; k<N; k++){
    X[k].re /= N;
    X[k].im /= N;
  }
}

/* Generate twiddle factor matrix of 2^(v-1) length */
void fft_twiddles_double(double Wn_r[], double Wn_i[], UWord16 v)
{
  UWord16 i;
  double theta;
    
  theta = 2*M_PI/N;
  /* Loop for each sub DFT */
  for(i=0; i<(1<<(v-1)); i++){ 
    /* Loop for each butterfly */
    Wn_r[i] = cos(i*theta);
    Wn_i[i] = -sin(i*theta);
  }
}

/* Sort matrix ib bit reverse order */
void bit_reverse_double(double xr[], double xi[], UWord16 v)
{
  UWord16 i,j,k;
  double tmp;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap re*/
      tmp  = xr[i];
      xr[i] = xr[j];
      xr[j] = tmp;
      /* swap ri*/
      tmp  = xi[i];
      xi[i] = xi[j];
      xi[j] = tmp;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

/* FFT radix-2 algorithm using double number representation */
/* Wn -- twiddle factor matrix (length N/2) */
void fft_radix2_double(double xr[], double xi[],UWord16 v, 
		       double Wn_r[], double Wn_i[])
{
  UWord16 i,j,k;
  UWord32 le,le2,n2,n1;
  double tmpr,tmpi;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_double(xr,xi,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){ 
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){ 
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
 	n2     = k + le2;
	tmpr   = xr[n2]*Wn_r[n1] - xi[n2]*Wn_i[n1];
	tmpi   = xr[n2]*Wn_i[n1] + xi[n2]*Wn_r[n1];
	xr[n2] = xr[k] - tmpr;
	xr[k]  = xr[k] + tmpr;
	xi[n2] = xi[k] - tmpi;
	xi[k]  = xi[k] + tmpi;
      }
    }
  }
}

/* Inverse FFT radix-2 algorithm using double number representation */
/* Wn -- twiddle factor matrix (length N/2) */
void ifft_radix2_double(double Xr[], double Xi[],UWord16 v, 
			double Wn_r[], double Wn_i[])
{
  UWord16 i,j,k;
  UWord32 le,le2,n2,n1;
  double tmpr,tmpi;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_double(Xr,Xi,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){ 
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){ 
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
 	n2     = k + le2;
	tmpr   = Xr[n2]*Wn_r[n1] + Xi[n2]*Wn_i[n1];
	tmpi   = -Xr[n2]*Wn_i[n1] + Xi[n2]*Wn_r[n1];
	Xr[n2] = Xr[k] - tmpr;
	Xr[k]  = Xr[k] + tmpr;
	Xi[n2] = Xi[k] - tmpi;
	Xi[k]  = Xi[k] + tmpi;
      }
    }
  }
  /* Scale output by N*/
  for(k=0; k<N; k++){
    Xr[k] /= N;
    Xi[k] /= N;
  }
}

/* Generate twiddle factor matrix of 2^(v-1) length */
void fft_twiddles_fixQ(Word32 Wn_r[], Word32 Wn_i[], UWord16 v, UWord8 Q)
{
  UWord16 i;
  double theta;
    
  theta = 2*M_PI/N;
  /* Loop for each sub DFT */
  for(i=0; i<(1<<(v-1)); i++){ 
    /* Loop for each butterfly */
    Wn_r[i] = FLTTOQ(cos(i*theta),Q);
    Wn_i[i] = FLTTOQ(-sin(i*theta),Q);
  }
}

/* Sort matrix ib bit reverse order */
void bit_reverse_fixQ(Word32 xr[], Word32 xi[], UWord16 v)
{
  UWord16 i,j,k;
  Word32 tmp;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap re*/
      tmp  = xr[i];
      xr[i] = xr[j];
      xr[j] = tmp;
      /* swap ri*/
      tmp  = xi[i];
      xi[i] = xi[j];
      xi[j] = tmp;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

/* FFT radix-2 algorithm using fixed Q number representation */
/* Wn -- twiddle factor matrix (length N/2) */
/* void fft_radix2_fixQ(Word32 xr[], Word32 xi[],UWord16 v, Word32 Wn_r[],  */
/* 		     Word32 Wn_i[], UWord8 *Q, UWord8 Qs) */
/* { */
/*   UWord32 le,le2,n2,n1; */
/*   Word32  tmpr,tmpi; */
/*   UWord16 i,j,k; */

/*   /\* Place the elements of the x array in bit-reversed order *\/ */
/*   bit_reverse_fixQ(xr,xi,v); */
/*   /\* Loop for each stage *\/ */
/*   for(i=1; i<=v; i++){  */
/*     le   = 1<<i; */
/*     le2  = le>>1; */
/*     /\* Loop for each sub DFT *\/ */
/*     for(j=0; j<le2; j++){  */
/*       /\* Loop for each butterfly *\/ */
/*       n1 = j<<(v-i); */
/*       for(k=j; k<N; k+=le){ */
/*  	n2     = k + le2; */
/* 	tmpr   = QSUB(QCONV(QMUL32(xr[n2],QCONV(Wn_r[n1],Qs,*Q),*Q),*Q,*Q-1), */
/* 		      QCONV(QMUL32(xi[n2],QCONV(Wn_i[n1],Qs,*Q),*Q),*Q,*Q-1)); */
/* 	tmpi   = QADD(QCONV(QMUL32(xr[n2],QCONV(Wn_i[n1],Qs,*Q),*Q),*Q,*Q-1), */
/* 		      QCONV(QMUL32(xi[n2],QCONV(Wn_r[n1],Qs,*Q),*Q),*Q,*Q-1)); */
/* 	xr[n2] = QSUB(QCONV(xr[k],*Q,*Q-1),tmpr); */
/* 	xr[k]  = QADD(QCONV(xr[k],*Q,*Q-1),tmpr); */
/* 	xi[n2] = QSUB(QCONV(xi[k],*Q,*Q-1),tmpi); */
/* 	xi[k]  = QADD(QCONV(xi[k],*Q,*Q-1),tmpi); */
/*       } */
/*     } */
/*     (*Q)--; */
/*   } */
/* } */

/* Inverse FFT radix-2 algorithm using fixed Q number representation */
/* Wn -- twiddle factor matrix (length N/2) */
/* void ifft_radix2_fixQ(Word32 Xr[], Word32 Xi[],UWord16 v, Word32 Wn_r[],  */
/* 		      Word32 Wn_i[], UWord8 *Q, UWord8 Qs) */
/* { */
/*   UWord32 le,le2,n2,n1; */
/*   Word32  tmpr,tmpi; */
/*   UWord16 i,j,k; */
/*   /\* Place the elements of the x array in bit-reversed order *\/ */
/*   bit_reverse_fixQ(Xr,Xi,v); */
/*   /\* Loop for each stage *\/ */
/*   for(i=1; i<=v; i++){  */
/*     le   = 1<<i; */
/*     le2  = le>>1; */
/*     /\* Loop for each sub DFT *\/ */
/*     for(j=0; j<le2; j++){  */
/*       /\* Loop for each butterfly *\/ */
/*       n1 = j<<(v-i); */
/*       for(k=j; k<N; k+=le){ */
/*  	n2     = k + le2; */
/* 	tmpr   = QADD(QCONV(QMUL32(Xr[n2],QCONV(Wn_r[n1],Qs,*Q),*Q),*Q,*Q-1), */
/* 		      QCONV(QMUL32(Xi[n2],QCONV(Wn_i[n1],Qs,*Q),*Q),*Q,*Q-1)); */
/* 	tmpi   = QADD(QCONV(-QMUL32(Xr[n2],QCONV(Wn_i[n1],Qs,*Q),*Q),*Q,*Q-1), */
/* 		      QCONV(QMUL32(Xi[n2],QCONV(Wn_r[n1],Qs,*Q),*Q),*Q,*Q-1)); */
/* 	Xr[n2] = QSUB(QCONV(Xr[k],*Q,*Q-1),tmpr); */
/* 	Xr[k]  = QADD(QCONV(Xr[k],*Q,*Q-1),tmpr); */
/* 	Xi[n2] = QSUB(QCONV(Xi[k],*Q,*Q-1),tmpi); */
/* 	Xi[k]  = QADD(QCONV(Xi[k],*Q,*Q-1),tmpi); */
/*       } */
/*     } */
/*     (*Q)--; */
/*   } */
/*   /\* Scale output by N = 2^v *\/ */
/*   for(k=0; k<N; k++){ */
/*     Xr[k] >>= v; */
/*     Xi[k] >>= v; */
/*   } */
/* } */

/* Sort matrix ib bit reverse order */
void bit_reverse_fixQ_scaled(Word32 xr[], Word32 xi[], UWord8 Qr[], UWord8 Qi[],
			     UWord16 v)
{
  UWord16 i,j,k;
  Word32 tmp;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap re */
      tmp  = xr[i];
      xr[i] = xr[j];
      xr[j] = tmp;
      /* swap ri */
      tmp  = xi[i];
      xi[i] = xi[j];
      xi[j] = tmp;
      /* swap Qr */
      tmp  = Qr[i];
      Qr[i] = Qr[j];
      Qr[j] = tmp;
      /* swap Qi */
      tmp  = Qi[i];
      Qi[i] = Qi[j];
      Qi[j] = tmp;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

/* FFT radix-2 algorithm using fixed Q scaled number representation */
/* Wn -- twiddle factor matrix (length N/2) */
void fft_radix2_fixQ_scaled(Word32 xr[], Word32 xi[],UWord16 v, Word32 Wn_r[], 
			    Word32 Wn_i[], UWord8 Qr[], UWord8 Qi[], UWord8 Qs)
{
  UWord32 le,le2,n2,n1;
  Word32  tmp1, tmp2, tmpr, tmpi;
  UWord16 i,j,k;
  UWord8  Qtmp1, Qtmp2, Qtmpr, Qtmpi;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_fixQ_scaled(xr,xi,Qr,Qi,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){ 
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){ 
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
 	n2     = k + le2;
	/* Obliczneie tmpr */
	tmp1  = QMUL32(xr[n2],QCONV(Wn_r[n1],Qs,Qr[n2]),Qr[n2]);
	Qtmp1 = Qr[n2];
	tmp2  = QMUL32(xi[n2],QCONV(Wn_i[n1],Qs,Qi[n2]),Qi[n2]);
	Qtmp2 = Qi[n2];
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,tmpr,Qtmp1,Qs);
	Qtmpr = Qtmp1;
 	/* Obliczneie tmpi */
	tmp1  = QMUL32(xr[n2],QCONV(Wn_i[n1],Qs,Qr[n2]),Qr[n2]);
	Qtmp1 = Qr[n2];
	tmp2  = QMUL32(xi[n2],QCONV(Wn_r[n1],Qs,Qi[n2]),Qi[n2]);
	Qtmp2 = Qi[n2];
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QADD32_PRECISE(tmp1,tmp2,tmpi,Qtmp1,Qs);
	Qtmpi = Qtmp1;
	/* Obliczenie xr[n2], xr[k] */
	tmp1  = xr[k];
	Qtmp1 = Qr[k];
	tmp2  = tmpr;
	Qtmp2 = Qtmpr;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,xr[n2],Qtmp1,Qs);
	Qr[n2] = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,xr[k],Qtmp2,Qs);
	Qr[k] = Qtmp2;
	/* Obliczenie xi[n2], xi[k] */
	tmp1  = xi[k];
	Qtmp1 = Qi[k];
	tmp2  = tmpi;
	Qtmp2 = Qtmpi;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,xi[n2],Qtmp1,Qs);
	Qi[n2] = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,xi[k],Qtmp2,Qs);
	Qi[k] = Qtmp2;
      }
    }
  }
}

/* IFFT radix-2 algorithm using fixed Q scaled number representation */
/* Wn -- twiddle factor matrix (length N/2) */
void ifft_radix2_fixQ_scaled(Word32 Xr[], Word32 Xi[],UWord16 v, Word32 Wn_r[], 
			    Word32 Wn_i[], UWord8 Qr[], UWord8 Qi[], UWord8 Qs)
{
  UWord32 le,le2,n2,n1;
  Word32  tmp1, tmp2, tmpr, tmpi;
  UWord16 i,j,k;
  UWord8  Qtmp1, Qtmp2, Qtmpr, Qtmpi;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_fixQ_scaled(Xr,Xi,Qr,Qi,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){ 
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){ 
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
	n2     = k + le2;
	/* Obliczneie tmpr */
	tmp1  = QMUL32(Xr[n2],QCONV(Wn_r[n1],Qs,Qr[n2]),Qr[n2]);
	Qtmp1 = Qr[n2];
	tmp2  = QMUL32(Xi[n2],QCONV(Wn_i[n1],Qs,Qi[n2]),Qi[n2]);
	Qtmp2 = Qi[n2];
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QADD32_PRECISE(tmp1,tmp2,tmpr,Qtmp1,Qs);
	Qtmpr = Qtmp1;
	/* Obliczneie tmpi */
	tmp1  = QMUL32(Xr[n2],QCONV(Wn_i[n1],Qs,Qr[n2]),Qr[n2]);
	Qtmp1 = Qr[n2];
	tmp2  = QMUL32(Xi[n2],QCONV(Wn_r[n1],Qs,Qi[n2]),Qi[n2]);
	Qtmp2 = Qi[n2];
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp2,tmp1,tmpi,Qtmp1,Qs);
	Qtmpi = Qtmp1;
	/* Obliczenie Xr[n2], Xr[k] */
	tmp1  = Xr[k];
	Qtmp1 = Qr[k];
	tmp2  = tmpr;
	Qtmp2 = Qtmpr;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,Xr[n2],Qtmp1,Qs);
	Qr[n2] = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,Xr[k],Qtmp2,Qs);
	Qr[k] = Qtmp2;
	/* Obliczenie Xi[n2], Xi[k] */
	tmp1  = Xi[k];
	Qtmp1 = Qi[k];
	tmp2  = tmpi;
	Qtmp2 = Qtmpi;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,Xi[n2],Qtmp1,Qs);
	Qi[n2] = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,Xi[k],Qtmp2,Qs);
	Qi[k] = Qtmp2;
      }
    }
  }
  /* Scale output by N = 2^v */
  for(k=0; k<N; k++){
    Xr[k] >>= v;
    Xi[k] >>= v;
  }
}

/* Generate twiddle factor matrix of 2^(v-1) length */
void fft_twiddles_fixQ_complex(complexQ_str Wn[], UWord8 Q, UWord16 v)
{
  UWord16 i;
  double theta;
    
  theta = 2*M_PI/N;
  /* Loop for each sub DFT */
  for(i=0; i<(1<<(v-1)); i++){ 
    /* Loop for each butterfly */
    Wn[i].re = FLTTOQ(cos(i*theta),Q);
    Wn[i].im = FLTTOQ(-sin(i*theta),Q);
  }
}


/* Sort matrix in bit reverse order */
void bit_reverse_fixQ_scaled_complex(complexQ_str x[], complexQFactor_str Q[], 
				    UWord16 v)
{
  complexQ_str       tmpx;
  UWord16            i,j,k;
  complexQFactor_str tmpq;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap x */
      tmpx  = x[i];
      x[i] = x[j];
      x[j] = tmpx;
      /* swap Q */
      tmpq  = Q[i];
      Q[i] = Q[j];
      Q[j] = tmpq;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

/* FFT radix-2 algorithm using fixed Q scaled number representation.
 * Input vector contain pair of real and imaginary part in the following 
 * order: (x1_r,x1_i), (x2_r,x2_i), ..., (xn_r,xn_i). Input wector length N. 
 * Wn is twiddle factor vector with order like input vector (length N/2) */
void fft_radix2_fixQ_scaled_complex(complexQ_str x[], complexQFactor_str Q[], 
				   complexQ_str Wn[], UWord8 Qs, UWord16 v)
{
  UWord32 le,le2,n2,n1;
  Word32  tmp1, tmp2, tmpr, tmpi;
  UWord16 i,j,k;
  UWord8  Qtmp1, Qtmp2, Qtmpr, Qtmpi;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_fixQ_scaled_complex(x,Q,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
 	n2     = k + le2;
	/* Obliczneie tmpr */
	tmp1  = QMUL32(x[n2].re,QCONV(Wn[n1].re,Qs,Q[n2].re),Q[n2].re);
	Qtmp1 = Q[n2].re;
	tmp2  = QMUL32(x[n2].im,QCONV(Wn[n1].im,Qs,Q[n2].im),Q[n2].im);
	Qtmp2 = Q[n2].im;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,tmpr,Qtmp1,Qs);
	Qtmpr = Qtmp1;
	/* Obliczneie tmpi */
	tmp1  = QMUL32(x[n2].re,QCONV(Wn[n1].im,Qs,Q[n2].re),Q[n2].re);
	Qtmp1 = Q[n2].re;
	tmp2  = QMUL32(x[n2].im,QCONV(Wn[n1].re,Qs,Q[n2].im),Q[n2].im);
	Qtmp2 = Q[n2].im;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QADD32_PRECISE(tmp1,tmp2,tmpi,Qtmp1,Qs);
	Qtmpi = Qtmp1;
	/* Obliczenie xr[n2], xr[k] */
	tmp1  = x[k].re;
	Qtmp1 = Q[k].re;
	tmp2  = tmpr;
	Qtmp2 = Qtmpr;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,x[n2].re,Qtmp1,Qs);
	Q[n2].re = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,x[k].re,Qtmp2,Qs);
	Q[k].re = Qtmp2;
	/* Obliczenie x[n2].im, xi[k] */
	tmp1  = x[k].im;
	Qtmp1 = Q[k].im;
	tmp2  = tmpi;
	Qtmp2 = Qtmpi;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,x[n2].im,Qtmp1,Qs);
	Q[n2].im = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,x[k].im,Qtmp2,Qs);
	Q[k].im = Qtmp2;
      }
    }
  }
}

/* IFFT radix-2 algorithm using fixed Q scaled number representation.
 * Input vector contain pair of real and imaginary part in the following 
 * order: (x1_r,x1_i), (x2_r,x2_i), ..., (xn_r,xn_i). Input wector length N. 
 * Wn is twiddle factor vector with order like input vector (length N/2) */
void ifft_radix2_fixQ_scaled_complex(complexQ_str X[], complexQFactor_str Q[], 
				     complexQ_str Wn[], UWord8 Qs, UWord16 v)
{
  UWord32 le,le2,n2,n1;
  Word32  tmp1, tmp2, tmpr, tmpi;
  UWord16 i,j,k;
  UWord8  Qtmp1, Qtmp2, Qtmpr, Qtmpi;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_fixQ_scaled_complex(X,Q,v);
  /* Loop for each stage */
  for(i=1; i<=v; i++){
    le   = 1<<i;
    le2  = le>>1;
    /* Loop for each sub DFT */
    for(j=0; j<le2; j++){
      /* Loop for each butterfly */
      n1 = j<<(v-i);
      for(k=j; k<N; k+=le){
 	n2     = k + le2;
	/* Obliczneie tmpr */
	tmp1  = QMUL32(X[n2].re,QCONV(Wn[n1].re,Qs,Q[n2].re),Q[n2].re);
	Qtmp1 = Q[n2].re;
	tmp2  = QMUL32(X[n2].im,QCONV(Wn[n1].im,Qs,Q[n2].im),Q[n2].im);
	Qtmp2 = Q[n2].im;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QADD32_PRECISE(tmp1,tmp2,tmpr,Qtmp1,Qs);
	Qtmpr = Qtmp1;
	/* Obliczneie tmpi */
	tmp1  = QMUL32(X[n2].re,QCONV(Wn[n1].im,Qs,Q[n2].re),Q[n2].re);
	Qtmp1 = Q[n2].re;
	tmp2  = QMUL32(X[n2].im,QCONV(Wn[n1].re,Qs,Q[n2].im),Q[n2].im);
	Qtmp2 = Q[n2].im;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp2,tmp1,tmpi,Qtmp1,Qs);
	Qtmpi = Qtmp1;
	/* Obliczenie xr[n2], xr[k] */
	tmp1  = X[k].re;
	Qtmp1 = Q[k].re;
	tmp2  = tmpr;
	Qtmp2 = Qtmpr;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,X[n2].re,Qtmp1,Qs);
	Q[n2].re = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,X[k].re,Qtmp2,Qs);
	Q[k].re = Qtmp2;
	/* Obliczenie X[n2].im, xi[k] */
	tmp1  = X[k].im;
	Qtmp1 = Q[k].im;
	tmp2  = tmpi;
	Qtmp2 = Qtmpi;
	QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
	QSUB32_PRECISE(tmp1,tmp2,X[n2].im,Qtmp1,Qs);
	Q[n2].im = Qtmp1;
	QADD32_PRECISE(tmp1,tmp2,X[k].im,Qtmp2,Qs);
	Q[k].im = Qtmp2;
      }
    }
  }
  /* Scale output by N = 2^v */
  for(k=0; k<N; k++){
    X[k].re >>= v;
    X[k].im >>= v;
  }
}

/*****************************************************************************
 * Fast Hartley Transform (FHT)
 *****************************************************************************/

/* Generate twiddle factor matrix of 2^(v-1)-2 length */
void fht_twiddles_double(double Wn[], UWord16 v)
{
  UWord16 i,j;
  double theta;
   
  theta = M_PI/(1<<(v-1));
  /* Loop for each sub DFT */
  for(i=0,j=1; j<(1<<(v-2)); i+=2,j++){ 
    /* Loop for each butterfly */
    Wn[i]   = cos(j*theta);
    Wn[i+1] = sin(j*theta);
  }
}

/* Sort matrix ib bit reverse order */
void bit_reverse_double_real(double x[], UWord16 v)
{
  UWord16 i,j,k;
  double tmp;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap */
      tmp  = x[i];
      x[i] = x[j];
      x[j] = tmp;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

void fht_radix2_double(double x[], double Wn[], UWord16 v)
{ 
  UWord16 i,j,k,l;
  UWord32 le,le2,le4,n3,n2,n1;
  double  tmp1,tmp2;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_double_real(x,v);
  /* Loop for each stage */
  for (i=1; i<=v; i++)
  {
    le   = 1<<i;
    le2  = le>>1;
    le4  = le2>>1;
    for (j=0; j<N; j+=le)
    {
      n2 = j + le2;
      tmp1 = x[j] - x[n2];
      x[j] += x[n2];
      x[n2] = tmp1;
      if(le4)
      {
	n3 = j + le4;
	n2 = n3 + le2;
	tmp1 = x[n3] - x[n2];
	x[n3] += x[n2];
	x[n2] = tmp1;
      }
      for(k=1,l=le2-1; k<l; k++,l--)
      {
	n1 = (k<<(v-i+1))-2;
	//printf("n1=%u\n",n1);
	n2    = j + le2 + l;
	n3    = j + le2 + k;
	tmp1  = x[n3];
	tmp2  = x[n2];
	x[n3] = tmp1*Wn[n1]   + tmp2*Wn[n1+1];
	x[n2] = tmp1*Wn[n1+1] - tmp2*Wn[n1];
	n3    = j + k;
	n2    = n3 + le2; 
	tmp1  = x[n3] - x[n2];
	x[n3] += x[n2];
	x[n2] = tmp1;
	n3    = j + l;
	n2    = n3 + le2;
	tmp1  = x[n3] - x[n2];
	x[n3] += x[n2];
	x[n2] = tmp1;
      }
    }
  }
}

void ifht_radix2_double(double x[], double Wn[], UWord16 v)
{
  UWord16 i;

  fht_radix2_double(x,Wn,v);
  for(i=0; i<N; i++){
    x[i] /= N;
  }
}

/* Generate twiddle factor matrix of 2^(v-1)-2 length */
void fht_twiddles_fixQ_scaled(Word32 Wn[], UWord8 Q, UWord16 v)
{
  UWord16 i,j;
  double theta;
   
  theta = M_PI/(1<<(v-1));
  /* Loop for each sub DFT */
  for(i=0,j=1; j<(1<<(v-2)); i+=2,j++){ 
    /* Loop for each butterfly */
    Wn[i]   = FLTTOQ(cos(j*theta),Q);
    Wn[i+1] = FLTTOQ(sin(j*theta),Q);
  }
}

/* Sort matrix ib bit reverse order */
void bit_reverse_fixQ_scaled_real(Word32 x[], UWord8 Q[], UWord16 v)
{
  UWord16 i,j,k;
  Word32  tmp;

  j=N/2;
  for (i=1; i < N-1; i++) {
    if (i < j) {
      /* swap x*/
      tmp  = x[i];
      x[i] = x[j];
      x[j] = tmp;
      /* swap Q */
      tmp  = Q[i];
      Q[i] = Q[j];
      Q[j] = tmp;
    }
    k = N/2;
    while(k <= j){
      j -= k;
      k /= 2;
    }
    j += k;
  }
}

void fht_radix2_fixQ_scaled(Word32 x[], UWord8 Q[], Word32 Wn[], UWord8 Qs, 
			    UWord16 v)
{ 
  UWord16 i,j,k,l;
  UWord32 le,le2,le4,n3,n2,n1;

  /* Place the elements of the x array in bit-reversed order */
  bit_reverse_fixQ_scaled_real(x,Q,v);
  /* Loop for each stage */
  for (i=1; i<=v; i++)
  {
    le   = 1<<i;
    le2  = le>>1;
    le4  = le2>>1;
    for (j=0; j<N; j+=le)
    {
      n2 = j + le2;
      FHT_ADDSUB_FIXQ(x[j],Q[j],x[n2],Q[n2],Qs);
      if(le4)
      {
	n3 = j + le4;
	n2 = n3 + le2;
	FHT_ADDSUB_FIXQ(x[n3],Q[n3],x[n2],Q[n2],Qs);
      }
      for(k=1,l=le2-1; k<l; k++,l--)
      {
	n1 = (k<<(v-i+1))-2;
	n2    = j + le2 + l;
	n3    = j + le2 + k;
	FHT_SHIFT_FIXQ(x[n3],Q[n3],x[n2],Q[n2],Wn[n1],Wn[n1+1],Qs);
	n3    = j + k;
	n2    = n3 + le2; 
	FHT_ADDSUB_FIXQ(x[n3],Q[n3],x[n2],Q[n2],Qs);
	n3    = j + l;
	n2    = n3 + le2;
	FHT_ADDSUB_FIXQ(x[n3],Q[n3],x[n2],Q[n2],Qs);
      }
    }
  }
}

void ifht_radix2_fixQ_scaled(Word32 X[], UWord8 Q[], Word32 Wn[], UWord8 Qs, 
			    UWord16 v)
{
  UWord16 i;

  fht_radix2_fixQ_scaled(X,Q,Wn,Qs,v);
  for(i=0; i<N; i++){
    X[i] >>= v;
  }
}

void fft_fht_double(double xr[], double xi[], double Wn[], UWord16 v)
{
  fht_radix2_double(xr,Wn,v);
  fht_radix2_double(xi,Wn,v);
  fht2fft_double(xr,xi,v,FALSE);
}

void ifft_fht_double(double Xr[], double Xi[], double Wn[], UWord16 v)
{
  UWord16 i;

  fht_radix2_double(Xr,Wn,v);
  fht_radix2_double(Xi,Wn,v);
  fht2fft_double(Xr,Xi,v,TRUE);
  for(i=0; i<N; i++){
    Xr[i] /= N;
    Xi[i] /= N;
  }
}

void fht2fft_double(double xr[], double xi[], UWord16 v, Boolean invers)
{
  double q, r, s, t;
  UWord16 i,j;

  if(invers)
  {
    for (i=1,j=N-1; i<j; i++,j--)
    {
      q = xr[i] + xr[j];
      r = xr[i] - xr[j];
      s = xi[i] + xi[j];
      t = xi[i] - xi[j];
      xr[j] = (q+t)*0.5;
      xr[i] = (q-t)*0.5;
      xi[i] = (s+r)*0.5;
      xi[j] = (s-r)*0.5;    
    }
  }
  else  
  {
    for (i=1,j=N-1; i<j; i++,j--)
    {
      q = xr[j] + xr[i];
      r = xr[j] - xr[i];
      s = xi[j] + xi[i];
      t = xi[j] - xi[i];
      xr[j] = (q+t)*0.5;
      xr[i] = (q-t)*0.5;
      xi[i] = (s+r)*0.5;
      xi[j] = (s-r)*0.5;
    }
  }
}

void fft_fht_fixQ_scaled(Word32 xr[], Word32 xi[], UWord8 Qr[], UWord8 Qi[], 
			 Word32 Wn[], UWord8 Qs, UWord16 v)
{
  fht_radix2_fixQ_scaled(xr,Qr,Wn,Qs,v);
  fht_radix2_fixQ_scaled(xi,Qi,Wn,Qs,v);
  fht2fft_fixQ_scaled(xr,xi,Qr,Qi,Qs,v,FALSE);
}

void ifft_fht_fixQ_scaled(Word32 Xr[], Word32 Xi[], UWord8 Qr[], UWord8 Qi[], 
			  Word32 Wn[], UWord8 Qs, UWord16 v)
{
  UWord16 i;

  fht_radix2_fixQ_scaled(Xr,Qr,Wn,Qs,v);
  fht_radix2_fixQ_scaled(Xi,Qi,Wn,Qs,v);
  fht2fft_fixQ_scaled(Xr,Xi,Qr,Qi,Qs,v,TRUE);
  for(i=0; i<N; i++){
    Xr[i] >>= v;
    Xi[i] >>= v;
  }
}

void fht2fft_fixQ_scaled(Word32 xr[], Word32 xi[], UWord8 Qr[], UWord8 Qi[], 
			 UWord8 Qs, UWord16 v, Boolean invers)
{
  Word32 _q, _r, _s, _t, tmp1, tmp2;
  UWord8 _Qq, _Qr, _Qs, _Qt, Qtmp1, Qtmp2;
  UWord16 i,j;

  if(invers)
  {
    for (i=1,j=N-1; i<j; i++,j--)
    {
      tmp1  = xr[i];
      Qtmp1 = Qr[i];
      tmp2  = xr[j];
      Qtmp2 = Qr[j];
      //q = xr[i] + xr[j];
      QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
      QADD32_PRECISE(tmp1,tmp2,_q,Qtmp1,Qs);
      _Qq = Qtmp1;
      //r = xr[i] - xr[j];
      QSUB32_PRECISE(tmp1,tmp2,_r,Qtmp2,Qs);
      _Qr = Qtmp2;
      tmp1  = xi[i];
      Qtmp1 = Qi[i];
      tmp2  = xi[j];
      Qtmp2 = Qi[j];
      //s = xi[i] + xi[j];
      QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
      QADD32_PRECISE(tmp1,tmp2,_s,Qtmp1,Qs);
      _Qs = Qtmp1;
      //t = xi[i] - xi[j];
      QSUB32_PRECISE(tmp1,tmp2,_t,Qtmp2,Qs);
      _Qt = Qtmp2;
      //xr[j] = (q+t)*0.5;
      QMAKEEQUAL(_q,_Qq,_t,_Qt);
      QADD32_PRECISE(_q,_t,xr[j],_Qq,Qs);
      xr[j] >>= 1;
      Qr[j] = _Qq;
      //xr[i] = (q-t)*0.5;
      QSUB32_PRECISE(_q,_t,xr[i],_Qt,Qs);
      xr[i] >>= 1;
      Qr[i] = _Qt;
      //xi[i] = (s+r)*0.5;
      QMAKEEQUAL(_s,_Qs,_r,_Qr);
      QADD32_PRECISE(_s,_r,xi[i],_Qs,Qs);
      xi[i] >>= 1;
      Qi[i] = _Qs;
      //xi[j] = (s-r)*0.5;    
      QSUB32_PRECISE(_s,_r,xi[j],_Qr,Qs);
      xi[j] >>= 1;
      Qi[j] = _Qr;
    }
  }
  else  
  {
    for (i=1,j=N-1; i<j; i++,j--)
    {
      tmp1  = xr[j];
      Qtmp1 = Qr[j];
      tmp2  = xr[i];
      Qtmp2 = Qr[i];
      //q = xr[j] + xr[i];
      QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
      QADD32_PRECISE(tmp1,tmp2,_q,Qtmp1,Qs);
      _Qq = Qtmp1;
      //r = xr[j] - xr[i];
      QSUB32_PRECISE(tmp1,tmp2,_r,Qtmp2,Qs);
      _Qr = Qtmp2;
      tmp1  = xi[j];
      Qtmp1 = Qi[j];
      tmp2  = xi[i];
      Qtmp2 = Qi[i];
      //s = xi[j] + xi[i];
      QMAKEEQUAL(tmp1,Qtmp1,tmp2,Qtmp2);
      QADD32_PRECISE(tmp1,tmp2,_s,Qtmp1,Qs);
      _Qs = Qtmp1;
      //t = xi[j] - xi[i];
      QSUB32_PRECISE(tmp1,tmp2,_t,Qtmp2,Qs);
      _Qt = Qtmp2;
      //xr[j] = (q+t)*0.5;
      QMAKEEQUAL(_q,_Qq,_t,_Qt);
      QADD32_PRECISE(_q,_t,xr[j],_Qq,Qs);
      xr[j] >>= 1;
      Qr[j] = _Qq;
      //xr[i] = (q-t)*0.5;
      QSUB32_PRECISE(_q,_t,xr[i],_Qt,Qs);
      xr[i] >>= 1;
      Qr[i] = _Qt;
      //xi[i] = (s+r)*0.5;
      QMAKEEQUAL(_s,_Qs,_r,_Qr);
      QADD32_PRECISE(_s,_r,xi[i],_Qs,Qs);
      xi[i] >>= 1;
      Qi[i] = _Qs;
      //xi[j] = (s-r)*0.5;    
      QSUB32_PRECISE(_s,_r,xi[j],_Qr,Qs);
      xi[j] >>= 1;
      Qi[j] = _Qr;
    }
  }
}

void fht_convolution_core(double x[], double y[], UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,ly1,ly2,sum,dif,scale;
  
  scale = 1.0/N;
  y[0] *= x[0]*scale;
  if ( le2>0 )
  {
    y[le2] *= x[le2]*scale;
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      sum  = lx1 + lx2;
      dif  = lx1 - lx2;
      ly1  = y[i];
      ly2  = y[j];
      y[i] = (ly1*sum + ly2*dif)*scale;
      y[j] = (ly2*sum - ly1*dif)*scale;
    }
  }
}

void fht_convolution(double x[], double y[], double Wn[], UWord16 v)
{
  fht_radix2_double(x,Wn,v);
  fht_radix2_double(y,Wn,v);
  fht_convolution_core(x,y,v);
  fht_radix2_double(y,Wn,v);
}

void fft_convolution_double(double xr[], double xi[], double yr[], double yi[], 
			    double Wn_r[], double Wn_i[], UWord16 v)
{
  UWord16 i;
  double tr,ti;

  fft_radix2_double(xr,xi,v,Wn_r,Wn_i);
  fft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  for(i=0;i<N;i++){
    tr = xr[i]*yr[i] - xi[i]*yi[i];
    ti = xi[i]*yr[i] + xr[i]*yi[i];
    yr[i] = tr;
    yi[i] = ti;
  }
  ifft_radix2_double(yr,yi,v,Wn_r,Wn_i);
}

void fht_autocorrelation_core(double x[], UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,scale;
  
  scale = 1.0/N;
  x[0] *= x[0]*scale;
  if ( le2>0 )
  {
    x[le2] *= x[le2]*scale;
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      x[i] = x[j] = (lx1*lx1 + lx2*lx2)*scale;
    }
  }
}

void fht_autocorrelation_double(double x[], double Wn[], UWord16 v)
{
  UWord16 i;
  double  tmp; 

  fht_radix2_double(x,Wn,v);
  fht_autocorrelation_core(x,v);
  fht_radix2_double(x,Wn,v);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp      = x[i];
    x[i]     = x[i+N/2];
    x[i+N/2] = tmp;
  }
}

void fht_autocorrelation_core_with_output(double x[], double AC[], UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,scale;
  
  scale = 1.0/N;
  AC[0] = x[0]*x[0]*scale;
  if ( le2>0 )
  {
    AC[le2] = x[le2]*x[le2]*scale;
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      AC[i] = AC[j] = (lx1*lx1 + lx2*lx2)*scale;
    }
  }
}

void fht_autocorrelation_double_with_output(double x[], double AC[], 
					    double Wn[], UWord16 v)
{
  UWord16 i;
  double  tmp; 

  fht_radix2_double(x,Wn,v);
  fht_autocorrelation_core_with_output(x,AC,v);
  ifht_radix2_double(x,Wn,v);
  fht_radix2_double(AC,Wn,v);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp       = AC[i];
    AC[i]     = AC[i+N/2];
    AC[i+N/2] = tmp;
  }
}

void fht_correlation_core(double x[], double y[], UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,ly1,ly2,sum,dif,scale;
  
  scale = 1.0/N;
  y[0] *= x[0]*scale;
  if ( le2>0 )
  {
    y[le2] *= x[le2]*scale;
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      sum  = lx1 + lx2;
      dif  = lx1 - lx2;
      ly1  = y[i];
      ly2  = y[j];
      y[i] = (ly1*dif + ly2*sum)*scale;
      y[j] = (ly1*sum - ly2*dif)*scale;
    }
  }
}

void fht_correlation_double(double x[], double y[], double Wn[], UWord16 v)
{
  UWord16 i;
  double  tmp; 

  fht_radix2_double(x,Wn,v);
  fht_radix2_double(y,Wn,v);
  fht_correlation_core(x,y,v);
  fht_radix2_double(y,Wn,v);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp      = y[i];
    y[i]     = y[i+N/2];
    y[i+N/2] = tmp;
  }
}

void fht_correlation_core_with_output(double x[], double y[], double CC[], 
				      UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,ly1,ly2,sum,dif,scale;
  
  scale = 1.0/N;
  CC[0] = x[0]*y[0]*scale;
  if (le2>0)
  {
    CC[le2] = x[le2]*y[le2]*scale;
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      sum  = lx1 + lx2;
      dif  = lx1 - lx2;
      ly1  = y[i];
      ly2  = y[j];
      CC[i] = (ly1*dif + ly2*sum)*scale;
      CC[j] = (ly1*sum - ly2*dif)*scale;
    }
  }
}

void fht_correlation_double_with_output(double x[], double y[], double CC[], 
					double Wn[], UWord16 v)
{
  UWord16 i;
  double  tmp; 

  fht_radix2_double(x,Wn,v);
  fht_radix2_double(y,Wn,v);
  fht_correlation_core_with_output(x,y,CC,v);
  ifht_radix2_double(x,Wn,v);
  ifht_radix2_double(y,Wn,v);
  fht_radix2_double(CC,Wn,v);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp       = CC[i];
    CC[i]     = CC[i+N/2];
    CC[i+N/2] = tmp;
  }
}

void fht_correlation_PHAT_core(double x[], double y[], UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,ly1,ly2,sum,dif,phat,scale;
  
  scale = 1.0/N;
  lx1   = x[0];
  ly1   = y[0];
  sum   = lx1*ly1*scale;
  phat  = sqrt(sum*sum);
  if(phat != 0){
    y[0] = sum/phat;
  }
  else{
    y[0] = 0;
  }
  if (le2>0)
  {
    lx1  = x[le2];
    ly1  = y[le2];
    sum  = lx1*ly1*scale;
    phat = sqrt(sum*sum);
    if(phat != 0){
      y[le2] = sum/phat;
    }
    else{
      y[le2] = 0;
    }
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      sum  = lx1 + lx2;
      dif  = lx1 - lx2;
      ly1  = y[i];
      ly2  = y[j];
      lx1 = (ly1*dif + ly2*sum)*scale;
      lx2 = (ly1*sum - ly2*dif)*scale;
      phat = sqrt((lx1*lx1 + lx2*lx2)/2);
      if(phat != 0){
	y[i] = lx1 / phat;
	y[j] = lx2 / phat;
      }
      else
      {
	y[i] = 0;
	y[j] = 0;
      }
    }
  }
}

void fht_correlation_PHAT_double(double x[], double y[], double Wn[], UWord16 v)
{
  UWord16 i;
  double  tmp; 

  fht_radix2_double(x,Wn,v);
  fht_radix2_double(y,Wn,v);
  fht_correlation_PHAT_core(x,y,v);
  fht_radix2_double(y,Wn,v);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp      = y[i];
    y[i]     = y[i+N/2];
    y[i+N/2] = tmp;
  }
}

void fht_correlation_PHAT_core_with_output(double x[], double y[], double CC[], 
					   UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,ly1,ly2,sum,dif,phat,scale;
  
  scale = 1.0/N;
  lx1   = x[0];
  ly1   = y[0];
  sum   = lx1*ly1*scale;
  phat  = sqrt(sum*sum);
  if(phat != 0){
    CC[0]  = sum/phat;
  }
  else{
    CC[0] = 0;
  }
  if ( le2>0 )
  {
    lx1   = x[le2];
    ly1   = y[le2];
    sum   = lx1*ly1*scale;
    phat  = sqrt(sum*sum);
    if(phat != 0){
      CC[le2]  = sum/phat;
    }
    else{
      CC[le2] = 0;
    }
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      sum  = lx1 + lx2;
      dif  = lx1 - lx2;
      ly1  = y[i];
      ly2  = y[j];
      lx1 = (ly1*dif + ly2*sum)*scale;
      lx2 = (ly1*sum - ly2*dif)*scale;
      phat = sqrt((lx1*lx1 + lx2*lx2)/2);
      if(phat != 0){
	CC[i] = lx1 / phat;
	CC[j] = lx2 / phat;
      }
      else
      {
	CC[i] = 0;
	CC[j] = 0;
      }
    }
  }
}

void fht_correlation_PHAT_double_with_output(double x[], double y[], 
					     double CC[], double Wn[], 
					     UWord16 v)
{
  UWord16 i;
  double  tmp; 

  fht_radix2_double(x,Wn,v);
  fht_radix2_double(y,Wn,v);
  fht_correlation_PHAT_core_with_output(x,y,CC,v);
  ifht_radix2_double(x,Wn,v);
  ifht_radix2_double(y,Wn,v);
  fht_radix2_double(CC,Wn,v);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp       = CC[i];
    CC[i]     = CC[i+N/2];
    CC[i+N/2] = tmp;
  }
}


void fft_correlation_double(double xr[], double xi[], double yr[], double yi[], 
			    double Wn_r[], double Wn_i[], UWord16 v)
{
  UWord16 i;
  double tr,ti,tmp;

  fft_radix2_double(xr,xi,v,Wn_r,Wn_i);
  fft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  for(i=0;i<N;i++){
    tr = xr[i]*yr[i] + xi[i]*yi[i];
    ti = xi[i]*yr[i] - xr[i]*yi[i];
    yr[i] = tr;
    yi[i] = ti;
  }
  ifft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp       = yr[i];
    yr[i]     = yr[i+N/2];
    yr[i+N/2] = tmp;
  }
}

void fft_correlation_PHAT_double(double xr[], double xi[], double yr[], 
				 double yi[], double Wn_r[], double Wn_i[], 
				 UWord16 v)
{
  UWord16 i;
  double tr,ti,phat,tmp;

  fft_radix2_double(xr,xi,v,Wn_r,Wn_i);
  fft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  for(i=0;i<N;i++){
    tr = xr[i]*yr[i] + xi[i]*yi[i];
    ti = xi[i]*yr[i] - xr[i]*yi[i];
    phat = sqrt(tr*tr + ti*ti);
    if(phat != 0){
      yr[i] = tr/phat;
      yi[i] = ti/phat;
    }
    else{
      yr[i] = 0;
      yi[i] = 0;
    }
  }
  ifft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp       = yr[i];
    yr[i]     = yr[i+N/2];
    yr[i+N/2] = tmp;
  }
}

void fft_correlation_SCOT_double(double xr[], double xi[], double yr[], 
				 double yi[], double Wn_r[], double Wn_i[], 
				 UWord16 v)
{
  UWord16 i;
  double tr,ti,a1,a2,scot,tmp;

  fft_radix2_double(xr,xi,v,Wn_r,Wn_i);
  fft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  for(i=0;i<N;i++){
    tr = xr[i]*yr[i] + xi[i]*yi[i];
    ti = xi[i]*yr[i] - xr[i]*yi[i];
    a1 = xr[i]*xr[i] + xi[i]*xi[i];
    a2 = yr[i]*yr[i] + yi[i]*yi[i];
    scot = sqrt(a1*a2);
    yr[i] = tr/scot;
    yi[i] = ti/scot;
  }
  ifft_radix2_double(yr,yi,v,Wn_r,Wn_i);
  /* Move negative lags before positive lags */
  for(i=0;i<N/2;i++){
    tmp       = yr[i];
    yr[i]     = yr[i+N/2];
    yr[i+N/2] = tmp;
  }
}

void hilbert_fft_fht_double(double x[], double Wn[], UWord16 v)
{
  UWord16 i;
  double  tmp;
  double xi[N];

  for(i=0;i<N;i++){
    xi[i]=0;
  }
  /* Convert x to frequency domain */
  fft_fht_double(x,xi,Wn,v);
  /* Zero frequency -- convolute X*0 */
  x[0] = xi[0] = 0;
  x[N/2] = xi[N/2] = 0;
  /* Positive frequency -- convolute X*(-j) */
  for(i=1;i<N/2;i++){
    tmp   =  x[i];
    x[i]  =  xi[i];
    xi[i] = -tmp;
  }
  /* Negative frequency -- convolute X*(j) */
  for(i=N/2+1;i<N;i++){
    tmp   =  x[i];
    x[i]  = -xi[i];
    xi[i] =  tmp;
  }
  /* Convert x to time domain */
  ifft_fht_double(x,xi,Wn,v);
}

void hilbert_core_fht_double(double x[], UWord16 v)
{
  UWord16 i,j;
  UWord32 le2 = N/2;
  double  lx1,lx2,ly1,ly2,sum,dif,scale;

  /* Convolute x and H(1/(pi*t))*/  
  scale = 1.0/N;
  x[0] = 0;
  if(le2 > 0)
  {
    x[le2] = 0;
    scale *= 0.5;
    for(i=1,j=N-1; i<j; i++,j--){
      lx1  = x[i];
      lx2  = x[j];
      sum  = lx1 + lx2;
      dif  = lx1 - lx2;
      ly1  = 1;
      ly2  = -1;
      x[i] = (ly1*sum + ly2*dif)*scale;
      x[j] = (ly2*sum - ly1*dif)*scale;
    }
  }
}

void hilbert_fht_double(double x[], double Wn[], UWord16 v)
{
  /* Convert x to frequency domain */
  fht_radix2_double(x,Wn,v);
  /* Compute hilbert transform */
  hilbert_core_fht_double(x,v);
  /* Convert x to time domain */
  fht_radix2_double(x,Wn,v);
}

void envelope_hilbert_fht_double(double x[], double E[], double Wn[], 
				 UWord16 v)
{
  UWord16 i;
  double  x1,x2,xp;

  /* FHT transform */
  fht_radix2_double(x,Wn,v);
  /* Remove constant coefficient from input signal */
  x1   = x[0];
  x2   = x[N/2];
  x[0] =  0;  //x[N/2] = 0;
  /* Compute constant coefficient amplitude */
  //xp   = sqrt(x1*x1 + x2*x2)/N;
  xp   = sqrt(x1*x1)/N;
  /* Copy inpit data*/
  for(i=0;i<N;i++){
    E[i]=x[i];
  }
  /* Compute hilbert transform */
  hilbert_core_fht_double(E,v);
  /* Convert E to time domain (without scaling by N) */
  fht_radix2_double(E,Wn,v);
  /* Convert x to time domain (without scaling by N) */
  fht_radix2_double(x,Wn,v);
  /* Compute envelope |x + iH(x)| and add constant coefficient*/
  for(i=0;i<N;i++){
    x1 = x[i]/N;
    x2 = E[i];
    E[i] = sqrt(x1*x1 + x2*x2) + xp;
    x[i] = x1 + xp;
  }
}

void power_spectrum_fft_fht_double(double xr[], double xi[], double P[], 
				   double Wn[], UWord16 v)
{
  UWord16 i;

  /* FHT transform */
  fft_fht_double(xr,xi,Wn,v);
  /* Power spectrum */
  for(i=0;i<N;i++){
    P[i] = sqrt(xr[i]*xr[i] + xi[i]*xi[i]);
  }
  /* IFHT transform */
  ifft_fht_double(xr,xi,Wn,v);

}

void power_spectrum_core_fht_double(double X[], double P[], UWord16 v)
{
  UWord16 i;

  /* Power spectrum */
  P[0]   = sqrt(X[0]*X[0]);
  P[N/2] = sqrt(X[N/2]*X[N/2]);
  for(i=1;i<N/2;i++){
    P[i] = P[N-i] = sqrt((X[i]*X[i] + X[N-i]*X[N-i])/2);
  }
}

void power_spectrum_fht_double(double x[], double P[], double Wn[], 
			       UWord16 v)
{
  /* FHT transform */
  fht_radix2_double(x,Wn,v);
  /* Compute power spectrum */
  power_spectrum_core_fht_double(x,P,v);
  /* IFHT transform */
  ifht_radix2_double(x,Wn,v);
}

void phase_fft_double(double xr[], double xi[], double phi[], double Wn_r[], 
		      double Wn_i[], UWord16 v)
{
  UWord16 i;

  /* FFT transform */
  fft_radix2_double(xr,xi,v,Wn_r,Wn_i);
  /* Power spectrum */
  for(i=0;i<N;i++){
    phi[i] = atan2(xi[i],xr[i]);
  }
  /* IFFT transform */
  ifft_radix2_double(xr,xi,v,Wn_r,Wn_i);
}


void phase_fft_fht_double(double xr[], double xi[], double phi[], 
			  double Wn[], UWord16 v)
{
  UWord16 i;

  /* FFT transform */
  fft_fht_double(xr,xi,Wn,v);
  /* Power spectrum */
  for(i=0;i<N;i++){
    phi[i] = atan2(xi[i],xr[i]);
  }
  /* IFFT transform */
  ifft_fht_double(xr,xi,Wn,v);

}

void phase_core_fht_double(double X[], double phi[], UWord16 v)
{
  UWord16 i,j;
  double phase;
  phi[0]   = atan2(0,X[0]);
  phi[N/2] = atan2(0,X[N/2]);
  /* Phase spectrum */
  for(i=1,j=N-1; i<j; i++,j--){
    phase = atan2(-(X[i]-X[j]),(X[i]+X[j]));
    phi[i] = phase;
    phi[j] = -phase;
  }
}

void phase_fht_double(double x[], double phi[], double Wn[], UWord16 v)
{
  /* FHT transform */
  fht_radix2_double(x,Wn,v);
  /* Compute power spectrum */
  phase_core_fht_double(x,phi,v);
  /* IFHT transform */
  ifht_radix2_double(x,Wn,v);
}

void filter_passband_core_fht_double(double X[], UWord16 v, UWord16 fi_min, 
				     UWord16 fi_max)
{
  UWord16 i;

  /* Remove f < fmin */
  for(i=1;i<fi_min;i++){
    X[i] = X[N-i] = 0;
  }
  /* Remove f > fmax */
  for(i=fi_max;i<N/2;i++){
    X[i] = X[N-i] = 0;
  }
}

void filter_passband_fht_double(double x[], double Wn[], UWord16 v, 
				UWord16 fi_min, UWord16 fi_max)
{
  /* FHT transform */
  fht_radix2_double(x,Wn,v);
  /* Filter */
  filter_passband_core_fht_double(x,v,fi_min,fi_max);
  /* IFHT transform */
  ifht_radix2_double(x,Wn,v);
}

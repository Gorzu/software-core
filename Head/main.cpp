#include <QtGui/QApplication>
#include <QMetaType>
#include "main_window.h"
#include "head_gl.h"
#include "utils.h"
#include "Driver/Driver.hh"

#include <iostream>
int main(int argc, char *argv[])
{

    QApplication a(argc, argv);

    MainWindow w;
    HeadGL * vis = new HeadGL("face.ASE", w.GLWidget);
    Driver * drv = new Driver;
    w.setVis(vis);
    w.setDrv(drv);
    w.setUpConnections();
    w.show();
    vis->start();
    return a.exec();
}

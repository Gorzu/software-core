/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QTextBrowser>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QGroupBox *faceVisBox;
    QWidget *GLwidget;
    QGroupBox *groupBox;
    QPushButton *lookDownBtn;
    QPushButton *swingRightBtn;
    QPushButton *swingLeftBtn;
    QPushButton *lookLeftBtn;
    QPushButton *lookRightBtn;
    QPushButton *lookUpBtn;
    QPushButton *lookCenterBtn;
    QPushButton *lookMiddleBtn;
    QPushButton *straightBtn;
    QGroupBox *groupBox_2;
    QPushButton *lookDownBtn_2;
    QPushButton *lookRightBtn_2;
    QPushButton *lookLeftBtn_2;
    QPushButton *lookMiddleBtn_2;
    QPushButton *lookUpBtn_2;
    QPushButton *lookCenterBtn_2;
    QGroupBox *groupBox_3;
    QLabel *xLabel;
    QLineEdit *xVal;
    QComboBox *facePartComboBox;
    QLabel *facePartLabel;
    QSlider *xSlider;
    QSlider *ySlider;
    QLabel *yLabel;
    QLineEdit *yVal;
    QSlider *zSlider;
    QLineEdit *zVal;
    QLabel *zLabel;
    QSlider *xRotSlider;
    QLineEdit *xRotVal;
    QLabel *xRotLabel;
    QLabel *zRotLabel;
    QLineEdit *yRotVal;
    QSlider *yRotSlider;
    QLabel *yRotLabel;
    QLineEdit *zRotVal;
    QSlider *zRotSlider;
    QGroupBox *groupBox_4;
    QPushButton *happyBtn;
    QPushButton *sadBtn;
    QPushButton *angryBtn;
    QPushButton *wickedBtn;
    QCheckBox *visEnable;
    QGroupBox *groupBox_5;
    QTextBrowser *console;
    QGroupBox *groupBox_6;
    QPushButton *LeftCamReset;
    QPushButton *RightCamReset;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1141, 574);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayoutWidget = new QWidget(centralWidget);
        horizontalLayoutWidget->setObjectName(QString::fromUtf8("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(20, 30, 1121, 541));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        faceVisBox = new QGroupBox(centralWidget);
        faceVisBox->setObjectName(QString::fromUtf8("faceVisBox"));
        faceVisBox->setGeometry(QRect(10, 10, 1119, 551));
        GLwidget = new QWidget(faceVisBox);
        GLwidget->setObjectName(QString::fromUtf8("GLwidget"));
        GLwidget->setGeometry(QRect(10, 20, 400, 400));
        groupBox = new QGroupBox(faceVisBox);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(820, 10, 291, 121));
        lookDownBtn = new QPushButton(groupBox);
        lookDownBtn->setObjectName(QString::fromUtf8("lookDownBtn"));
        lookDownBtn->setGeometry(QRect(200, 50, 75, 24));
        swingRightBtn = new QPushButton(groupBox);
        swingRightBtn->setObjectName(QString::fromUtf8("swingRightBtn"));
        swingRightBtn->setGeometry(QRect(20, 80, 75, 24));
        swingLeftBtn = new QPushButton(groupBox);
        swingLeftBtn->setObjectName(QString::fromUtf8("swingLeftBtn"));
        swingLeftBtn->setGeometry(QRect(200, 80, 75, 24));
        lookLeftBtn = new QPushButton(groupBox);
        lookLeftBtn->setObjectName(QString::fromUtf8("lookLeftBtn"));
        lookLeftBtn->setGeometry(QRect(200, 20, 75, 24));
        lookRightBtn = new QPushButton(groupBox);
        lookRightBtn->setObjectName(QString::fromUtf8("lookRightBtn"));
        lookRightBtn->setGeometry(QRect(20, 20, 75, 24));
        lookUpBtn = new QPushButton(groupBox);
        lookUpBtn->setObjectName(QString::fromUtf8("lookUpBtn"));
        lookUpBtn->setGeometry(QRect(20, 50, 75, 24));
        lookCenterBtn = new QPushButton(groupBox);
        lookCenterBtn->setObjectName(QString::fromUtf8("lookCenterBtn"));
        lookCenterBtn->setGeometry(QRect(110, 20, 75, 24));
        lookMiddleBtn = new QPushButton(groupBox);
        lookMiddleBtn->setObjectName(QString::fromUtf8("lookMiddleBtn"));
        lookMiddleBtn->setGeometry(QRect(110, 50, 75, 24));
        straightBtn = new QPushButton(groupBox);
        straightBtn->setObjectName(QString::fromUtf8("straightBtn"));
        straightBtn->setGeometry(QRect(110, 80, 75, 24));
        groupBox_2 = new QGroupBox(faceVisBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(820, 160, 291, 91));
        lookDownBtn_2 = new QPushButton(groupBox_2);
        lookDownBtn_2->setObjectName(QString::fromUtf8("lookDownBtn_2"));
        lookDownBtn_2->setGeometry(QRect(200, 50, 75, 24));
        lookRightBtn_2 = new QPushButton(groupBox_2);
        lookRightBtn_2->setObjectName(QString::fromUtf8("lookRightBtn_2"));
        lookRightBtn_2->setGeometry(QRect(20, 20, 75, 24));
        lookLeftBtn_2 = new QPushButton(groupBox_2);
        lookLeftBtn_2->setObjectName(QString::fromUtf8("lookLeftBtn_2"));
        lookLeftBtn_2->setGeometry(QRect(200, 20, 75, 24));
        lookMiddleBtn_2 = new QPushButton(groupBox_2);
        lookMiddleBtn_2->setObjectName(QString::fromUtf8("lookMiddleBtn_2"));
        lookMiddleBtn_2->setGeometry(QRect(110, 50, 75, 24));
        lookUpBtn_2 = new QPushButton(groupBox_2);
        lookUpBtn_2->setObjectName(QString::fromUtf8("lookUpBtn_2"));
        lookUpBtn_2->setGeometry(QRect(20, 50, 75, 24));
        lookCenterBtn_2 = new QPushButton(groupBox_2);
        lookCenterBtn_2->setObjectName(QString::fromUtf8("lookCenterBtn_2"));
        lookCenterBtn_2->setGeometry(QRect(110, 20, 75, 24));
        groupBox_3 = new QGroupBox(faceVisBox);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setGeometry(QRect(420, 10, 381, 241));
        xLabel = new QLabel(groupBox_3);
        xLabel->setObjectName(QString::fromUtf8("xLabel"));
        xLabel->setGeometry(QRect(10, 60, 20, 20));
        xVal = new QLineEdit(groupBox_3);
        xVal->setObjectName(QString::fromUtf8("xVal"));
        xVal->setGeometry(QRect(40, 60, 51, 20));
        facePartComboBox = new QComboBox(groupBox_3);
        facePartComboBox->setObjectName(QString::fromUtf8("facePartComboBox"));
        facePartComboBox->setGeometry(QRect(110, 20, 161, 21));
        facePartLabel = new QLabel(groupBox_3);
        facePartLabel->setObjectName(QString::fromUtf8("facePartLabel"));
        facePartLabel->setGeometry(QRect(20, 20, 81, 20));
        xSlider = new QSlider(groupBox_3);
        xSlider->setObjectName(QString::fromUtf8("xSlider"));
        xSlider->setGeometry(QRect(100, 60, 261, 16));
        xSlider->setOrientation(Qt::Horizontal);
        ySlider = new QSlider(groupBox_3);
        ySlider->setObjectName(QString::fromUtf8("ySlider"));
        ySlider->setGeometry(QRect(100, 90, 261, 16));
        ySlider->setOrientation(Qt::Horizontal);
        yLabel = new QLabel(groupBox_3);
        yLabel->setObjectName(QString::fromUtf8("yLabel"));
        yLabel->setGeometry(QRect(10, 90, 20, 20));
        yVal = new QLineEdit(groupBox_3);
        yVal->setObjectName(QString::fromUtf8("yVal"));
        yVal->setGeometry(QRect(40, 90, 51, 20));
        zSlider = new QSlider(groupBox_3);
        zSlider->setObjectName(QString::fromUtf8("zSlider"));
        zSlider->setGeometry(QRect(100, 120, 261, 16));
        zSlider->setOrientation(Qt::Horizontal);
        zVal = new QLineEdit(groupBox_3);
        zVal->setObjectName(QString::fromUtf8("zVal"));
        zVal->setGeometry(QRect(40, 120, 51, 20));
        zLabel = new QLabel(groupBox_3);
        zLabel->setObjectName(QString::fromUtf8("zLabel"));
        zLabel->setGeometry(QRect(10, 120, 10, 20));
        xRotSlider = new QSlider(groupBox_3);
        xRotSlider->setObjectName(QString::fromUtf8("xRotSlider"));
        xRotSlider->setGeometry(QRect(100, 150, 261, 16));
        xRotSlider->setOrientation(Qt::Horizontal);
        xRotVal = new QLineEdit(groupBox_3);
        xRotVal->setObjectName(QString::fromUtf8("xRotVal"));
        xRotVal->setGeometry(QRect(40, 150, 51, 21));
        xRotLabel = new QLabel(groupBox_3);
        xRotLabel->setObjectName(QString::fromUtf8("xRotLabel"));
        xRotLabel->setGeometry(QRect(10, 150, 31, 20));
        zRotLabel = new QLabel(groupBox_3);
        zRotLabel->setObjectName(QString::fromUtf8("zRotLabel"));
        zRotLabel->setGeometry(QRect(10, 180, 31, 20));
        yRotVal = new QLineEdit(groupBox_3);
        yRotVal->setObjectName(QString::fromUtf8("yRotVal"));
        yRotVal->setGeometry(QRect(40, 180, 51, 20));
        yRotSlider = new QSlider(groupBox_3);
        yRotSlider->setObjectName(QString::fromUtf8("yRotSlider"));
        yRotSlider->setGeometry(QRect(100, 180, 261, 16));
        yRotSlider->setOrientation(Qt::Horizontal);
        yRotLabel = new QLabel(groupBox_3);
        yRotLabel->setObjectName(QString::fromUtf8("yRotLabel"));
        yRotLabel->setGeometry(QRect(10, 210, 31, 20));
        zRotVal = new QLineEdit(groupBox_3);
        zRotVal->setObjectName(QString::fromUtf8("zRotVal"));
        zRotVal->setGeometry(QRect(40, 210, 51, 20));
        zRotSlider = new QSlider(groupBox_3);
        zRotSlider->setObjectName(QString::fromUtf8("zRotSlider"));
        zRotSlider->setGeometry(QRect(100, 210, 261, 16));
        zRotSlider->setOrientation(Qt::Horizontal);
        groupBox_4 = new QGroupBox(faceVisBox);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        groupBox_4->setGeometry(QRect(420, 260, 381, 61));
        happyBtn = new QPushButton(groupBox_4);
        happyBtn->setObjectName(QString::fromUtf8("happyBtn"));
        happyBtn->setGeometry(QRect(20, 20, 75, 24));
        sadBtn = new QPushButton(groupBox_4);
        sadBtn->setObjectName(QString::fromUtf8("sadBtn"));
        sadBtn->setGeometry(QRect(110, 20, 75, 24));
        angryBtn = new QPushButton(groupBox_4);
        angryBtn->setObjectName(QString::fromUtf8("angryBtn"));
        angryBtn->setGeometry(QRect(200, 20, 75, 24));
        wickedBtn = new QPushButton(groupBox_4);
        wickedBtn->setObjectName(QString::fromUtf8("wickedBtn"));
        wickedBtn->setGeometry(QRect(290, 20, 75, 24));
        visEnable = new QCheckBox(faceVisBox);
        visEnable->setObjectName(QString::fromUtf8("visEnable"));
        visEnable->setEnabled(true);
        visEnable->setGeometry(QRect(830, 280, 161, 19));
        visEnable->setChecked(true);
        groupBox_5 = new QGroupBox(faceVisBox);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        groupBox_5->setGeometry(QRect(420, 330, 691, 91));
        console = new QTextBrowser(groupBox_5);
        console->setObjectName(QString::fromUtf8("console"));
        console->setGeometry(QRect(10, 20, 671, 61));
        groupBox_6 = new QGroupBox(faceVisBox);
        groupBox_6->setObjectName(QString::fromUtf8("groupBox_6"));
        groupBox_6->setGeometry(QRect(10, 430, 1101, 111));
        LeftCamReset = new QPushButton(groupBox_6);
        LeftCamReset->setObjectName(QString::fromUtf8("LeftCamReset"));
        LeftCamReset->setGeometry(QRect(10, 30, 131, 26));
        RightCamReset = new QPushButton(groupBox_6);
        RightCamReset->setObjectName(QString::fromUtf8("RightCamReset"));
        RightCamReset->setGeometry(QRect(10, 60, 131, 26));
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        faceVisBox->setTitle(QString());
        groupBox->setTitle(QApplication::translate("MainWindow", "Head Movement", 0, QApplication::UnicodeUTF8));
        lookDownBtn->setText(QApplication::translate("MainWindow", "Look Down", 0, QApplication::UnicodeUTF8));
        swingRightBtn->setText(QApplication::translate("MainWindow", "Swing Right", 0, QApplication::UnicodeUTF8));
        swingLeftBtn->setText(QApplication::translate("MainWindow", "Swing Left", 0, QApplication::UnicodeUTF8));
        lookLeftBtn->setText(QApplication::translate("MainWindow", "Look Left", 0, QApplication::UnicodeUTF8));
        lookRightBtn->setText(QApplication::translate("MainWindow", "Look Right", 0, QApplication::UnicodeUTF8));
        lookUpBtn->setText(QApplication::translate("MainWindow", "Look Up", 0, QApplication::UnicodeUTF8));
        lookCenterBtn->setText(QApplication::translate("MainWindow", "Look Center", 0, QApplication::UnicodeUTF8));
        lookMiddleBtn->setText(QApplication::translate("MainWindow", "Look Middle", 0, QApplication::UnicodeUTF8));
        straightBtn->setText(QApplication::translate("MainWindow", "Straight", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("MainWindow", "Eyes Movement", 0, QApplication::UnicodeUTF8));
        lookDownBtn_2->setText(QApplication::translate("MainWindow", "Look Down", 0, QApplication::UnicodeUTF8));
        lookRightBtn_2->setText(QApplication::translate("MainWindow", "Look Right", 0, QApplication::UnicodeUTF8));
        lookLeftBtn_2->setText(QApplication::translate("MainWindow", "Look Left", 0, QApplication::UnicodeUTF8));
        lookMiddleBtn_2->setText(QApplication::translate("MainWindow", "Look Middle", 0, QApplication::UnicodeUTF8));
        lookUpBtn_2->setText(QApplication::translate("MainWindow", "Look Up", 0, QApplication::UnicodeUTF8));
        lookCenterBtn_2->setText(QApplication::translate("MainWindow", "Look Center", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("MainWindow", "Anchor Coordinates", 0, QApplication::UnicodeUTF8));
        xLabel->setText(QApplication::translate("MainWindow", "x:", 0, QApplication::UnicodeUTF8));
        facePartLabel->setText(QApplication::translate("MainWindow", "Face part:", 0, QApplication::UnicodeUTF8));
        yLabel->setText(QApplication::translate("MainWindow", "y:", 0, QApplication::UnicodeUTF8));
        zLabel->setText(QApplication::translate("MainWindow", "z:", 0, QApplication::UnicodeUTF8));
        xRotLabel->setText(QApplication::translate("MainWindow", "xRot:", 0, QApplication::UnicodeUTF8));
        zRotLabel->setText(QApplication::translate("MainWindow", "yRot:", 0, QApplication::UnicodeUTF8));
        yRotLabel->setText(QApplication::translate("MainWindow", "zRot:", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("MainWindow", "Programmer's Emotions", 0, QApplication::UnicodeUTF8));
        happyBtn->setText(QApplication::translate("MainWindow", "Happy", 0, QApplication::UnicodeUTF8));
        sadBtn->setText(QApplication::translate("MainWindow", "Sad", 0, QApplication::UnicodeUTF8));
        angryBtn->setText(QApplication::translate("MainWindow", "Angry", 0, QApplication::UnicodeUTF8));
        wickedBtn->setText(QApplication::translate("MainWindow", "Wicked", 0, QApplication::UnicodeUTF8));
        visEnable->setText(QApplication::translate("MainWindow", "Enable Visualisation", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("MainWindow", "Log", 0, QApplication::UnicodeUTF8));
        groupBox_6->setTitle(QApplication::translate("MainWindow", "Driver Module test options", 0, QApplication::UnicodeUTF8));
        LeftCamReset->setText(QApplication::translate("MainWindow", "Left Camera Reset", 0, QApplication::UnicodeUTF8));
        RightCamReset->setText(QApplication::translate("MainWindow", "Right Camera Reset", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H

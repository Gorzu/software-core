/******************************************************************************
*
* fft.h
*
* Fast Fourier Transformation module.
* 
* Autor: Marisz Janiak
*
* Wroclaw 2008
*
******************************************************************************/

#ifndef _MY_FFT_
#define _MY_FFT_

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define FFT_INPUTBIT 10                 /* number of significant bits */
#define FFT_QTWIDDLE 16                 /* optimal Q for twiddle factors */
#define FFT_QINPUT   (31-FFT_INPUTBIT)  /* */

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL ((void *)0)  /* or for C++ #define NULL 0 */
#endif

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

#ifndef _UWORD32_
#define _UWORD32_
typedef unsigned long  UWord32;
#endif

#ifndef _WORD32_
#define _WORD32_
typedef long           Word32;
#endif

#ifndef _UWORD16_
#define _UWORD16_
typedef unsigned short UWord16;
#endif

#ifndef _WORD16_
#define _WORD16_
typedef short          Word16;
#endif

#ifndef _UWORD8_
#define _UWORD8_
typedef unsigned char  UWord8;
#endif

#ifndef _WORD8_
#define _WORD8_
typedef signed char    Word8;
#endif

#ifndef _BOOLEAN_
#define _BOOLEAN_
typedef unsigned char  Boolean;
#endif

typedef struct{
  double re;
  double im;
}complex_str;

typedef struct{
  Word32 re;
  Word32 im;
}complexQ_str;

typedef struct{
  UWord8 re;
  UWord8 im;
}complexQFactor_str;


/*****************************************************************************
 * Macros 
 *****************************************************************************/

#ifdef UNUSED
#elif defined(__GNUC__)
# define UNUSED(x) UNUSED_ ## x __attribute__((unused))
#else
# define UNUSED(x) x
#endif

#define FHT_ADDSUB_FIXQ(a,qa,b,qb,qs){		\
    Word32  _t1,_t2, _t3;			\
    UWord8  _Qt1, _Qt2, _Qt3;			\
    						\
    _t1  = a;					\
    _Qt1 = qa;					\
    _t2  = b;					\
    _Qt2 = qb;					\
    QMAKEEQUAL(_t1,_Qt1,_t2,_Qt2);		\
    QSUB32_PRECISE(_t1,_t2,_t3,_Qt1,qs);	\
    _Qt3 = _Qt1;				\
    QADD32_PRECISE(_t1,_t2,a,_Qt2,qs);		\
    qa = _Qt2;					\
    b  = _t3;					\
    qb = _Qt3;					\
  }						\

#define FHT_SHIFT_FIXQ(a,qa,b,qb,Wc,Ws,qs){	\
    Word32  _t1,_t2, _t3,_t4;			\
    UWord8  _Qt1, _Qt2, _Qt3, _Qt4;		\
						\
    _t1  = a;					\
    _Qt1 = qa;					\
    _t2  = b;					\
    _Qt2 = qb;					\
    _t3  = QMUL32(_t1,QCONV(Wc,qs,_Qt1),_Qt1);	\
    _Qt3 = _Qt1;				\
    _t4  = QMUL32(_t2,QCONV(Ws,qs,_Qt2),_Qt2);	\
    _Qt4 = _Qt2;				\
    QMAKEEQUAL(_t3,_Qt3,_t4,_Qt4);		\
    QADD32_PRECISE(_t3,_t4,a,_Qt3,qs);		\
    qa   = _Qt3;				\
    _t3  = QMUL32(_t1,QCONV(Ws,qs,_Qt1),_Qt1);	\
    _Qt3 = _Qt1;				\
    _t4  = QMUL32(_t2,QCONV(Wc,qs,_Qt2),_Qt2);	\
    _Qt4 = _Qt2;				\
    QMAKEEQUAL(_t3,_Qt3,_t4,_Qt4);		\
    QSUB32_PRECISE(_t3,_t4,b,_Qt3,qs);		\
    qb   = _Qt3;				\
  }						\

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

/*****************************************************************************
 * Function prototypes
 *****************************************************************************/

/* Fast Fourier Transform (FFT) -- radix-2 algorithm */
void fft_twiddles_complex(complex_str Wn[], UWord16 v);
void bit_reverse_complex(complex_str x[], UWord16 v);
void fft_radix2_complex(complex_str x[], UWord16 v, complex_str Wn[]);
void ifft_radix2_complex(complex_str X[], UWord16 v, complex_str Wn[]);

void fft_twiddles_double(double Wn_r[], double Wn_i[], UWord16 v);
void bit_reverse_double(double xr[], double xi[], UWord16 v);
void fft_radix2_double(double xr[], double xi[],UWord16 v, double Wn_r[], 
		       double Wn_i[]);
void ifft_radix2_double(double Xr[], double Xi[],UWord16 v, double Wn_r[], 
			double Wn_i[]);

void fft_twiddles_fixQ(Word32 Wn_r[], Word32 Wn_i[], UWord16 v, UWord8 Q);
void bit_reverse_fixQ(Word32 xr[], Word32 xi[], UWord16 v);
/* void fft_radix2_fixQ(Word32 xr[], Word32 xi[],UWord16 v, Word32 Wn_r[],  */
/* 		     Word32 Wn_i[], UWord8 *Q, UWord8 Qs); */
/* void ifft_radix2_fixQ(Word32 xr[], Word32 xi[],UWord16 v, Word32 Wn_r[],  */
/* 		      Word32 Wn_i[], UWord8 *Q, UWord8 Qs); */

void bit_reverse_fixQ_scaled(Word32 xr[], Word32 xi[], UWord8 Qr[], UWord8 Qi[],
			     UWord16 v);
void fft_radix2_fixQ_scaled(Word32 xr[], Word32 xi[],UWord16 v, Word32 Wn_r[], 
			    Word32 Wn_i[], UWord8 Qr[], UWord8 Qi[], UWord8 Qs);
void ifft_radix2_fixQ_scaled(Word32 Xr[], Word32 Xi[],UWord16 v, Word32 Wn_r[], 
			     Word32 Wn_i[], UWord8 Qr[], UWord8 Qi[], 
			     UWord8 Qs);

void fft_twiddles_fixQ_complex(complexQ_str Wn[], UWord8 Q, UWord16 v);
void bit_reverse_fixQ_scaled_complex(complexQ_str x[], complexQFactor_str Q[], 
				    UWord16 v);
void fft_radix2_fixQ_scaled_complex(complexQ_str x[], complexQFactor_str Q[], 
				   complexQ_str Wn[], UWord8 Qs, UWord16 v);
void ifft_radix2_fixQ_scaled_complex(complexQ_str X[], complexQFactor_str Q[], 
				     complexQ_str Wn[], UWord8 Qs, UWord16 v);


/* Fast Hartley Transform (FHT) */
void fht_twiddles_double(double Wn[], UWord16 v);
void bit_reverse_double_real(double x[], UWord16 v);
void fht_radix2_double(double x[], double Wn[], UWord16 v);
void ifht_radix2_double(double x[], double Wn[], UWord16 v);

void fht_twiddles_fixQ_scaled(Word32 Wn[], UWord8 Q, UWord16 v);
void bit_reverse_fixQ_scaled_real(Word32 x[], UWord8 Q[], UWord16 v);
void fht_radix2_fixQ_scaled(Word32 x[], UWord8 Q[], Word32 Wn[], UWord8 Qs, 
			    UWord16 v);
void ifht_radix2_fixQ_scaled(Word32 X[], UWord8 Q[], Word32 Wn[], UWord8 Qs, 
			     UWord16 v);

/* Fast Fourier Transform (FFT) -- FHT algorithm */
void fft_fht_double(double xr[], double xi[], double Wn[], UWord16 v);
void ifft_fht_double(double Xr[], double Xi[], double Wn[], UWord16 v);
void fht2fft_double(double xr[], double xi[], UWord16 v, Boolean invers);

void fft_fht_fixQ_scaled(Word32 xr[], Word32 xi[], UWord8 Qr[], UWord8 Qi[], 
			 Word32 Wn[], UWord8 Qs, UWord16 v);
void ifft_fht_fixQ_scaled(Word32 Xr[], Word32 Xi[], UWord8 Qr[], UWord8 Qi[], 
			  Word32 Wn[], UWord8 Qs, UWord16 v);
void fht2fft_fixQ_scaled(Word32 xr[], Word32 xi[], UWord8 Qr[], UWord8 Qi[], 
			 UWord8 Qs, UWord16 v, Boolean invers);

/* Convolution */
void fht_convolution_core(double x[], double y[], UWord16 v);
void fht_convolution(double x[], double y[], double Wn[], UWord16 v);
void fft_convolution_double(double xr[], double xi[], double yr[], double yi[], 
			    double Wn_r[], double Wn_i[], UWord16 v);

/* Correlation */
void fht_correlation_core(double x[], double y[], UWord16 v);
void fht_correlation_double(double x[], double y[], double Wn[], UWord16 v);
void fht_correlation_core_with_output(double x[], double y[], double CC[], 
				      UWord16 v);
void fht_correlation_double_with_output(double x[], double y[], double CC[], 
					double Wn[], UWord16 v);
void fht_correlation_PHAT_core(double x[], double y[], UWord16 v);
void fht_correlation_PHAT_double(double x[], double y[], double Wn[], 
				 UWord16 v);
void fht_correlation_PHAT_core_with_output(double x[], double y[], double CC[], 
					   UWord16 v);
void fht_correlation_PHAT_double_with_output(double x[], double y[], 
					     double CC[], double Wn[], 
					     UWord16 v);

void fft_correlation_double(double xr[], double xi[], double yr[], double yi[], 
			    double Wn_r[], double Wn_i[], UWord16 v);
void fft_correlation_PHAT_double(double xr[], double xi[], double yr[], 
				 double yi[], double Wn_r[], double Wn_i[], 
				 UWord16 v);
void fft_correlation_SCOT_double(double xr[], double xi[], double yr[], 
				 double yi[], double Wn_r[], double Wn_i[], 
				 UWord16 v);

/* Autocorrelation */
void fht_autocorrelation_core(double x[], UWord16 v);
void fht_autocorrelation_double(double x[], double Wn[], UWord16 v);
void fht_autocorrelation_core_with_output(double x[], double AC[], UWord16 v);
void fht_autocorrelation_double_with_output(double x[], double AC[], 
					    double Wn[], UWord16 v);

/* Hilbert transform */
void hilbert_fft_fht_double(double x[], double Wn[], UWord16 v);

void hilbert_core_fht_double(double x[], UWord16 v);
void hilbert_fht_double(double x[], double Wn[], UWord16 v);
void envelope_hilbert_fht_double(double x[], double E[], double Wn[], 
				 UWord16 v);

/* Signal power spectrum */
void power_spectrum_fft_fht_double(double xr[], double xi[], double P[], 
				   double Wn[], UWord16 v);

void power_spectrum_core_fht_double(double X[], double P[], UWord16 v);
void power_spectrum_fht_double(double x[], double P[], double Wn[], 
			       UWord16 v);


/* Signal phase */
void phase_fft_double(double xr[], double xi[], double phi[], double Wn_r[], 
		      double Wn_i[], UWord16 v);
void phase_fft_fht_double(double xr[], double xi[], double phi[], 
			  double Wn[], UWord16 v);

void phase_core_fht_double(double X[], double phi[], UWord16 v);
void phase_fht_double(double x[], double phi[], double Wn[], UWord16 v);

/* Filters */
void filter_passband_core_fht_double(double X[], UWord16 v, UWord16 fi_min, 
				     UWord16 fi_max);
void filter_passband_fht_double(double x[], double Wn[], UWord16 v, 
				UWord16 fi_min, UWord16 fi_max);
  

#ifdef __cplusplus
}
#endif

#endif /*End _MY_FFT_*/

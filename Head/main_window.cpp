#include "main_window.h"
#include "ui_mainwindow.h"
#include "head_gl_utils.h"



MainWindow::MainWindow():QMainWindow(0)
{
    posFactor = .2;

    ui = new Ui::MainWindow;
/*
    mainLayout = new QHBoxLayout;
    mainLayout->addWidget(vis);
    setLayout(mainLayout);*/

    ui->setupUi(this);
    setWindowTitle(tr("HeadTest"));

    GLWidget = ui->GLwidget;

    ui->xSlider->setRange(-200, 200);
    ui->ySlider->setRange(-200, 200);
    ui->zSlider->setRange(-200, 200);
    ui->xRotSlider->setRange(-180, 180);
    ui->zRotSlider->setRange(-180, 180);
    ui->yRotSlider->setRange(-180, 180);



    facePartCoor coor;

    faceCoor[facePart::facePartName[facePart::Head]] =
            facePartCoor(0, 0, 0, 0, 0, 0);
    faceCoor[facePart::facePartName[facePart::LeftEye]] =
            facePartCoor(3.2, 14, 3.6, -15, 0, 180);
    faceCoor[facePart::facePartName[facePart::RightEye]] =
            facePartCoor(-3.2, 14, 3.6, -15, 0, 180);
    faceCoor[facePart::facePartName[facePart::LeftEyeLid]] =
            facePartCoor(3.5, 8, -4.6, -0, 10, 0);
    faceCoor[facePart::facePartName[facePart::RightEyeLid]] =
            facePartCoor(-3.4, 8, -4.6, -0, -10, 0);
    faceCoor[facePart::facePartName[facePart::Camera]] = coor;
    faceCoor[facePart::facePartName[facePart::Lips]] =
             facePartCoor(0, 0, 0, 0, 0, 0);
    faceCoor[facePart::facePartName[facePart::upperLipsPt1]] =
            facePartCoor(-3.4, 5.4 ,11.5, 0, 30, 0);
    faceCoor[facePart::facePartName[facePart::upperLipsPt2]] =
            facePartCoor(3.4, 5.4 ,11.5, 0, -30, 0);
    faceCoor[facePart::facePartName[facePart::lowerLipsPt1]] =
            facePartCoor(-3.4, 5.1 ,11.5, 0, 30, 0);
    faceCoor[facePart::facePartName[facePart::lowerLipsPt2]] =
            facePartCoor(3.4, 5.1 ,11.5, 0, -30, 0);


    ui->facePartComboBox->addItem(facePart::facePartName[facePart::Head]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::LeftEye]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::RightEye]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::LeftEyeLid]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::RightEyeLid]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::Lips]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::Camera]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::upperLipsPt1]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::upperLipsPt2]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::lowerLipsPt1]);
    ui->facePartComboBox->addItem(facePart::facePartName[facePart::lowerLipsPt2]);



    currSel = facePart::facePartName[facePart::Head];

    connect(ui->facePartComboBox, SIGNAL(currentIndexChanged(QString)), this,
            SLOT(facePartChanged(QString)));


    connect(ui->xSlider, SIGNAL(valueChanged(int)), this, SLOT(xChanged(int)));
    connect(ui->ySlider, SIGNAL(valueChanged(int)), this, SLOT(yChanged(int)));
    connect(ui->zSlider, SIGNAL(valueChanged(int)), this, SLOT(zChanged(int)));
    connect(ui->xRotSlider, SIGNAL(valueChanged(int)), this,
            SLOT(xRotChanged(int)));
    connect(ui->yRotSlider, SIGNAL(valueChanged(int)), this,
            SLOT(yRotChanged(int)));
    connect(ui->zRotSlider, SIGNAL(valueChanged(int)), this,
            SLOT(zRotChanged(int)));
    connect(ui->xVal, SIGNAL(textChanged(QString)), this,
            SLOT(xChanged(QString)));
    connect(ui->yVal, SIGNAL(textChanged(QString)), this,
            SLOT(yChanged(QString)));
    connect(ui->zVal, SIGNAL(textChanged(QString)), this,
            SLOT(zChanged(QString)));
    connect(ui->xRotVal, SIGNAL(textChanged(QString)), this,
            SLOT(xRotChanged(QString)));
    connect(ui->yRotVal, SIGNAL(textChanged(QString)), this,
            SLOT(yRotChanged(QString)));
    connect(ui->zRotVal, SIGNAL(textChanged(QString)), this,
            SLOT(zRotChanged(QString)));

    connect(ui->xVal, SIGNAL(editingFinished()), this, SLOT(updateSliders()));
    connect(ui->yVal, SIGNAL(editingFinished()), this, SLOT(updateSliders()));
    connect(ui->zVal, SIGNAL(editingFinished()), this, SLOT(updateSliders()));

    connect(this, SIGNAL(changeX(int)), ui->xSlider, SLOT(setValue(int)));
    connect(this, SIGNAL(changeY(int)), ui->ySlider, SLOT(setValue(int)));
    connect(this, SIGNAL(changeZ(int)), ui->zSlider, SLOT(setValue(int)));
    connect(this, SIGNAL(changeXrot(int)), ui->xRotSlider, SLOT(setValue(int)));
    connect(this, SIGNAL(changeYrot(int)), ui->yRotSlider, SLOT(setValue(int)));
    connect(this, SIGNAL(changeZrot(int)), ui->zRotSlider, SLOT(setValue(int)));

    emit xChanged(static_cast<int>(faceCoor[currSel].x/posFactor));
    emit yChanged(static_cast<int>(faceCoor[currSel].y/posFactor));
    emit zChanged(static_cast<int>(faceCoor[currSel].z/posFactor));
    emit xRotChanged(static_cast<int>(faceCoor[currSel].xRot));
    emit yRotChanged(static_cast<int>(faceCoor[currSel].yRot));
    emit zRotChanged(static_cast<int>(faceCoor[currSel].zRot));
    updateSliders();

    connect(ui->lookRightBtn, SIGNAL(clicked()), this, SLOT (lookRight()));
    connect(ui->lookCenterBtn, SIGNAL(clicked()), this, SLOT (lookCenter()));
    connect(ui->lookLeftBtn, SIGNAL(clicked()), this, SLOT (lookLeft()));
    connect(ui->lookUpBtn, SIGNAL(clicked()), this, SLOT (lookUp()));
    connect(ui->lookMiddleBtn, SIGNAL(clicked()), this, SLOT (lookMiddle()));
    connect(ui->lookDownBtn, SIGNAL(clicked()), this, SLOT (lookDown()));
    connect(ui->swingRightBtn, SIGNAL(clicked()), this, SLOT (swingRight()));
    connect(ui->straightBtn, SIGNAL(clicked()), this, SLOT (straight()));
    connect(ui->swingLeftBtn, SIGNAL(clicked()), this, SLOT (swingLeft()));
    connect(ui->happyBtn, SIGNAL(clicked()), this, SLOT(happy()));
    connect(ui->sadBtn, SIGNAL(clicked()), this, SLOT(sad()));
    connect(ui->angryBtn, SIGNAL(clicked()), this, SLOT(angry()));
    connect(ui->wickedBtn, SIGNAL(clicked()), this, SLOT(wicked()));

    connect(ui->lookRightBtn_2, SIGNAL(clicked()), this, SLOT (eyesRight()));
    connect(ui->lookCenterBtn_2, SIGNAL(clicked()), this, SLOT (eyesCenter()));
    connect(ui->lookLeftBtn_2, SIGNAL(clicked()), this, SLOT (eyesLeft()));
    connect(ui->lookUpBtn_2, SIGNAL(clicked()), this, SLOT (eyesUp()));
    connect(ui->lookMiddleBtn_2, SIGNAL(clicked()), this, SLOT (eyesMiddle()));
    connect(ui->lookDownBtn_2, SIGNAL(clicked()), this, SLOT (eyesDown()));

    connect(ui->RightCamReset, SIGNAL(clicked()), this, SLOT (RCamReset()));
    connect(ui->LeftCamReset, SIGNAL(clicked()), this, SLOT (LCamReset()));

    connect(ui->visEnable, SIGNAL(clicked(bool)), this, SLOT(visChange(bool)));
    //ui->console->addScrollBarWidget();
}
void MainWindow::visChange(bool b)
{

    if (b) vis->enable(); else vis->disable();
}
void MainWindow::setVis(HeadGL * visA)
{
    vis = visA;

}

void MainWindow::setDrv(Driver * drvA)
{
    drv = drvA;

}

void MainWindow::angry()
{
    QTime time;
    emit emitFrame(Frame(Servo::LeftEyeLidV, time.currentTime(), .6, .08));
    emit emitFrame(Frame(Servo::RightEyeLidV, time.currentTime(), .6, .08));
    emit emitFrame(Frame(Servo::LeftEyeLidR, time.currentTime(), 1, .08));
    emit emitFrame(Frame(Servo::RightEyeLidR, time.currentTime(), 0, .08));
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), .6, .5));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), .6, .5));
    emit emitFrame(Frame(Servo::TopRightLips, time.currentTime(),.57, .3));
    emit emitFrame(Frame(Servo::TopLeftLips, time.currentTime(),.5, .3));
    emit emitFrame(Frame(Servo::BotRightLips, time.currentTime(),.52, .25));
    emit emitFrame(Frame(Servo::BotLeftLips, time.currentTime(),.5, .25));
}

void MainWindow::sad()
{
    QTime time;
    emit emitFrame(Frame(Servo::LeftEyeLidV, time.currentTime(), .7, .04));
    emit emitFrame(Frame(Servo::RightEyeLidV, time.currentTime(), .7, .04));
    emit emitFrame(Frame(Servo::LeftEyeLidR, time.currentTime(), .1, .04));
    emit emitFrame(Frame(Servo::RightEyeLidR, time.currentTime(), .9, .04));
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), 1, -1));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), 1, -1));
    emit emitFrame(Frame(Servo::BotRightLips, time.currentTime(),.54, .2));
    emit emitFrame(Frame(Servo::BotLeftLips, time.currentTime(),.54, .2));
    emit emitFrame(Frame(Servo::TopRightLips, time.currentTime(),.55, .2));
    emit emitFrame(Frame(Servo::TopLeftLips, time.currentTime(),.53, .2));
}

void MainWindow::wicked()
{
    QTime time;
    emit emitFrame(Frame(Servo::LeftEyeLidV, time.currentTime(), .5, .08));
    emit emitFrame(Frame(Servo::RightEyeLidV, time.currentTime(), .5, .08));
    emit emitFrame(Frame(Servo::LeftEyeLidR, time.currentTime(), 1, .08));
    emit emitFrame(Frame(Servo::RightEyeLidR, time.currentTime(), 0, .08));
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), .3, .2));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), .8, .7));
    emit emitFrame(Frame(Servo::RightEyeH, time.currentTime(), .7, .2));
    emit emitFrame(Frame(Servo::TopRightLips, time.currentTime(),.6, .45));
    emit emitFrame(Frame(Servo::TopLeftLips, time.currentTime(),.56, .4));
    emit emitFrame(Frame(Servo::BotRightLips, time.currentTime(),.56, .4));
    emit emitFrame(Frame(Servo::BotLeftLips, time.currentTime(),.7, .4));
}

void MainWindow::happy()
{
    QTime time;
    emit emitFrame(Frame(Servo::LeftEyeLidV, time.currentTime(), .2, .2));
    emit emitFrame(Frame(Servo::RightEyeLidV, time.currentTime(), .2, .2));
    emit emitFrame(Frame(Servo::LeftEyeLidR, time.currentTime(), .5, .2));
    emit emitFrame(Frame(Servo::RightEyeLidR, time.currentTime(), .5, .2));
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), .5, .5));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), .5, .5));
    emit emitFrame(Frame(Servo::TopRightLips, time.currentTime(),.45, .3));
    emit emitFrame(Frame(Servo::TopLeftLips, time.currentTime(),.55, .3));
    emit emitFrame(Frame(Servo::BotRightLips, time.currentTime(),.4, .3));
    emit emitFrame(Frame(Servo::BotLeftLips, time.currentTime(),.6, .3));

}

void MainWindow::showError(exception x)
{
    QString s;
    s = s.number(x.errorCount);
    s.append(": error in ");
    s.append(x.source);
    s.append(" : ");
    s.append(x.msg);
    ui->console->append(s);
}

void MainWindow::showMsg(QString s)
{
    ui->console->append(s);
}
void MainWindow::facePartChanged(QString sel)
{
    currSel = sel;

    //updateSliders();

    float x = faceCoor[currSel].x;
    QString s = s.number(x);
    //ui->xVal->setText(s);

    ui->xVal->setText(s.number((faceCoor[currSel].x)));
    ui->yVal->setText(s.number(static_cast<double>(faceCoor[currSel].y)));
    ui->zVal->setText(s.number(static_cast<double>(faceCoor[currSel].z)));
    ui->xRotVal->setText(s.number(static_cast<double>(faceCoor[currSel].xRot)));
    ui->yRotVal->setText(s.number(static_cast<double>(faceCoor[currSel].yRot)));
    ui->zRotVal->setText(s.number(static_cast<double>(faceCoor[currSel].zRot)));

    updateSliders();

}

void MainWindow::updateSliders()
{
    emit changeX(static_cast<int>(round(faceCoor[currSel].x/posFactor)));
    emit changeY(static_cast<int>(round(faceCoor[currSel].y/posFactor)));
    emit changeZ(static_cast<int>(round(faceCoor[currSel].z/posFactor)));
    emit changeXrot(static_cast<int>(round(faceCoor[currSel].xRot)));
    emit changeYrot(static_cast<int>(round(faceCoor[currSel].yRot)));
    emit changeZrot(static_cast<int>(round(faceCoor[currSel].zRot)));
}
void MainWindow::setUpConnections()
{
    connect(vis, SIGNAL(errorOccured(exception)), this,
            SLOT(showError(exception)));
    connect(vis, SIGNAL(sendMsg(QString)), this, SLOT(showMsg(QString)));

    connect(this, SIGNAL(faceCoorChanged(std::map<QString,facePartCoor>)),
            vis, SLOT(receiveFaceCoor(std::map<QString,facePartCoor>)));
    //emit faceCoorChanged(faceCoor);

    connect(this, SIGNAL(emitFrame(Frame)), vis, SLOT(receiveFrame(Frame)));
    connect(this, SIGNAL(emitFrame(FrameCam)),
            vis, SLOT(receiveFrame(FrameCam)));

    lookMiddle();
    lookCenter();
    straight();
    eyesMiddle();
    eyesCenter();
    happy();

    connect(this, SIGNAL(emitFrame(Frame)), drv, SLOT(receiveFrame(Frame)));
    connect(this, SIGNAL(emitFrame(FrameCam)),
	    drv, SLOT(receiveFrame(FrameCam)));

}

void MainWindow::startThreads()
{
    emit startMainThread(QThread::LowPriority);
    emit startHeadControlThread(QThread::HighPriority);
}
void MainWindow::xChanged(int i)
{
    float x = static_cast<float>(i)*posFactor;
    faceCoor[currSel].x = x;
    QString s = s.number(x);
    ui->xVal->setText(s);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::xChanged(QString s)
{
    bool ok;
    float x = s.toFloat(&ok);
    if (ok)
    {
        faceCoor[currSel].x = x;
        emit faceCoorChanged(faceCoor);
    }
}
void MainWindow::yChanged(int i)
{
    float y = static_cast<float>(i)*posFactor;
    faceCoor[currSel].y = y;
    QString s = s.number(y);
    ui->yVal->setText(s);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::yChanged(QString s)
{
    bool ok;
    float y = s.toFloat(&ok);
    if (ok)
    {
        faceCoor[currSel].y = y;
        emit faceCoorChanged(faceCoor);
    }
}
void MainWindow::zChanged(int i)
{
    float z = static_cast<float>(i)*posFactor;
    faceCoor[currSel].z = z;
    QString s = s.number(z);
    ui->zVal->setText(s);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::zChanged(QString s)
{
    bool ok;
    float z = s.toFloat(&ok);
    if (ok)
    {
        faceCoor[currSel].z = z;
        emit faceCoorChanged(faceCoor);
    }
}
void MainWindow::xRotChanged(int i)
{
    faceCoor[currSel].xRot = i;
    QString s = s.number(static_cast<double>(i));
    ui->xRotVal->setText(s);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::xRotChanged(QString s)
{
    int x = s.toInt();
    faceCoor[currSel].xRot = x;
    emit changeXrot(x);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::yRotChanged(int i)
{
    faceCoor[currSel].yRot = i;
    QString s = s.number(static_cast<double>(i));
    ui->yRotVal->setText(s);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::yRotChanged(QString s)
{
    int y = s.toInt();
    faceCoor[currSel].yRot = y;
    emit changeYrot(y);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::zRotChanged(int i)
{
    faceCoor[currSel].zRot = i;
    QString s = s.number(static_cast<double>(i));
    ui->zRotVal->setText(s);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::zRotChanged(QString s)
{
    int z = s.toInt();
    faceCoor[currSel].zRot = z;
    emit changeZrot(z);
    emit faceCoorChanged(faceCoor);
}
void MainWindow::lookRight()
{
    QTime time;
    Frame frame(Servo::HeadH, time.currentTime(), .1, .2);
    emit emitFrame(frame);
}
void MainWindow::lookCenter()
{
    QTime time;
    Frame frame(Servo::HeadH, time.currentTime(), .5, .2);
    emit emitFrame(frame);
}
void MainWindow::lookLeft()
{
    QTime time;
    Frame frame(Servo::HeadH, time.currentTime(), .9, .2);
    emit emitFrame(frame);
}
void MainWindow::lookUp()
{
    QTime time;
    Frame frame(Servo::HeadR, time.currentTime(), .1, .2);
    emit emitFrame(frame);
}
void MainWindow::lookMiddle()
{
    QTime time;
    Frame frame(Servo::HeadR, time.currentTime(), .5, .2);
    emit emitFrame(frame);
}
void MainWindow::lookDown()
{
    QTime time;
    Frame frame(Servo::HeadR, time.currentTime(), .9, .2);
    emit emitFrame(frame);
}
void MainWindow::swingRight()
{
    QTime time;
    Frame frame(Servo::HeadV, time.currentTime(), .7, .05);
    emit emitFrame(frame);
}
void MainWindow::straight()
{
    QTime time;
    Frame frame(Servo::HeadV, time.currentTime(), .5, .05);
    emit emitFrame(frame);
}
void MainWindow::swingLeft()
{
    QTime time;
    Frame frame(Servo::HeadV, time.currentTime(), .2, .05);
    emit emitFrame(frame);
}

void MainWindow::eyesRight()
{
    QTime time;    
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), -1, 1));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), -1, 1));
}

void MainWindow::eyesCenter()
{
    QTime time;
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), -1, .5));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), -1, .5));
}
void MainWindow::eyesLeft()
{
    QTime time;
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), -1, 0));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), -1, 0));
}
void MainWindow::eyesUp()
{
    QTime time;
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), .2, -1));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), .2, -1));
}

void MainWindow::eyesMiddle()
{
    QTime time;
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), .5, -1));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), .5, -1));
}
void MainWindow::eyesDown()
{
    QTime time;
    emit emitFrame(FrameCam(Servo::LeftEyeH, time.currentTime(), 1, -1));
    emit emitFrame(FrameCam(Servo::RightEyeH, time.currentTime(), 1, -1));
}

void MainWindow::LCamReset()
{
drv->LeftCam.Reset();
}

void MainWindow::RCamReset()
{
drv->RightCam.Reset();
}

MainWindow::~MainWindow()
{
    delete ui;
}

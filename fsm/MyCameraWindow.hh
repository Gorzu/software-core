#ifndef MYCAMERAWINDOW_H_
#define MYCAMERAWINDOW_H_

#include <QWidget>
#include <QVBoxLayout>
#include "QOpenCVWidget.hh"

//#include <opencv/cv.h>
//#include <opencv/highgui.h>
//#include "facedetect.hh"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/core/core_c.h"
#include "opencv2/videoio/legacy/constants_c.h"
#include "opencv2/highgui/highgui_c.h"
#include <opencv2/imgproc.hpp>

#include <QCheckBox>

class MyCameraWindow : public QWidget
{
    Q_OBJECT
    public:


//IplImage * image;
cv::Mat image;
//IplImage * tmpsize;
cv::Mat tmpsize;
        QOpenCVWidget *cvwidget;
  //CvCapture *camera;
  QCheckBox *PreviewCheckBox1;
        
  //  public:
        MyCameraWindow(QWidget *parent=0);
         
    protected:
        void timerEvent(QTimerEvent*);        
};


#endif /*MYCAMERAWINDOW_H_*/

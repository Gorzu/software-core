#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtGui/QMainWindow>
#include <QObject>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QThread>
#include <QString>
#include "ui_main_window.h"
#include "head_control_thread.h"
#include "head_gl.h"
#include "moves.h"
#include "utils.h"
#include "head_gl_utils.h"
#include "XmlRW.h"
#include "BElements.h"
#include <vector>
#include <map>
#include "Driver/Driver.hh"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    void setUpConnections();
	void setVis(HeadGL *_vis);
void setDrv(Driver *_drv);
	void setHeadControlThread(HeadControlThread *_headControl);
    void startThreads();
    ~MainWindow();

    QWidget * GLWidget;

public slots:
	void ResetFaceButton();
	void SendHeadBtn();
	void SendBFrameButton();

	void NewBElements();
	void OpenBElements();
	void SaveBElements();
	
	void ListRefresh();
	void SimpleMovesRefresh();
	
	void AddSpeechButton();
	void DeleteSpeechButton();
	void ReplicateSpeechButton();
	void AddMoveButton();
	void DeleteMoveButton();
	void ReplicateMoveButton();
	void AddEmotionButton();
	void DeleteEmotionButton();
	void ReplicateEmotionButton();

	void ElementChosen(int ind);

	void AddSimpleMoveButton();
	void ReplicateSimpleMoveButton();
	void DeleteSimpleMoveButton();
	void SimpleMoveUpButton();
	void SimpleMoveDownButton();

	void SoundPlayButton();
	void SoundFileFindButton();
	void SoundSaveButton();

	void SaveSetButton();


	void SaveSimpleMoveButton();

	void SMChosen(int ind);

	void slidersChanged();
	
	void SetTrackingCheckBox(bool b);

signals:
    void startHeadControlThread(QThread::Priority prior);
	void emitBFrame(BFrame);
	void cancelQueue();
	void sendHead(TargetInfo);

private:
    Ui::MainWindow *ui;
    HeadGL * vis;
    Driver * drv;
	HeadControlThread *headControl;
	BElements belements;
	SpeechSet tempSet;

	void SetSliders(SimpleMove sm);
	SimpleMove ReadSliders();



};

#endif // MAIN_WINDOW_H

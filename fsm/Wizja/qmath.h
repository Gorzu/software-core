/*******************************************************************************
 *
 * qmath.c
 *
 * Fixed point arithmetic Q-form
 * 
 * Autor: Marisz Janiak
 *
 * Wroclaw 2008
 *
 * Note:
 * Based on application note 33 "Fixed Point Arithmetic on the ARM"
 *
 ******************************************************************************/

#ifndef _MY_QMATH_
#define _MY_QMATH_

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _WORD32_
#define _WORD32_
typedef signed long      Word32;
#endif

#ifndef _UWORD32_
#define _UWORD32_
typedef unsigned long    UWord32;
#endif

#ifndef _WORD64_
#define _WORD64_
typedef signed long long Word64;
#endif

/* Basic operations on 'a', 'b' numbers of fixed point Q-form , return number
   in Q-form */
#define QADD(a,b)     ((a)+(b))
#define QSUB(a,b)     ((a)-(b))
#define QMUL(a,b,q)   (((a)*(b))>>(q))
#define QDIV(a,b,q)   (((a)<<(q))/(b))
#define QMUL32(a,b,q) (((Word64)(a)*(Word64)(b))>>(q)) 
#define QDIV32(a,b,q) (((Word64)(a)<<(q))/(b))

/* CAUTION: When QMUL32 is used, one have to take care about numbers
 * magnitude, in particular with conjuction with QADD32_PRECISE or
 * QSUB32_PRECISE which are able to increse Q factor therefore alsow number
 * magnitude. */

/* Add two variables 'a' and 'b' in 'q' format, store result in 'c'. Increase
 * or decrease 'q' precision if needed. 'qs' is upper bound for 'q' */    
#define QADD32_PRECISE(a,b,c,q,qs){				\
    UWord32 QMATH_sum;						\
    QMATH_sum = (UWord32)(a) + (UWord32)(b);			\
    if(((a)>0) && ((b)>0)){					\
      if(QMATH_sum >> 31){					\
	c = (Word32)(QMATH_sum>>1);				\
	(q)--;							\
      }								\
      else{							\
	if(((q)<(qs)) && (((QMATH_sum>>30) == 0) ||		\
			  (((QMATH_sum>>30) == 3) &&		\
			   ((QMATH_sum<<2) != 0)))){		\
	  c = ((Word32)QMATH_sum)<<1;				\
	  (q)++;						\
	}							\
	else{							\
	  c = (Word32)QMATH_sum;				\
	}							\
      }								\
    }								\
    else if(((a)<0) && ((b)<0)){				\
      if(!(QMATH_sum >> 31)){					\
	c = (Word32)((QMATH_sum>>1)|(1L<<31));			\
	(q)--;							\
      }								\
      else{							\
	if(((q)<(qs)) && (((QMATH_sum>>30) == 0) ||		\
			  (((QMATH_sum>>30) == 3) &&		\
			   ((QMATH_sum<<2) != 0)))){		\
	  c = ((Word32)QMATH_sum)<<1;				\
	  (q)++;						\
	}							\
	else{							\
	  c = (Word32)QMATH_sum;				\
	}							\
      }								\
    }								\
    else{							\
      if(((q)<(qs)) && (((QMATH_sum>>30) == 0) ||		\
			(((QMATH_sum>>30) == 3) &&		\
			 ((QMATH_sum<<2) != 0)))){		\
	c = ((Word32)QMATH_sum)<<1;				\
	(q)++;							\
      }								\
      else{							\
	c = (Word32)QMATH_sum;					\
      }								\
    }								\
  }								\

/* Add two variables 'a' and 'b' in 'q' format, store result in 'c'. Decrease
 * 'q' precision if needed */    
#define QADD32(a,b,c,q){			\
    UWord32 QMATH_sum;				\
    QMATH_sum = (UWord32)(a) + (UWord32)(b);	\
    if(((a)>0) && ((b)>0)){			\
      if(QMATH_sum >> 31){			\
	c = (Word32)(QMATH_sum>>1);		\
	(q)--;					\
      }						\
      else{					\
	c = (Word32)QMATH_sum;			\
      }						\
    }						\
    else if(((a)<0) && ((b)<0)){		\
      if(!(QMATH_sum >> 31)){			\
	c = (Word32)((QMATH_sum>>1)|(1L<<31));	\
	(q)--;					\
      }						\
      else{					\
	c = (Word32)QMATH_sum;			\
      }						\
    }						\
    else{					\
      c = (Word32)QMATH_sum;			\
    }						\
  }						\


/* Sub two variables 'a' and 'b' in 'q' format, store result in 'c'. Increase
 * or decrease 'q' precision if needed. 'qs' is upper bound for 'q' */    
#define QSUB32_PRECISE(a,b,c,q,qs){			\
    UWord32 QMATH_sum;					\
    QMATH_sum = (UWord32)(a) - (UWord32)(b);		\
    if(((a)>0) && ((b)<0)){				\
      if(QMATH_sum >> 31){				\
	c = (Word32)(QMATH_sum>>1);			\
	(q)--;						\
      }							\
      else{						\
	if(((q)<(qs)) && (((QMATH_sum>>30) == 0) ||	\
			  (((QMATH_sum>>30) == 3) &&	\
			   ((QMATH_sum<<2) != 0)))){	\
	  c = ((Word32)QMATH_sum)<<1;			\
	  (q)++;					\
	}						\
	else{						\
	  c = (Word32)QMATH_sum;			\
	}						\
      }							\
    }							\
    else if(((a)<0) && ((b)>0)){			\
      if(!(QMATH_sum >> 31)){				\
	c = (Word32)((QMATH_sum>>1)|(1L<<31));		\
	(q)--;						\
      }							\
      else{						\
	if(((q)<(qs)) && (((QMATH_sum>>30) == 0) ||	\
			  (((QMATH_sum>>30) == 3) &&	\
			   ((QMATH_sum<<2) != 0)))){	\
	  c = ((Word32)QMATH_sum)<<1;			\
	  (q)++;					\
	}						\
	else{						\
	  c = (Word32)QMATH_sum;			\
	}						\
      }							\
    }							\
    else{						\
      if(((q)<(qs)) && (((QMATH_sum>>30) == 0) ||	\
			(((QMATH_sum>>30) == 3) &&	\
			 ((QMATH_sum<<2) != 0)))){	\
	c = ((Word32)QMATH_sum)<<1;			\
	(q)++;						\
      }							\
      else{						\
	c = (Word32)QMATH_sum;				\
      }							\
    }							\
  }

/* Sub two variables 'a' and 'b' in 'q' format, store result in 'c'. Decrease
 * 'q' precision if needed */    
#define QSUB32(a,b,c,q){				\
    UWord32 QMATH_sum;					\
    QMATH_sum = (UWord32)(a) - (UWord32)(b);		\
    if(((a)>0) && ((b)<0)){				\
      if(QMATH_sum >> 31){				\
	c = (Word32)(QMATH_sum>>1);			\
	(q)--;						\
      }							\
      else{						\
	c = (Word32)QMATH_sum;				\
      }							\
    }							\
    else if(((a)<0) && ((b)>0)){			\
      if(!(QMATH_sum >> 31)){				\
	c = (Word32)((QMATH_sum>>1)|(1L<<31));		\
	(q)--;						\
      }							\
      else{						\
	c = (Word32)QMATH_sum;				\
      }							\
    }							\
    else{						\
      c = (Word32)QMATH_sum;				\
    }							\
  }

/* Equalise Q factor of two numbers 'a' and 'b' */
#define QMAKEEQUAL(a,qa,b,qb){			\
    if((qa) < (qb)){				\
      (b) >>= ((qb)-(qa));			\
      qb -= (qb)-(qa);				\
    }						\
    else if ((qa) > (qb)){			\
      (a) >>= ((qa)-(qb));			\
      qa -= (qa)-(qb);				\
    }						\
  }						

/* Basic operations on 'a' number of fixed point Q-form and 'b' is integer,
   return number in Q-form */
#define QADDI(a,b,q)  ((a)+((b)<<(q)))
#define QSUBI(a,b,q)  ((a)-((b)<<(q)))
#define QMULI(a,b)    ((a)*(b))
#define QDIVI(a,b)    ((a)/(b)

/* Convert 'a' from 'q1' format to 'q2' format */
/* CAUTION: GCC generate following warning:
 *   warning: left shift count is negative 
 * This warning is not critical, and should be ignored */
#define QCONV(a,q1,q2) ( ((q2)>(q1)) ? (a)<<((q2)-(q1)) : (a)>>((q1)-(q2)) )

/* The general operation between a in q1 format and b in q2 format
   returning the result in q3 format */
#define QADDG(a,b,q1,q2,q3)   (QCONV(a,q1,q3)+QCONV(b,q2,q3))
#define QSUBG(a,b,q1,q2,q3)   (QCONV(a,q1,q3)-QCONV(b,q2,q3))
#define QMULG(a,b,q1,q2,q3)   QCONV((a)*(b),(q1)+(q2), q3)
#define QDIVG(a,b,q1,q2,q3)   (QCONV(a,q1,(q2)+(q3))/(b))
#define QMULG32(a,b,q1,q2,q3) QCONV((Word64)(a)*(Word64)(b),(q1)+(q2), q3)
#define QDIVG32(a,b,q1,q2,q3) (QCONV((Word64)a, q1, (q2)+(q3))/(b))

/* Convert to and from floating point */
#define FLTTOQ(d, q) ((int)((d)*(double)(1<<(q))+((d)>=0 ? 0.5 : -0.5)))
#define QTOFLT(a, q) ((double)(a)/(double)(1<<(q)))

/* Convert to and from integer */
#define QTOINT(a,q) ((a)>>(q))
#define INTTOQ(a,q) ((a)<<(q))

#ifdef __cplusplus
}
#endif

#endif /* End _MY_QMATH_ */

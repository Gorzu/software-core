#ifndef UTILS_H
#define UTILS_H

#include <QTime>
#include <vector>

/* Typ wyliczeniowy wiazacy numer serwa z ruchem, za ktory to serwo jest
      odpowiedzialne. */
struct Servo
{
	   enum ServoId
    {
	RightEyeLidR = 3,
	RightEyeLidV =2,
        LeftEyeLidV = 1, // vertical left eye lid servo
        LeftEyeLidR = 0, // rotating left eye lid servo

        TopLeftLips = 7,
        TopRightLips = 5,
        BotLeftLips = 6,
        BotRightLips = 4,

        
        HeadV = 11, // vertical head servo
        HeadH = 9, // horizontal head servo
        NeckV = 8,

        HeadR = 10,

        LeftEyeV = 12,  // te serwa nie powinny byc obslugiwane jak dynamixel
        LeftEyeH = 13,
        RightEyeV = 14,
        RightEyeH = 15,

	OverrideEyes = 16, // musi byc to id
	TOTAL = 17,
    };

}

Q_ENUMS(ServoId);



/*    Struktura sluzaca do przekazywania ruchow prostych.
      Zawiera jeden ruch prosty dla jednego serwa.

      Zeby ruch okreslony w ramce mogl zostac wykonany w module HeadGL,
      zdefiniowana w nim musi byc konfiguracja serw. Obiekty zawierajace
      konfiguracje poszczegolnych serw ServoConfig znajduja sie w wektorze
      HeadGL::servoConfigs. Obiekty ServoConfig sa w tym wektorze indeksowane
      zgodnie z numerami serw w typie wyliczeniowym servoId.

      ***DO UZUPELNIENIA: Wymagania na modul Driver***
*/
struct Frame
{
    // Numer serwa odpowiedzialnego za ruch.
    Servo::ServoId ID;
    // Czas w ktorym rozpoczac sie ma wykonywanie ruchu.
    QTime time;
    // Pozycja docelowa, znormalizowana do zakresu (0, 1).
    // Wartosc 0 odpowiada ServoConfig::minPos
    // Wartpsc 1 odpowiada ServoConfig::maxPos
    double destPos;
    // Predkosc z jaka wykonac ma sie ruch, znormalizowana do zakresu (0, 1).
    // Wartosc 0 odpowiada ServoConfig::minVel
    // Wartosc 1 odpowiada ServoConfig::maxVel
    double vel;

    Frame(Servo::ServoId ID, QTime time, double destPos, double vel):
            ID(ID), time(time), destPos(destPos), vel(vel)
    {}

    Frame() {}
};

/*    Struktura sluzaca do przekazywania ruchow kamer.
      Zawiera ruch dla jednej kamery.

      Zeby ruch okreslony w ramce mogl zostac wykonany w module HeadGL,
      zdefiniowana w nim musi byc konfiguracja serw. Obiekty zawierajace
      konfiguracje poszczegolnych serw ServoConfig znajduja sie w wektorze
      HeadGL::servoConfigs. Obiekty ServoConfig sa w tym wektorze indeksowane
      zgodnie z numerami serw w typie wyliczeniowym servoId.

      Wartosc ujemna ktorejs z zadanych pozycji oznacza brak zmiany tej pozycji.

      ***DO UZUPELNIENIA: Wymagania na modul Driver***
*/
struct FrameCam
{
    // Numer serwa odpowiedzialnego za ruch.
    Servo::ServoId ID;
    // Czas w ktorym rozpoczac sie ma wykonywanie ruchu.
    QTime time;
    // Pozycja docelowa, znormalizowana do zakresu (0, 1).
    // Wartosc 0 odpowiada ServoConfig::minPos
    // Wartpsc 1 odpowiada ServoConfig::maxPos
    // Wartosc ujemna oznacza brak zmiany pozycji.
    double destPosV; // wertykalna
    double destPosH; // horyzontalna

    FrameCam(Servo::ServoId ID, QTime time, double destPosV, double destPosH):
            ID(ID), time(time), destPosV(destPosV), destPosH(destPosH)
    {}

    FrameCam() {}
};

#endif // UTILS_H



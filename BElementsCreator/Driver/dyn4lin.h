/*
 * dyn4lin.h
 *
 *  Created on: 2009-03-18
 *      Author: aoleksy
 */

#ifndef DYN4LIN_H_
#define DYN4LIN_H_

#include <stdint.h>
#include <stdarg.h>
#ifndef __XENO__
#  include <termios.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

/*****************************************************************************
 * Constants
 *****************************************************************************/

#ifdef __XENO__
#  define DYN_XENO_BNAME_LEN 31
#  define DYN_XENO_TASK_PRIO 60
#endif

/* Packet frame order */
#define DYN_FRAMEORDER_1ST_START_BYTE         0
#define DYN_FRAMEORDER_2ND_START_BYTE         1
#define DYN_FRAMEORDER_ID                     2
#define DYN_FRAMEORDER_LENGTH                 3
#define DYN_FRAMEORDER_INSTRUCTION            4
#define DYN_FRAMEORDER_ERROR                  4
#define DYN_FRAMEORDER_PARAMETER              5

/* Instruction set (instruction packet) */
#define DYN_INS_PING                          0x01
#define DYN_INS_READ_DATA                     0x02
#define DYN_INS_WRITE_DATA                    0x03
#define DYN_INS_REG_WRITE                     0x04
#define DYN_INS_ACTION                        0x05
#define DYN_INS_RESET                         0x06
#define DYN_INS_SYNC_WRITE                    0x83

/* Error mask set (status packet) */
#define DYN_MASK_INSTRUCTION_ERR              (0x01<<6)
#define DYN_MASK_OVERLOAD_ERR                 (0x01<<5)
#define DYN_MASK_CHECKSUM_ERR                 (0x01<<4)
#define DYN_MASK_RANGE_ERR                    (0x01<<3)
#define DYN_MASK_OVERHEATING_ERR              (0x01<<2)
#define DYN_MASK_ANGLE_LIMIT_ERR              (0x01<<1)
#define DYN_MASK_INPUT_VOLTAGE_ERR            (0x01<<0)

/* Control table */
#define DYN_ADR_MODEL_NUMBER_L                0x00
#define DYN_ADR_MODEL_NUMBER_H                0x01
#define DYN_ADR_VERSION_OF_FIRMWARE           0x02
#define DYN_ADR_ID                            0x03
#define DYN_ADR_BAUD_RATE                     0x04
#define DYN_ADR_RETURN_DELAY_TIME             0x05
#define DYN_ADR_CW_ANGLE_LIMIT_L              0x06
#define DYN_ADR_CW_ANGLE_LIMIT_H              0x07
#define DYN_ADR_CCW_ANGLE_LIMIT_L             0x08
#define DYN_ADR_CCW_ANGLE_LIMIT_H             0x09
#define DYN_ADR_UNUSED_WORD                   0x0A
#define DYN_ADR_HIGHEST_LIMIT_TEMPERATURE     0x0B
#define DYN_ADR_LOWEST_LIMIT_VOLTAGE          0x0C
#define DYN_ADR_HIGHEST_LIMIT_VOLTAGE         0x0D
#define DYN_ADR_MAX_TORQUE_L                  0x0E
#define DYN_ADR_MAX_TORQUE_H                  0x0F
#define DYN_ADR_STATUS_RETURN_LEVEL           0x10
#define DYN_ADR_ALARM_LED                     0x11
#define DYN_ADR_ALARM_SHUTDOWN                0x12
#define DYN_ADR_TORQUE_ENABLE                 0x18
#define DYN_ADR_LED                           0x19
#define DYN_ADR_CW_COMPLIANCE_MARGIN          0x1A
#define DYN_ADR_CCW_COMPLIANCE_MARGIN         0x1B
#define DYN_ADR_CW_COMPLIANCE_SLOPE           0x1C
#define DYN_ADR_CCW_COMPLIANCE_SLOPE          0x1D
#define DYN_ADR_GOAL_POSITION_L               0x1E
#define DYN_ADR_GOAL_POSITION_H               0x1F
#define DYN_ADR_MOVING_SPEED_L                0x20
#define DYN_ADR_MOVING_SPEED_H                0x21
#define DYN_ADR_TORQUE_LIMIT_L                0x22
#define DYN_ADR_TORQUE_LIMIT_H                0x23
#define DYN_ADR_PRESENT_POSITION_L            0x24
#define DYN_ADR_PRESENT_POSITION_H            0x25
#define DYN_ADR_PRESENT_SPEED_L               0x26
#define DYN_ADR_PRESENT_SPEED_H               0x27
#define DYN_ADR_PRESENT_LOAD_L                0x28
#define DYN_ADR_PRESENT_LOAD_H                0x29
#define DYN_ADR_PRESENT_VOLTAGE               0x2A
#define DYN_ADR_PRESENT_TEMPERATURE           0x2B
#define DYN_ADR_REGISTERED_INSTRUCTION        0x2C
#define DYN_ADR_MOVING                        0x2E
#define DYN_ADR_LOCK                          0x2F
#define DYN_ADR_PUNCH_L                       0x30
#define DYN_ADR_PUNCH_H                       0x31

/* Control table data range */
#define DYN_MIN_ID                            0x00
#define DYN_MAX_ID                            0xFD
#define DYN_MIN_BAUD_RATE                     0x00
#define DYN_MAX_BAUD_RATE                     0xFE
#define DYN_MIN_RETURN_DELAY_TIME             0x00
#define DYN_MAX_RETURN_DELAY_TIME             0xFE
#define DYN_MIN_CW_ANGLE_LIMIT                0x0000
#define DYN_MAX_CW_ANGLE_LIMIT                0x03FF
#define DYN_MIN_CCW_ANGLE_LIMIT               0x0000
#define DYN_MAX_CCW_ANGLE_LIMIT               0x03FF
#define DYN_MIN_THE_HIGEST_LIMIT_TEMPERATURE  0x0A
#define DYN_MAX_THE_HIGEST_LIMIT_TEMPERATURE  0x63
#define DYN_MIN_THE_LOWEST_LIMIT_VOLTAGE      0x32
#define DYN_MAX_THE_LOWEST_LIMIT_VOLTAGE      0xFA
#define DYN_MIN_THE_HIGHEST_LIMIT_VOLTAGE     0x32
#define DYN_MAX_THE_HIGHEST_LIMIT_VOLTAGE     0xFA
#define DYN_MIN_MAX_TORQUE                    0x0000
#define DYN_MAX_MAX_TORQUE                    0x03FF
#define DYN_MIN_STATUS_RETURN_LEVEL           0x00
#define DYN_MAX_STATUS_RETURN_LEVEL           0x02
#define DYN_MIN_ALARM_LED                     0x00
#define DYN_MAX_ALARM_LED                     0x7F
#define DYN_MIN_ALARM_SHUTDOWN                0x00
#define DYN_MAX_ALARM_SHUTDOWN                0x7F
#define DYN_MIN_TORQUE_ENABLE                 0x00
#define DYN_MAX_TORQUE_ENABLE                 0x01
#define DYN_MIN_LED                           0x00
#define DYN_MAX_LED                           0x01
#define DYN_MIN_CW_COMPLIANCE_MARGIN          0x00
#define DYN_MAX_CW_COMPLIANCE_MARGIN          0xFE
#define DYN_MIN_CCW_COMPLIANCE_MARGIN         0x00
#define DYN_MAX_CCW_COMPLIANCE_MARGIN         0xFE
#define DYN_MIN_CW_COMPLIANCE_SLOPE           0x01
#define DYN_MAX_CW_COMPLIANCE_SLOPE           0xFE
#define DYN_MIN_CCW_COMPLIANCE_SLOPE          0x01
#define DYN_MAX_CCW_COMPLIANCE_SLOPE          0xFE
#define DYN_MIN_GOAL_POSITION                 0x0000
#define DYN_MAX_GOAL_POSITION                 0x03FF
#define DYN_MIN_MOVING_SPEED                  0x0000
#define DYN_MAX_MOVING_SPEED                  0x03FF
#define DYN_MIN_TORQUE_LIMIT                  0x0000
#define DYN_MAX_TORQUE_LIMIT                  0x03FF
#define DYN_MIN_REGISTERED_INSTRUCTION        0x00
#define DYN_MAX_REGISTERED_INSTRUCTION        0x01
#define DYN_MIN_LOCK                          0x00
#define DYN_MAX_LOCK                          0x01
#define DYN_MIN_PUNCH                         0x0000
#define DYN_MAX_PUNCH                         0x03FF

/* Control table default values */
#define DYN_DEF_ID                            0x01
#define DYN_DEF_BAUD_RATE                     0x22
#define DYN_DEF_RETURN_DELAY_TIME             0xFA
#define DYN_DEF_CW_ANGLE_LIMIT                0x0000
#define DYN_DEF_CCW_ANGLE_LIMIT               0x03FF
#define DYN_DEF_THE_HIGEST_LIMIT_TEMPERATURE  0x50
#define DYN_DEF_THE_LOWEST_LIMIT_VOLTAGE      0x3C
#define DYN_DEF_THE_HIGHEST_LIMIT_VOLTAGE     0xF0
#define DYN_DEF_MAX_TORQUE                    0x03FF
#define DYN_DEF_STATUS_RETURN_LEVEL           0x02
#define DYN_DEF_ALARM_LED                     0x24
#define DYN_DEF_ALARM_SHUTDOWN                0x24
#define DYN_DEF_TORQUE_ENABLE                 0x00
#define DYN_DEF_LED                           0x00
#define DYN_DEF_CW_COMPLIANCE_MARGIN          0x00
#define DYN_DEF_CWW_COMPLIANCE_MARGIN         0x00
#define DYN_DEF_CW_COMPLIANCE_SLOPE           0x20
#define DYN_DEF_CWW_COMPLIANCE_SLOPE          0x20
#define DYN_DEF_TORQUE_LIMIT                  DYN_DEF_MAX_TORQUE
#define DYN_DEF_REGISTERED_INSTRUCTION        0x00
#define DYN_DEF_LOCK                          0x00
#define DYN_DEF_PUNCH                         0x0020

/* Limitations */
#define DYN_MAX_BYTE_TO_BYTE_TIME_MS          100
#define DYN_MIN_FRAME_LENGTH                  6
#define DYN_MAX_FRAME_LENGTH                  143
#define DYN_MAX_PARAM_LENGTH                  (DYN_MAX_FRAME_LENGTH-DYN_MIN_FRAME_LENGTH)
#define DYN_STATUS_PACKET_LENGTH              6
#define DYN_PRESET_QUERY_LENGTH               2
#define DYN_ROM_TABLE_LENGTH                  19

/* Error table */
#define DYN_NO_ERROR                          0
#define DYN_OUT_OF_RANGE                      -1
#define DYN_TOO_MANY_DATA                     -2
#define DYN_ID_OUT_OF_RANGE                   -3
#define DYN_UNKNOWN_INSTRUCTION               -4
#define DYN_ADDRESS_OUT_OF_RANGE              -5
#define DYN_CHECKSUM_ERROR                    -6
#define DYN_TIMEOUT_ERROR                     -8
#define DYN_PORT_SOCKET_FAILURE               -9
#define DYN_SELECT_FAILURE                    -10

/* Others */
#define DYN_BROADCAST                         0xFE
#define DYN_WORD8_SKIP                        0xFF
#define DYN_START_BYTE                        0xFF
#define DYN_WORD16_SKIP                       0xFFFF

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef MAX
#define MAX(a,b) ((a) > (b) ? (a) : (b))
#endif

#define DYN_Word16MSB(val) ((uint8_t)((val) >> 8))
#define DYN_Word16LSB(val) ((uint8_t)((val) & 0xFF))
#define DYN_MSBLSBWord16(msb,lsb) ((uint16_t)(((msb) << 8) | (lsb)))
#define DYN_GET_SPEED(msb,lsb) (DYN_MSBLSBWord16(msb, lsb) & 0x03FF) * ((msb & 0x04) ? -1 : 1)
#define DYN_GET_LOAD(msb,lsb) (DYN_MSBLSBWord16(msb, lsb) & 0x01FF) * ((msb & 0x02) ? -1 : 1)

typedef enum {
  FLUSH_ON_ERROR, NOP_ON_ERROR
} error_handling_t;

typedef enum {
  DYN_UINT8, DYN_PUINT8, DYN_UINT16, DYN_PUINT16
} dyn_parameter_type_t;

typedef union {
  uint8_t *puint8;
  uint8_t uint8;
  uint16_t *puint16;
  uint16_t uint16;
} dyn_parameter_t;

#define dyn_uint8(param) DYN_UINT8, param
#define dyn_puint8(param) DYN_PUINT8, param
#define dyn_uint16(param) DYN_UINT16, param
#define dyn_puint16(param) DYN_PUINT16, param

typedef struct {
  uint16_t model; /* model number */
  uint8_t firmware; /* firmware version */
  uint8_t id; /* dynamixel id*/
  uint8_t baud; /* baud rate */
  uint8_t delay; /* return delay time */
  uint16_t CWLim; /* clockwise angle limit */
  uint16_t CCWLim; /* counterclockwise angle limit*/
  uint8_t hTempLimit; /* internal limit temperature */
  uint8_t lVoltLimit; /* lowest limit voltage */
  uint8_t hVoltLimit; /* highest limit voltage */
  uint16_t torque; /* maximum torque */
  uint8_t statLevel; /* status return level */
  uint8_t led; /* led for alarm */
  uint8_t shutdown; /* shutdown for alarm */
} dyn_config_t;

#define DYN_DEFAULT_CONFIG                                                    \
{                                                                             \
  DYN_WORD16_SKIP,                                                            \
  DYN_WORD8_SKIP,                                                             \
  DYN_DEF_ID,                                                                 \
  DYN_DEF_BAUD_RATE,                                                          \
  DYN_DEF_RETURN_DELAY_TIME,                                                  \
  DYN_DEF_CW_ANGLE_LIMIT,                                                     \
  DYN_DEF_CCW_ANGLE_LIMIT,                                                    \
  DYN_DEF_THE_HIGEST_LIMIT_TEMPERATURE,                                       \
  DYN_DEF_THE_LOWEST_LIMIT_VOLTAGE,                                           \
  DYN_DEF_THE_HIGHEST_LIMIT_VOLTAGE,                                          \
  DYN_DEF_MAX_TORQUE,                                                         \
  DYN_DEF_STATUS_RETURN_LEVEL,                                                \
  DYN_DEF_ALARM_LED,                                                          \
  DYN_DEF_ALARM_SHUTDOWN,                                                     \
}

typedef struct {
  uint8_t id;
  uint8_t return_level;
  uint8_t error_code;
} dyn_servo_t;

#define DYN_LENGTH_PATH_DEV 16
typedef struct {
  /* Descriptor (tty) */
  int fd;
  /* Flag debug */
  int debug;
  /* Device: "/dev/ttyS0", "/dev/ttyUSB0" or "/dev/tty.USA19*" */
  char device[DYN_LENGTH_PATH_DEV];
  /* Bauds: 9600, 19200, 57600, 115200, etc */
  int baud;
#ifndef __XENO__
  /* Save old termios settings */
  struct termios old_tios;
#endif
  error_handling_t error_handling;
  /**/
  dyn_servo_t *dyn_servo;
  uint8_t dyn_servo_length;
} dyn_param_t;

typedef dyn_param_t* dyn_param_p;

#ifdef __XENO__
typedef struct {
  dyn_param_t *dyn_param;
  uint8_t *data;
  uint16_t len;
  int ret;
}dyn_xeno_param;
#endif

/** Initialize <code>dyn_param_t</code> structure.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param device Path to the serial device.
 * \param baud Baud rate.
 * \param dyn_servo Pointer to the array of the <code>dyn_servo_t</code> structures.
 * \param length of the array of the <code>dyn_servo_t</code> structures.
 */
void dyn_init(dyn_param_t *dyn_param, const char *device, int baud,
    dyn_servo_t *dyn_servo, uint8_t length);

/** Open connection.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \return 0 if there was no error, -1 otherwise.
 */
int dyn_connect(dyn_param_t *dyn_param);

/** Close connection.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 */
void dyn_close(dyn_param_t *dyn_param);

/** Enable/disable debug mode.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param boolean 1 enable debug mode, 0 disable debug mode.
 */
void dyn_set_debug(dyn_param_t *dyn_param, int boolean);

/** Set error handling
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param error_handling Set behaviour for errors.
 */
void dyn_set_error_handling(dyn_param_t *dyn_param,
    error_handling_t error_handling);

/* Primitives */
/** Ping servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_ping(dyn_param_t *dyn_param, uint8_t id);

/** Read data from servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param start_address Address of the first byte to read.
 * \param nb Number of bytes to read.
 * \param data Pointer to the array to store read data.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_read_data(dyn_param_t *dyn_param, uint8_t id, uint8_t start_address,
    uint8_t nb, uint8_t *data);

/** Write data to servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param start_address Address of the first byte to write.
 * \param nb Number of bytes to write.
 * \param data Pointer to the array with data to write.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_write_data(dyn_param_t *dyn_param, uint8_t id, uint8_t start_address,
    uint8_t nb, uint8_t *data);

/** Write reg data to servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param start_address Address of the first byte to write.
 * \param nb Number of bytes to write.
 * \param data Pointer to the array with data to write.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_reg_write_data(dyn_param_t *dyn_param, uint8_t id,
    uint8_t start_address, uint8_t nb, uint8_t *data);

/** Send action instruction to servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_action(dyn_param_t *dyn_param, uint8_t id);

/** Send resetinstruction to servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_reset(dyn_param_t *dyn_param, uint8_t id);

/** Synchronous write to servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param start_address Address of the first byte to write.
 * \param data Pointer to the array with data to write (id numbers and parameters).
 * \param n Length of <code>data</code> array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_sync_write_data(dyn_param_t *dyn_param, uint8_t start_address,
    uint8_t *data, uint8_t n);

/* High */
/** Get servo's configuration.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param dyn_config Pointer to <code>dyn_config_t</code> structure, where configuration will write.
 * \return DYN_NO_ERROR if there was no error.
 */
int
    dyn_get_config(dyn_param_t *dyn_param, uint8_t id, dyn_config_t *dyn_config);

/** Set servo's configuration.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param dyn_config Pointer to <code>dyn_config_t</code> structure with configuration to write.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_config(dyn_param_t *dyn_param, uint8_t id, const dyn_config_t *dyn_config);

/** Verify and correct status return level.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_return_level_verify_config(dyn_param_t *dyn_param);

/** Disable/Enable torque for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param enable 1 torque is enabled, 0 otherwise.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_torque(dyn_param_t *dyn_param, uint8_t id, uint8_t enable);

/** Disable/Enable torque for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param enable Pointer to array. 1 torque is enabled, 0 otherwise.
 * \param n Length of array
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_torque_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *enable,
    uint8_t n);

/** Disable/Enable led for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param enable 1 led is enabled, 0 otherwise.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_led(dyn_param_t *dyn_param, uint8_t id, uint8_t enable);

/** Disable/Enable led for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param enable Pointer to array. 1 led is enabled, 0 otherwise.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_led_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *enable,
    uint8_t n);

/** Set compliance margin for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param cw CW Compliance Margin.
 * \param ccw CCW Compliance Margin.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_margin(dyn_param_t *dyn_param, uint8_t id, uint8_t cw, uint8_t ccw);

/** Set compliance margins for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param cw CW Compliance Margins.
 * \param ccw CCW Compliance Margins.
 * \param n Length of array
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_margin_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n);

/** Get compliance margin for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param cw CW Compliance Margin.
 * \param ccw CCW Compliance Margin.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_margin(dyn_param_t *dyn_param, uint8_t id, uint8_t *cw,
    uint8_t *ccw);

/** Get compliance margins for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param cw CW Compliance Margins.
 * \param ccw CCW Compliance Margins.
 * \param n Length of array
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_margin_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n);

/** Set compliance sargin for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param cw CW Compliance sargin.
 * \param ccw CCW Compliance sargin.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_slope(dyn_param_t *dyn_param, uint8_t id, uint8_t cw, uint8_t ccw);

/** Set compliance slopes for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param cw CW Compliance slopes.
 * \param ccw CCW Compliance slopes.
 * \param n Length of array
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_slope_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n);

/** Get compliance slope for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param cw CW Compliance slope.
 * \param ccw CCW Compliance slope.
 * \return DYN_NO_ERROR if there was no error.
 */
int
    dyn_get_slope(dyn_param_t *dyn_param, uint8_t id, uint8_t *cw, uint8_t *ccw);

/** Get compliance slopes for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param cw CW Compliance slopes.
 * \param ccw CCW Compliance slopes.
 * \param n Length of array
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_slope_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n);

/** Set position for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param position Position of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_position(dyn_param_t *dyn_param, uint8_t id, uint16_t position);

/** Set positions for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param position Positions of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_position_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *position, uint8_t n);

/** Set speed for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param speed Speed of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_speed(dyn_param_t *dyn_param, uint8_t id, uint16_t speed);

/** Set speeds for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param speed Speeds of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_speed_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *speed,
    uint8_t n);

/** Set position and speed for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param position Position of the servo.
 * \param speed Speed of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_movement(dyn_param_t *dyn_param, uint8_t id, uint16_t position,
    uint16_t speed);

/** Set positions and speeds for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servo.
 * \param positions Position of the servo.
 * \param speed Speeds of the servo.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_movement_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *position, uint16_t *speed, uint8_t n);

/** Set torque limit for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param speed Torque limit of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_torque_limit(dyn_param_t *dyn_param, uint8_t id, uint16_t torque);

/** Set torque limits for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param speed Torque limit of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_torque_limit_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *torque, uint8_t n);

/** Get torque limit for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param speed Torque limit of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_torque_limit(dyn_param_t *dyn_param, uint8_t id, uint16_t *data);

/** Get torque limits for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param speed Torque limit of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_torque_limit_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *data, uint8_t n);

/** Get position for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param position Position of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_position(dyn_param_t *dyn_param, uint8_t id, uint16_t *data);

/** Get positions for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param position Positions of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_position_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *data,
    uint8_t n);

/** Get speed for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param speed Speed of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_speed(dyn_param_t *dyn_param, uint8_t id, int16_t *data);

/** Get speeds for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param speed Speeds of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_speed_multi(dyn_param_t *dyn_param, uint8_t *id, int16_t *data,
    uint8_t n);

/** Get position and speed for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param position Position of the servo.
 * \param speed Speed of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_movement(dyn_param_t *dyn_param, uint8_t id, uint16_t *position,
    int16_t *speed);

/** Get positions and speeds for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servo.
 * \param positions Position of the servo.
 * \param speed Speeds of the servo.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_movement_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *position, int16_t *speed, uint8_t n);

/** Get load for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param load Load of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_load(dyn_param_t *dyn_param, uint8_t id, int16_t *load);

/** Get loads for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param load Loads of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_load_multi(dyn_param_t *dyn_param, uint8_t *id, int16_t *load,
    uint8_t n);

/** Get voltage for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param voltage Voltage of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_voltage(dyn_param_t *dyn_param, uint8_t id, uint8_t *voltage);

/** Get voltages for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param voltage Voltages of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_voltage_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint8_t *voltage, uint8_t n);

/** Get temperature of servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param temperature Temperature of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_temperature(dyn_param_t *dyn_param, uint8_t id,
    uint8_t *temperature);

/** Get termperatures of servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param temperature Temperatures of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_temperature_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint8_t *temperature, uint8_t n);

/** Get registered byte of servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return 1 if there is registered instruction, 0 is not, less than 0 if there was error.
 */
int dyn_get_registered(dyn_param_t *dyn_param, uint8_t id);

/** Get registered bytes of servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param registered Registered bytes of the servo.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_registered_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint8_t *registered, uint8_t n);

/** Get moving byte of servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return 1 if there is movement, 0 is not, less than 0 if there was error.
 */
int dyn_get_moving(dyn_param_t *dyn_param, uint8_t id);

/** Get moving bytes of servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param moving Moving bytes of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_moving_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *moving,
    uint8_t n);

/** Enable/Disable lock byte of servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param enable 1 to lock, 0 otherwise.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_lock(dyn_param_t *dyn_param, uint8_t id, uint8_t enable);

/** Enable/Disable lock bytes of servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param enable 1 to lock, 0 otherwise.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_lock_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *enable,
    uint8_t n);

/** Set punch for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param punch Punch of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_punch(dyn_param_t *dyn_param, uint8_t id, uint16_t punch);

/** Set punches for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param punch Punches of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_set_punch_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *punch,
    uint8_t n);

/** Get punch for servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \param punch Punch of the servo.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_punch(dyn_param_t *dyn_param, uint8_t id, uint16_t *data);

/** Get punches for servos.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Addresses of the servos.
 * \param punch Punches of the servos.
 * \param n Length of array.
 * \return DYN_NO_ERROR if there was no error.
 */
int dyn_get_punch_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *data,
    uint8_t n);

/** Convert config structure to array of bytes.
 * \param dyn_config Strcucture with configuration to convert.
 * \param data Array of bytes.
 */
void dyn_config2data(const dyn_config_t *dyn_config, uint8_t *data);

/** Convert array of bytes to config structure.
 * \param data Array of bytes.
 * \param dyn_config Strcucture with configuration to convert.
 */
void dyn_data2config(uint8_t *data, dyn_config_t *dyn_config);

/** Get structure with servo's status.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return Structure with information about servo.
 */
dyn_servo_t *dyn_get_servo(dyn_param_t *dyn_param, uint8_t id);

/** Get last error code of the servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return Last error code.
 */
uint8_t dyn_get_error_code(dyn_param_t *dyn_param, uint8_t id);

/** Get status return level of the servo.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param id Address of the servo.
 * \return Status return level.
 */
uint8_t dyn_get_return_level(dyn_param_t *dyn_param, uint8_t id);

/** Convert parameters to the array.
 * \param dyn_param Pointer to <code>dyn_param_t</code> structure.
 * \param n Number of parameters,
 * \param length Length of the parameters arrays.
 * \param ... Pair of the dyn_parameter_type_t and parameter.
 * \example dyn_param2_data(data, 3, n, dyn_uint8(2), dyn_puint8(id),
 *            dyn_puint16(speed))
 */
int dyn_param2data(uint8_t *data, int n, int length, ...);

#ifdef __cplusplus
}
#endif

#endif /* DYN4LIN_H_ */

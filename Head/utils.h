#ifndef UTILS_H
#define UTILS_H

#include <QTime>
#include <vector>

/* Odpowiada jednegmu ruchowi prostemu.
Do dyskusji jakie informacje sa zapisane.
Nastawienia poszczegolnych serw , badz struktury nazwane -> oczy, usta , brwi ... i dopiero wypelniane*/
struct SimpleMove
{
    int servoCount;
    QTime executeTime;
    std::vector<int> servoId;
    std::vector<float> servoPosition;

    SimpleMove(int _servoCount, QTime _executeTime, std::vector<int> _servoId, std::vector<float> _servoPosition)
    {
        servoCount = _servoCount ;
        executeTime = _executeTime;
        servoId = _servoId;
        servoPosition = _servoPosition;
    }

    SimpleMove() {}

};

struct HeadPosition{
    double horizontalAngle;
    double verticalAngle;

    HeadPosition(double _horizontalAngle, double _verticalAngle){
        horizontalAngle = _horizontalAngle;
        verticalAngle = _verticalAngle;
    }

    HeadPosition(){}
};

struct SpeechElem
{
    Q_ENUMS(Say);

    enum Say
    {
        NullSay = 0,
        HelloSay = 1,
        YesSay = 2,
        NoSay = 3
    };

    Say thingToSay;
    QTime totalTime;
    double intensity;

    SpeechElem(Say _thingToSay = SpeechElem::NullSay , QTime _totalTime = QTime(0,0,0,0), double _intensity = 0)
    {
        thingToSay = _thingToSay;
        totalTime = _totalTime;
        intensity = _intensity;
    }
    //SpeechElem(){};
};

struct MoveElem
{
    Q_ENUMS(Name);

    enum Name
    {
        NullMove = 0,
        EyesMove = 1,
        HeadMove = 2
    };
    Name moveName;
    HeadPosition head;
    QTime totalTime;
    QTime periodTime;
    int repeats;

    MoveElem(HeadPosition _head, Name _moveName = MoveElem::NullMove, QTime _totalTime=QTime(0,0,0,0),
             QTime _periodTime = QTime(0,0,0,0), int _repeats=1)
    {
        moveName = _moveName;
        head = _head;
        totalTime = _totalTime;
        periodTime = _periodTime;
        repeats = _repeats;
    }
    MoveElem(){}

};

struct EmotionElem{

    Q_ENUMS(Name);

    enum Name
    {
        NullEmot = 0,
        HappyEmot = 1,
        SadEmot = 2,
        AngryEmot =3
    };

    Name emotionName;
    double intensity;

    EmotionElem(Name _emotionName = EmotionElem::NullEmot, double _intensity = 0)
    {
        emotionName = _emotionName;
        intensity = _intensity;
    }
};

struct BehaviorFrame{
public:
    SpeechElem speech;
    MoveElem move;
    EmotionElem emotion;

    BehaviorFrame(SpeechElem _speech, MoveElem _move, EmotionElem _emotion)
    {
        speech = _speech;
        move = _move;
        emotion = _emotion;
    }
};

/* Typ wyliczeniowy wiazacy numer serwa z ruchem, za ktory to serwo jest
      odpowiedzialne. */
struct Servo
{

  /**
   * @par Zasady implementacji tej struktury.
   * W SrvoId numerować ID napędów kolejno 0. 
   * Kamery umieścić na końcu, a jako ostatni element
   * podać ile jest zdefiniowanych napędów (TOTAL). 
   */
  enum ServoId
    {
      TopLeftLips  = 0,
      TopRightLips = 1,
      
      HeadH        = 2, 

      BotRightLips = 3,
      LeftEyeLidV  = 4,
      LeftEyeLidR  = 5, 
      RightEyeLidV = 6,
      RightEyeLidR = 7,
      HeadV        = 8, 
    
      BotLeftLips  = 9,

      HeadR        = 10,
      LeftEyeV     = 11,
      LeftEyeH     = 12,
      RightEyeV    = 13,
      RightEyeH    = 14,
      TOTAL        = 15
    };
}
  
Q_ENUMS(ServoId);

/*    Struktura sluzaca do przekazywania ruchow prostych.
      Zawiera jeden ruch prosty dla jednego serwa.

      Zeby ruch okreslony w ramce mogl zostac wykonany w module HeadGL,
      zdefiniowana w nim musi byc konfiguracja serw. Obiekty zawierajace
      konfiguracje poszczegolnych serw ServoConfig znajduja sie w wektorze
      HeadGL::servoConfigs. Obiekty ServoConfig sa w tym wektorze indeksowane
      zgodnie z numerami serw w typie wyliczeniowym servoId.

      ***DO UZUPELNIENIA: Wymagania na modul Driver***
*/
struct Frame
{
    // Numer serwa odpowiedzialnego za ruch.
    Servo::ServoId ID;
    // Czas w ktorym rozpoczac sie ma wykonywanie ruchu.
    QTime time;
    // Pozycja docelowa, znormalizowana do zakresu (0, 1).
    // Wartosc 0 odpowiada ServoConfig::minPos
    // Wartpsc 1 odpowiada ServoConfig::maxPos
    double destPos;
    // Predkosc z jaka wykonac ma sie ruch, znormalizowana do zakresu (0, 1).
    // Wartosc 0 odpowiada ServoConfig::minVel
    // Wartosc 1 odpowiada ServoConfig::maxVel
    double vel;

    Frame(Servo::ServoId ID, QTime time, double destPos, double vel):
            ID(ID), time(time), destPos(destPos), vel(vel)
    {}

    Frame() {}
};

/*    Struktura sluzaca do przekazywania ruchow kamer.
      Zawiera ruch dla jednej kamery.

      Zeby ruch okreslony w ramce mogl zostac wykonany w module HeadGL,
      zdefiniowana w nim musi byc konfiguracja serw. Obiekty zawierajace
      konfiguracje poszczegolnych serw ServoConfig znajduja sie w wektorze
      HeadGL::servoConfigs. Obiekty ServoConfig sa w tym wektorze indeksowane
      zgodnie z numerami serw w typie wyliczeniowym servoId.

      Wartosc ujemna ktorejs z zadanych pozycji oznacza brak zmiany tej pozycji.

      ***DO UZUPELNIENIA: Wymagania na modul Driver***
*/
struct FrameCam
{
    // Numer serwa odpowiedzialnego za ruch.
    Servo::ServoId ID;
    // Czas w ktorym rozpoczac sie ma wykonywanie ruchu.
    QTime time;
    // Pozycja docelowa, znormalizowana do zakresu (0, 1).
    // Wartosc 0 odpowiada ServoConfig::minPos
    // Wartpsc 1 odpowiada ServoConfig::maxPos
    // Wartosc ujemna oznacza brak zmiany pozycji.
    double destPosV; // wertykalna
    double destPosH; // horyzontalna

    FrameCam(Servo::ServoId ID, QTime time, double destPosV, double destPosH):
            ID(ID), time(time), destPosV(destPosV), destPosH(destPosH)
    {}

    FrameCam() {}
};

#endif // UTILS_H



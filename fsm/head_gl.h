// Lukasz Kucharczyk 2009

#ifndef HEAD_GL_H
#define HEAD_GL_H

#include <QGLWidget>
#include <QObject>
#include <QTimerEvent>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <math.h>
#include "utils.h"
#include "head_gl_utils.h"
#include "GL/glu.h"

inline double round(double val)
{   
    return floor(val + 0.5);
}

class HeadGL : public QGLWidget
{
    Q_OBJECT

public:
    HeadGL(std::string fileName, QWidget *parent = 0);
    ~HeadGL();
    // Inicjalizacja i wlaczenie modulu wizualizacji.
    bool start(int period = 25);
    // Wlaczenie modulu. Wymaga wczesniejszego wywolania funkcji start
    // i wczesniejeszego wylaczenia modulu.
    bool enable(int period = 25);
    // Wylaczenie modulu. Wymaga wczesniejszego wlaczenia modulu.
    void disable();
    // Rozmiary widgetu.
    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    // Zwraca aktulane wspolrzedne bazowe.
    std::vector<facePartCoor> getFacePartCoor();
    // Ustawienie predkosci poruszania oczu.
    void setEyeVel(float eyeVel_);
public slots:
    // Odebranie i rozpoczecie wykonywania ramki z ruchem prostym.
    void receiveFrame(Frame frame);
    // Odebranie i rozpoczecie wykonywania ramki z ruchem oczu.
    void receiveFrame(FrameCam frame);
    // Odebranie i ustawienie wspolrzednych bazowych.
    void receiveFaceCoor(std::map<QString, facePartCoor>);
    // Ustawienie konfiguracji rysowania ust.
    void setBezierConfig(bool drawBezierControls_ = false,
                         float bezierControlLengthX = 10,
                         float bezierControlLengthZ = 1, float lipsRadius = 3);
    // Ustawienie konfiguracji serw.
    void setServConf(std::vector<ServoConfig>);
signals:
    // Zgloszenie bledu.
    void errorOccured(exception);
    // Zgloszenie informacji o pracy modulu.
    void sendMsg(QString);

private:
    // Ilosc serw.
    unsigned int servAmount;
    // Okres renderowania w ms.
    int Period;
    // Indeks timera.
    int timerId;
    // Nazwa wczytywanego pliku z modelem 3d.
    std::string fileName;
    // Okresla czy obiekty zostaly poprawnie zaladowane.
    bool objectsCorrect;
    // Zmienne zwiazane z wczytywaniem modeli 3d.
    std::vector<std::vector<vertex> > vertexListCont;
    std::vector<std::vector<vertex> > normListCont;
    std::vector<std::vector<face> > faceListCont;
    std::vector<material> matCont;
    std::vector<int> matInd;
    // Nazwy obiektow.
    std::vector<std::string> names;
    // Nazwy poszczegolnych czesci twarzy.
    const std::string FACE;
    const std::string HEAD_BACK;
    const std::string LEFT_EYE;
    const std::string LEFT_EYE_SPOT;
    const std::string RIGHT_EYE;
    const std::string RIGHT_EYE_SPOT;
    const std::string RIGHT_EYE_LID;
    const std::string LEFT_EYE_LID;
    // Przechowywanie modeli.
    std::map<std::string, GLuint*> objCont;
    std::map<std::string, GLuint*>::iterator it;
    unsigned int objAmount;
    // Szybkosc poruszania oczu.
    float eyeVel;
    // Rysowanie wektorow kontrolnych dla krzywych beziera opsiujacych usta.
    bool drawBezierControls;
    // Wspolczynniki beziera.
    std::vector<bezierCoef> bezierCoefs;
    // Dokladnosc estymowania krzywej beziera.
    float bezierPrec;
    // Dlugosc wektorow kontrolnych.
    float bezContLenX;
    float bezContLenZ;
    // Grubos ust.
    float lipsRad;
    // Wspolrzedne bazowe.
    std::vector<facePartCoor> BaseCoor;
    // Wartosci przesuniecia poszczegolnych serw.
    std::vector<double> offset;
    // Aktualnie wykonywane ruchy.
    std::map<Servo::ServoId, Frame> currMoves;
    // Konfiguracje serw.
    std::vector<ServoConfig> servConfigs;
    // Kolor tla.
    QColor purple;


    // Tworzenie prostego modelu ust.
    GLuint generateSimpleLips(facePartCoor Pt1, float Pt1offset,
                              facePartCoor Pt2, float Pt2offset);
    // Wewnetrzne zgloszenie bledu.
    void handleError(exception);
    // Sprawdzenie czy zostaly zainicjalizowane wszystkie obiekty.
    bool checkObjects();
    // Usuwanie wszystkich obiekt�w.
    void deleteObjects();
    // Wczytywanie i parsowanie pliku.
    bool loadFile(std::string fileName);
    // Inicjalizacja obiektow.
    bool initObjs();

protected:
    void initializeGL();
    void paintGL();
    void timerEvent(QTimerEvent*);
    void resizeGL(int width, int height);
};

#endif // HEAD_GL_H

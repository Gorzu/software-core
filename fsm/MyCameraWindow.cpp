
#include "MyCameraWindow.hh"
//#include <iostream>
MyCameraWindow::MyCameraWindow(/*CvCapture *cam,*/ QWidget *parent) : QWidget(parent) {
    //camera = cam;
    QVBoxLayout *layout = new QVBoxLayout;
    cvwidget = new QOpenCVWidget(this);
    PreviewCheckBox1 = new QCheckBox(tr("&preview"));
    PreviewCheckBox1->setChecked(false);
    layout->addWidget(cvwidget);
    layout->addWidget(PreviewCheckBox1);
    setLayout(layout);

    //image=cvLoadImage("gfx/novideo.jpg");  // old
    //tmpsize=cvLoadImage("gfx/novideo_big.jpg"); // old
    tmpsize=cv::imread("gfx/novideo_big.jpg"); 
    image = cv::imread("gfx/novideo.jpg"); 

    startTimer(125);
 }


void MyCameraWindow::timerEvent(QTimerEvent*) {  
if(PreviewCheckBox1->checkState()==2){
    CvSize size = cvSize(3*320,3*240);
    //cvResize(image,tmpsize,CV_INTER_LINEAR);
    cv::resize(image, tmpsize, tmpsize.size(), cv::INTER_LINEAR);
    cvwidget->putImage(tmpsize);
    }
}

#include "head_control_thread.h"

HeadControlThread::HeadControlThread(long sleepTimeMs)
:sleepTimeMs(sleepTimeMs){
	mowa = new Wypowiedz();
	endRequest = false;
	OverrideEyesSpeed = 0.1;
	OverrideHeadSpeed = 0.1;
	target.visionVerticalAngle = 0;
	target.visionHorizontalAngle = 0;
	target.exist = false;
        PermitEyesMove = true;

}

HeadControlThread::~HeadControlThread(){
	endRequest = true;
	delete mowa;
}

void HeadControlThread::setServConf(std::vector<ServoConfig> configs)
{

    servConfigs.clear();
	servConfigs.reserve(Servo::TOTAL);
	servConfigs = std::vector<ServoConfig>(Servo::TOTAL);

    for (unsigned int i=0; i<configs.size(); i++)
    {
        servConfigs[i] = configs[i];
    }
}

SimpleMove HeadControlThread::LookAtTarget(const TargetInfo& target, double eyesUse){
	SimpleMove look;

	double eyesHAngle = eyesUse * target.visionHorizontalAngle/abs(servConfigs[Servo::LeftEyeH].maxPos-servConfigs[Servo::LeftEyeH].minPos) + .5 ;
	double eyesVAngle = eyesUse * target.visionVerticalAngle/abs(servConfigs[Servo::LeftEyeV].maxPos-servConfigs[Servo::LeftEyeV].minPos) + .5 ;
	if (eyesHAngle >1) eyesHAngle =1; if (eyesHAngle <0) eyesHAngle =0;
	if (eyesVAngle >1) eyesVAngle =1; if (eyesVAngle <0) eyesVAngle =0;

	double headHAngle = (1-eyesUse)* target.visionHorizontalAngle/abs(servConfigs[Servo::HeadH].maxPos-servConfigs[Servo::HeadH].minPos) + .5 ;
	double headVAngle = (1-eyesUse)* target.visionVerticalAngle/abs(servConfigs[Servo::HeadV].maxPos-servConfigs[Servo::HeadV].minPos) + .5 ;
	if (headHAngle >1) headHAngle =1; if (headHAngle <0) headHAngle =0;
	if (headVAngle >1) headVAngle =1; if (headVAngle <0) headVAngle =0;

	look.setElement(Servo::LeftEyeH, eyesHAngle, OverrideEyesSpeed);
	look.setElement(Servo::LeftEyeV, eyesVAngle, OverrideEyesSpeed);
	look.setElement(Servo::RightEyeH, eyesHAngle, OverrideEyesSpeed);
	look.setElement(Servo::RightEyeV, eyesVAngle, OverrideEyesSpeed);

	look.setElement(Servo::HeadH, headHAngle, OverrideHeadSpeed);
	look.setElement(Servo::HeadR, headVAngle, OverrideHeadSpeed);

	return look;
}

void HeadControlThread::run()
{
    forever
    {     
		if (endRequest)
			return;
		SafetyLock.lock();

		if( ( !speechQueue.empty() )
			&&( speechQueue.front().executeTime <= QTime::currentTime()) ){
				
				mowa->powiedz(speechQueue.front().soundFileName);
			
				speechQueue.pop();
		}
		

		// Jezeli jest ruch do obsluzenia i "nadszedl jego czas" obsluguje go
		if( ( !simpleMovesQueue.empty() )
			&&( simpleMovesQueue.front().executeTime <= QTime::currentTime()) )
		{
			SimpleMove toSend = simpleMovesQueue.front();
			// dolozenie OverrideEyes czyli sledzenia
			if ( toSend.isSet[Servo::OverrideEyes] )
				toSend = LookAtTarget(target, toSend.servoPosition[Servo::OverrideEyes] ) + toSend;


			sendSimpleMove( toSend );
			simpleMovesQueue.pop();
		}

              if (( !PermitEyesMove)&&( PermitTime < QTime::currentTime() ))
                  PermitEyesMove =true;


		SafetyLock.unlock();
		// Usypia watek
        msleep(sleepTimeMs);
    }
}

void HeadControlThread::targetUpdate(TargetInfo t){
	SafetyLock.lock();
	target = t;
	SafetyLock.unlock();
}

SimpleMove HeadControlThread::SetSpeed( SimpleMove s1 , SimpleMove s2,long  moveTime){
	SimpleMove sm(s2);

	for ( unsigned int i = 0; i <s2.servoVelocity.size(); i++){
		if (s2.isSet[i]){
			sm.servoVelocity[i] = abs((servConfigs[i].maxPos-servConfigs[i].minPos)*(s1.servoPosition[i] - s2.servoPosition[i]))
				/ (moveTime * (s2.normalizedTime - s1.normalizedTime) )
				/ (servConfigs[i].maxVel - servConfigs[i].minVel);
		}
	}

	return sm;

	}

SimpleMove HeadControlThread::ChooseSM(const std::vector<SimpleMove>& list, double moment, long moveTime){

	if (moment <=1){
		for(unsigned int i=0; i < list.size(); i++){
                    if( moment <= list[i].normalizedTime){
				if( i==0){
					return list[i];
				} else{
					return SetSpeed(list[i-1] , list[i], moveTime);
				}
                   }
		}
		return SimpleMove();
	} else {
		return SimpleMove();
	}
};


void HeadControlThread::receiveBFrame(BFrame bframe)
{
	long totalTime = bframe.totalTime;
	int stepTime = sleepTimeMs;
	SafetyLock.lock();
	if ( simpleMovesQueue.empty() ){ // w razie pustej kolejki ustaw czas na teraz
		LastMoveTime = QTime::currentTime();
	}

	// Rozklad na ruchy proste i ich kolejkowanie
	// Uproszczenie dostepu to tablic i czasow
	const std::vector<SimpleMove>& sp = belements->speeches[bframe.speechElems[0].name].simpleMoves;
	const std::vector<SimpleMove>& mov = belements->moves[bframe.moveElems[0].name].simpleMoves;
	const std::vector<SimpleMove>& emo = belements->emotions[bframe.emotionElems[0].name].simpleMoves;
	long spTime = bframe.speechElems[0].totalTime + 1;
	long movTime = bframe.moveElems[0].totalTime + 1;
	long emoTime = bframe.emotionElems[0].totalTime + 1;

	if ( belements->speeches[bframe.speechElems[0].name].soundFileName != "" ){
	speechQueue.push( SpeechQueueElem(
		belements->speeches[bframe.speechElems[0].name].soundFileName,
		LastMoveTime) );
	}
	// Rozlozenie na poszczegolne ramki
	for(double i=0 ; i< totalTime ; i+=stepTime)
	{
		SimpleMove move;
		move = ChooseSM(sp,i/spTime, spTime) + ChooseSM(mov,i/movTime, movTime) + ChooseSM(emo,i/emoTime, emoTime);
		move.executeTime = LastMoveTime.addMSecs(i); //czas kolejnych ruchow prostych
		
		simpleMovesQueue.push(move);
	}
	LastMoveTime = LastMoveTime.addMSecs(totalTime + stepTime);  // Czas wskazujacy na poczatek kolejnego ruchu
	SafetyLock.unlock();

}

void HeadControlThread::receiveSimpleMove(SimpleMove sm){
	SafetyLock.lock();
	simpleMovesQueue.push(sm);
	SafetyLock.unlock();
}

void HeadControlThread::sendSimpleMove(SimpleMove move)
{

	LastMove=move;
        bool usingEyes =false;
	//Wysyla aktywne elementy z ruchu prostego na serwa
	for (int i=0 ; i< Servo::TOTAL ; i++)
	{
		if ( move.isSet[i] )
		{
			// Przypadki dla oczu     
                        if ( ( (i == Servo::LeftEyeV)||(i == Servo::LeftEyeH)) ||( (i == Servo::RightEyeH)||(i == Servo::RightEyeV) )){

			} else {  // Przypadek dla serwomechanizmow
				emit emitFrame(Frame( static_cast<Servo::ServoId>(i) , QTime::currentTime(), 
								move.servoPosition[i], move.servoVelocity[i]));
			}
		}
	}
        if (((move.isSet[Servo::LeftEyeH])||(move.isSet[Servo::LeftEyeV])) && (PermitEyesMove) ){
           emit emitFrame( FrameCam(Servo::LeftEyeH,QTime::currentTime(),
                                        move.servoPosition[Servo::LeftEyeV],move.servoPosition[Servo::LeftEyeH] ));
           usingEyes= true;

        }

        if( ( (move.isSet[Servo::RightEyeH])||(move.isSet[Servo::RightEyeV]) ) && (PermitEyesMove) ){
           emit emitFrame( FrameCam(Servo::RightEyeH,QTime::currentTime(),
                                        move.servoPosition[Servo::RightEyeV],move.servoPosition[Servo::RightEyeH] ));
           usingEyes= true;
        }

        if( usingEyes)
            CalculateNextPermitTime(move);
}

void HeadControlThread::cancelQueue()
{
	SafetyLock.lock();
	while (!simpleMovesQueue.empty())
		simpleMovesQueue.pop();
	while (!speechQueue.empty())
		speechQueue.pop();
	SafetyLock.unlock();
}

void HeadControlThread::SetBElements(BElements *b){
	belements = b;
}

void HeadControlThread::setDrv(Driver * _drv)
{
drv=_drv;
}

void HeadControlThread::initialize()
{

	//wysrodkowanie glowy
        emit emitFrame(Frame(Servo::HeadH,QTime::currentTime(),.5,.05));
        emit emitFrame(Frame(Servo::HeadR,QTime::currentTime(),.5,.05));
        emit emitFrame(Frame(Servo::HeadV,QTime::currentTime(),.5,.05));
        emit emitFrame(Frame(Servo::NeckV,QTime::currentTime(),.5,.05));
        /*
	//wysrodkowanie oczu
        emit emitFrame(FrameCam(Servo::LeftEyeH,QTime::currentTime(),.5,.05));
        emit emitFrame(FrameCam(Servo::RightEyeH,QTime::currentTime(),.5,.05));
        */
	//ustawienie ust
        emit emitFrame(Frame(Servo::BotLeftLips,QTime::currentTime(),.5,.05));
        emit emitFrame(Frame(Servo::BotRightLips,QTime::currentTime(),.5,.05));
        emit emitFrame(Frame(Servo::TopLeftLips,QTime::currentTime(),.5,.05));
        emit emitFrame(Frame(Servo::TopRightLips,QTime::currentTime(),.5,.05));
	//ustawienie brwi
        emit emitFrame(Frame(Servo::LeftEyeLidR,QTime::currentTime(), .45 ,.05));
        emit emitFrame(Frame(Servo::LeftEyeLidV,QTime::currentTime(), .4 ,.05));
        emit emitFrame(Frame(Servo::RightEyeLidR,QTime::currentTime(), .55 ,.05));
        emit emitFrame(Frame(Servo::RightEyeLidV,QTime::currentTime(), .4 ,.05));

}

void HeadControlThread::SetOverrideEyesSpeed(double s){
	SafetyLock.lock();
	OverrideEyesSpeed = s;
	SafetyLock.unlock();
}

void HeadControlThread::SetOverrideHeadSpeed(double s){
	SafetyLock.lock();
	OverrideHeadSpeed = s;
	SafetyLock.unlock();
}

void HeadControlThread::SendSightPosition(){
    //pozycja pozioma
    double hPos = 0;

    hPos += drv->DynServos.GetDeg( Servo::HeadH,LastMove.servoPosition[Servo::HeadH]);

    hPos += drv->DynServos.GetDeg( Servo::RightEyeH,LastMove.servoPosition[Servo::RightEyeH]);

    //pozycja pionowa
    double vPos =0;
    vPos += drv->DynServos.GetDeg( Servo::NeckV,LastMove.servoPosition[Servo::NeckV]);
    vPos += drv->DynServos.GetDeg( Servo::HeadR,LastMove.servoPosition[Servo::HeadR]);
    hPos += drv->DynServos.GetDeg( Servo::RightEyeV,LastMove.servoPosition[Servo::RightEyeV]);

    emit sightPosition(hPos, vPos);
}

void HeadControlThread::CalculateNextPermitTime(const SimpleMove& toSend){
    double msDelay=0;

    if (toSend.isSet[Servo::LeftEyeH] )
    msDelay = fmax(msDelay ,abs( (toSend.servoPosition[Servo::LeftEyeH] - LastEyes.servoPosition[Servo::LeftEyeH])
              * (servConfigs[Servo::LeftEyeH].maxPos - servConfigs[Servo::LeftEyeH].minPos)
              / (servConfigs[Servo::LeftEyeH].maxVel - servConfigs[Servo::LeftEyeH].minVel) ) );

    if (toSend.isSet[Servo::LeftEyeV] )
    msDelay = fmax(msDelay , abs( (toSend.servoPosition[Servo::LeftEyeV] - LastEyes.servoPosition[Servo::LeftEyeV])
              * (servConfigs[Servo::LeftEyeV].maxPos - servConfigs[Servo::LeftEyeV].minPos)
              / (servConfigs[Servo::LeftEyeV].maxVel - servConfigs[Servo::LeftEyeV].minVel) ) );

    if (toSend.isSet[Servo::RightEyeH] )
    msDelay = fmax(msDelay , abs( (toSend.servoPosition[Servo::RightEyeH] - LastEyes.servoPosition[Servo::RightEyeH])
              * (servConfigs[Servo::RightEyeH].maxPos - servConfigs[Servo::RightEyeH].minPos)
              / (servConfigs[Servo::RightEyeH].maxVel - servConfigs[Servo::RightEyeH].minVel) ) );

    if (toSend.isSet[Servo::RightEyeV] )
    msDelay = fmax(msDelay , abs( (toSend.servoPosition[Servo::RightEyeV] - LastEyes.servoPosition[Servo::RightEyeV])
              * (servConfigs[Servo::RightEyeV].maxPos - servConfigs[Servo::RightEyeV].minPos)
              / (servConfigs[Servo::RightEyeV].maxVel - servConfigs[Servo::RightEyeV].minVel) ) );
    PermitEyesMove = false;
    PermitTime = QTime::currentTime().addMSecs(msDelay);
    LastEyes = toSend;


}

void HeadControlThread::startThread(QThread::Priority prior)
{
    start(prior);
}

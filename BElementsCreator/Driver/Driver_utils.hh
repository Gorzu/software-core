#ifndef DRIVER_UTILS_HH
#define DRIVER_UTILS_HH

#include <stdint.h>

/**
 * Konfiguracja pojedynczego serwa.
 */
struct DrvServoConfig
{
  /**
   * Maksymalna wartosc pozycji w stopniach.
   */
  double maxPosDeg;

  /**
   * Minimalna wartosc pozycji w stopniach.
   */
  double minPosDeg;

  /**
   * Maksymalna wartosc pozycji HEX.
   */
  double maxPosHex;

  /**
   * Minimalna wartosc pozycji HEX.
   */
  double minPosHex;

  /**
   * Maksymalna pozycja w HEX.
   */
  uint16_t maxVelHex;

  /**
   * Minimalna pozycha w HEX.
   */
  uint16_t minVelHex;

  /**
   * Return level mowi o sposobie odpowaiadania przez serwo
   * na wysylane komendy.
   */
  int ret_lev;

  DrvServoConfig(double _minPosDeg, double _maxPosDeg,
      		uint16_t _minPosHex, uint16_t _maxPosHex,
		 uint16_t _minVelHex, uint16_t _maxVelHex, int _ret_lev)
  {
    maxPosDeg = _maxPosDeg;
    minPosDeg = _minPosDeg;
    maxPosHex = _maxPosHex;
    minPosHex = _minPosHex;
    maxVelHex = _maxVelHex;
    minVelHex = _minVelHex;
    ret_lev = _ret_lev;
  }
  
};

#endif // DRIVER_UTILS_HH

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtGui/QMainWindow>
#include <QHBoxLayout>
#include <QThread>
#include "head_gl.h"
#include <map>
#include "Driver/Driver.hh"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    void setUpConnections();
    void startThreads();
    void setVis(HeadGL * visA);
    void setDrv(Driver * drvA);
    ~MainWindow();

    QWidget * GLWidget;

public slots:
    void LCamReset();
    void RCamReset();
    void xChanged(int i);
    void xChanged(QString s);
    void yChanged(int i);
    void yChanged(QString s);
    void zChanged(int i);
    void zChanged(QString s);
    void xRotChanged(int i);
    void xRotChanged(QString s);
    void yRotChanged(int i);
    void yRotChanged(QString s);
    void zRotChanged(int i);
    void zRotChanged(QString s);
    void facePartChanged(QString s);
    void updateSliders();
    void lookLeft();
    void lookCenter();
    void lookRight();
    void lookUp();
    void lookMiddle();
    void lookDown();
    void swingLeft();
    void straight();
    void swingRight();
    void eyesLeft();
    void eyesCenter();
    void eyesRight();
    void eyesUp();
    void eyesMiddle();
    void eyesDown();
    void happy();
    void sad();
    void angry();
    void wicked();
    void visChange(bool);
    void showError(exception);
    void showMsg(QString);
signals:
    void startMainThread(QThread::Priority prior);
    void startHeadControlThread(QThread::Priority prior);
    void changeX(int);
    void changeY(int);
    void changeZ(int);
    void changeXrot(int);
    void changeYrot(int);
    void changeZrot(int);
    void faceCoorChanged(std::map<QString, facePartCoor>);
    void emitFrame(Frame);
    void emitFrame(FrameCam);

private:
    Ui::MainWindow *ui;
    QHBoxLayout * mainLayout;
    HeadGL * vis;
    Driver * drv;

    std::map<QString, facePartCoor> faceCoor;
    QString currSel;

    float posFactor;

};

#endif // MAIN_WINDOW_H

#include "main_window.h"

MainWindow::MainWindow():QMainWindow(0)
{
	
    ui = new Ui::MainWindow;

    ui->setupUi(this);

	GLWidget = ui->visWidget;

	ui->SimpleMovesBox->hide();
	ui->SimpleMoveBox->hide();
	ui->SoundBox->hide();
}
MainWindow::~MainWindow()
{
	headControl->terminate();
	delete headControl;
    delete ui;
}

void MainWindow::setVis(HeadGL * _vis)
{
    vis = _vis;
}

void MainWindow::setHeadControlThread(HeadControlThread *_headControl)
{
	headControl = _headControl;
	headControl->SetBElements( &belements);
}

void MainWindow::setDrv(Driver * _drv)
{
    drv = _drv;
}

void MainWindow::ResetFaceButton(){
	headControl -> initialize();
}

void MainWindow::SendHeadBtn()
{
	TargetInfo h;
		
	h.visionHorizontalAngle = - ui->headHorizontalSlider->value();
	h.visionVerticalAngle = - ui->headVerticalSlider->value();
	h.exist = ui->headExistBox->isChecked();
	h.newTarget = ui->NewTargetBox->isChecked();
	h.crowd = ui->CrowdBox->isChecked();

	h.soundHorizontalAngle = - ui->soundHorizontalSlider->value();
	h.soundVerticalAngle = - ui->soundVerticalSlider->value();
	h.targetSpeaking = ui->TargetSpeakingBox->isChecked();
	h.otherSpeaking = ui->OtherSpeakingBox->isChecked();

	emit sendHead(h);
			
}

std::vector<ServoConfig> GetServoConfigs(){
	std::vector<ServoConfig> servConfigs(Servo::TOTAL);

    servConfigs[Servo::HeadH] = ServoConfig(50, -50, 0, .684);
    servConfigs[Servo::HeadR] = ServoConfig(-50, 50, 0, .684);
    servConfigs[Servo::HeadV] = ServoConfig(-50, 50, 0, .684);
	servConfigs[Servo::NeckV] = ServoConfig(-50, 50, 0, .684);

    servConfigs[Servo::LeftEyeH] = ServoConfig(40, -40, 0, .05);
    servConfigs[Servo::RightEyeH] = ServoConfig(40, -40, 0, .05);
    servConfigs[Servo::LeftEyeV] = ServoConfig(-20, 20, 0, .05);
    servConfigs[Servo::RightEyeV] = ServoConfig(-20, 20, 0, .05);

    servConfigs[Servo::RightEyeLidV] = ServoConfig(-16, 12, 0, .684);
    servConfigs[Servo::LeftEyeLidV] = ServoConfig(-16, 12, 0, .684);
    servConfigs[Servo::RightEyeLidR] = ServoConfig(-10, 25, 0, .684);
    servConfigs[Servo::LeftEyeLidR] = ServoConfig(-25, 10, 0, .684);
    servConfigs[Servo::TopRightLips] = ServoConfig(-55, 55, 0, .684);
    servConfigs[Servo::TopLeftLips] = ServoConfig(-55, 55, 0, .684);
    servConfigs[Servo::BotRightLips] = ServoConfig(-55, 55, 0, .684);
    servConfigs[Servo::BotLeftLips] = ServoConfig(-55, 55, 0, .684);
	return servConfigs;
}

void MainWindow::setUpConnections()
{
	// Rejestracja typow uzywanych w sygnalach
	qRegisterMetaType<Frame>("Frame");
	qRegisterMetaType<FrameCam>("FrameCam");
	qRegisterMetaType<BFrame>("BFrame");
	qRegisterMetaType<TargetInfo>("TargetInfo");

	// Polaczenia HeadControl z wizualizacja dla serw i kamer
	connect(headControl, SIGNAL(emitFrame(Frame)),
			vis, SLOT(receiveFrame(Frame)));
	connect(headControl, SIGNAL(emitFrame(FrameCam)),
			vis, SLOT(receiveFrame(FrameCam)));

//=============================
//polaczenie HeadControl z modulem Driver
   connect(headControl, SIGNAL(emitFrame(Frame)), drv, SLOT(receiveFrame(Frame)));
    connect(headControl, SIGNAL(emitFrame(FrameCam)),
	    drv, SLOT(receiveFrame(FrameCam)));
//=============================
	
	// Umozliwia temu oknu wysylanie ramek zachowan do HC; oraz kasowanie kolejki
	connect(this,SIGNAL(emitBFrame(BFrame)),
			headControl,SLOT(receiveBFrame(BFrame)));
	connect(this,SIGNAL(cancelQueue()),
			headControl, SLOT(cancelQueue()));


	// Ustawianie predkosci podazania
	connect(ui->EyesSpeedSpinBox, SIGNAL(valueChanged(double)),
			headControl, SLOT(SetOverrideEyesSpeed(double)));
	connect(ui->HeadSpeedSpinBox, SIGNAL(valueChanged(double)),
			headControl, SLOT(SetOverrideHeadSpeed(double)));


	//symulator glowy
	connect (this,SIGNAL( sendHead(TargetInfo) ),
			headControl, SLOT( targetUpdate(TargetInfo) ));

	//Ustalenie poczatkowej konfiguracji serw
	headControl->setServConf( GetServoConfigs() );
	vis->setServConf ( GetServoConfigs() );
	
	headControl->initialize();
}

void MainWindow::NewBElements(){
	belements = BElements();
	ListRefresh();
}

void MainWindow::OpenBElements(){
	QString file = QFileDialog::getOpenFileName(this,
		tr("Open XML"), "xml", tr("XML Files (*.xml)"));
	belements = GetBElementsFromFile(file);
	ListRefresh();
}

void MainWindow::SaveBElements(){
	QString file = QFileDialog::getSaveFileName(this,
		tr("Save XML"), "xml", tr("XML Files (*.xml)"));
	WriteBElementsToFile(belements, file);
}

void MainWindow::ListRefresh(){

	ui->SpeechesList->clear();
	ui->SpeechComboBox->clear();
	for (std::map<QString, SpeechSet>::iterator it = belements.speeches.begin() ; it != belements.speeches.end() ; it++){
		ui->SpeechesList->addItem(it->second.name);
		ui->SpeechComboBox->addItem(it->second.name);
	}

	ui->MovesList->clear();
	ui->MoveComboBox->clear();
	for (std::map<QString, MovesSet>::iterator it = belements.moves.begin() ; it != belements.moves.end() ; it++){
		ui->MovesList->addItem(it->second.name);
		ui->MoveComboBox->addItem(it->second.name);
	}

	ui->EmotionsList->clear();
	ui->EmotionComboBox->clear();
	for (std::map<QString, MovesSet>::iterator it = belements.emotions.begin() ; it != belements.emotions.end() ; it++){
		ui->EmotionsList->addItem(it->second.name);
		ui->EmotionComboBox->addItem(it->second.name);
	}

}

void MainWindow::SimpleMovesRefresh(){
	QString s1;
	ui->SimpleMovesList->clear();
	for(unsigned int i=0; i < tempSet.simpleMoves.size() ; i++){
		ui->SimpleMovesList->addItem(s1.setNum(i));
	}
}

void MainWindow::AddSpeechButton(){
	if ((ui->SpeechesEdit->text().trimmed().isEmpty()) 
		|| (belements.speeches.find( ui->SpeechesEdit->text().trimmed() ) != belements.speeches.end() )  )
		return;
	SpeechSet sp;
	sp.name = ui->SpeechesEdit->text().trimmed();
	belements.speeches[sp.name] = sp;
	ui->SpeechesEdit->setText("");
	ListRefresh();
}

void MainWindow::DeleteSpeechButton(){
	if (ui->SpeechesList->currentRow() < 0 )
		return;
	belements.speeches.erase( belements.speeches.find( ui->SpeechesList->currentItem()->text() ));
	ListRefresh();
}

void MainWindow::ReplicateSpeechButton(){
	if (ui->SpeechesList->currentRow() < 0 )
		return;
	if ((ui->SpeechesEdit->text().trimmed().isEmpty()) 
		|| (belements.speeches.find( ui->SpeechesEdit->text().trimmed() ) != belements.speeches.end() )  )
		return;
	SpeechSet sp = belements.speeches.find( ui->SpeechesList->currentItem()->text() )->second;
	sp.name = ui->SpeechesEdit->text().trimmed();
	belements.speeches[sp.name] = sp;
	ui->SpeechesEdit->setText("");
	ListRefresh();
}

void MainWindow::AddMoveButton(){
	if ((ui->MovesEdit->text().trimmed().isEmpty()) 
		|| (belements.moves.find( ui->MovesEdit->text().trimmed() ) != belements.moves.end() )  )
		return;
	MovesSet sp;
	sp.name = ui->MovesEdit->text().trimmed();
	belements.moves[sp.name] = sp;
	ui->MovesEdit->setText("");
	ListRefresh();
}

void MainWindow::DeleteMoveButton(){
	if (ui->MovesList->currentRow() < 0 )
		return;
	belements.moves.erase( belements.moves.find( ui->MovesList->currentItem()->text() ));
	ListRefresh();
}

void MainWindow::ReplicateMoveButton(){
	if (ui->MovesList->currentRow() < 0 )
		return;
	if ((ui->MovesEdit->text().trimmed().isEmpty()) 
		|| (belements.moves.find( ui->MovesEdit->text().trimmed() ) != belements.moves.end() )  )
		return;
	MovesSet sp = belements.moves.find( ui->MovesList->currentItem()->text() )->second;
	sp.name = ui->MovesEdit->text().trimmed();
	belements.moves[sp.name] = sp;
	ui->MovesEdit->setText("");
	ListRefresh();
}

void MainWindow::AddEmotionButton(){
	if ((ui->EmotionsEdit->text().trimmed().isEmpty()) 
		|| (belements.emotions.find( ui->EmotionsEdit->text().trimmed() ) != belements.emotions.end() )  )
		return;
	MovesSet sp;
	sp.name = ui->EmotionsEdit->text().trimmed();
	belements.emotions[sp.name] = sp;
	ui->EmotionsEdit->setText("");
	ListRefresh();
}

void MainWindow::DeleteEmotionButton(){
	if (ui->EmotionsList->currentRow() < 0 )
		return;
	belements.emotions.erase( belements.emotions.find( ui->EmotionsList->currentItem()->text() ));
	ListRefresh();
}

void MainWindow::ReplicateEmotionButton(){
	if (ui->EmotionsList->currentRow() < 0 )
		return;
	if ((ui->EmotionsEdit->text().trimmed().isEmpty()) 
		|| (belements.emotions.find( ui->EmotionsEdit->text().trimmed() ) != belements.emotions.end() )  )
		return;
	MovesSet sp = belements.emotions.find( ui->EmotionsList->currentItem()->text() )->second;
	sp.name = ui->EmotionsEdit->text().trimmed();
	belements.emotions[sp.name] = sp;
	ui->EmotionsEdit->setText("");
	ListRefresh();
}

void MainWindow::ElementChosen(int ind){
	if (ind <0){
		ui->SimpleMovesBox->hide();
		return;
	} 
	
	switch( ui->BElementsTab->currentIndex() ){
	case 0:
		tempSet = belements.speeches[ ui->SpeechesList->currentItem()->text() ];
		ui->SoundFileEdit->setText( tempSet.soundFileName );
		ui->SoundBox->show();
		break;
	case 1:
		tempSet.name =  belements.moves[ ui->MovesList->currentItem()->text() ].name ;
		tempSet.simpleMoves =  belements.moves[ ui->MovesList->currentItem()->text() ].simpleMoves ;
		ui->SoundBox->hide();
		break;
	case 2:
		tempSet.name = belements.emotions[ ui->EmotionsList->currentItem()->text() ].name ;
		tempSet.simpleMoves = belements.emotions[ ui->EmotionsList->currentItem()->text() ].simpleMoves ;
		ui->SoundBox->hide();
		break;
	}
	ui->SimpleMovesBox->show();
	SimpleMovesRefresh();	
}

void MainWindow::AddSimpleMoveButton(){
	tempSet.simpleMoves.push_back( SimpleMove() );
	SimpleMovesRefresh();
}

void MainWindow::ReplicateSimpleMoveButton(){
	int ind = ui->SimpleMovesList->currentRow();
	if (ind < 0)
		return;
	tempSet.simpleMoves.insert( tempSet.simpleMoves.begin() + ind +1 , tempSet.simpleMoves[ind] ); 
	SimpleMovesRefresh();
	ui->SimpleMovesList->setCurrentRow(ind +1);
}

void MainWindow::DeleteSimpleMoveButton(){
	int ind = ui->SimpleMovesList->currentRow();
	if (ind < 0)
		return;
	tempSet.simpleMoves.erase( tempSet.simpleMoves.begin() + ind);
	SimpleMovesRefresh();
}

void MainWindow::SimpleMoveUpButton(){
	int ind = ui->SimpleMovesList->currentRow();
	if (ind < 1)   // zaden, lub zerowy element, ktory wyzej isc nie moze
		return;
	std::swap( tempSet.simpleMoves[ind] , tempSet.simpleMoves[ind-1]);
	ui->SimpleMovesList->setCurrentRow( ind-1);
}

void MainWindow::SimpleMoveDownButton(){
	int ind = ui->SimpleMovesList->currentRow();
	if ((ind < 0) || ( ind + 1 == tempSet.simpleMoves.size() ))   // zaden, lub ostatni element
		return;
	std::swap( tempSet.simpleMoves[ind] , tempSet.simpleMoves[ind+1]);
	ui->SimpleMovesList->setCurrentRow( ind+1);
}

void MainWindow::SoundPlayButton(){
	if ( ui->SoundFileEdit->text().trimmed().isEmpty() )
		return;
	headControl->mowa->powiedz( ui->SoundFileEdit->text().trimmed() );
}

void MainWindow::SoundFileFindButton(){
    QFileInfo pathInfo( QFileDialog::getOpenFileName(this,
		tr("Open Sound"), "sounds", tr("wav Files (*.wav)"), 0 , QFileDialog::DontResolveSymlinks ) );
    ui->SoundFileEdit->setText( pathInfo.fileName() );

}

void MainWindow::SoundSaveButton(){
	tempSet.soundFileName = ui->SoundFileEdit->text();
}

void MainWindow::SaveSetButton(){
	int ind;
	switch( ui->BElementsTab->currentIndex() ){
	case 0:
		ind = ui->SpeechesList->currentRow();
		if (ind <0)
			return;
		belements.speeches[ui->SpeechesList->currentItem()->text()] = tempSet;
		break;
	case 1:
		ind = ui->MovesList->currentRow();
		if (ind <0)
			return;
		belements.moves[ui->MovesList->currentItem()->text()] = (MovesSet) tempSet;
		break;
	case 2:
		ind = ui->EmotionsList->currentRow();
		if (ind <0)
			return;
		belements.emotions[ui->EmotionsList->currentItem()->text()] = (MovesSet) tempSet;
		break;
	}
}

void MainWindow::SaveSimpleMoveButton(){
	tempSet.simpleMoves[ui->SimpleMovesList->currentRow()] = ReadSliders();
}

void MainWindow::SMChosen(int ind){
	if (ind < 0){
		ui->SimpleMoveBox->hide();	
		return;
	}
	SetSliders(tempSet.simpleMoves[ind]);
	ui->SimpleMoveBox->show();

}

void MainWindow::SetSliders(SimpleMove sm){
	ui->NormalizedTimeSpinBox->setValue( sm.normalizedTime );

	ui->ServoOverrideEyesCheckBox->setChecked(sm.isSet[Servo::OverrideEyes]);
	ui->ServoOverrideEyesSlider->setValue(sm.servoPosition[Servo::OverrideEyes]*100);

	ui->ServoHeadVCheckBox->setChecked(sm.isSet[Servo::HeadV]);
	ui->ServoHeadVSlider->setValue(sm.servoPosition[Servo::HeadV]*100);

	ui->ServoHeadHCheckBox->setChecked(sm.isSet[Servo::HeadH]);
	ui->ServoHeadHSlider->setValue(sm.servoPosition[Servo::HeadH]*100);

	ui->ServoNeckVCheckBox->setChecked(sm.isSet[Servo::NeckV]);
	ui->ServoNeckVSlider->setValue(sm.servoPosition[Servo::NeckV]*100);

	ui->ServoLeftEyeVCheckBox->setChecked(sm.isSet[Servo::LeftEyeV]);
	ui->ServoLeftEyeVSlider->setValue(sm.servoPosition[Servo::LeftEyeV]*100);

	ui->ServoLeftEyeHCheckBox->setChecked(sm.isSet[Servo::LeftEyeH]);
	ui->ServoLeftEyeHSlider->setValue(sm.servoPosition[Servo::LeftEyeH]*100);

	ui->ServoLeftEyeLidVCheckBox->setChecked(sm.isSet[Servo::LeftEyeLidV]);
	ui->ServoLeftEyeLidVSlider->setValue(sm.servoPosition[Servo::LeftEyeLidV]*100);

	ui->ServoLeftEyeLidRCheckBox->setChecked(sm.isSet[Servo::LeftEyeLidR]);
	ui->ServoLeftEyeLidRSlider->setValue(sm.servoPosition[Servo::LeftEyeLidR]*100);

	ui->ServoRightEyeVCheckBox->setChecked(sm.isSet[Servo::RightEyeV]);
	ui->ServoRightEyeVSlider->setValue(sm.servoPosition[Servo::RightEyeV]*100);

	ui->ServoRightEyeHCheckBox->setChecked(sm.isSet[Servo::RightEyeH]);
	ui->ServoRightEyeHSlider->setValue(sm.servoPosition[Servo::RightEyeH]*100);

	ui->ServoRightEyeLidVCheckBox->setChecked(sm.isSet[Servo::RightEyeLidV]);
	ui->ServoRightEyeLidVSlider->setValue(sm.servoPosition[Servo::RightEyeLidV]*100);

	ui->ServoRightEyeLidRCheckBox->setChecked(sm.isSet[Servo::RightEyeLidR]);
	ui->ServoRightEyeLidRSlider->setValue(sm.servoPosition[Servo::RightEyeLidR]*100);

	ui->ServoTopLeftLipsCheckBox->setChecked(sm.isSet[Servo::TopLeftLips]);
	ui->ServoTopLeftLipsSlider->setValue(sm.servoPosition[Servo::TopLeftLips]*100);

	ui->ServoTopRightLipsCheckBox->setChecked(sm.isSet[Servo::TopRightLips]);
	ui->ServoTopRightLipsSlider->setValue(sm.servoPosition[Servo::TopRightLips]*100);

	ui->ServoBotLeftLipsCheckBox->setChecked(sm.isSet[Servo::BotLeftLips]);
	ui->ServoBotLeftLipsSlider->setValue(sm.servoPosition[Servo::BotLeftLips]*100);

	ui->ServoBotRightLipsCheckBox->setChecked(sm.isSet[Servo::BotRightLips]);
	ui->ServoBotRightLipsSlider->setValue(sm.servoPosition[Servo::BotRightLips]*100);

	ui->ServoHeadRCheckBox->setChecked(sm.isSet[Servo::HeadR]);
	ui->ServoHeadRSlider->setValue(sm.servoPosition[Servo::HeadR]*100);
}

SimpleMove MainWindow::ReadSliders(){
	SimpleMove s;
	
	s.normalizedTime = ui->NormalizedTimeSpinBox->value();
	//Override i glowa
	if( ui->ServoOverrideEyesCheckBox->isChecked() )
                s.setElement(Servo::OverrideEyes, (double)ui->ServoOverrideEyesSlider->value()/100, .1);
	if( ui->ServoHeadVCheckBox->isChecked() )
                s.setElement(Servo::HeadV, (double)ui->ServoHeadVSlider->value()/100, .1);
	if( ui->ServoHeadHCheckBox->isChecked() )
                s.setElement(Servo::HeadH, (double)ui->ServoHeadHSlider->value()/100, .1);
	if( ui->ServoHeadRCheckBox->isChecked() )
                s.setElement(Servo::HeadR, (double)ui->ServoHeadRSlider->value()/100, .1);
	if( ui->ServoNeckVCheckBox->isChecked() )
                s.setElement(Servo::NeckV, (double)ui->ServoNeckVSlider->value()/100, .1);

	// Oko lewe
	if( ui->ServoLeftEyeVCheckBox->isChecked() )
                s.setElement(Servo::LeftEyeV, (double)ui->ServoLeftEyeVSlider->value()/100, .1);
	if( ui->ServoLeftEyeHCheckBox->isChecked() )
                s.setElement(Servo::LeftEyeH, (double)ui->ServoLeftEyeHSlider->value()/100, .1);

	if( ui->ServoLeftEyeLidVCheckBox->isChecked() )
                s.setElement(Servo::LeftEyeLidV, (double)ui->ServoLeftEyeLidVSlider->value()/100, .1);
	if( ui->ServoLeftEyeLidRCheckBox->isChecked() )
                s.setElement(Servo::LeftEyeLidR, (double)ui->ServoLeftEyeLidRSlider->value()/100, .1);

	// Oko prawe
	if( ui->ServoRightEyeVCheckBox->isChecked() )
                s.setElement(Servo::RightEyeV, (double)ui->ServoRightEyeVSlider->value()/100, .1);
	if( ui->ServoRightEyeHCheckBox->isChecked() )
                s.setElement(Servo::RightEyeH, (double)ui->ServoRightEyeHSlider->value()/100, .1);

	if( ui->ServoRightEyeLidVCheckBox->isChecked() )
                s.setElement(Servo::RightEyeLidV, (double)ui->ServoRightEyeLidVSlider->value()/100, .1);
	if( ui->ServoRightEyeLidRCheckBox->isChecked() )
                s.setElement(Servo::RightEyeLidR, (double)ui->ServoRightEyeLidRSlider->value()/100, .1);

	// Usta
	if( ui->ServoTopLeftLipsCheckBox->isChecked() )
                s.setElement(Servo::TopLeftLips, (double)ui->ServoTopLeftLipsSlider->value()/100, .1);
	if( ui->ServoTopRightLipsCheckBox->isChecked() )
                s.setElement(Servo::TopRightLips, (double)ui->ServoTopRightLipsSlider->value()/100, .1);

	if( ui->ServoBotLeftLipsCheckBox->isChecked() )
                s.setElement(Servo::BotLeftLips, (double)ui->ServoBotLeftLipsSlider->value()/100, .1);
	if( ui->ServoBotRightLipsCheckBox->isChecked() )
                s.setElement(Servo::BotRightLips, (double)ui->ServoBotRightLipsSlider->value()/100, .1);

	return s;
}

void MainWindow::slidersChanged(){
	if (! ui->SimpleMovePreviewBox->isChecked() )
		return;
	SimpleMove sm = ReadSliders();
	headControl->cancelQueue();
	headControl->receiveSimpleMove(sm);
}

void MainWindow::SetTrackingCheckBox(bool b){
	ui->ServoOverrideEyesSlider->setTracking(b);
	ui->ServoHeadVSlider->setTracking(b);
	ui->ServoHeadHSlider->setTracking(b);
	ui->ServoNeckVSlider->setTracking(b);
	ui->ServoLeftEyeVSlider->setTracking(b);
	ui->ServoLeftEyeHSlider->setTracking(b);
	ui->ServoLeftEyeLidVSlider->setTracking(b);
	ui->ServoLeftEyeLidRSlider->setTracking(b);
	ui->ServoRightEyeVSlider->setTracking(b);
	ui->ServoRightEyeHSlider->setTracking(b);
	ui->ServoRightEyeLidVSlider->setTracking(b);
	ui->ServoRightEyeLidRSlider->setTracking(b);
	ui->ServoTopLeftLipsSlider->setTracking(b);
	ui->ServoTopRightLipsSlider->setTracking(b);
	ui->ServoBotLeftLipsSlider->setTracking(b);
	ui->ServoBotRightLipsSlider->setTracking(b);
	ui->ServoHeadRSlider->setTracking(b);
}

void MainWindow::SendBFrameButton(){
	BFrame bframe;
	bframe.speechElems.push_back( Elem( ui->SpeechComboBox->currentText() , 1 , ui->SpeechTimeSpinBox->value() ) );
	bframe.moveElems.push_back( Elem( ui->MoveComboBox->currentText() , 1 , ui->MoveTimeSpinBox->value() ) );
	bframe.emotionElems.push_back( Elem( ui->EmotionComboBox->currentText() , 1 , ui->EmotionTimeSpinBox->value() ) );
	bframe.totalTime = ui->TotalTimeSpinBox->value();
	emit emitBFrame( bframe );
}







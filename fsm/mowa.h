#ifndef MOWA_H
#define MOWA_H

#include <phonon>
#include <AudioOutput>
#include <MediaObject>
#include <MediaSource>

//#include "/usr/include/qt4/phonon/mediaobject.h"
//#include "/usr/include/qt4/phonon/audiooutput.h"
//#include "/usr/include/qt4/phonon/volumeslider.h"
//#include "/usr/include/qt4/phonon/seekslider.h"


//Rozroznienie stosowanych znakow w sciezkach dostepu
// pod windowsem i pod linuksem.
#ifdef Q_WS_X11
 static const QString sciezka = "sounds/";
#endif

#ifdef Q_WS_WIN
 static const QString sciezka = "sounds\\";
#endif



// Maksymalna glebokosc balansu (patrz dokumentacja)
static const int MAX_BALANS = 7;

// Ustawienie mozliwosci rownoczesnego odtwarzania kilku plikow
static const bool ROWNOLEGLE_ODTWARZANIE = false;


class Wypowiedz :QObject
{

public:

    //  Nazwa pliku do odtworzenia wraz ze sciezka i rozszerzeniem.
    //  patrz dokumentacja i plik zrodlowy mainwindow.cpp, funkcja wymowa().
    QString plik;

	Phonon::MediaObject *mediaObject;
	Phonon::AudioOutput *audioOutput;

    Wypowiedz();
	~Wypowiedz();
    bool powiedz(QString);
    void run();
};


#endif // MOWA_H

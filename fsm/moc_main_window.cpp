/****************************************************************************
** Meta object code from reading C++ file 'main_window.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "main_window.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'main_window.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      16,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       6,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   46,   52,   52, 0x05,
      53,   46,   52,   52, 0x05,
      95,   52,   52,   52, 0x05,
     114,   52,   52,   52, 0x05,
     131,   52,   52,   52, 0x05,
     145,   52,   52,   52, 0x05,

 // slots: signature, parameters, type, tag, flags
     166,   52,   52,   52, 0x0a,
     185,   52,   52,   52, 0x0a,
     196,   52,   52,   52, 0x0a,
     212,   52,   52,   52, 0x0a,
     230,   52,   52,   52, 0x0a,
     244,   52,   52,   52, 0x0a,
     260,   52,   52,   52, 0x0a,
     270,   52,   52,   52, 0x0a,
     286,  307,   52,   52, 0x0a,
     312,  307,   52,   52, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0startMainThread(QThread::Priority)\0"
    "prior\0\0startHeadControlThread(QThread::Priority)\0"
    "emitBFrame(BFrame)\0emitFrame(Frame)\0"
    "cancelQueue()\0sendHead(TargetInfo)\0"
    "writeText(QString)\0startFsm()\0"
    "StopFSMButton()\0ResetEyesButton()\0"
    "headSendBtn()\0VisEnable(bool)\0LoadFSM()\0"
    "LoadBElements()\0LoadFSMFile(QString)\0"
    "file\0LoadBElementsFile(QString)\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->startMainThread((*reinterpret_cast< QThread::Priority(*)>(_a[1]))); break;
        case 1: _t->startHeadControlThread((*reinterpret_cast< QThread::Priority(*)>(_a[1]))); break;
        case 2: _t->emitBFrame((*reinterpret_cast< BFrame(*)>(_a[1]))); break;
        case 3: _t->emitFrame((*reinterpret_cast< Frame(*)>(_a[1]))); break;
        case 4: _t->cancelQueue(); break;
        case 5: _t->sendHead((*reinterpret_cast< TargetInfo(*)>(_a[1]))); break;
        case 6: _t->writeText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 7: _t->startFsm(); break;
        case 8: _t->StopFSMButton(); break;
        case 9: _t->ResetEyesButton(); break;
        case 10: _t->headSendBtn(); break;
        case 11: _t->VisEnable((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 12: _t->LoadFSM(); break;
        case 13: _t->LoadBElements(); break;
        case 14: _t->LoadFSMFile((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 15: _t->LoadBElementsFile((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 16)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 16;
    }
    return _id;
}

// SIGNAL 0
void MainWindow::startMainThread(QThread::Priority _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainWindow::startHeadControlThread(QThread::Priority _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainWindow::emitBFrame(BFrame _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MainWindow::emitFrame(Frame _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MainWindow::cancelQueue()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}

// SIGNAL 5
void MainWindow::sendHead(TargetInfo _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}
QT_END_MOC_NAMESPACE

#ifndef DYNAMIXEL_HH
#define DYNAMIXEL_HH

#include <cstdlib>
#include <iostream>
#include <stdint.h>
#include <cstring>
#include <QObject>
#include "Driver_utils.hh"
#include "../utils.h"
#include "dyn4lin.h"

/**
 * @brief Klasa do komunikacji z serwami Dynamixel'a.
 *
 * Klasa stanowi pomost pomiędzy aplikacją,
 * a portem szeregowym.
 */
class Dynamixel : public QObject
{

public:

  /**
   * Konstruktor.
   */
  Dynamixel();

  /**
   * Destruktor zamyka połączenie z portem szeregowy.
   */
  ~Dynamixel();

  /**
   * Medota inicjalizująca pola i alokująca
   * pamięć dla struktor danych modułu.
   * @param _Config wskaźnik do wektora konfiguracji serw.
   * @param ServosDebug Jeśli true, na ekran wypisywane
   * będą wysyłane i odbierane ramki z portu szeregowego.
   * @return void
   */
  void InitDynamixelModule(std::vector<DrvServoConfig> * _Config,
			   bool ServosDebug,
			   std::string dev);

  /**
   * Metoda pozwalająca na odpytanie serwo czy jest w ruchu.
   * @return Zwraca true jeśli serwo jest w ruchu.
   */
  bool IsMoving(uint8_t ID);

  /**
   * Pozwala na ustawienie nowej pozycji serwa.
   * @param NewPos nowa pozycja serwa z dopuszczalnego
   * przedziału.
   * @return void
   */
  void SetPos(uint8_t ID, uint16_t NewPos);

  /**
   * Pozwala na ustawienie prędkości ruchów serwa.
   * @param NewSpeed prędkość z dopuszczalnego przedziału.
   * @return void
   */
  void SetSpeed(uint8_t ID, uint16_t NewSpeed);

  /**
   * Tablica ostatnio wysłanych pozycji.
   * Pamiętając te dane, nie wysyłamy niepotrzebnych
   * informacji przez port szeregowy.
   */
  uint16_t LastPositions[Servo::TOTAL];

  /**
   * Tablica ostatnio wysłanych prędkości.
   * Pamiętając te dane, nie wysyłamy niepotrzebnych
   * informacji przez port szeregowy.
   */
  uint16_t LastVelocities[Servo::TOTAL];


double GetDeg(Servo::ServoId id, double norm);


private:

  /**
   * Wskaźnik do wektora konfiguracji serw.
   */
   std::vector<DrvServoConfig> * Configs;

  /**
   * Tablica struktur dla serw.
   */
  dyn_servo_t* dynServoArray;

  /**
   * Struktura parametrów dla biblioteki 
   * obsługi serw.
   */
  dyn_param_t dyn_param;

  /**
   * Wskaźnik do struktury parametrów dla biblioteki 
   * obsługi serw.
   */ 
  dyn_param_t* dpp;

  /**
   * Metoda inicjalizująca struktury danych.
   * @return void
   */
  void InitServoArray();

  /**
   * Metoda uruchamiana podczas inicjalizacji.
   * Ustawia serwa w pozycji 0 (w celu zachowania
   * zgodności z wizualizacją).
   * @return void
   */
  void CheckAndResetPositions();

  /**
   * True, gdy udało się przeprowadzić inicjalizację
   * portu szeregowego i alokacje pamieci.
   */
  bool ModuleWorks;

  /**
   * Online[i]==true gdy serwo odpowiedziało
   * podczas inicjalizacji.
   */
  bool Online[Servo::TOTAL];

};

#endif // DYNAMIXEL_HH

#ifndef FACEDETECTOR_HH_
#define FACEDETECTOR_HH_
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/core/core_c.h"
#include "opencv2/videoio/legacy/constants_c.h"
#include "opencv2/highgui/highgui_c.h"
#include <opencv2/imgproc/types_c.h>
#include "opencv2/objdetect.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/core/types.hpp>
//#include "GL/glu.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <list>
#include <time.h>
#include <QThread>
#include <QMutex>
#include <QObject>
#include <sys/time.h>
#include <sys/types.h>
#include "libsls.h"
#include "../moves.h"
#include "../Driver/Driver.hh"
#include "../MyCameraWindow.hh"

#define FACEDETECTOR_CONFIG_XML "cfg/FaceDetectorConfig.xml"

const int premia_dzwieku = 25;
const int granica_ufnosci = 40;
const int przyrost_ufnosci = 10;
const int spadek_ufnosci = 5;

struct Face {
	Face() {
		z = 10;
		Tx = 100;
		Ty = 50;
		wykryto = true;
	}

	int x, y; //wspolrzedne srodka
	int z; //poziom ufnosci
	int Tx, Ty; //tolerancja
	CvPoint pt1, pt2; //wspolrzedne prostokata
	int ID;
	bool wykryto;

	bool Sprawdz(Face a) {
		if ((x < a.x + Tx && x > a.x - Tx) && (y < a.y + Ty && y > a.y - Ty)) {
			wykryto = true;
			x = a.x;
			y = a.y;
			pt1 = a.pt1;
			pt2 = a.pt2;
			Tolerancja();
			return true;
		} else
			return false;
	}

	void ufaj() {
           // z+=przyrost_ufnosci;
           // if(z>granica_ufnosci){
            //    z = 80;
            //    }
                if (z < 200)
                        z += przyrost_ufnosci;
                if(z > granica_ufnosci)
                        if(z<200)
                                z+= przyrost_ufnosci;
	}
	void nieufaj() {
		z -= spadek_ufnosci;
	}

	void Tolerancja() //dynamiczny dobor tolerancji	
	{
		//Tx = (pt2.x - pt1.x)*0.5;
		//Ty = (pt2.y - pt1.y)*0.3;		
	}

};

class FaceDetector: public QThread {
Q_OBJECT
public:
	FaceDetector();
	//~FaceDetector();
	void run();
	void setDrv(Driver* drv);
void setWin(MyCameraWindow* _win); 
	std::list<Face> FACES;
        //IplImage* curr_img;
		cv::Mat curr_img;

	QMutex faceMutex;
public slots:
	void receivePosition(double hAngle, double vAngle);
	void runningRequest(bool req);
signals:
	void sendTargetInfo(TargetInfo);

private:
MyCameraWindow* win;
	//int wykryj_filtruj_rysuj(IplImage* img);
	int wykryj_filtruj_rysuj(cv::Mat img);
	TargetInfo target;
	Driver* _drv;
	volatile bool shouldRun;//1
	
	//cv::CvMemStorage* storage;//0 // old
	cv::Mat storage; // output array
	
	//CvHaarClassifierCascade* cascade;//0 // old
	//cv::CascadeClassifier* cascade;
	cv::CascadeClassifier cascade;

	int wartosc_przeguby_x; //uproszczone parametr kierunku głowy
	int wartosc_przeguby_y; //uproszczone parametr kierunku głowy
	double obrotX;//0
	double obrotY;//0
	double obrotXtmp;
	double obrotYtmp;
	int IDS;//0

	double zwracany_kat_x;//=0;
	double zwracany_kat_y;//=0;
	int ile_zaufanych_twarzy;//=0;

	int kat_widzenia_x;//stały parametr kamery -- wczytywany z XML
	int kat_widzenia_y;//stały parametr kamery -- wczytywany z XML

	double prog_gorny;
	double prog_dolny;
	int prog_tlumu;//ilość osób klasyfikowana jako tłum
	int obraz_szer;
	int obraz_dlug;
	//do odbioru obrazu

	//do pomiaru czasu
	int _ileKlatek;
	struct timeval _tp;
	double _sec, _usec, _poczatek, _koniec, _opoznienie, _zadanyCzasWykonania;

};

#endif /*FACEDETECTOR_HH_*/

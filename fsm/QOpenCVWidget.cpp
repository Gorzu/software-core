
#include "QOpenCVWidget.hh"

// Constructor
QOpenCVWidget::QOpenCVWidget(QWidget *parent) : QWidget(parent) {
    layout = new QVBoxLayout;
    imagelabel = new QLabel;
    QImage dummy(100,100,QImage::Format_RGB32);
    image = dummy;
    layout->addWidget(imagelabel);
    for (int x = 0; x < 100; x ++) {
        for (int y =0; y < 100; y++) {
            image.setPixel(x,y,qRgb(x, y, y));
        }
    }
    imagelabel->setPixmap(QPixmap::fromImage(image));

    setLayout(layout);
}

QOpenCVWidget::~QOpenCVWidget(void) {
    
}
//void QOpenCVWidget::putImage(IplImage *cvimage) {
void QOpenCVWidget::putImage(cv::Mat& cvimage) {
    int cvIndex, cvLineStart;
    cv::Mat mat(image.height(), image.width(), CV_8UC3, (cv::Scalar*)image.scanLine(0));  // konwersja QImage do Mat
    // switch between bit depths
    std::cout << "depth: " << cvimage.depth() << "  channels: " << cvimage.channels() << cvimage.size() << std::endl;
   
    switch (cvimage.depth()) {
        //case IPL_DEPTH_8U:
        case 0: // sprawdzenie dzialania kamery
        cv::cvtColor(cvimage, mat, cv::COLOR_BGR2RGB);  // jeśli nie wyswietla sie obrazek to prawdopodobnie nie dziala automatyczne wyswietlanie
            /*
            switch (cvimage.nChannels) {
                case 3:
                    if ( (cvimage.size().width != image.width()) || (cvimage.size().height != image.height())) {
                        QImage temp(cvimage.width, cvimage.height, QImage::Format_RGB32);
                        image = temp;
                    }
                    cvIndex = 0; cvLineStart = 0;
                    for (int y = 0; y < cvimage.height; y++) {
                        unsigned char red,green,blue;
                        cvIndex = cvLineStart;
                        for (int x = 0; x < cvimage.width; x++) {
                            // DO it
                            red = cvimage.imageData[cvIndex+2];
                            green = cvimage.imageData[cvIndex+1];
                            blue = cvimage.imageData[cvIndex+0];
                            
                            image.setPixel(x,y,qRgb(red, green, blue));
                            cvIndex += 3;
                        }
                        cvLineStart += cvimage.widthStep;                        
                    }
                    break;
                default:
                    printf("This number of channels is not supported\n");
                    break;
            } */
             switch (cvimage.channels()) {
                case 3:
                    if ( (cvimage.size().width != image.size().width()) || (cvimage.size().height != image.height())) {
                        QImage temp(cvimage.size().width, cvimage.size().height, QImage::Format_RGB32);
                        image = temp;
                    }
                    cvIndex = 0; cvLineStart = 0;
                    for (int y = 0; y < cvimage.size().height; y++) {
                        unsigned char red,green,blue;
                        cvIndex = cvLineStart;
                        for (int x = 0; x < cvimage.size().width; x++) {
                            // DO it
                            red = cvimage.data[cvIndex+2];
                            green = cvimage.data[cvIndex+1];
                            blue = cvimage.data[cvIndex+0];
                            
                            image.setPixel(x,y,qRgb(red, green, blue));
                            cvIndex += 3;
                        }
                        cvLineStart += cvimage.step;                        
                    }
                    break;
                default:
                    printf("This number of channels is not supported\n");
                    break;
            }
            break;
            
        default:
            printf("This type of cv::Mat image is not implemented in QOpenCVWidget\n");
            break;
    }
    imagelabel->setPixmap(QPixmap::fromImage(image));    
}


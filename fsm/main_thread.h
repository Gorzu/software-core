#ifndef MAIN_THREAD_H
#define MAIN_THREAD_H

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QString>
#include <string>
#include <map>
#include <cstdlib>
#include <time.h>


#include "moves.h"
#include "FSM.h"


class MainThread : public QThread
{


    Q_OBJECT 

public:
    // Konstruktor.
	MainThread();


	State* currState;
	State* prevState;
	FSM fsm;

	TargetInfo target;

	State* GenerateNextState();
	void sendBFrame(BFrame);
	bool checkConditions( const std::vector<QString>& conditions);
	FSM ReadFSM();


public slots:
	void startThread(QString enterState, QString prev, QThread::Priority prior);
	void targetUpdate( TargetInfo);
	void SetFSM(FSM &fsm);
	void Stop();

signals:
	void sendText(QString);
	void emitBFrame(BFrame);
	void cancelQueue();

private:
	void InitConditionsMap();
    // Zarzadzanie dostepem do zmiennych.
    QMutex mutex;

	QMutex targetIntMutex;
	QWaitCondition targetInt;
	bool stopRequest;

	enum Conditions{timeout = 0,
					seen,  // czy widzi jakikolwiek cel
					newTarget, // czy znaleziono nowy cel
					crowd, // czy jest tlok - wiecej ludzi od wyznaczonej stalej
					targetSpeaking, // czy cel mowi
					otherSpeaking //czy mowi ktos inny niz cel
					};
	std::map<QString, Conditions> conditonsMap;


    // Glowna funkcja watku.
    void run();
};

#endif // MAIN_THREAD_H

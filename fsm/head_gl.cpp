// Lukasz Kucharczyk 2009

#include "head_gl.h"

int exception::errorCount = 0;
const QString facePart::facePartName[11] =
{"Head", "LeftEye", "LeftEyeLid", "RightEye", "RightEyeLid",  "Lips", "Camera",
 "upperLipsPt1", "upperLipsPt2", "lowerLipsPt1", "lowerLipsPt2"};


HeadGL::HeadGL(std::string fileName, QWidget *parent)
    : QGLWidget(parent),
    fileName(fileName),
    FACE("face"), HEAD_BACK("head_back"),
    LEFT_EYE("leftEye"), LEFT_EYE_SPOT("leftEyeSpot"),
    RIGHT_EYE("rightEye"), RIGHT_EYE_SPOT("rightEyeSpot"),
    RIGHT_EYE_LID("rightEyeLid"), LEFT_EYE_LID("leftEyeLid")
{
    eyeVel = .25;

	servAmount = Servo::TOTAL;

    setBezierConfig();

    BaseCoor.reserve(facePart::elemCount);
	//modyfikacja
	BaseCoor = std::vector<facePartCoor>(facePart::elemCount);

    BaseCoor[facePart::Head] = facePartCoor(0, 0, 0, 0, 0, 0);
    BaseCoor[facePart::Camera] = facePartCoor();
    BaseCoor[facePart::LeftEye] = facePartCoor(3.2, 14, 3.6, -15, 0, 180);
    BaseCoor[facePart::LeftEyeLid] = facePartCoor(3.5, 8, -4.6, -0, 10, 0);
    BaseCoor[facePart::RightEye] = facePartCoor(-3.2, 14, 3.6, -15, 0, 180);
    BaseCoor[facePart::RightEyeLid] = facePartCoor(-3.4, 8, -4.6, -0, -10, 0);
    BaseCoor[facePart::Lips] = facePartCoor(0, 0, 0, 0, 0, 0);
    BaseCoor[facePart::lowerLipsPt1] = facePartCoor(-3.4, 5.1 ,11.5, 0, 30,  0);
    BaseCoor[facePart::lowerLipsPt2] = facePartCoor(3.4, 5.1 ,11.5, 0, -30, 0);
    BaseCoor[facePart::upperLipsPt1] = facePartCoor(-3.4, 5.4 ,11.5, 0, 30, 0);
    BaseCoor[facePart::upperLipsPt2] = facePartCoor(3.4, 5.4 ,11.5, 0, -30, 0);

    purple = QColor::fromCmykF(0.39, 0.39, 0.0, 0.0);

    // Wyliczenie wspolczynnikow beziera.
    float a, b, c, d;
    bezierPrec = .1;
    for (float t=0; t<=1.1; t+= bezierPrec)
    {
        a = pow(1-t, 3);
        b = 3*t*pow(1-t, 2);
        c = 3*(1-t)*pow(t, 2);
        d = pow(t, 3);
        bezierCoefs.push_back(bezierCoef(a, b, c, d));
    }
}
bool HeadGL::start(int period)
{
    if (period <= 0)
    {
        exception ex(exception::slotError, "HeadGL::start()",
                     "Uncorrect period value.");
        emit errorOccured(ex);
        return false;
    }
    Period = period;

    // Zaladowanie pliku z modelem 3d.
    QString msg = "Loading ";
    msg.append(msg.fromStdString(fileName));
    msg.append(" in visualisation module...");
    emit sendMsg(msg);
    if (!loadFile(fileName))
    {
        QString s = "Could not load ";
        s.append(s.fromStdString(fileName));
        exception ex(exception::slotError, "HeadGL::start()", s);
        emit errorOccured(ex);
        return false;
    }

    msg = msg.fromStdString(fileName);
    msg.append(" loaded.");
    emit sendMsg(msg);

    // Utworzenie obiekt�w.
    if (!initObjs())
    {
        exception ex(exception::error, "HeadGL::start()",
                     "Could not initialize 3d objects.");
        emit errorOccured(ex);
        return false;
    }
    objectsCorrect = checkObjects();
    if (!objectsCorrect)
    {
        exception ex(exception::error, "HeadGL::start()",
                     "Could not initialize 3d objects.");
        emit errorOccured(ex);
        return false;
    }

    timerId = startTimer(Period);
    emit sendMsg(msg);

    return true;
}

bool HeadGL::enable(int period)
{
    if (timerId != -1)
    {
        exception ex(exception::slotWarning, "HeadGL::enable()",
                     "HeadGL already enabled.");
        emit errorOccured(ex);
        return false;
    }
    if (period <= 0)
    {
        exception ex(exception::slotError, "HeadGL::enable()",
                     "Uncorrect period value.");
        emit errorOccured(ex);
        return false;
    }
    Period = period;
    if (objectsCorrect)
    {
        timerId = startTimer(period);
        return true;
    }

    exception ex(exception::slotError, "HeadGL::enable()",
                 "3d not initialized.");
    emit errorOccured(ex);
    return false;
}
void HeadGL::disable()
{
    if (timerId == -1)
    {
        exception ex(exception::slotWarning, "HeadGL::disable()",
                     "HeadGL already disabled.");
        emit errorOccured(ex);
        return;
    }
    killTimer(timerId);
    timerId = -1;
}
HeadGL::~HeadGL()
{
    makeCurrent();
}
void HeadGL::timerEvent(QTimerEvent*)
{

    float velDeg, maxVelDeg, curPos, destPos, maxPosDeg, posChange;
    int id;
    int sign;
    std::map<Servo::ServoId, Frame>::iterator it;
    QTime time;
    int timeChange;
    std::vector<std::map<Servo::ServoId, Frame>::iterator > eraseCont;
    // Przejscie po wszystkich aktualnie wykonywanych ruchach.
    for (it = currMoves.begin(); it != currMoves.end(); it++)
    {
        // Zmiana czasu od ostatniego renderowania.
        timeChange = (it->second.time.msecsTo(time.currentTime()));
        // Indeks serwa.
        id = it->second.ID;
        // Maksymalna predkosc serwa.
        maxVelDeg = servConfigs[id].maxVel;
        // Zadana predkosc serwa.
        velDeg = it->second.vel*maxVelDeg;
        // Aktualna pozycja.
        curPos = offset[id];
        // Maksymalna zmiana pozycji.
        maxPosDeg = (servConfigs[id].maxPos - servConfigs[id].minPos);
        // Docelowa pozycja.
        destPos = it->second.destPos*maxPosDeg;
        // Zmiana pozycji wzgledem ostatniego renderowania.
        posChange = velDeg*timeChange;
        // Sprawdzenie, czy ruch nie powinnien sie zakonczyc.
        if (qAbs(curPos-destPos) < posChange)
        {
            // Zakonczenie ruchu.
            offset[id] = destPos;
            eraseCont.push_back(it);
        }
        else
        {
            // Ustalenie kierunku ruchu.
            (curPos < destPos) ? sign = 1 : sign = -1;
            // Uaktualnienie biezacej pozycji.
            offset[id] += sign*posChange;
            // Uaktualnienie czasu ostatniego renderowania.
            it->second.time = time.currentTime();
        }
    }
    // Usuniecie ruchow ktore zostaly wykonane ze zbioru wykonywanych ruchow.
    while(eraseCont.size())
    {
        currMoves.erase(eraseCont.back());
        eraseCont.pop_back();
    }
    updateGL();
}

void HeadGL::receiveFrame(Frame frame)
{
    if ((frame.destPos < 0) || (frame.destPos > 1))
    {
        exception ex(exception::slotWarning, "HeadGL::receiveFrame()",
                     "Uncorrect position request.");
        emit errorOccured(ex);
        return;
    }
    if ((frame.vel < 0) || (frame.vel > 1))
    {
        exception ex(exception::slotWarning, "HeadGL::receiveFrame()",
                     "Uncorrect velocity request.");
        emit errorOccured(ex);
        return;
    }
    if ((frame.ID > static_cast<int>(servAmount)) || (frame.ID < 0))
    {
        exception ex(exception::slotWarning, "HeadGL::receiveFrame()",
                     "Uncorrect serv ID.");
        emit errorOccured(ex);
        return;
    }
    currMoves[frame.ID] = frame;
}

void HeadGL::receiveFrame(FrameCam frame)
{
    if ((frame.ID == Servo::LeftEyeH) || (frame.ID == Servo::LeftEyeV))
    {
        if (frame.destPosH >= 0)
            receiveFrame(Frame(Servo::LeftEyeH, frame.time, frame.destPosH,
                               eyeVel));
        if (frame.destPosV >= 0)
            receiveFrame(Frame(Servo::LeftEyeV, frame.time, frame.destPosV,
                               eyeVel));
        return;
    }
    if ((frame.ID == Servo::RightEyeH) || (frame.ID == Servo::RightEyeV))
    {
        if (frame.destPosH >= 0)
            receiveFrame(Frame(Servo::RightEyeH, frame.time, frame.destPosH,
                               eyeVel));
        if (frame.destPosV >= 0)
            receiveFrame(Frame(Servo::RightEyeV, frame.time, frame.destPosV,
                               eyeVel));
        return;
    }

    exception ex(exception::slotWarning, "HeadGL::receiveFrame(FrameCam)",
                 "Uncorrect serv ID.");
    emit errorOccured(ex);
}

void HeadGL::receiveFaceCoor(std::map<QString, facePartCoor> faceCoor)
{
    unsigned int corrSize = faceCoor.size();

    BaseCoor[facePart::Head] = faceCoor[facePart::facePartName[facePart::Head]];
    BaseCoor[facePart::LeftEye] =
            faceCoor[facePart::facePartName[facePart::LeftEye]];
    BaseCoor[facePart::RightEye] =
            faceCoor[facePart::facePartName[facePart::RightEye]];
    BaseCoor[facePart::RightEyeLid] =
            faceCoor[facePart::facePartName[facePart::RightEyeLid]];
    BaseCoor[facePart::LeftEyeLid] = faceCoor[facePart::facePartName[facePart::LeftEyeLid]];
    BaseCoor[facePart::Lips] = faceCoor[facePart::facePartName[facePart::Lips]];
    BaseCoor[facePart::Camera] = faceCoor[facePart::facePartName[facePart::Camera]];
    BaseCoor[facePart::upperLipsPt1] = faceCoor[facePart::facePartName[facePart::upperLipsPt1]];
    BaseCoor[facePart::upperLipsPt2] = faceCoor[facePart::facePartName[facePart::upperLipsPt2]];
    BaseCoor[facePart::lowerLipsPt1] = faceCoor[facePart::facePartName[facePart::lowerLipsPt1]];
    BaseCoor[facePart::lowerLipsPt2] = faceCoor[facePart::facePartName[facePart::lowerLipsPt2]];

    // Zmiana rozmiaru oznacza, ze brakowalo ktoregos elementu.
    if (corrSize != faceCoor.size())
    {
        exception x(exception::slotWarning, "HeadGL::receiveFaceCoor()",
                    "Uncorrect base coordinates.");
        handleError(x);
    }
    resizeGL(400, 400);
}

void HeadGL::handleError(exception x)
{
    emit errorOccured(x);
}
void HeadGL::setEyeVel(float eyeVel_)
{
    eyeVel = eyeVel_;
}
std::vector<facePartCoor> HeadGL::getFacePartCoor()
{
    return BaseCoor;
}
void HeadGL::initializeGL()
{
    // Kolor tla.
    qglClearColor(purple.dark());
    // Konfiguracja swiatla.
    GLfloat light_position[] = {10.0, -40.0, -80.0, 1.0};
    GLfloat light_ambient[] = {.3, .3, .3, 1.0};
    GLfloat light_diffuse[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat light_specular[]= {1.0, 1.0, 1.0, 1.0};
    GLfloat att_constant  = {1};
    GLfloat att_linear    = {0.0001};
    GLfloat att_quadratic  = {0.0001};
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, att_constant);
    glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, att_linear);
    glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, att_quadratic);
    // Wlaczenie swiatla.
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    // Inne ustawienia.
    glShadeModel(GL_SMOOTH); // GL_FLAT
    glEnable(GL_DEPTH_TEST);
}

bool HeadGL::checkObjects()
{
    if (objCont.find(FACE) == objCont.end()) return false;
    if (objCont.find(HEAD_BACK) == objCont.end()) return false;
    if (objCont.find(LEFT_EYE) == objCont.end()) return false;
    if (objCont.find(LEFT_EYE_SPOT) == objCont.end()) return false;
    if (objCont.find(LEFT_EYE_LID) == objCont.end()) return false;
    if (objCont.find(RIGHT_EYE) == objCont.end()) return false;
    if (objCont.find(RIGHT_EYE_SPOT) == objCont.end()) return false;
    if (objCont.find(RIGHT_EYE_LID) == objCont.end()) return false;

    return true;
}

void HeadGL::deleteObjects()
{
    for (it = objCont.begin(); it != objCont.end(); it++)
    {
        glDeleteLists(*(it->second), 1);
    }
    objCont.clear();
    objectsCorrect = false;

}
void HeadGL::setServConf(std::vector<ServoConfig> configs)
{
    if (configs.size() != servAmount)
    {
        exception ex(exception::slotError, "HeadGL::setServConf()",
                     "Uncorrect serv configuration data.");
        emit errorOccured(ex);
        return;
    }

    servConfigs.clear();
    servConfigs.reserve(servAmount);
	//modyfikacja
	servConfigs = std::vector<ServoConfig>(servAmount);
	offset = std::vector<double>(servAmount);

    for (unsigned int i=0; i<configs.size(); i++)
    {
        servConfigs[i] = configs[i];
        offset[i] = 0;
    }
}

void HeadGL::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    qglClearColor(purple.dark());

    if (!objectsCorrect) return;

    float headZoffest = -120;
    float headYoffset = -10;

    glLoadIdentity();
    float xHead, yHead, zHead, x, y, z;
    float xRotHead, yRotHead, zRotHead, xRot, yRot, zRot;
    xHead = BaseCoor[facePart::Head].x;
    yHead = BaseCoor[facePart::Head].y + headYoffset;
    zHead = BaseCoor[facePart::Head].z + headZoffest;
    xRotHead = BaseCoor[facePart::Head].xRot + offset[Servo::HeadR] +
               servConfigs[Servo::HeadR].minPos;
    yRotHead = BaseCoor[facePart::Head].yRot + offset[Servo::HeadH] +
               servConfigs[Servo::HeadH].minPos;
    zRotHead = BaseCoor[facePart::Head].zRot + offset[Servo::HeadV] +
               servConfigs[Servo::HeadV].minPos;
    glTranslated(xHead, yHead, zHead);
    glRotated(xRotHead, 1, 0, 0);
    glRotated(yRotHead, 0, 1, 0);
    glRotated(zRotHead, 0, 0, 1);

    glCallList(*objCont[FACE]);
    glCallList(*objCont[HEAD_BACK]);

    glLoadIdentity();

    x = BaseCoor[facePart::LeftEye].x;
    y = BaseCoor[facePart::LeftEye].y;
    z = BaseCoor[facePart::LeftEye].z;
    xRot = BaseCoor[facePart::LeftEye].xRot + offset[Servo::LeftEyeV] +
           servConfigs[Servo::LeftEyeV].minPos;;
    yRot = BaseCoor[facePart::LeftEye].yRot + offset[Servo::LeftEyeH] +
           servConfigs[Servo::LeftEyeH].minPos;;
    zRot = BaseCoor[facePart::LeftEye].zRot;

    glTranslated(xHead, yHead, zHead);
    glRotated(xRotHead, 1, 0, 0);
    glRotated(yRotHead, 0, 1, 0);
    glRotated(zRotHead, 0, 0, 1);
    glTranslated(x, y, z);
    glRotated(xRot, 1, 0, 0);
    glRotated(yRot, 0, 1, 0);
    glRotated(zRot, 0, 0, 1);
    glCallList(*objCont[LEFT_EYE]);
    glCallList(*objCont[LEFT_EYE_SPOT]);


    glLoadIdentity();
    x = BaseCoor[facePart::RightEye].x;
    y = BaseCoor[facePart::RightEye].y;
    z = BaseCoor[facePart::RightEye].z;
    xRot = BaseCoor[facePart::RightEye].xRot + offset[Servo::RightEyeV] +
           servConfigs[Servo::RightEyeV].minPos;
    yRot = BaseCoor[facePart::RightEye].yRot + offset[Servo::RightEyeH] +
           servConfigs[Servo::RightEyeH].minPos;
    zRot = BaseCoor[facePart::RightEye].zRot;

    glTranslated(xHead, yHead, zHead);
    glRotated(xRotHead, 1, 0, 0);
    glRotated(yRotHead, 0, 1, 0);
    glRotated(zRotHead, 0, 0, 1);
    glTranslated(x, y, z);
    glRotated(xRot, 1, 0, 0);
    glRotated(yRot, 0, 1, 0);
    glRotated(zRot, 0, 0, 1);
    glCallList(*objCont[RIGHT_EYE]);
    glCallList(*objCont[RIGHT_EYE_SPOT]);

    glLoadIdentity();
    x = BaseCoor[facePart::RightEyeLid].x;
    y = BaseCoor[facePart::RightEyeLid].y;
    z = BaseCoor[facePart::RightEyeLid].z;
    xRot = BaseCoor[facePart::RightEyeLid].xRot + offset[Servo::RightEyeLidV] +
           servConfigs[Servo::RightEyeLidV].minPos;
    yRot = BaseCoor[facePart::RightEyeLid].yRot;
    zRot = BaseCoor[facePart::RightEyeLid].zRot + offset[Servo::RightEyeLidR] +
           servConfigs[Servo::RightEyeLidR].minPos;
    glTranslated(xHead, yHead, zHead);
    glRotated(xRotHead, 1, 0, 0);
    glRotated(yRotHead, 0, 1, 0);
    glRotated(zRotHead, 0, 0, 1);
    glTranslated(x, y, z);
    glRotated(xRot, 1, 0, 0);

    glTranslated(0, 9, 0);
    glRotated(zRot, 0, 0, 1);
    glTranslated(0, -9, 0);
    glTranslated(0, 0, 10);
    glRotated(yRot, 0, 1, 0);
    glTranslated(0, 0, -10);
    glCallList(*objCont[RIGHT_EYE_LID]);

    glLoadIdentity();
    x = BaseCoor[facePart::LeftEyeLid].x;
    y = BaseCoor[facePart::LeftEyeLid].y;
    z = BaseCoor[facePart::LeftEyeLid].z;
    xRot = BaseCoor[facePart::LeftEyeLid].xRot + offset[Servo::LeftEyeLidV] +
           servConfigs[Servo::LeftEyeLidV].minPos;
    yRot = BaseCoor[facePart::LeftEyeLid].yRot;
    zRot = BaseCoor[facePart::LeftEyeLid].zRot + offset[Servo::LeftEyeLidR] +
           servConfigs[Servo::LeftEyeLidR].minPos;
    glTranslated(xHead, yHead, zHead);
    glRotated(xRotHead, 1, 0, 0);
    glRotated(yRotHead, 0, 1, 0);
    glRotated(zRotHead, 0, 0, 1);
    glTranslated(x, y, z);
    glRotated(xRot, 1, 0, 0);

    glTranslated(0, 9, 0);
    glRotated(zRot, 0, 0, 1);
    glTranslated(0, -9, 0);
    glTranslated(0, 0, 10);
    glRotated(yRot, 0, 1, 0);
    glTranslated(0, 0, -10);
    glCallList(*objCont[LEFT_EYE_LID]);


    glLoadIdentity();
    x = BaseCoor[facePart::Lips].x;
    y = BaseCoor[facePart::Lips].y;
    z = BaseCoor[facePart::Lips].z;
    xRot = BaseCoor[facePart::Lips].xRot;
    yRot = BaseCoor[facePart::Lips].yRot;
    zRot = BaseCoor[facePart::Lips].zRot;

    glTranslated(xHead, yHead, zHead);
    glRotated(xRotHead, 1, 0, 0);
    glRotated(yRotHead, 0, 1, 0);
    glRotated(zRotHead, 0, 0, 1);
    glTranslated(x, y, z);
    glRotated(xRot, 1, 0, 0);
    glRotated(yRot, 0, 1, 0);
    glRotated(zRot, 0, 0, 1);
    glCallList(generateSimpleLips(BaseCoor[facePart::upperLipsPt1],
                                  offset[Servo::TopRightLips] +
                                  servConfigs[Servo::TopRightLips].minPos,
                                  BaseCoor[facePart::upperLipsPt2],
                                  offset[Servo::TopLeftLips] +
                                  servConfigs[Servo::TopLeftLips].minPos));
    glCallList(generateSimpleLips(BaseCoor[facePart::lowerLipsPt1],
                                  offset[Servo::BotRightLips] +
                                  servConfigs[Servo::BotRightLips].minPos,
                                  BaseCoor[facePart::lowerLipsPt2],
                                  offset[Servo::BotLeftLips] +
                                  servConfigs[Servo::BotLeftLips].minPos));
}



bool HeadGL::initObjs()
{
    GLuint * object;
    face e(0, 0, 0);
    std::string name;

    GLfloat mat_diffuse[]  = {1.0, 1.0, 1.0, 1.0};
    GLfloat mat_specular[] = {1.0, 1.0, 1.0, 1.0};
    GLfloat mat_shininess  = {100.0};


    objAmount = names.size();
    if ((objAmount != faceListCont.size())||(objAmount != vertexListCont.size())
        ||(objAmount != normListCont.size()))
    {
        emit errorOccured(exception(exception::slotError, "HeadGL::initObjs()"
                                    , "Uncorrect data in file."));
        return false;
    }

    for (unsigned int i=0; i<objAmount; i++)
    {
        object = new GLuint;
        objCont[names[i]] = object;
    }

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
    glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);

    // Tworzenie kolejnych czesci modelu.
    for (unsigned int i=0; i<objAmount; i++)
    {
        name = names[i];
        *(objCont[name]) = glGenLists(1);
        if (matInd[i] >= static_cast<int>(matCont.size()))
        {
            QString msg = "Uncorrect data in file: material not found.";
            emit errorOccured(exception(exception::slotError,
                                        "HeadGL::initObjects()", msg));
            return false;
        }
        glNewList(*(objCont[name]), GL_COMPILE);
        glMaterialfv(GL_FRONT, GL_AMBIENT, matCont[matInd[i]].ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, matCont[matInd[i]].diffuse);

        glBegin(GL_TRIANGLES);
        // Tworzenie kolejnych face'ow.
        for (unsigned int j=0; j < faceListCont[i].size(); j++)
        {
            // Aktualnie rysowany face.
            e = faceListCont[i][j];
            if ((static_cast<int>(vertexListCont[i].size()) <= e.v1)||
                (static_cast<int>(vertexListCont[i].size()) <= e.v2)||
                (static_cast<int>(vertexListCont[i].size()) <= e.v3)||
                (normListCont[i].size() <= j*3+2 ))
            {
                emit errorOccured(exception(exception::slotError,
                                  "HeadGL::initObjects()",
                                  "Uncorrect data in file."));
                glEnd();
                glEndList();
                return false;
            }
            glNormal3f(normListCont[i][j*3].x, normListCont[i][j*3].y,
                       normListCont[i][j*3].z);
            glVertex3f(vertexListCont[i][e.v1].x, vertexListCont[i][e.v1].y,
                       vertexListCont[i][e.v1].z);
            glNormal3f(normListCont[i][j*3+1].x, normListCont[i][j*3+1].y,
                       normListCont[i][j*3+1].z);
            glVertex3f(vertexListCont[i][e.v2].x, vertexListCont[i][e.v2].y,
                       vertexListCont[i][e.v2].z);
            glNormal3f(normListCont[i][j*3+2].x, normListCont[i][j*3+2].y,
                       normListCont[i][j*3+2].z);
            glVertex3f(vertexListCont[i][e.v3].x, vertexListCont[i][e.v3].y,
                       vertexListCont[i][e.v3].z);
        }
        glEnd();
        glEndList();
    }
    // Czyszczenie pamieci.
    for (unsigned int i=0; i<objAmount; i++)
    {
        vertexListCont[i].clear();
        vertexListCont[i].clear();
        vertexListCont[i].clear();
        normListCont[i].clear();
    }
    return true;
}

GLuint HeadGL::generateSimpleLips(facePartCoor Pt1, float Pt1offset,
                                  facePartCoor Pt2, float Pt2offset)
{
    float dPi = 6.28;

    GLuint object;
    glNewList(object, GL_COMPILE);
    float material[3] = {0.4667, 0.7490,	1.0000};

    //qglColor(purple.dark());
    //glDisable(GL_LIGHTING);
    // Wspolrzedne punktu poczatkowego.
    float xB = Pt1.x;
    float yB = Pt1.y;
    float zB = Pt1.z;
    // Wspolrzedne punktu poczatkowego kontrolnego.
    float yRotB = Pt1.yRot/360*dPi;
    float zRotB = (-Pt1.zRot + Pt1offset)/360*dPi;
    float cxB = bezContLenX*sin(yRotB);
    float czB = zB + bezContLenZ*cos(yRotB);
    float cyB = yB + cxB*sin(zRotB);
    cxB = xB + cxB*cos(zRotB);

    // Wspolrzedne punktu koncowego.
    float xE = Pt2.x;
    float yE = Pt2.y;
    float zE = Pt2.z;
    // Wspolrzedne koncowego punktu kontrolnego.
    float yRotE = Pt2.yRot/360*dPi;
    float zRotE = (-Pt2.zRot+Pt2offset)/360*dPi;
    float cxE = bezContLenX*sin(yRotE);
    float czE = zE + bezContLenZ*cos(yRotE);
    float cyE = yE + cxE*sin(zRotE);
    cxE = xE + cxE*cos(zRotE);

    // Rysowanie wektorow kontrolnych.
    if (drawBezierControls)
    {
        glDisable(GL_LIGHTING);
        glLineWidth(1);
        glColor3f(.8, .1, .1);
        glBegin(GL_LINES);
        glVertex3f(xB, yB, zB);
        glVertex3f(cxB, cyB, czB);
        glVertex3f(xE, yE, zE);
        glVertex3f(cxE, cyE, czE);        
        glEnd();
        glEnable(GL_LIGHTING);
    }

    // Rysowanie krzywej opisujacej usta.
    glMaterialfv(GL_FRONT, GL_AMBIENT, material);
    glMaterialfv(GL_FRONT, GL_DIFFUSE, material);
    glLineWidth(lipsRad);
    float x, y, z;
    float a, b, c, d;
    glBegin(GL_LINE_STRIP);
    for (unsigned int t=0; t<bezierCoefs.size(); t++)
    {
        a = bezierCoefs[t].a;
        b = bezierCoefs[t].b;
        c = bezierCoefs[t].c;
        d = bezierCoefs[t].d;
        x = a*xB + b*cxB + c*cxE + d*xE;
        y = a*yB + b*cyB + c*cyE + d*yE;
        z = a*zB + b*czB + c*czE + d*zE;
        glVertex3f(x, y, z);
    }
    glEnd();
    glEndList();

    return object;
}
void HeadGL::resizeGL(int width, int height)
{
    float camFactor = .1;
    int side = qMin(width, height);
    glViewport((width - side) / 2, (height - side) / 2, side, side);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glTranslated(BaseCoor[facePart::Camera].x*camFactor,
                 BaseCoor[facePart::Camera].y*camFactor,
                 BaseCoor[facePart::Camera].z*camFactor);
    glRotated(BaseCoor[facePart::Camera].xRot, 1, 0, 0);
    glRotated(BaseCoor[facePart::Camera].yRot, 0, 1, 0);
    glRotated(BaseCoor[facePart::Camera].zRot, 0, 0, 1);
    // https://www.khronos.org/opengl/wiki/GluPerspective_code
    gluPerspective(30, 1, 40, 300); // nie powinno być używane w aktualnym standardzie
    glMatrixMode(GL_MODELVIEW);
}

QSize HeadGL::minimumSizeHint() const
{
    return QSize(50, 50);
}
QSize HeadGL::sizeHint() const
{
    return QSize(400, 400);
}

void HeadGL::setBezierConfig(bool drawBezierControls_,
                             float bezierControlLengthX,
                             float bezierControlLengthZ,
                             float lipsRadius)
{
    drawBezierControls = drawBezierControls_;
    bezContLenX = bezierControlLengthX;
    bezContLenZ = bezierControlLengthZ;
    lipsRad = lipsRadius;
}

bool HeadGL::loadFile(std::string fileName)
{
	//modyfikacja
    const unsigned int maxLineLen = 1000;
    // Lanuchy znakow okreslajace sekcje.
    std::string MATERIAL_LIST = "MATERIAL_LIST";
    std::string OTHER = "OTHER";
    std::string GEOMOBJECT = "GEOMOBJECT";
    std::string NODE_NAME = "NODE_NAME";
    std::string NODE_TM = "NODE_TM";
    std::string MESH_VERTEX_LIST = "MESH_VERTEX_LIST";
    std::string MESH_VERTEX = "*MESH_VERTEX";
    std::string MESH_FACE_LIST = "*MESH_FACE_LIST";
    std::string MESH_FACE = "*MESH_FACE";
    std::string MESH_NORMALS = "*MESH_NORMALS";
    std::string MESH_VERTEXNORMAL  = "*MESH_VERTEXNORMAL";
    std::string MATERIAL_AMBIENT = "*MATERIAL_AMBIENT";
    std::string MATERIAL_DIFFUSE = "*MATERIAL_DIFFUSE";
    std::string MATERIAL_REF = "*MATERIAL_REF";

    // Wczytywany jest do niego niepotrzebny fragment lini, przy wczytywaniu
    // wertexa lub face'a.
    char temp_s[maxLineLen];
    // Przechowuja pobierane linie.
    std::string s;
    char line[maxLineLen];
    // Wspolrzedne wczytywanego wektora.
    float x, y, z;
    // Numer wczytywanego wektora/face'a.
    unsigned int id;
    // Indeksy wektorow w vecCont wczytywanego face'a.
    int v1, v2, v3;
    // Other ustawione na prawde oznacza poczatek niezdefiniowanej sekcji.
    bool other;
    // Zmienna umozliwia zapamietanie ilosci otwartych sekcji, przy rozpoznaniu
    // poczatku sekcji, ktora nalezy pominac. Pozwala to na zignorowanie
    // wszystkich podsekcji pomijanej sekcji.
    // Wartosc 0 oznacza, ze zadna sekcja, ktora nalezy zignorowac nie jest
    // otwarta.
    unsigned int skip = 0;
    // Wektor z owartymi sekcjami ( mode.back() to aktualnie otwarta sekcja ).
    std::vector<std::string> mode;

    // Zbior wertexow wczytywanego modelu.
    std::vector<vertex> * verCont;
    // Zbior face'ow wczytywanego modelu.
    std::vector<face> * faceCont;
    // Zbior normalnych wczytywanego modelu.
    std::vector<vertex> * normCont;
    // Nazwa wczytywanego modelu.
    std::string name;

    // Wczytywany wertex.
    vertex * myVertex;
    // Wczytywany face.
    face * myFace;
    // Wczytywany material.
    material * myMaterial;

    // Plik wejsciowy.
    std::fstream sIn;
    sIn.open(fileName.c_str());

    if (!sIn.is_open()) return false;

    while (sIn.getline(line, maxLineLen))
    {
        s = line;
        other = true;
        if ((skip==0))
        {
            // Wczytanie wertexa.
            if (s.find(MESH_VERTEX)!=std::string::npos)
            {
                if (!mode.empty())
                {
                    if (mode.back() == MESH_VERTEX_LIST)
                    {
                        sscanf(line, "%s %d %f %f %f\n",
                               temp_s, &id, &x, &y, &z);
                        //std::cout << "id: " << id <<" x: " << x << " y: ";
                        //std::cout << y << " z: " << z << std::endl;
                        myVertex = new vertex(x, y, z);
                        if ((*verCont).size() != id)
                        {
                            std::cerr << "\nBlad w ASE::loadModel,";
                            std::cerr << "id!= verCont.size()\n";
                        }
                        verCont->push_back(*myVertex);
                    }
                }
            }
            // Wczytanie normalnej.
            if (s.find(MESH_VERTEXNORMAL)!=std::string::npos)
            {
                if (!mode.empty())
                {
                    if (mode.back() == MESH_NORMALS)
                    {
                        sscanf(line, "%s %d %f %f %f\n",
                               temp_s, &id, &x, &y, &z);
                        myVertex = new vertex(x, y, z);
                        normCont->push_back(*myVertex);
                    }
                }
            }
            // Wczytanie face'a.
            if (s.find(MESH_FACE)!=std::string::npos)
            {
                if (!mode.empty())
                {
                    if (mode.back() == MESH_FACE_LIST)
                    {
                        sscanf(line, "%s %d: A: %d B: %d C: %d %s\n",
                               temp_s, &id, &v1, &v2, &v3, temp_s);
                        //std::cout << "v1: " << v1 <<" v2: " << v2;
                        //std::cout << " v3: " << v3 << std::endl;
                        myFace = new face(v1, v2, v3);
                        if ((*faceCont).size() != id)
                        {
                            std::cerr << "\nBlad w ASE::loadModel,";
                            std::cerr << "id!= faceCont.size()\n";
                        }
                        faceCont->push_back(*myFace);
                    }
                }
            }
            // Sekcja materialow.
            if (s.find(MATERIAL_LIST)!=std::string::npos)
            {
                mode.push_back(MATERIAL_LIST);
                other = false;
            }
            if (s.find(MATERIAL_REF)!=std::string::npos)
            {
                sscanf(line, "%s %d \n", temp_s, &id);
                matInd.push_back(id);
            }
            // Pobranie materialu.
            if (s.find(MATERIAL_AMBIENT)!=std::string::npos)
            {
                sscanf(line, "%s %f %f %f\n", temp_s, &x, &y, &z);
                myMaterial = new material(x, y, z);
                matCont.push_back(*myMaterial);
            }
            if (s.find(MATERIAL_DIFFUSE)!=std::string::npos)
            {
                sscanf(line, "%s %f %f %f\n", temp_s, &x, &y, &z);
                matCont.back().setDiffuse(x, y, z);
            }

            // Sekcja obiektu.
            if (s.find(GEOMOBJECT)!=std::string::npos)
            {
                mode.push_back(GEOMOBJECT);
                other = false;
            }
            // Sekcja NODE_TM, nalezy ja zignorowac.
            if (s.find(NODE_TM)!=std::string::npos)
            {
                mode.push_back(NODE_TM);
                skip = mode.size();
                other = false;
            }
            // Pobranie nazwy.
            if (s.find(NODE_NAME)!=std::string::npos)
            {
                name = s.substr(s.find_first_of('"')+1, s.length());
                name = name.substr(0, name.find_first_of('"'));
                other = false;
                //std::cout << name << std::endl;
                names.push_back(name);
            }
            // Poczatek sekcji vertexow. Inicjalizacja verCont.
            if (s.find(MESH_VERTEX_LIST)!=std::string::npos)
            {
                mode.push_back(MESH_VERTEX_LIST);
                verCont = new std::vector<vertex>;
                other = false;
            }
            // Poczatek sekcji face'ow. Inicjalizacja faceCont.
            if (s.find(MESH_FACE_LIST)!=std::string::npos)
            {
                mode.push_back(MESH_FACE_LIST);
                faceCont = new std::vector<face>;
                other = false;
            }
            // Poczatek sekcji normalnych. Inicjalicazja normCont.
            if (s.find(MESH_NORMALS)!=std::string::npos)
            {
                mode.push_back(MESH_NORMALS);
                normCont = new std::vector<vertex>;
                other = false;
            }
        }

        // Wykrycie nowej sekcji. Jesli nie zostala wykryta jako jedna ze
        // zdefiniowanych sekcji, zapamietywana jest jako OTHER.
        if (s.find("{")!= std::string::npos)
        {
            if (other) mode.push_back(OTHER);
        }
        // Wykrycie konca sekcji. Jesli zapamietana glebokosc sekcji, ktora
        // nalezy zignorowac jest rowna ilosci otwartych sekcji, oznacza to
        // koniec sekcji, ktora nalezy zignorowac.
        if (s.find("}")!= std::string::npos)
        {
            if (mode.back() == MESH_VERTEX_LIST)
            {
                vertexListCont.push_back(*verCont);
            }
            if (mode.back() == MESH_FACE_LIST)
            {
                faceListCont.push_back(*faceCont);
            }
            if (mode.back() == MESH_NORMALS)
            {
                normListCont.push_back(*normCont);
            }
            if (skip == mode.size()) skip = 0;
            if (!mode.empty()) mode.pop_back();
        }
    } // while(sIn.getLine(...))

    sIn.close();
    return true;
} // headGL::loadFile(...)


/* Watek kontroli glowy.
Jest to warstwa posrednia miedzy warstwa mozgu, a warstwa sprzetowa.
Obsluga polega na wysylaniu do watku ramek z zachowaniami, gdzie zostana
one rozlozone na ruchy proste i w odpowiednim czasie wyslane na 
serwa, kamery , czy syntezator.
Korzysta z definicji elementow ruchu opisanych w 'moves.h'. */

#ifndef HEAD_CONTROL_THREAD_H
#define HEAD_CONTROL_THREAD_H

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QTime>
#include <queue>
#include <cmath>

#include "head_gl_utils.h" // dla konfiguracji serw
#include "utils.h"
#include "moves.h"
#include "FSM.h"
#include "BElements.h"
#include "mowa.h"
#include "Driver/Driver.hh"
#include "facedetector.hh"


class HeadControlThread : public QThread
{
    Q_OBJECT

public:



   bool PermitLidsMove; //Stachu



	//Kontruktor
	// sleepTimeMs - czestotliwosc odswiezania
    HeadControlThread(long sleepTimeMs);

	//Destruktor
	~HeadControlThread();



	// pozycja celu, czyli  obiektu do interakcji
	TargetInfo target;
	
	// mowienie
	Wypowiedz* mowa;

        // Sterownik Kamer
        FaceDetector* facedetector;

Driver * drv;
void setDrv(Driver * _drv);
void setFaceDet(FaceDetector * fcdt);

	// zadanie zakonczenia watku
	bool endRequest;
public slots:
    // Uruchomienie watku.
    void startThread(QThread::Priority prior);
	// Ustawienie konfiguracji serw
	void setServConf(std::vector<ServoConfig> configs);
	// Ustawienie bazy BElementow
	void SetBElements(BElements* b);
	// Ustawienie predkosci nadpisywanych serw przy podazaniu
	void SetOverrideEyesSpeed(double s);
	void SetOverrideHeadSpeed(double s);
	// Nowa pozycja celu
	void targetUpdate( TargetInfo);
	// Przyjecie i rozklad ramki zachowan 
	void receiveBFrame(BFrame);
	// Przyjecie ruchu prostego i niezwloczna obsluga
	void receiveSimpleMove(SimpleMove);
	// Inicjalizacja twarzy
	void initialize();
        // Reset oczu
        void ResetEyes();
	// Czysci kolejke ruchow
	void cancelQueue();

signals:
	//void probeBehaviorFrame(void);

	// Ramka dla kamer
	void emitFrame(FrameCam);
	// Ramka dla serw
	void emitFrame(Frame);
        void sightPosition(double hPos,double vPos);
        void runningRequest(bool shouldRun);
	
private:
	// Obsluga jednego ruchu prostego - wyslanie nastawow na sprzet
	void sendSimpleMove(SimpleMove move);
    // Zabezpiecza przed probami jednoczesnego dostepu do danych.
    QMutex SafetyLock;
    // Okres z jakim wykonuje sie watek. Podany w ms.
    long sleepTimeMs;

	double OverrideEyesSpeed;
	double OverrideHeadSpeed;

	



	// Kolejka ruchow prostych. Kazdy ruch prosty obslugiwany w swoim czasie.
	std::queue<SimpleMove> simpleMovesQueue;
	// Kolejka mowy
	std::queue<SpeechQueueElem> speechQueue;
	// Ostatni ruch prosty - potrzebny do wyliczania predkosci 
	SimpleMove LastMove;
	QTime LastMoveTime;

        void CalculateNextPermitTime(const SimpleMove& toSend);
        bool PermitEyesMove;
        bool ResetEyesRequest;
        SimpleMove LastEyes;
        QTime PermitTime;


	//baza ruchow
	BElements* belements;
	// Konfiguracje serw.
    std::vector<ServoConfig> servConfigs;

	// Sledzenie celu
	SimpleMove LookAtTarget(const TargetInfo& target, double eyesUse);

        void SendSightPosition();
	// Generacja predkosci pomiedzy ruchami prostymi
	SimpleMove SetSpeed( SimpleMove s1 , SimpleMove s2,long  moveTime);
	// Wybor ruchu prostego na podstawie danego czasu, uzupelnia o predkosci
	SimpleMove ChooseSM(const std::vector<SimpleMove>& list, double moment, long moveTime);
    // Glowna funkcja watku.
    void run();
};

#endif // HEAD_CONTROL_THREAD_H

#include "Dynamixel.hh"

Dynamixel::Dynamixel()
{
}

Dynamixel::~Dynamixel()
{
  dyn_close(dpp);
}

void Dynamixel::InitDynamixelModule(std::vector<DrvServoConfig> * _Config,
				    bool ServosDebug,
				    std::string dev)
{
  Configs = _Config;
  dpp = &dyn_param;

  if(ServosDebug)
    dpp->debug=1;
  else
    dpp->debug=0;

  InitServoArray();
  dyn_init(dpp,dev.c_str(),57600,dynServoArray,Servo::TOTAL-5);
  if( dyn_connect(dpp) )
    {
      ModuleWorks=false;
      std::cerr<<"Driver: InitDynamixelModule(): error: module is off"<<std::endl;
    }
  else
    {
      ModuleWorks=true;
      CheckAndResetPositions();
    }
}

void Dynamixel::InitServoArray()
{
  dynServoArray=(dyn_servo_t*)malloc((Servo::TOTAL-4)*sizeof(dyn_servo_t));
  for (unsigned int i=0;i<Servo::TOTAL-5;++i) // bez 4 napędów kamer i ovveride eyes
    {
      dynServoArray[i].id=i;
      dynServoArray[i].return_level=Configs->at(i).ret_lev;
    }
}

void Dynamixel::CheckAndResetPositions()
{

  for(unsigned int i=0;i<Servo::TOTAL;i++)
    {
  /*    if(dyn_set_speed(dpp,i, 0)==0)
	{
*/
	  Online[i]=true; //jesli sie udalo, jest połączenie
	  LastPositions[i]=0;
	  LastVelocities[i]=0xfff; // ustawienie wartości poza zakresem
	                           // wymusza zadanie prędkości
	                           // podczas wykonywania pierwszego ruchu
	                           // (Driver::ServoControl(...))
/*
	}
      else
	{
	  std::cerr<<"Dynamixel: GetConfigs(): error: servo "<<i<<" is offline"<<std::endl;
	  Online[i]=false;
	}
*/
    }

}

void Dynamixel::SetSpeed(uint8_t ID, uint16_t NewSpeed)
{
if(NewSpeed!=0)
{
  if(Online[ID] and ModuleWorks)
     if(dyn_set_speed(dpp, ID, NewSpeed)==0)
       LastVelocities[ID]=NewSpeed;
}
else
LastVelocities[ID]=0;
}

void Dynamixel::SetPos(uint8_t ID, uint16_t NewPos)
{
  if(Online[ID] and ModuleWorks and LastVelocities[ID]!=0)
    if(dyn_set_position(dpp, ID, NewPos)==0)
      LastPositions[ID]=NewPos;
}

bool Dynamixel::IsMoving(uint8_t ID)
{
  if(Online[ID] and ModuleWorks)
    {
      if(dyn_get_moving(dpp, ID)==1)
	return true;
      else
	return false;
    }
  else
    return false;
}

double Dynamixel::GetDeg(Servo::ServoId id, double norm)
{

//double minDeg=HexToDeg ( Configs->at(id).minPosHex );
//double maxDeg=HexToDeg ( Configs->at(id).maxPosHex );

double minDeg=Configs->at(id).minPosDeg;
double maxDeg=Configs->at(id).maxPosDeg ;

double avg=(minDeg+maxDeg)/2;
double half=(maxDeg-minDeg)/2;

return avg+half*(norm-0.5)*2;
}

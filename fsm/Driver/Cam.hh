#ifndef CAM_HH
#define CAM_HH

#include "cam_utils.hh"
#include "color.hh"
#include <cstdlib>
#include "v4l2uvc.hh"
#include <iostream>
#include <unistd.h>
//#include <opencv/cv.h>
//#include <opencv/highgui.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/core/core_c.h"
#include "opencv2/videoio/legacy/constants_c.h"
#include "opencv2/highgui/highgui_c.h"

#include <cstdio>
#include <cstring>
#include <QObject>


/**
 * @brief Obiekt tej klasy to kamera (oko robota).
 *
 * Klasa posiada pola mówiące o stanie kamery, oraz metody do komunikacji
 * z urządzeniem.
 * Pozwala na sterowanie ruchem, oraz pobieranie obrazu.
 * @note Metody tej klasy są dedykowane pracy w systemie Linux.
 */
class Cam : public QObject
{
public:

  /**
   * Konstruktor ustawia wartości początkowe niektórych pól.
   */
  Cam();

  /**
   * Destruktor.
   */
  ~Cam();

  /**
   * Metoda inicjalizująca urządzenie i niektóre pola obiektu klasy Cam.
   * @param dev Nazwa urządzenia (np. "/dev/video0").
   * @return void 
   * @note Choć zwraca void, ustawiana jest wartość pola Works.
   */
  void CamInit(std::string dev);


  /**
   * Metoda, która pobiera z kamery obraz w formacie YUYV, i konwertuje
   * go na obraz typu IplImage (OpenCV). 
   * @return IplImage* Wskaźnik do obrazu typu IplImage.
   */

  //IplImage * getIplImage(); // old
  cv::Mat getMatImage();
  //IplImage ipl = cvIplImage(m); 
  //  Mat img = cv::cvarrToMat(ipl); 


  //Mat(const IplImage* img, bool copyData=false);
  
  /**
   * Metoda wysyła do kamery "polecenie resetu", i zerująca pola PosH i PosV.
   * @return  void
   */
  void Reset();

  /**
   * Metoda pozwala na zmianę ustawienia napędów kamery, o zadane wartości.
   * @param V_change Wertykalna zmiana położenia, w stopniach.
   * @param H_change Horyzontalna zmiana położenia, w stopniach.
   * @return void
   */
  void MoveVH(int V_change, int H_change);
  
  /**
   * Zwraca aktualnie ustawioną pozycję "napędu horyzontalnego"
   * (tj. napędu odpowiedzialnego za zmianę położenia w poziomie) kamerki.
   * @return int Wartość pola prywatnego PosH.
   */
  int getPosH() const;

  /**
   * Zwraca aktualnie ustawioną pozycję "napędu wertykalnego"
   * (tj. napędu odpowiedzialnego za zmianę położenia w pionie) kamerki.
   * @return int Wartość pola prywatnego PosV.
   */
  int getPosV() const;

  private:
  
  /**
   * Nazwa urządzenia systemowego (np. /dev/video1).
   */
  const char *videodevice;

  /**
   * Format pobieranego obrazu (np. V4L2_PIX_FMT_YUY).
   */
  int format;

  /**
   * Metoda pobierania obrazu (np. 1).
   */
  int grabmethod; 

  /**
   * Szerokość obrazu w pikselach.
   */
  int width; 

  /**
   * Wysokość obrazu w pikselach.
   */
  int height; 

  /**
   * Liczba klatek pobieranych na sekundę (np. 30).
   */
  int fps; 

  /**
   *  Wskaźnik do obrazu w formacie IplImage (dla OpenCV).
   */
  //IplImage * img; //old
  cv::Mat img;

  /**
   * Strutura obsługująca kamerą (Video for Linux).
   */
  struct vdIn *videoIn;

  /**
   * Wskźnik do obszaru pamięci zawierającego obraz z kamery.
   */
  unsigned char *picture;

  /**
   * Aktualna pozycja kamery w poziomie.
   */
  int PosH; 

  /**
   * Aktualna pozycja kamery w pionie.
   */
  int PosV; 

  /**
   * Metoda konwertująca obraz w formacie RGB2 na IplImage.
   * @param img Wskaznik do obszaru w ktorym zapisany zostanie obraz IplImage.
   * @param rgbArray Wskaznik do tablicy zawierającej obraz RGB.
   * @param width Szerokość obrazu.
   * @param height Wysokość obrazu.
   * @param return void.
   */
  /*
  void RGB2IplImage(IplImage *img,
		    const unsigned char* rgbArray, 
		    unsigned int width, 
		    unsigned int height);
  */
  void RGB2MatImage(cv::Mat &img,
		    const unsigned char* rgbArray, 
		    unsigned int width, 
		    unsigned int height);


  /**
   * true, jeśli kamera zainicjalizowała się poprawnie. 
   * Jeśli false, żadna komenda nie zostanie wysłana do urządzenia.
   */
  bool Works;
    
};

#endif // CAM_HH

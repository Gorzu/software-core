#include "facedetector.hh"

extern SoundSource soundSource;//dźwięki

using namespace std;
//parametry kamery i obrazu
//const int obraz_szer=640;
//const int obraz_dlug=480;
//const int kat_widzenia_x = 49;
//const int kat_widzenia_y = 37;


typedef std::list<Face>::iterator ListIter;
#define Looop(l) for(ListIter it = l.begin() ; it!=l.end(); it++)

FaceDetector::FaceDetector() {
        shouldRun = false;
	storage = 0;
	//cascade = 0;
	wartosc_przeguby_x = 0;
	wartosc_przeguby_y = 0;
	obrotX = 0;
	obrotY = 0;
	IDS = 0;

	zwracany_kat_x = 0;
	zwracany_kat_y = 0;
	ile_zaufanych_twarzy = 0;

	//read XML configxml
	QDomDocument doc("FaceDetectorConfig");
	QFile file(FACEDETECTOR_CONFIG_XML);
	if (!file.open(QIODevice::ReadOnly)) {
		std::cerr
				<< "FaceDetector::FaceDetector():: error: can't open FACEDETECTOR_CONFIG_XML file"
				<< std::endl;
		exit(-1);
	}
	if (!doc.setContent(&file)) {
		file.close();
		std::cerr
				<< "FaceDetector::FaceDetector():: error: can't set XML file content"
				<< std::endl;
		exit(-2);
	}
	file.close();

	QDomElement root = doc.documentElement();
	if (root.tagName() != "FaceDetectorConfig") {
		std::cerr
				<< "FaceDetector::FaceDetector():: error: can't find root in FACEDETECTOR_CONFIG_XML file "
				<< std::endl;
		exit(-3);
	}

	//Odczyt parametrów
	QDomNode n = root.firstChild();
	QDomElement e = n.toElement();
	if (e.tagName() == "configs") {
		_ileKlatek = e.attribute("FrameRate", "").toInt();
		kat_widzenia_y = e.attribute("ViewRangeVertical", "").toInt();
		kat_widzenia_x = e.attribute("ViewRangeHorizontal", "").toInt();
	} else {
		std::cerr
				<< "FaceDetector::FaceDetector()::error: cfg/FaceDetectorConfig.xml wrong construction"
				<< std::endl;
		exit(-4);
	}

	//prog_gorny = 0.06984; // 4 stopnie w radianach
	//prog_dolny = 0.06984; // 4 stopnie w radianach
	prog_gorny = 5; //5 stopni w stopniach
	prog_dolny = 5; //5 stopni w stopniach
	prog_tlumu = 3;

	_zadanyCzasWykonania = 1.0 / _ileKlatek; // czas wykonywania jednej klatki

	//curr_img = cvCreateImage(cvSize(640, 480), 8, 3);
//curr_img = cvCreateImage(cvSize(320, 240), 8, 3); //!!S
}

void FaceDetector::setDrv(Driver* drv) {
	_drv = drv;
}

void FaceDetector::setWin(MyCameraWindow* _win) { 
	win = _win;

}

void FaceDetector::runningRequest(bool req) {
	shouldRun = req;

}

void FaceDetector::receivePosition(double hAngle, double vAngle) {
	faceMutex.lock();
	wartosc_przeguby_x = hAngle;
	wartosc_przeguby_y = vAngle;

	obrotX = -160 + (wartosc_przeguby_x * obraz_szer / kat_widzenia_x);
	obrotY = -120 + (wartosc_przeguby_y * obraz_dlug / kat_widzenia_y);
	faceMutex.unlock();
}

void FaceDetector::run() {
	std::cout << "FaceDetector::Start" << std::endl;

	int state = 1;
 const char* cascade_name = "haarcascade_frontalface_alt.xml";
        //cascade = (CvHaarClassifierCascade*) cvLoad(cascade_name, 0, 0, 0); // old
		//cascade = (CvHaarClassifierCascade*) cv::load(cascade_name, 0, 0, 0);
	

	if(!cascade.load(cascade_name))
		cout << "Blad ladowania kaskady" << endl;
		/*
        if (!cascade) {
                fprintf(stderr, "BLAD: nie mozna zaladowac klasyfikatora\n");
                state = 0;
	} */

	//storage = cvCreateMemStorage(0);

	msleep(1000);//wstępne opóźnienie
	while (state) {
		if (shouldRun) {
			//marker czasu początkowy cascade_tp.tv_sec);
			_usec = static_cast<double> (_tp.tv_usec) / 1E6;
			_poczatek = _sec + _usec;

			//pobranie obrazka
			//IplImage obraz = *_drv->RightCam.getIplImage();

	    // Mat img = cv::cvarrToMat(iplimg); 

			//cv::Mat obraz = *_drv->RightCam.getIplImage(); // old
			//cv::Mat obraz = cv::cvarrToMat(_drv->RightCam.getIplImage()); 
			cv::Mat obraz = _drv->RightCam.getMatImage();
			cv::Size s = obraz.size();
			//obraz_szer = obraz.width;  // old
			//obraz_dlug = obraz.height; // old
			obraz_szer = s.width;
			obraz_dlug = s.height;

			//kinematyk w momencie akwizycji
			obrotYtmp = obrotY;
			obrotXtmp = obrotX;
			//wywolanie funkcji wykrywania i filtracji ryjkow
			wykryj_filtruj_rysuj(obraz);

			//marker czasu początkowy
			gettimeofday(&_tp, NULL);
			_sec = static_cast<double> (_tp.tv_sec);
			_usec = static_cast<double> (_tp.tv_usec) / 1E6;
			_koniec = _sec + _usec;

			_opoznienie = _zadanyCzasWykonania - (_koniec - _poczatek);

			if (_opoznienie > 0.0) {
				msleep(static_cast<unsigned long> (_opoznienie * 1000));
			}
		} else {
			msleep(5);//krótkie uśpienie jeśli czeka na zakończenie ruchu, etc.
		}
	}
}

//int FaceDetector::wykryj_filtruj_rysuj(IplImage* img) {
int FaceDetector::wykryj_filtruj_rysuj(cv::Mat img) {
	ile_zaufanych_twarzy = 0;
	double tmp_kat;
	int tmp_premia;
	int mowi;
	int znaleziony;
	int scale = 1;
	//IplImage* temp = cvCreateImage(cvSize(img->width / scale, img->height / scale), 8, 3);//temp -- ???
	cv::Mat temp = cv::Mat(cvSize(img.size().width / scale, img.size().height / scale), 8, 3);
	//CvFont myFont; //czcionka
	//cvInitFont(&myFont, CV_FONT_HERSHEY_DUPLEX, 0.5, 0.5); //czcionka
	char numer_twarzy[8]; //do wypisania numeru twarzy
	CvPoint pt1, pt2;
	int i;

	//cvClearMemStorage(storage);
	//CvSeq* faces = 0;
	vector<cv::Rect> faces;

	Face Nowy;
	bool dodaj;

	if (!cascade.empty()) {
                //std::cout << "detekcja" << std::endl;
                // faces = cvHaarDetectObjects(img, cascade, storage, 1.2, 5,
                                // CV_HAAR_DO_CANNY_PRUNING, cvSize(25, 25)); //klasyfikator haara
   			    cascade.detectMultiScale(img, faces, 1.2, 5,
                                cv::CASCADE_DO_CANNY_PRUNING, cvSize(25, 25)); //klasyfikator haara
		/*
		cvLine(img, cvPoint(obraz_szer / 2, 0), cvPoint(obraz_szer / 2,
				obraz_dlug), CV_RGB(200,250,250)); //linia pionowa
		cvLine(img, cvPoint(0, obraz_dlug / 2), cvPoint(obraz_szer, obraz_dlug
				/ 2), CV_RGB(200,250,250)); //linia pozioma
		for (i = 0; i < (faces ? faces->total : 0); i++) {
			CvRect* r = (CvRect*) cvGetSeqElem(faces, i);
		*/
		cv::line(img, cvPoint(obraz_szer / 2, 0), cvPoint(obraz_szer / 2,
				obraz_dlug), CV_RGB(200,250,250)); //linia pionowa
		cv::line(img, cvPoint(0, obraz_dlug / 2), cvPoint(obraz_szer, obraz_dlug
				/ 2), CV_RGB(200,250,250)); //linia pozioma
		
		//for (i = 0; i < (faces ? faces->total : 0); i++) {
		for(auto r:faces){
			//CvRect* r = (CvRect*) cvGetSeqElem(faces, i); //old
			//PUNKTY PROSTOKATA OBRYSU
			//UWZGLEDNIENIE PRZESUNIECIA
//			pt1.x = r->x * scale + (obrotX);
//			pt2.x = (r->x + r->width) * scale + (obrotX);
//			pt1.y = r->y * scale + (obrotY);
//			pt2.y = (r->y + r->height) * scale + (obrotY);

			pt1.x = r.x * scale + (obrotXtmp);
			pt2.x = (r.x + r.width) * scale + (obrotXtmp);
			pt1.y = r.y * scale + (obrotYtmp);
			pt2.y = (r.y + r.height) * scale + (obrotYtmp);

			//TWORZENIE NOWEJ FACJATY DO SPRAWDZENIA
			//UWZGLEDNIENIE PRZESUNIECIA KAMERY
			//SRODEK OBRYSU
//			Nowy.x = r->x + r->width / 2 + (obrotX);
//			Nowy.y = r->y + r->height / 2 + (obrotY);
			Nowy.x = r.x + r.width / 2 + (obrotXtmp);
			Nowy.y = r.y + r.height / 2 + (obrotYtmp);
			Nowy.pt1 = pt1;
			Nowy.pt2 = pt2;

			//
			//OBRYS BEZ PRZESUNIECIA - WYSWIETLONY
			pt1.x = r.x * scale;
			pt2.x = (r.x + r.width) * scale;
			pt1.y = r.y * scale;
			pt2.y = (r.y + r.height) * scale;

			//cvRectangle(img, pt1, pt2, CV_RGB(100,100,100), 2, 8, 0); //obrys twarzy
			cv::rectangle(img, pt1, pt2, CV_RGB(100,100,100), 2, 8, 0); //obrys twarzy
			// numer_twarzy=intToStr(i); //numer wykrytej twarzy, z int do char*


			dodaj = true;
			if (!FACES.empty()) { //NIE PUSTA
				Looop(FACES) {
					if ((it->Sprawdz(Nowy))) {
						//JEST W POBLIZU
						dodaj = false;
						break; //jak nowy -- nic więcej nie rób
					}
				}

				if (dodaj) {
					IDS++;
					Nowy.ID = IDS;
					Nowy.Tolerancja();
					FACES.push_front(Nowy);
                                        //cout << "DODANO DO LISTY" << endl;
				}
			} else //PUSTA
			{
				IDS++;
				Nowy.ID = IDS;
				Nowy.Tolerancja();
				FACES.push_front(Nowy);
				//cout<<"LISTA PUSTA"<<endl;
			}

		}

		Looop(FACES) {
			//TO MOZE BYC W JEDNEJ FUNKCJI
			//UFA albo NIE
			if (it->wykryto) {
				it->wykryto = false;
				it->ufaj(); //UF UF
			} else {
				it->nieufaj();
			}

			if (it->z > granica_ufnosci) {
				//WYSWIETLANIE TWARZY
				//UWZGLEDNIENIE PRZE0SUNIECIA
				pt1 = it->pt1;
				pt2 = it->pt2;
				//pt2.y -= obrotY;
				pt1.x -= obrotXtmp;
				pt2.x -= obrotXtmp;
				pt1.y -= obrotYtmp;
				pt2.y -= obrotYtmp;
				cv::rectangle(img, pt1, pt2, CV_RGB(255,0,0), 2, 8, 0); //obrys zaufanej twarzy
				sprintf(numer_twarzy, "%d", it->ID);//numer zaufanej twarzy, z int do char*
				//cv::putText(img, numer_twarzy, cvPoint(pt1.x, pt1.y - 5), &myFont, cvScalar(0, 0, 255)); //rysuj numer zaufanej twarzy
				cv::putText(img, numer_twarzy, cv::Point(pt1.x, pt1.y - 5), cv::FONT_HERSHEY_DUPLEX, 0.5, cvScalar(0, 0, 255)); //rysuj numer zaufanej twarzy
				//pt1.x = it->x - obrotX;
				//pt1.y = it->y - obrotY;
				pt1.x = it->x - obrotXtmp;
				pt1.y = it->y - obrotYtmp;
				pt2 = pt1;
				pt1.x -= it->Tx;
				pt1.y -= it->Ty;

				pt2.x += it->Tx;
				pt2.y += it->Ty;

				cv::rectangle(img, pt1, pt2, CV_RGB(255,200,0), 1, 8, 0); //rysuj zakres tolerancji zaufanej twarzy
			}

			if (it->z < 3) {
				it = FACES.erase(it);
			}
		}

		faceMutex.lock();
		znaleziony = false;
		mowi = 0;
		int tmpGranicaUfnosci = granica_ufnosci;
		Looop(FACES) {
			//dopisywanie dźwięku
			tmp_kat = it->x * kat_widzenia_x / obraz_szer - soundSource.angle;
			if ((tmp_kat < prog_gorny) && (tmp_kat > prog_dolny)) {
				tmp_premia = premia_dzwieku;
				soundSource.isLonely = false;
			} else {
				tmp_premia = 0;
			}
			//właściwe przeszukiwanie listy twarzy
			if (it->z + tmp_premia > granica_ufnosci) {
				ile_zaufanych_twarzy++;
			}
			if (it->z + tmp_premia >= tmpGranicaUfnosci) {
				// zmiana progu tak, aby wybierana była twarz z maksymalnym poziomem zaufania
				tmpGranicaUfnosci = it->z + tmp_premia;

				znaleziony = true;
				mowi = tmp_premia;
				//it->z += 20;
				//if(it->z > 200) it->z = 200;
				zwracany_kat_x = it->x * kat_widzenia_x / obraz_szer;
				zwracany_kat_y = it->y * kat_widzenia_y / obraz_dlug;
                                //cout << "\nTEEEST: " << zwracany_kat_x << " " << zwracany_kat_y
                                //		<< "\n"; //dopisane
			}
		}

		//budowanie TargetInfo
		if (ile_zaufanych_twarzy) {
			if (znaleziony) {
				target.exist = true;
				//if (((zwracany_kat_x - target.visionHorizontalAngle
					//	< prog_gorny) && (zwracany_kat_x
						//- target.visionHorizontalAngle > prog_dolny))
					//	&& ((zwracany_kat_y - target.visionVerticalAngle
						//		< prog_gorny) && (zwracany_kat_y
							//	- target.visionVerticalAngle > prog_dolny))) {
				if(abs((zwracany_kat_x - target.visionHorizontalAngle) < prog_gorny)&&(abs(zwracany_kat_y - target.visionVerticalAngle)< prog_gorny)){
					target.newTarget = false;
					target.visionHorizontalAngle = zwracany_kat_x;
					target.visionVerticalAngle = zwracany_kat_y;
				} else {
					target.newTarget = true;
					target.visionHorizontalAngle = zwracany_kat_x;
					target.visionVerticalAngle = zwracany_kat_y;
				}
				if (mowi) {
					target.targetSpeaking = true;
					target.otherSpeaking = false;
					target.soundHorizontalAngle = soundSource.angle;
					target.soundVerticalAngle = 0;//nie dotyczy
				} else {
					target.targetSpeaking = false;

				}

			} else {
				target.exist = false;
				target.newTarget = false;
				target.targetSpeaking = false;
				//target.visionHorizontalAngle = 179;
				//target.visionVerticalAngle = 180;
			}
			if (ile_zaufanych_twarzy > prog_tlumu) {
				target.crowd = true;
			} else {
				target.crowd = false;
			}
		} else {
			target.exist = false;
			target.newTarget = false;
			target.targetSpeaking = false;
			//target.visionHorizontalAngle = 178;
			//target.visionVerticalAngle = 180;
		}
		if ((soundSource.found) && (!mowi)) {
			target.otherSpeaking = true;
			target.soundHorizontalAngle = soundSource.angle;
			target.soundVerticalAngle = 0;//nie dotyczy
		}
		if (!soundSource.found) {
			target.targetSpeaking = false;
			target.otherSpeaking = false;
			target.soundHorizontalAngle = 0;
		}
		curr_img = img;
		faceMutex.unlock();
		msleep(10);
                //std::cout << target.exist << std::endl;
		emit
		sendTargetInfo(target);

                //cout << endl << endl << "**************************" << endl;
                //cout << "Wartosc przegubu X: " << wartosc_przeguby_x << endl;
                //cout << "Wartosc przegubu Y: " << wartosc_przeguby_y << endl;
                //cout << "**************************" << endl;

                cout << "Widze " << ile_zaufanych_twarzy << " zaufane twarze \n";
                //cout << "Zwracam katy " << zwracany_kat_x << " " << zwracany_kat_y
                //		<< "\n";

                //Looop(FACES) {
                //	cout << "\tTwarz ID: " << it->ID << "\tx: " << it->x << "\ty: "
                //			<< it->y << "\t\tufnosc: " << it->z << endl;
                //}

	}

if(win->PreviewCheckBox1->checkState()==2)
{
//win->image=*img;
    //cvCopy(img,win->image);
	//cv::MatAllocator::copy(img,image);
	win->image = img;
}

	//cvNamedWindow( "Obraz z kamery", 1 );
	//cvShowImage("Obraz z kamery", img);
	
	//cvReleaseImage(&temp);
	
	//cvSaveImage("temp.jpg",img);
	//cvDestroyWindow("Obraz z kamery");
	return ile_zaufanych_twarzy;
}

/*
 char* intToStr(int n) //konwersja z int do string
 {
 int i = 0;
 char *tmp = (char*) malloc(12); // stdlib
 char *ret = (char*) malloc(12);
 if (n < 0) {
 *ret = '-';
 i++;
 n = -n;
 }
 do {
 *tmp = n % 10 + 48;
 n -= n % 10;
 if (n > 9)
 *tmp++;
 } while (n /= 10);
 while (ret[i++] = *tmp--)
 ;
 return ret;
 }*/


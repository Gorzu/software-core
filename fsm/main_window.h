#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QtGui/QMainWindow>
#include <QFileDialog>
#include <QHBoxLayout>
#include <QThread>
#include <QString>
#include "main_thread.h"
#include "head_control_thread.h"
#include "BElements.h"
#include "head_gl.h"
#include "moves.h"
#include "XmlRW.h"
#include "Driver/Driver.hh"
#include "facedetector.hh"

#include <map>


const QString DEFAULT_FSM = "FSM_def.xml";
const QString DEFAULT_BEL = "BEL_def.xml";


namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow();
    void setUpConnections();
	void setVis(HeadGL *_vis);
        void setDrv(Driver *_drv);
        void setDet(FaceDetector *_facedetector);
	void setHeadControlThread(HeadControlThread *_headControl);
	void setMainThread(MainThread *_mainThread);
    void startThreads();
    ~MainWindow();

    QWidget * GLWidget;

public slots:
	void writeText(QString);
	void startFsm();
	void StopFSMButton();
        void ResetEyesButton();
	void headSendBtn();
        void VisEnable(bool);

        void LoadFSM();
	void LoadBElements();

        void LoadFSMFile(const QString & file);
        void LoadBElementsFile(const QString & file);
signals:
    void startMainThread(QThread::Priority prior);
    void startHeadControlThread(QThread::Priority prior);

	void emitBFrame(BFrame);
        void emitFrame(Frame);
	void cancelQueue();
	void sendHead(TargetInfo);

private:
        FSM fsm;
	BElements belements;

    Ui::MainWindow *ui;
    HeadGL * vis;
    Driver * drv;
    FaceDetector * facedetector;
	HeadControlThread *headControl;
	MainThread *mainThread;

        protected:

        void timerEvent(QTimerEvent*);
        int ResetControlCounter;

};

#endif // MAIN_WINDOW_H

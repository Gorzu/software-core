#include <QtGui/QApplication>
#include <QObject>
#include <QMetaType>
#include <QThread>
#include "main_window.h"
#include "head_control_thread.h"
#include "main_thread.h"
#include "head_gl.h"
#include "utils.h"
#include "Driver/Driver.hh"
#include "wysylaczipl.h"
#include <iostream>

#include "QOpenCVWidget.hh" 
#include "MyCameraWindow.hh" 

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	//a.setApplicationName("Szatan"); // ???
    a.setApplicationName("Main App");

	MainThread *brain = new MainThread();
Driver * drv = new Driver();
	HeadControlThread *headControl = new HeadControlThread(100);
headControl->setDrv(drv);


    MainWindow w;
	MyCameraWindow * camWin=new MyCameraWindow();
    HeadGL * vis = new HeadGL("face.ASE", w.GLWidget);
    

    //    char* deviceName = "/dev/ttyUSB2"; //lepiej jako argument by było ale nie wiem co jest
        SoundLocalizationSubsystem* ucho = new SoundLocalizationSubsystem();
        ucho->start();
        FaceDetector* oko = new FaceDetector;
        oko->setDrv(drv);
oko->setWin(camWin);
        oko->start();



        WysylaczIPL* serwer = new WysylaczIPL();
        serwer->setFaceDetector(oko);
        serwer->start();

    w.setVis(vis);
    w.setDrv(drv);
    w.setDet(oko);
	w.setMainThread(brain);
	w.setHeadControlThread(headControl);
    w.setUpConnections();


    camWin->setWindowTitle("Right eye preview");
    camWin->show();    

    w.show();
    headControl->setFaceDet(oko);
    vis->start();
    vis->disable();
	headControl->start();

    return a.exec();
}

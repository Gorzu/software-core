 #include <netinet/in.h>
 #include <sys/socket.h>
 #include <arpa/inet.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <unistd.h>
 #include <pthread.h>
 //#include "opencv/cv.h"
 //#include "opencv/highgui.h"
 #include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include "opencv2/core/core_c.h"
#include "opencv2/videoio/legacy/constants_c.h"
#include "opencv2/highgui/highgui_c.h"

#include "wysylaczipl.h"


/* konwerter obrazu do 'Mat' (refaktoryzacja kodu z openCV 2 do 4) */
using namespace cv;

/*
cv::Mat image_to_mat(image im)
{
image copy = copy_image(im);
constrain_image(copy);
if(im.c == 3) rgbgr_image(copy);

Mat m(cv::Size(im.w,im.h), CV_8UC(im.c));
int x,y,c;

int step = m.step;
for(y = 0; y < im.h; ++y){
    for(x = 0; x < im.w; ++x){
        for(c= 0; c < im.c; ++c){
            float val = im.data[c*im.h*im.w + y*im.w + x];
            m.data[y*step + x*im.c + c] = (unsigned char)(val*255);
        }
    }
}

free_image(copy);
return m;
}
*/

/* konwerter 'Mat' do obrazu (refaktoryzacja kodu z openCV 2 do 4) */
/*
image mat_to_image(Mat m)
{

int h = m.rows;
int w = m.cols;
int c = m.channels();
image im = make_image(w, h, c);
unsigned char *data = (unsigned char *)m.data;
int step = m.step;
int i, j, k;

for(i = 0; i < h; ++i){
    for(k= 0; k < c; ++k){
        for(j = 0; j < w; ++j){
            im.data[k*w*h + i*w + j] = data[i*step + j*c + k]/255.;
        }
    }
}
rgbgr_image(im);
return im;
}
*/


WysylaczIPL::WysylaczIPL()
{
    okoWskaznik=NULL;
    is_data_ready = 0;
}

void WysylaczIPL::setFaceDetector(FaceDetector* oko)
{
    okoWskaznik=oko;
}

void WysylaczIPL::run(){
    //Tutaj przyjecie polaczenia
    #define PORT 8888

 int is_data_ready = 0;
 int serversock, clientsock;

 //pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;


 int key;

 //if (argc == 2) {
 //capture = cvCaptureFromFile(argv[1]);
 //} else {
 //capture = cvCaptureFromCAM(0);
 //}

 //if (!capture) {
 //quit("cvCapture failed", 1);
 //}
 //CvCapture* capture2;
//IplImage* img0=cvCreateImage(cvSize(640, 480), IPL_DEPTH_8U, 3);
 //capture2 = cvCaptureFromFile("temp.jpg");
 //IplImage*     img0 = cvQueryFrame(capture2);

 //img1 = cvCreateImage(cvGetSize(img0), IPL_DEPTH_8U, 3);

 //cvZero(img1);

 //cvNamedWindow("stream_server", CV_WINDOW_AUTOSIZE);

 /* print the width and height of the frame, needed by the client */
 //fprintf(stdout, "width: %d\nheight: %d\n\n", img0->width, img0->height);
 //fprintf(stdout, "Press 'q' to quit.\n\n");

 /* run the streaming server as a separate thread */
 //if (pthread_create(&thread_s, NULL, streamServer, this)) {
 //this->quit("pthread_create failed.", 1);
 //}

 is_data_ready = 1;


 /* user has pressed 'q', terminate the streaming server */
 //if (pthread_cancel(thread_s)) {
 //this->quit("pthread_cancel failed.", 1);
 //}


  struct sockaddr_in server;
 //WysylaczIPL* wysylacz;
 //wysylacz=(WysylaczIPL*) arg;

  //IplImage* img0 = this->okoWskaznik->curr_img;  // old
  cv::Mat img0 = this->okoWskaznik->curr_img;
  //std::cout << this->okoWskaznik->curr_img;
  //IplImage img0 = cvIplImage(&okoWskaznik); // tutaj trzeba wymusić, aby podać obiekt jako argument
  

 /* make this thread cancellable using pthread_cancel() */
 //pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
 //pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);

 /* open socket */
 if ((this->serversock = socket(PF_INET, SOCK_STREAM, 0)) == -1) {
 //this->quit("socket() failed", 1);
 }

 /* setup server's IP and port */
 memset(&server, 0, sizeof(server));
 server.sin_family = AF_INET;
 server.sin_port = htons(PORT);
 server.sin_addr.s_addr = INADDR_ANY;

 /* bind the socket */
 if (bind(this->serversock, (const sockaddr*)&server, sizeof(server)) == -1) {
 //this->quit("bind() failed", 1);
 }

 /* wait for connection */
 if (listen(this->serversock, 10) == -1) {
 //this->quit("listen() failed.", 1);
 }

 /* accept a client */
 if ((this->clientsock = accept(this->serversock, NULL, NULL)) == -1) {
 //this->quit("accept() failed", 1);
 }

 /* the size of the data to be sent */
 //int imgsize = img0->imageSize*3;
 //int bytes, i;

 /* start sending images */
 while(1)
{
    /* send the grayscaled frame, thread safe */
     img0=this->okoWskaznik->curr_img;
     cv::imwrite("temp0.jpg", img0);
    //img0=okoWskaznik->curr_img;
    

    /* Nie wiadomo do czego sluzy client/socket */
    /*
    imgsize = img0->imageSize*3;
    bytes=0;

    //if (this->is_data_ready) {
    bytes = send(this->clientsock, img0->imageData, imgsize, 0);
    //this->is_data_ready = 0;
    //}


    //cvSaveImage("temp0.jpg",img0); // old
   
    // TO DO  
    //cv::Mat zm = cv::cvarrToMat(img0); // konwersja IplImage do Mat. W przyszłości do zrobienia na wczesniejszym etapie  
    //cv::imwrite("temp0.jpg", zm);
    

    this->okoWskaznik->faceMutex.unlock();
usleep(10000);

    // if something went wrong, restart the connection 
    if (bytes != imgsize) {
    fprintf(stderr, "Connection closed.\n");
    close(this->clientsock);

    if ((this->clientsock = accept(this->serversock, NULL, NULL)) == -1) {
    //this->quit("accept() failed", 1);

    }
 }

 // have we terminated yet? 
 //pthread_testcancel();

 // no, take a rest for a while 
 usleep(1000);
 */
 }


 /* free memory */
 //cvDestroyWindow("stream_server");
 //quit(NULL, 0);
 }



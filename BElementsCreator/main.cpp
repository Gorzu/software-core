#include <QtGui/QApplication>
#include <QMetaType>
#include <QThread>
#include "main_window.h"
#include "head_control_thread.h"
#include "head_gl.h"
#include "utils.h"
#include "Driver/Driver.hh"

#include <iostream>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
a.setApplicationName( "Szatan" ); //needed for Phonon/ DBUS will fix it.



    MainWindow w;
    HeadGL * vis = new HeadGL("face.ASE", w.GLWidget);
    Driver * drv = new Driver;

    HeadControlThread *headControl = new HeadControlThread(100);
    headControl->setDrv(drv);

    w.setVis(vis);
	w.setHeadControlThread(headControl);
    w.setDrv(drv);
    w.setUpConnections();
    w.show();
    vis->start();
	headControl->start();
    return a.exec();
}

/****************************************************************************
** Meta object code from reading C++ file 'head_gl.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "head_gl.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'head_gl.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_HeadGL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      11,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
       7,   31,   31,   31, 0x05,
      32,   31,   31,   31, 0x05,

 // slots: signature, parameters, type, tag, flags
      49,   69,   31,   31, 0x0a,
      75,   69,   31,   31, 0x0a,
      98,   31,   31,   31, 0x0a,
     146,  186,   31,   31, 0x0a,
     259,  293,   31,   31, 0x2a,
     355,  383,   31,   31, 0x2a,
     424,  446,   31,   31, 0x2a,
     466,   31,   31,   31, 0x2a,
     484,   31,   31,   31, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_HeadGL[] = {
    "HeadGL\0errorOccured(exception)\0\0"
    "sendMsg(QString)\0receiveFrame(Frame)\0"
    "frame\0receiveFrame(FrameCam)\0"
    "receiveFaceCoor(std::map<QString,facePartCoor>)\0"
    "setBezierConfig(bool,float,float,float)\0"
    "drawBezierControls_,bezierControlLengthX,bezierControlLengthZ,lipsRadi"
    "us\0"
    "setBezierConfig(bool,float,float)\0"
    "drawBezierControls_,bezierControlLengthX,bezierControlLengthZ\0"
    "setBezierConfig(bool,float)\0"
    "drawBezierControls_,bezierControlLengthX\0"
    "setBezierConfig(bool)\0drawBezierControls_\0"
    "setBezierConfig()\0"
    "setServConf(std::vector<ServoConfig>)\0"
};

void HeadGL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        HeadGL *_t = static_cast<HeadGL *>(_o);
        switch (_id) {
        case 0: _t->errorOccured((*reinterpret_cast< exception(*)>(_a[1]))); break;
        case 1: _t->sendMsg((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->receiveFrame((*reinterpret_cast< Frame(*)>(_a[1]))); break;
        case 3: _t->receiveFrame((*reinterpret_cast< FrameCam(*)>(_a[1]))); break;
        case 4: _t->receiveFaceCoor((*reinterpret_cast< std::map<QString,facePartCoor>(*)>(_a[1]))); break;
        case 5: _t->setBezierConfig((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3])),(*reinterpret_cast< float(*)>(_a[4]))); break;
        case 6: _t->setBezierConfig((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2])),(*reinterpret_cast< float(*)>(_a[3]))); break;
        case 7: _t->setBezierConfig((*reinterpret_cast< bool(*)>(_a[1])),(*reinterpret_cast< float(*)>(_a[2]))); break;
        case 8: _t->setBezierConfig((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 9: _t->setBezierConfig(); break;
        case 10: _t->setServConf((*reinterpret_cast< std::vector<ServoConfig>(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData HeadGL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject HeadGL::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_HeadGL,
      qt_meta_data_HeadGL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &HeadGL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *HeadGL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *HeadGL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_HeadGL))
        return static_cast<void*>(const_cast< HeadGL*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int HeadGL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 11)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 11;
    }
    return _id;
}

// SIGNAL 0
void HeadGL::errorOccured(exception _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void HeadGL::sendMsg(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE

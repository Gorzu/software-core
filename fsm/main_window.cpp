#include "main_window.h"
#include "ui_main_window.h"
#include "head_gl_utils.h"
#include "moves.h"
#include <vector>

MainWindow::MainWindow():QMainWindow(0)
{
	
    ui = new Ui::MainWindow;

    ui->setupUi(this);

	GLWidget = ui->visWidget;

	connect(ui->StartFSMButton,SIGNAL(clicked()),
			this,SLOT(startFsm()));
	connect(ui->headSendBtn,SIGNAL(clicked()),
			this, SLOT(headSendBtn()));

	connect(ui->actionLoadFSM, SIGNAL(triggered()),
			this, SLOT(LoadFSM()));
	connect(ui->actionLoadBElements, SIGNAL(triggered()),
			this, SLOT(LoadBElements()));


            //startTimer(0.5* 60000);
        ResetControlCounter=0;
        startTimer(100); //0.1 sek



}

void MainWindow::timerEvent(QTimerEvent*) {

    switch(ResetControlCounter)
    {
        case 100: // powieki w dol, 10 sek
         headControl->PermitLidsMove=false;
        std::cerr<<"reset cykliczny"<<std::endl;
        headControl->ResetEyes();
        //break;
        //case 105: // reset

         std::cerr<<"powieki w dol"<<std::endl;

         emit emitFrame(Frame(Servo::RightEyeLidR, QTime::currentTime(),.5, .46));
         emit emitFrame(Frame(Servo::LeftEyeLidR, QTime::currentTime(),.5,  .46));
         emit emitFrame(Frame(Servo::RightEyeLidV, QTime::currentTime(),1,  .46));
         emit emitFrame(Frame(Servo::LeftEyeLidV, QTime::currentTime(),1,   .46));
        break;
        case 113:
        std::cerr<<"powieki w gore"<<std::endl;
         emit emitFrame(Frame(Servo::RightEyeLidR, QTime::currentTime(),.5, .46));
         emit emitFrame(Frame(Servo::LeftEyeLidR, QTime::currentTime(),.5,  .46));
         emit emitFrame(Frame(Servo::RightEyeLidV, QTime::currentTime(),0,  .46));
         emit emitFrame(Frame(Servo::LeftEyeLidV, QTime::currentTime(),0,   .46));

         ResetControlCounter=0;
                  headControl->PermitLidsMove=true;
        break;
    }

        ResetControlCounter++;
}

void MainWindow::LoadFSM(){
        QString file = QFileDialog::getOpenFileName(this,
                tr("Open FSM XML"), "xml", tr("XML Files (*.xml)"));
        LoadFSMFile(file);
}

void MainWindow::LoadBElements(){
        QString file = QFileDialog::getOpenFileName(this,
                tr("Open BElements XML"), "xml", tr("XML Files (*.xml)"));
        LoadBElementsFile(file);

}

void MainWindow::LoadFSMFile(const QString& file){

        fsm = GetFSMfromFile( file );
	ui->CurrentStateComboBox->clear();
	ui->PreviousStateComboBox->clear();
	for ( std::map<QString, State>::iterator it = fsm.states.begin() ; it != fsm.states.end() ; it++){
		ui->CurrentStateComboBox->addItem( it->second.name );
		ui->PreviousStateComboBox->addItem( it->second.name );
	}
	
	mainThread->SetFSM ( fsm );
}

void MainWindow::LoadBElementsFile(const QString& file){

	belements = GetBElementsFromFile( file );
	headControl->SetBElements(&belements);
}

void MainWindow::setVis(HeadGL * _vis)
{
    vis = _vis;
}

void MainWindow::setDrv(Driver * _drv)
{
   drv = _drv;
}

void MainWindow::setHeadControlThread(HeadControlThread *_headControl)
{
	headControl = _headControl;
}

void MainWindow::setMainThread(MainThread *_mainThread)
{
	mainThread = _mainThread;
}

void MainWindow::setDet( FaceDetector *_facedetector){
        facedetector = _facedetector;
}

void MainWindow::startFsm()
{
	if( ui->StartFromStateBox->isChecked() ){
		mainThread->startThread(ui->CurrentStateComboBox->currentText(), ui->PreviousStateComboBox->currentText()
			, QThread::NormalPriority);
	} else {
		mainThread->startThread("start", "start", QThread::NormalPriority);
	}
}

void MainWindow::StopFSMButton(){
	mainThread->Stop();
}

void MainWindow::VisEnable(bool b){
    if (b) vis->enable(); else vis->disable();
}

void MainWindow::ResetEyesButton(){
        headControl->ResetEyes();
}

void MainWindow::headSendBtn()
{
	TargetInfo h;
		
	h.visionHorizontalAngle = - ui->headHorizontalSlider->value();
	h.visionVerticalAngle = - ui->headVerticalSlider->value();
	h.exist = ui->headExistBox->isChecked();
	h.newTarget = ui->NewTargetBox->isChecked();
	h.crowd = ui->CrowdBox->isChecked();

	h.soundHorizontalAngle = - ui->soundHorizontalSlider->value();
	h.soundVerticalAngle = - ui->soundVerticalSlider->value();
	h.targetSpeaking = ui->TargetSpeakingBox->isChecked();
	h.otherSpeaking = ui->OtherSpeakingBox->isChecked();

	emit sendHead(h);
			
}

void MainWindow::writeText(QString s){
	ui->infoWindow->append(s);
}

std::vector<ServoConfig> GetServoConfigs(){
	std::vector<ServoConfig> servConfigs(Servo::TOTAL);

    servConfigs[Servo::HeadH] = ServoConfig(90, -90, 0, .684);
    servConfigs[Servo::HeadR] = ServoConfig(-25, 25, 0, .684);
    servConfigs[Servo::HeadV] = ServoConfig(-50, 50, 0, .684);
    servConfigs[Servo::HeadV] = ServoConfig(-45, 45, 0, .684);

    servConfigs[Servo::LeftEyeH] = ServoConfig(40, -40, 0, .05);
    servConfigs[Servo::RightEyeH] = ServoConfig(40, -40, 0, .05);
    servConfigs[Servo::LeftEyeV] = ServoConfig(-20, 20, 0, .05);
    servConfigs[Servo::RightEyeV] = ServoConfig(-20, 20, 0, .05);

    //servConfigs[Servo::LeftEyeH] = ServoConfig(150, 210, 0, .684);
    //servConfigs[Servo::RightEyeH] = ServoConfig(150, 210, 0, .684);
    //servConfigs[Servo::LeftEyeV] = ServoConfig(150, 210, 0, .684);
    //servConfigs[Servo::RightEyeV] = ServoConfig(150, 210, 0, .684);

    servConfigs[Servo::RightEyeLidV] = ServoConfig(-16, 12, 0, .684);
    servConfigs[Servo::LeftEyeLidV] = ServoConfig(-16, 12, 0, .684);
    servConfigs[Servo::RightEyeLidR] = ServoConfig(-10, 25, 0, .684);
    servConfigs[Servo::LeftEyeLidR] = ServoConfig(-25, 10, 0, .684);
    servConfigs[Servo::TopRightLips] = ServoConfig(-55, 55, 0, .684);
    servConfigs[Servo::TopLeftLips] = ServoConfig(-55, 55, 0, .684);
    servConfigs[Servo::BotRightLips] = ServoConfig(-55, 55, 0, .684);
    servConfigs[Servo::BotLeftLips] = ServoConfig(-55, 55, 0, .684);
	return servConfigs;
}

void MainWindow::setUpConnections()
{
	// Rejestracja typow uzywanych w sygnalach
	qRegisterMetaType<Frame>("Frame");
	qRegisterMetaType<FrameCam>("FrameCam");
	qRegisterMetaType<BFrame>("BFrame");
	qRegisterMetaType<TargetInfo>("TargetInfo");



	// Polaczenia HeadControl z wizualizacja dla serw i kamer
	connect(headControl, SIGNAL(emitFrame(Frame)),
			vis, SLOT(receiveFrame(Frame)));
	connect(headControl, SIGNAL(emitFrame(FrameCam)),
			vis, SLOT(receiveFrame(FrameCam)));
	

	// Polaczenie watku z wyswietlaczem oraz z headControl
	connect(mainThread, SIGNAL(sendText(QString)),
			this, SLOT(writeText(QString)));
	connect(mainThread, SIGNAL(emitBFrame(BFrame)),
			headControl,SLOT(receiveBFrame(BFrame)));
	connect(mainThread, SIGNAL(cancelQueue()),
			headControl,SLOT(cancelQueue()));

//=============================
//polaczenie HeadControl z modulem Driver
   connect(headControl, SIGNAL(emitFrame(Frame)), drv, SLOT(receiveFrame(Frame)));
    connect(headControl, SIGNAL(emitFrame(FrameCam)),
	    drv, SLOT(receiveFrame(FrameCam)));
//=============================
	

//=============================
//polaczenie HeadControl i mainThread z modulem FaceDetecor

    connect(facedetector, SIGNAL(sendTargetInfo(TargetInfo)),
            headControl, SLOT(targetUpdate(TargetInfo)));

    connect(facedetector, SIGNAL(sendTargetInfo(TargetInfo)),
            mainThread, SLOT(targetUpdate(TargetInfo)));


   connect(headControl, SIGNAL(sightPosition(double,double)),
           facedetector, SLOT(receivePosition(double,double)));

    connect(headControl, SIGNAL(runningRequest(bool)),
           facedetector, SLOT(runningRequest(bool)) );

//=============================

	//symulator glowy
	connect (this,SIGNAL( sendHead(TargetInfo) ),
			mainThread, SLOT( targetUpdate(TargetInfo) ));
	connect (this,SIGNAL( sendHead(TargetInfo) ),
			headControl, SLOT( targetUpdate(TargetInfo) ));

		// Ustawianie predkosci podazania
	connect(ui->EyesSpeedSpinBox, SIGNAL(valueChanged(double)),
			headControl, SLOT(SetOverrideEyesSpeed(double)));
	connect(ui->HeadSpeedSpinBox, SIGNAL(valueChanged(double)),
			headControl, SLOT(SetOverrideHeadSpeed(double)));


	//Ustalenie poczatkowej konfiguracji serw
	headControl->setServConf( GetServoConfigs() );
	vis->setServConf ( GetServoConfigs() );

	headControl->initialize();

        //Wgranie automatu stanu
        if ( QFileInfo(DEFAULT_FSM).exists() )
            LoadFSMFile(DEFAULT_FSM);
        if ( QFileInfo(DEFAULT_BEL).exists() )
        LoadBElementsFile(DEFAULT_BEL);


        // uwzglednienie cyklicznego resetu
        // generowanego przez timer z main_window
        connect(this, SIGNAL(emitFrame(Frame)),
                vis, SLOT(receiveFrame(Frame)));
           connect(this, SIGNAL(emitFrame(Frame)),
                   drv, SLOT(receiveFrame(Frame)));

}


MainWindow::~MainWindow()
{
	headControl->terminate();
	delete headControl;
    delete ui;
}

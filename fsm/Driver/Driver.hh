#ifndef DRIVER_HH
#define DRIVER_HH

#include <cstdlib>
#include <iostream>
#include <string>
#include <map>
#include <QObject>
#include <QtXml>
#include <QString>
#include <QStringList>
#include <stdint.h>
#include "../utils.h"
#include "Driver_utils.hh"
#include "Dynamixel.hh"
#include "Cam.hh"

#define DRIVER_CONFIG_XML "Driver/DriverConfig.xml"
#define DRIVER_SERVO_CONF_XML "Driver/DrvServoConfig.xml"

/**
 * @mainpage 
 * Moduł Driver głowy robota.
 * @par Info:
 * Głowną klasa modułu jest klasa Driver, zdefiniowana w pliku Driver.hh.
 * @version beta (testy wykonywano na jednym serwie!)
 * @date 2009.12.07
 */

/**
 * @brief Główna klasa modułu.
 *
 * Obiekt tej klasy pozwala na komunikację modułu "HeadControl" z rzeczywistym
 * sprzętem (napędy, kamery), w taki sposób jak przy sterowaniu wizualizacją.
 * Ponadto driver udostępnia wskaźnik do obrazu typu IplImage (OpenCV),
 * z kamer (po konwersji z YUYV).
 * @note Moduł ten nie jest wątkiem, ani nie posiada timerów. Wywoływanie
 * jego metod powoduje natychmiastowe wysłanie sygnałów do urządzeń.
 * @par Uwagi:
 * Deklaracja obiektu tej klasy rozpoczyna konfigurację sprzetu.
 * Należy deklarować tylko jeden obiekt tej klasy.
 * @note Sposób inicjalizacji w programie głównym: 
 * Driver * drv = new Driver;
 * @note Sposób inicjalizacji slotu serw:
 * connect(this, SIGNAL(emitFrame(Frame)),
 *         drv, SLOT(receiveFrame(Frame)));
 * @note Sposób inicjalizacji slotu kamerek:
 * connect(this, SIGNAL(emitFrame(FrameCam)),
 *         drv, SLOT(receiveFrame(FrameCam)));
 */
class Driver : public QObject
{
  Q_OBJECT
  
  public:
  
  /**
   * Konstruktor inicjalizuje połączenia ze sprzętem.
   * Pobierane są elementy konfiguracji z pliku
   * "DriverConfig.xml".
   * Odbywa się inicjalizacja potrzebnych struktur danych.
   */
  Driver();
    
  /**
   * Obiekt reprezentujący prawą kamerę.
   */
  Cam RightCam;
  
  /**
   * Obiekt reprezentujący lewą kamerę.
   */
  Cam LeftCam;
  
  /**
   * Klasa do komunikacji z serwami.
   */
  Dynamixel DynServos;

  /**
   * Wektor konfiguracji serw i napędów kamer. Definiuje dopuszczlne
   * zakresy ruchów i dopuszczalne prędkości.
   * @note i-ty element tego wektora to serwo o ID=i-1.
   * @note Na prośbę Mateusza, przenoszę ten wektor do sekcji publicznej.
   */
  std::vector<DrvServoConfig> ServoConfigs;
		     
public slots:

  /**
   * Slot odbierajacy ramkę sterujacą.
   * @param frame Ramka sterująca, zdefiniowana w ../utils.h.
   * @return void
   */
  void receiveFrame(Frame frame);

  /**
   * Slot odbierajacy ramkę sterujacą kamerkami.
   * @note Jest to przeciążenie, które postonowiliśmy dodać,
   * gdy okazało się, że nie można wysyłać dwóch sygnałów
   * do kamer, jeden po drugim (drugi sygnał można wysłać
   * dopiero po zakończeniu się poprzedniego ruchu).
   * Dzięki temu przeciążeniu możliwe jest zadanie zmiany
   * położenia od razu na dwie osie (porównaj ramki
   * Frame i FrameCam).
   * @param frame Ramka sterująca, zdefiniowana w ../utils.h.
   * @return void
   */
  void receiveFrame(FrameCam frame);

private:

  /**
   * Metoda sterowania kamerkami.
   * @param camID 'r' - prawa kamera, 'l' - lewa kamera.
   * @param destPosV zadana znormalizowana pozycja wertykalna.
   * @param destPosH zadana znormalizowana pozycja horyzontalna.
   * @return void
   */
  void CamsControl(char camID, double destPosV, double destPosH);

  /**
   * Metoda seterowania serwami.
   * @param ID identyfikator serwa.
   * @param destPos pozycja do ustawienia.
   * @param Vel prędkość ruchu.
   * @return void
   */
  void ServoControl(unsigned int ID, double destPos, double Vel);

  /**
   * Inicjalizuje wektor ServoConfigs (konfiguracja napędów - również
   * kamer).	
   * @return void
   */
  void InitServoConfigs();

  /**
   * Gdy true wypisuje na ekran wysyłane ramki sterujące.
   */
  bool debug;

protected:

};

#endif // DRIVER_HH

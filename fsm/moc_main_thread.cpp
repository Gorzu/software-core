/****************************************************************************
** Meta object code from reading C++ file 'main_thread.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "main_thread.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'main_thread.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainThread[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
      11,   29,   29,   29, 0x05,
      30,   29,   29,   29, 0x05,
      49,   29,   29,   29, 0x05,

 // slots: signature, parameters, type, tag, flags
      63,  110,   29,   29, 0x0a,
     132,   29,   29,   29, 0x0a,
     157,  170,   29,   29, 0x0a,
     174,   29,   29,   29, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MainThread[] = {
    "MainThread\0sendText(QString)\0\0"
    "emitBFrame(BFrame)\0cancelQueue()\0"
    "startThread(QString,QString,QThread::Priority)\0"
    "enterState,prev,prior\0targetUpdate(TargetInfo)\0"
    "SetFSM(FSM&)\0fsm\0Stop()\0"
};

void MainThread::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainThread *_t = static_cast<MainThread *>(_o);
        switch (_id) {
        case 0: _t->sendText((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->emitBFrame((*reinterpret_cast< BFrame(*)>(_a[1]))); break;
        case 2: _t->cancelQueue(); break;
        case 3: _t->startThread((*reinterpret_cast< QString(*)>(_a[1])),(*reinterpret_cast< QString(*)>(_a[2])),(*reinterpret_cast< QThread::Priority(*)>(_a[3]))); break;
        case 4: _t->targetUpdate((*reinterpret_cast< TargetInfo(*)>(_a[1]))); break;
        case 5: _t->SetFSM((*reinterpret_cast< FSM(*)>(_a[1]))); break;
        case 6: _t->Stop(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainThread::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainThread::staticMetaObject = {
    { &QThread::staticMetaObject, qt_meta_stringdata_MainThread,
      qt_meta_data_MainThread, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainThread::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainThread::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainThread::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainThread))
        return static_cast<void*>(const_cast< MainThread*>(this));
    return QThread::qt_metacast(_clname);
}

int MainThread::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QThread::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void MainThread::sendText(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MainThread::emitBFrame(BFrame _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MainThread::cancelQueue()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}
QT_END_MOC_NAMESPACE

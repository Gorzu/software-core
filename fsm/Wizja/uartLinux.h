/*****************************************************************************
 *
 * uartLinux.h
 *
 * Linux UART module
 *
 * Target: x86 (-m32)
 *
 * Mariusz Janiak
 *
 * Wroclaw 2008
 *
 *****************************************************************************/

#ifndef _UART_LINUX_
#define _UART_LINUX_

#ifdef __cplusplus
extern "C" {
#endif

#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>

/*****************************************************************************
 * Constants
 *****************************************************************************/

#define UL_BOUDRATE_9600   9600
#define UL_BOUDRATE_19200  19200
#define UL_BOUDRATE_38400  38400
#define UL_BOUDRATE_57600  57600
#define UL_BOUDRATE_115200 115200
#define UL_BOUDRATE_230400 230400
#define UL_BOUDRATE_460800 460800
#define UL_BOUDRATE_576000 576000
#define UL_BOUDRATE_921600 921600

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef NULL
#define NULL ((void *)0)  /* or for C++ #define NULL 0 */
#endif

/*****************************************************************************
 * Type definitions
 *****************************************************************************/

#ifndef _UWORD32_
#define _UWORD32_
typedef unsigned long  UWord32;
#endif

#ifndef _WORD32_
#define _WORD32_
typedef long           Word32;
#endif

#ifndef _UWORD16_
#define _UWORD16_
typedef unsigned short UWord16;
#endif

#ifndef _WORD16_
#define _WORD16_
typedef short          Word16;
#endif

#ifndef _UWORD8_
#define _UWORD8_
typedef unsigned char  UWord8;
#endif

#ifndef _WORD8_
#define _WORD8_
typedef signed char    Word8;
#endif

#ifndef _BOOLEAN_
#define _BOOLEAN_
typedef unsigned char  Boolean;
#endif

/*****************************************************************************
 * Globals variables 
 *****************************************************************************/

/***************************************************************************** 
 * Function prototypes
 *****************************************************************************/

int     UL_open(const char *devName, UWord32 baud, Boolean block);
void    UL_close(int fd);
UWord32 UL_stat(int fd);
Boolean UL_rx(int fd, UWord8 *data);
Boolean UL_tx(int fd, UWord8 data);

#ifdef __cplusplus
}
#endif

#endif /* End _UART_LINUX_ */

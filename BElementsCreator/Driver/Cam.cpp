#include "Cam.hh"

Cam::Cam()
{
  Works=false;
}

Cam::~Cam()
{
  if(Works)
    close_v4l2(videoIn);
}

void Cam::CamInit(std::string dev)
{
 
  videoIn = (struct vdIn *) calloc(1, sizeof(struct vdIn));
  picture = (unsigned char *)malloc(width * height *3*sizeof(char));
  
  videodevice = dev.c_str(); 
  format = V4L2_PIX_FMT_YUYV;
  grabmethod = 1;
  width = 640;
  height = 480;
  fps = 30;
  
  if (init_videoIn(videoIn, (char *) videodevice, 
		   width, height, fps, format,grabmethod) < 0)
    {
      std::cerr<< "Driver: CamInit(): warning: "<< videodevice;
      std::cerr<<" doesn't work properly"<<std::endl;
      //Works=false; // domyślnie i tak false
    }
  else
    Works=true;
  
  Reset();
  img = cvCreateImage(cvSize(width,height),IPL_DEPTH_8U,3); // obraz OpenCV
}

void Cam::RGB2IplImage(IplImage *img,
		       const unsigned char* rgbArray, 
		       unsigned int width, 
		       unsigned int height)
{
  unsigned int i,j;
  // zamienione sa kolory R i B
  for(i=0;i<height;i++) {
    for(j=0;j<width;j++) {
      ((uchar *)(img->imageData + i*img->widthStep))[j*img->nChannels + 2]=
	*(rgbArray+i*width*3+j*3+0);
      ((uchar *)(img->imageData + i*img->widthStep))[j*img->nChannels + 1]=
	*(rgbArray+i*width*3+j*3+1);
      ((uchar *)(img->imageData + i*img->widthStep))[j*img->nChannels + 0]=
	*(rgbArray+i*width*3+j*3+2);
    }
  }
}

void Cam::Reset()
{
  PosH=0;
  PosV=0;
  if(Works)
    v4l2ResetPanTilt(videoIn);
}

IplImage * Cam::getIplImage()
{
  
  if (uvcGrab(videoIn) < 0) {    
    std::cerr<<"error: Driver: uvcGrab() error"<<std::endl;
    exit(1);
  }
  initLut(); // potrzebne wlaczenie LUT do konwersji UYV422 do RBG24
  if(picture){ // jesli jest zarezerwowana pamiec to konwertujemy do RGB24
    Pyuv422torgb24(videoIn->framebuffer,picture,videoIn->width,videoIn->height);
  }
  freeLut();
  RGB2IplImage(img,picture, width, height); // konwertujemy do OpenCV
  usleep(10000); // 10 [ms]
  
  if( !img ) {
    std::cerr<<"error: Driver: getIplImage(): couldn't convert image"<<std::endl;
    exit(1);
  }
  return img;
}

void Cam::MoveVH(int V_change, int H_change)
{
  PosV = PosV + V_change;
  PosH = PosH + H_change;
  if(Works)
    v4L2UpDownPanTilt(videoIn, H_change*64, V_change*64);
}

int Cam::getPosH() const
{
  return PosH;
}

int Cam::getPosV() const
{
  return PosV;
}

#!/bin/bash
clear
echo
echo +++ SAMUEL - skrypt inicjalizacyjny kamer
echo  
echo \*\*\* Ten skrypt przeprowadzi Cię przez proces
echo \*\*\* inicjalizacji kamer.
echo \*\*\* Wyłącz oprogramowanie korzystające z ka-
echo \*\*\* mer i odłącz je od komputera.
echo \*\*\* Upewnij się, że komputer został popra-
echo \*\*\* wnie umieszczony w stacji dokującej.
echo 
echo \*\*\* Naciśnij coś, aby kontynuować...

read KEY

echo \*\*\* Wymagane są uprawnienia administatora...
sudo echo 

echo \*\*\* OK!
echo
echo \*\*\* Podłącz prawą kamerę \(prawe oko\).
echo \*\*\* Wtyczka powinna być oznaczona.
echo
echo \*\*\* Naciśnij coś, aby kontynuować...

read KEY

echo \*\*\* Proszę czekać...

xvfb-run sudo guvcview -d /dev/video0 -o &
sleep 7
killall xvfb-run
killall Xvfb

echo \*\*\* OK!
echo
echo \*\*\* Podłącz lewą kamerę \(lewe oko\).
echo
echo \*\*\* Naciśnij coś, aby kontynuować...

read KEY

echo \*\*\* Proszę czekać...

xvfb-run sudo guvcview -d /dev/video1 -o &
sleep 7
killall xvfb-run
killall Xvfb

echo \*\*\* OK!
echo
echo \*\*\* Jeśli program \'guvcview\' nie 
echo \*\*\* zgłosił problemów kamery powinny działać 
echo \*\*\* poprawnie.
echo \*\*\* Możesz uruchomić automat stanu: \'./FSM\'.



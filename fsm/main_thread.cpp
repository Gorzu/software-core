#include "main_thread.h"


double getRand(){
	return (double)rand()/(RAND_MAX);
}



void MainThread::targetUpdate(TargetInfo h){
	TargetInfo old = target;
	target = h;
	target.newTarget = target.newTarget || old.newTarget;  // zapamieta jesli w trakcie ramki choc raz zmienil sie cel
														   // trzeba to gasic po sprawdzeniu relacji
	if( h.exist && currState->visionInterrupt)
		targetInt.wakeAll();
	if( (h.targetSpeaking || h.otherSpeaking) && currState->soundInterrupt)
		targetInt.wakeAll();
}

void MainThread::SetFSM(FSM &fsm){
	this->fsm = fsm;
}


void MainThread::sendBFrame(BFrame bframe){
	emit emitBFrame(bframe);
}
// ma zwrocic false jesli ktorykolwiek z listy nie jest spelniony, inaczej true
bool MainThread::checkConditions(const std::vector<QString> &conditions){
	bool result = true;
	for(unsigned int i=0; i<conditions.size(); i++){
		switch( conditonsMap[conditions[i]]){
		case timeout: // zawsze true
			break;
		case seen:
			if ( !target.exist )
				result = false;
			break;
		case newTarget:
                        //if( !target.newTarget )
				result = false;
			break;
		case crowd:
			if ( !target.crowd )
				result = false;
			break;
		case targetSpeaking:
			if ( !target.targetSpeaking )
				result = false;
			break;
		case otherSpeaking:
			if ( !target.otherSpeaking )
				result = false;
			break;
		default:
			//error!
			result = false;
			break;
		}
	}
	return result;
}

void MainThread::InitConditionsMap(){
	conditonsMap["timeout"] = timeout;
	conditonsMap["seen"] = seen;
	conditonsMap["newTarget"] = newTarget;
	conditonsMap["crowd"] = crowd;
	conditonsMap["targetSpeaking"] = targetSpeaking;
	conditonsMap["otherSpeaking"] = otherSpeaking;

}


MainThread::MainThread()
{	
	stopRequest=false;
	target.visionHorizontalAngle = 0;
	target.visionVerticalAngle =0;
	target.exist = false;
        currState = prevState = &(fsm.states["start"]) ;
	InitConditionsMap();
	srand( (int)time(NULL) );
	rand();
}

void MainThread::run()
{
	forever{
		if (stopRequest)
			break;
		emit sendText( currState->name );
		emit cancelQueue();
		double r1 = (double)rand()/RAND_MAX;
		double r2 = (double)rand()/RAND_MAX;
		double r3 = (double)rand()/RAND_MAX;
		
		for(unsigned int i=0; i< currState->BFrameList.size() ;i++){
                        BFrame bframe = currState->BFrameList[i].SingleBFrame(r1,r2,r3);
			sendBFrame( bframe );
			targetIntMutex.lock();
			if( targetInt.wait(&targetIntMutex, bframe.totalTime) ){
				targetIntMutex.unlock();
				break;
			}
 			targetIntMutex.unlock();
		}
		State* nextState;
		nextState = GenerateNextState();
		prevState = currState;
		currState = nextState;
	}
    
}

State* MainThread::GenerateNextState(){
	for(unsigned int i=0; i<currState->relations.size(); i++){
		if ( currState->relations[i].inEntryState( prevState->name ) ){
			if ( checkConditions(currState->relations[i].conditions) ){
				double r = (double)rand()/RAND_MAX;
				Relation exitRel = currState->relations[i];
				for (unsigned int j=0; j< exitRel.exitStates.size();j++){
					if( r > exitRel.exitStates[j].probability)
						r -= exitRel.exitStates[j].probability;
					else{
                                                emit sendText( target.newTarget?"T":"F" );
						// gaszenie newTarget
						target.newTarget = false;

                                                emit sendText( target.newTarget?"T":"F" );
						if( exitRel.exitStates[j].name == "#" )
							return &(fsm.states[prevState->name]);
						else
							return &(fsm.states[exitRel.exitStates[j].name]);
					}
						
				}
			}
		}
	}
	//error
	emit sendText("BLAD!!! Nie znaleziono stanu wyjsciowego.");
	stopRequest=true;
	return NULL;

}


void MainThread::startThread(QString enterState, QString prev, QThread::Priority prior)
{
	if ( !isRunning() ){
	stopRequest=false;
	currState = &(fsm.states[enterState]) ;
	prevState = &(fsm.states[prev]);
    start(prior);
	}
}

void MainThread::Stop(){
	stopRequest = true;
}


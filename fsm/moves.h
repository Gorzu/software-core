#ifndef MOVES_H
#define MOVES_H

#include <QTime>
#include <vector>
#include <cmath>

#include "utils.h" //potrzebne do Servo enum

/* Odpowiada jednegmu ruchowi prostemu.
	Korzysta z typu wyliczeniowego Servo::ServoID do ustalenia ilosc serw.
	Dane trzymane sa w wektorach i mozna odnosic sie do elementow poprzez wyliczenie.
	np. */
struct SimpleMove
{ 
	/* Nastawy serw */
	std::vector<double> servoPosition;
	/* Predkosci serw */
	std::vector<double> servoVelocity;
	/*  Flaga uzywania serwa 
	- wykorzytywane przy rozbiorze SimpleMove na ramki dla serw (struct Frame) */
	std::vector<bool> isSet;
	/* comment */
	double normalizedTime;
	/* Czas w jakim ma zostac wykonany ruch - nastawy przeslane na serwa*/
	QTime executeTime;
	

	/* Pozwala na ustalenie pozycji i predkosci pojedyczego serwa, ustawia 
	flage uzywania. Bazuje na typie wyliczeniowym Servo::ServoId */
	void setElement(Servo::ServoId servoId, double position, double velocity)
	{
		servoPosition[servoId] = position;
		servoVelocity[servoId] = velocity;
		isSet[servoId] = true;
	}

	/* Konstruktor. Tworzy nowy, pusty ruch prosty i go inicjalizuje. 
	Pisanie do niezainicjalizowanego pola moze powodowac bledy programu.*/   
	SimpleMove() 
	{
		servoPosition = std::vector<double>(Servo::TOTAL);
		servoVelocity = std::vector<double>(Servo::TOTAL);
		isSet =  std::vector<bool>(Servo::TOTAL);
		for (unsigned int i=0; i< Servo::TOTAL; i++)
			servoPosition[i] = 0.5;
	}

	SimpleMove operator+(const SimpleMove& sm){
		SimpleMove result = *this;
		for(unsigned int i=0; i<result.isSet.size(); i++){
			if ( !result.isSet[i] && sm.isSet[i] ){
				result.servoPosition[i] = sm.servoPosition[i];
				result.servoVelocity[i] = sm.servoVelocity[i];
				result.isSet[i] = true ;
			}
		}
		return result;
	}


};

	/* Struktura przechowujaca zauwazona glowe.
	Wspolrzedne globalne!!
	*/
struct TargetInfo{
	//pozycja horyzontalna (pozioma), kat w stopniach
	double visionHorizontalAngle;
	//pozycja vertykalna (pionowa), kat w stopniach
	double visionVerticalAngle;

	//czy glowa istnieje(znaleziona)
	bool exist;
	//czy zmieniono cel (czy zwracana glowa rozni sie od poprzedniej)
	bool newTarget;
	//czy jest tlok (ilosc ludzi wieksza od okreslonej)
	bool crowd;

	//pozycja horyzontalna (pozioma), kat w stopniach
	double soundHorizontalAngle;
	//pozycja vertykalna (pionowa), kat w stopniach
	double soundVerticalAngle;

	//czy cel mowi
	bool targetSpeaking;
	//czy ktos inny mowi
	bool otherSpeaking;

	/* Kontruktor pusty - do wypelniania
	 Uwaga: bool`e domyslnie sa ustawione na true*/
	TargetInfo(){}
};

#endif // MOVES_H
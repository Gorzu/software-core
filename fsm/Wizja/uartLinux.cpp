/*****************************************************************************
 *
 * uartLinux.c
 *
 * Linux UART module
 *
 * Target: x86 (-m32)
 *
 * Mariusz Janiak
 *
 * Wroclaw 2008
 *
 *****************************************************************************/

#include "uartLinux.h"

int UL_open(const char *devName, UWord32 baud, Boolean block)
{
  int fd;
  struct termios opt;

  /* Open port */
  if(block)
    fd = open(devName, O_RDWR | O_NOCTTY);
  else
    fd = open(devName, O_RDWR | O_NOCTTY | O_NONBLOCK);
  if(fd < 0) return fd;
  /* Get port parameters */
  tcgetattr(fd, &opt);
  /* Set port boudrate */
  switch(baud){
  case UL_BOUDRATE_9600:
    cfsetispeed(&opt, B9600);
    cfsetospeed(&opt, B9600);
    break;
  case UL_BOUDRATE_19200:
    cfsetispeed(&opt, B19200);
    cfsetospeed(&opt, B19200);
    break;
  case UL_BOUDRATE_38400:
    cfsetispeed(&opt, B38400);
    cfsetospeed(&opt, B38400);
    break;
  case UL_BOUDRATE_57600:
    cfsetispeed(&opt, B57600);
    cfsetospeed(&opt, B57600);
    break;
  case UL_BOUDRATE_115200:
    cfsetispeed(&opt, B115200);
    cfsetospeed(&opt, B115200);
    break;
  case UL_BOUDRATE_230400:
    cfsetispeed(&opt, B230400);
    cfsetospeed(&opt, B230400);
    break;
  case UL_BOUDRATE_460800:
    cfsetispeed(&opt, B460800);
    cfsetospeed(&opt, B460800);
    break;
  case UL_BOUDRATE_576000:
    cfsetispeed(&opt, B576000);
    cfsetospeed(&opt, B576000);
    break;
  case UL_BOUDRATE_921600:
    cfsetispeed(&opt, B921600);
    cfsetospeed(&opt, B921600);
    break;
  default:
    cfsetispeed(&opt, B9600);
    cfsetospeed(&opt, B9600);
    break;
  }
  /* 8N1 frame */
  opt.c_cflag &= ~PARENB;
  opt.c_cflag &= ~CSTOPB;
  opt.c_cflag &= ~CSIZE;
  opt.c_cflag |= CS8;
  /* Disable hardware flow control */
  opt.c_cflag &= ~CRTSCTS;
  /* Enable receiver and local mode */
  opt.c_cflag |= (CLOCAL | CREAD);
  /* Raw input */
  opt.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
  /* Disable parity checking */
  opt.c_iflag &= ~(INPCK | ISTRIP);
  /* Don't map CR to NL or NL to CR */
  opt.c_iflag &= ~(ICRNL | INLCR);
  /* Don't map uppercase to lowercase */
  opt.c_iflag &= ~IUCLC;
  /* Don't ignore CR */
  opt.c_iflag &= ~IGNCR;
  /* Ignore BREAK condition */
  opt.c_iflag |= IGNBRK;
  /* Disable software flow control */
  opt.c_iflag &= ~(IXON | IXOFF | IXANY);
  /* Raw output - other c_oflag bits ignored */
  opt.c_oflag &= ~OPOST;
  /* Set new settings */
  tcsetattr(fd, TCSANOW, &opt);
  return fd;
}

void UL_close(int fd)
{
  close(fd);
}

UWord32 UL_stat(int fd)
{
  UWord32 n;

  ioctl(fd, FIONREAD, &n);
  return n;
}

Boolean UL_rx(int fd, UWord8 *data)
{
  if(read(fd, data, 1) > 0) return TRUE;
  else return FALSE;
}

Boolean UL_tx(int fd, UWord8 data)
{
  if(write(fd, &data, 1) > 0) return TRUE;
  else return FALSE;
}

/* void UL_putText(int fd, const char *str) */
/* { */
/*   while(*str){ */
/*     if(UART0_tx(fd,*str))  */
/*       ++str; */
/*   } */
/* } */

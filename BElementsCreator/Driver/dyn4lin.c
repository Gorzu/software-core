/*
 ============================================================================
 Name        : dyn4lin.c
 Author      : Adam Oleksy (XENO support Mariusz Janiak)
 Version     :
 Copyright   : Your copyright notice
 Description : Dynamixel RX64, RX28
 ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>
#include <errno.h>
#include <limits.h>

#ifdef __XENO__
#  include <native/task.h>
#  include <rtdm/rtserial.h>
#else
#  include <termios.h>
#  include <fcntl.h>
#endif

#include "dyn4lin.h"

const size_t dyn_rom_table_index[] = { sizeof(uint16_t), /* Model number */
sizeof(uint8_t), /* Version */
sizeof(uint8_t), /* ID */
sizeof(uint8_t), /* Baud rate */
sizeof(uint8_t), /* Return delay time */
sizeof(uint16_t), /* CW Angle limit */
sizeof(uint16_t), /* CCW Angle limit */
sizeof(uint8_t), /* Unused word */
sizeof(uint8_t), /* Temperature limit */
sizeof(uint8_t), /* Lowest limit voltage */
sizeof(uint8_t), /* Highest limit voltage */
sizeof(uint16_t), /* Max torque */
sizeof(uint8_t), /* Status return level */
sizeof(uint8_t), /* Alarm LED */
sizeof(uint8_t) }; /* Alarm shutdown */

const uint8_t dyn_rom_table_address[] = { DYN_ADR_MODEL_NUMBER_L,
    DYN_ADR_VERSION_OF_FIRMWARE, DYN_ADR_ID, DYN_ADR_BAUD_RATE,
    DYN_ADR_RETURN_DELAY_TIME, DYN_ADR_CW_ANGLE_LIMIT_L,
    DYN_ADR_CCW_ANGLE_LIMIT_L, DYN_ADR_UNUSED_WORD,
    DYN_ADR_HIGHEST_LIMIT_TEMPERATURE, DYN_ADR_LOWEST_LIMIT_VOLTAGE,
    DYN_ADR_HIGHEST_LIMIT_VOLTAGE, DYN_ADR_MAX_TORQUE_L,
    DYN_ADR_STATUS_RETURN_LEVEL, DYN_ADR_ALARM_LED, DYN_ADR_ALARM_SHUTDOWN };

static int check_parameters(uint8_t, uint8_t, uint8_t, uint8_t);
static void error_treat(dyn_param_t *, int, const char *);
static unsigned int compute_response_length(dyn_param_t *, uint8_t *);
static int build_query_basis(uint8_t, uint8_t, uint8_t, uint8_t *);
static uint8_t checksum(uint8_t *, uint16_t);
static int check_checksum(uint8_t *, const uint16_t);
static int dyn_send(dyn_param_t *, uint8_t *, uint16_t);
static int receive_msg(dyn_param_t *, int, uint8_t *, int *);
static int dyn_receive(dyn_param_t *, uint8_t *, uint8_t *);
static int write_data(dyn_param_t *, uint8_t, uint8_t, uint8_t, uint8_t *,
    uint8_t);
static int compare_dyn_servo_binsearch(const uint8_t *, const dyn_servo_t *);
static int compare_dyn_servo_qsort(const uint8_t *, const uint8_t *);

#ifdef __XENO__
static void TSR_write(void *ptr);
static void TSR_read(void *ptr);
#endif

/* Treats errors and flush or close connection if necessary */
static void error_treat(dyn_param_t *dyn_param, int code, const char *string) {
  printf("\nERROR %s (%d)\n", string, code);
  if (dyn_param->error_handling == FLUSH_ON_ERROR) {
    switch (code) {
    default:
#ifndef __XENO__
      tcflush(dyn_param->fd, TCIOFLUSH);
#endif
      break;
    }
  }
}

static int compare_dyn_servo_binsearch(const uint8_t *id,
    const dyn_servo_t *element) {
  return *id != element->id ? *id < element->id ? -1 : 1 : 0;
}

static int compare_dyn_servo_qsort(const uint8_t *first, const uint8_t *second) {
  return *first != *second ? *first < *second ? -1 : 1 : 0;
}

/* Computes the length of the expected response
 * Status: not completed. Add statusReturnLevel checking.
 */
static unsigned int compute_response_length(dyn_param_t *dyn_param,
    uint8_t *query) {
  int respone_length = 0;
  int return_level;

  /* If id is DYN_BROADCAST then servo will be NULL */
  return_level = dyn_get_return_level(dyn_param, query[DYN_FRAMEORDER_ID]);
  if (return_level == DYN_WORD8_SKIP)
    return 0;

  switch (query[DYN_FRAMEORDER_INSTRUCTION]) {
  case DYN_INS_READ_DATA:
    if (return_level > 0)
      respone_length += DYN_STATUS_PACKET_LENGTH
          + query[DYN_FRAMEORDER_PARAMETER + 1];
    break;
  case DYN_INS_WRITE_DATA:
  case DYN_INS_REG_WRITE:
  case DYN_INS_ACTION:
  case DYN_INS_RESET:
    if (return_level < 2)
      break;
  case DYN_INS_PING:
    respone_length = DYN_STATUS_PACKET_LENGTH;
    break;
  case DYN_INS_SYNC_WRITE:
    break;
  }

  return respone_length;
}

/* Build query header
 * Returns the index to the first free byte in query.
 * Status: incomplete.
 */
static int build_query_basis(uint8_t id, uint8_t instruction,
    uint8_t param_length, uint8_t *query) {
  query[DYN_FRAMEORDER_1ST_START_BYTE] = DYN_START_BYTE;
  query[DYN_FRAMEORDER_2ND_START_BYTE] = DYN_START_BYTE;
  query[DYN_FRAMEORDER_ID] = id;
  query[DYN_FRAMEORDER_LENGTH] = DYN_PRESET_QUERY_LENGTH + param_length;
  query[DYN_FRAMEORDER_INSTRUCTION] = instruction;
  return DYN_MIN_FRAME_LENGTH - 1; /**< Może trzeba zmienić? */
}

static int check_parameters(uint8_t id, uint8_t instruction,
    uint8_t start_address, uint8_t nb) {
  uint8_t ret = 0;

  if (start_address > DYN_ADR_PUNCH_H) {
    printf("ERROR Bad address\n");
    return DYN_ADDRESS_OUT_OF_RANGE;
  }

  switch (instruction) {
  case DYN_INS_READ_DATA:
    /* Check parameters length and id number */
    if (nb + 1 > DYN_MAX_PARAM_LENGTH) {
      printf("ERROR Too many data requested\n");
      ret = DYN_TOO_MANY_DATA;
    }
  case DYN_INS_PING:
    /* Check id number */
    if (id >= DYN_BROADCAST) {
      printf("ERROR Invalid ID number 0x%.2X\n", id);
      ret = DYN_ID_OUT_OF_RANGE;
    }
    break;
  case DYN_INS_WRITE_DATA:
  case DYN_INS_REG_WRITE:
  case DYN_INS_SYNC_WRITE:
    /* Check parameters length */
    if (nb + 1 > DYN_MAX_PARAM_LENGTH) {
      printf("ERROR Too many data sended\n");
      ret = DYN_TOO_MANY_DATA;
    }
    break;
  case DYN_INS_ACTION:
  case DYN_INS_RESET:
    break;
  default:
    ret = DYN_UNKNOWN_INSTRUCTION;
    break;
  }

  return ret;
}

/* Compute Checksum
 * Status: Complete.
 */
static uint8_t checksum(uint8_t *buffer, uint16_t buffer_length) {
  uint8_t i;
  uint8_t sum = 0;

  for (i = DYN_FRAMEORDER_ID; i < buffer_length; ++i)
    sum += buffer[i];

  return ~sum;
}

/* Check checksum
 * Status: Complete.
 */
static int check_checksum(uint8_t *msg, const uint16_t msg_length) {
  int ret;
  uint8_t checksum_calc;
  uint8_t checksum_received;

  checksum_calc = checksum(msg, msg_length - 1);
  checksum_received = msg[msg_length - 1];

  /* Check checksum of msg */
  if (checksum_calc == checksum_received)
    ret = 0;
  else
    ret = DYN_CHECKSUM_ERROR;

  return ret;
}

#ifdef __XENO__
/* Thread Service Rutines */
void TSR_write(void *ptr)
{
  dyn_xeno_param *xprm = ptr;

  /* Write data */
  xprm->ret = rt_dev_write(xprm->dyn_param->fd,xprm->data,xprm->len);
}

void TSR_read(void *ptr)
{
  dyn_xeno_param *xprm = ptr;

  /* Read data */
  xprm->ret = rt_dev_read(xprm->dyn_param->fd, xprm->data, xprm->len);
}
#endif

/* Send query.
 * Status: complete.
 */
int dyn_send(dyn_param_t *dyn_param, uint8_t *query, uint16_t query_length)
{
  int ret;
  uint8_t s_crc;
  int i;

  s_crc = checksum(query, query_length);
  query[query_length++] = s_crc;

  if (dyn_param->debug) {
    for (i = 0; i < query_length; ++i)
      printf("[0x%.2X]", query[i]);
    printf("\n");
  }

#ifdef __XENO__
  {
    RT_TASK *td;
    /* Get current task */
    td = rt_task_self();
    /* Check if it is Xenomai task */
    if(td){ /* Xenomai task */
      ret = rt_dev_write(dyn_param->fd, query, query_length);
    }
    else{ /* Linux task */
      RT_TASK task;
      dyn_xeno_param xprm;
      char           bname[DYN_XENO_BNAME_LEN];
      /* Init variable */
      xprm.dyn_param  = dyn_param;
      xprm.data       = query;
      xprm.len        = query_length;
      xprm.ret        = 0;
      /* Get proces pid and create base name for rt task */
      ret = getpid();
      sprintf(bname,"pid%i_dyn4lin_w",ret);
      /* Create and start real time task */
      ret = rt_task_spawn(&task,bname,0,DYN_XENO_TASK_PRIO,T_FPU|T_JOINABLE
			  ,TSR_write,&xprm);
      if(ret < 0){
	error_treat(dyn_param, ret,"Real time task failure");
	return DYN_PORT_SOCKET_FAILURE;
      }
      /* Wait on the termination of a real-time task */
      ret = rt_task_join(&task);
      if(ret < 0){
	error_treat(dyn_param, ret,"Real time task join failure");
	return DYN_PORT_SOCKET_FAILURE;
      }
      /* Set return value */
      ret = xprm.ret;
    }
    if ((ret < 0) || (ret != query_length)){
      ret = DYN_PORT_SOCKET_FAILURE;
      error_treat(dyn_param, ret, "Write port/socket failure");
    }
  }
#else
  ret = write(dyn_param->fd, query, query_length);
  if ((ret == -1) || (ret != query_length)) {
    ret = DYN_PORT_SOCKET_FAILURE;
    error_treat(dyn_param, ret, "Write port/socket failure");
  }
#endif

  return ret;
}

#ifndef __XENO__
#define WAIT_DATA()                                                             \
{                                                                               \
  while ((select_ret = select(dyn_param->fd+1, &rfds, NULL, NULL, &tv)) == -1) {\
    if (errno == EINTR) {                                                       \
      printf("A non blocked signal was caught\n");                              \
      /* Necessary after an error */                                            \
      FD_ZERO(&rfds);                                                           \
      FD_SET(dyn_param->fd, &rfds);                                             \
    } else {                                                                    \
      error_treat(dyn_param, DYN_SELECT_FAILURE, "Select failure");             \
      return DYN_SELECT_FAILURE;                                                \
    }                                                                           \
  }                                                                             \
                                                                                \
  if (select_ret == 0) {                                                        \
    /* Call to error_treat is done later to manage exceptions */                \
    return DYN_TIMEOUT_ERROR;                                                   \
  }                                                                             \
}
#endif

/* Receive message.
 * Status: complete.
 */
static int receive_msg(dyn_param_t *dyn_param, int msg_length_computed,
    uint8_t *msg, int *p_msg_length) {
#ifndef __XENO__
  struct timeval tv;
  fd_set rfds;
  int select_ret;
#endif

  int read_ret;
  int length_to_read;
  uint8_t *p_msg;

  if (dyn_param->debug)
    printf("Wait for a message (%d bytes)...\n", msg_length_computed);

#ifndef __XENO__
  /* Add a file descriptor to the set */
  FD_ZERO(&rfds);
  FD_SET(dyn_param->fd, &rfds);
  tv.tv_sec = 0;
  tv.tv_usec = DYN_MAX_BYTE_TO_BYTE_TIME_MS * 10000;
  select_ret = 0;
  WAIT_DATA();
#endif

  /* Set mesage length */
  length_to_read = msg_length_computed;
  /* Initialize the readin the message */
  (*p_msg_length) = 0;
  p_msg = msg;

#ifdef __XENO__
  while (length_to_read) {
    RT_TASK *td;
    /* Get current task */
    td = rt_task_self();
    /* Check if it is Xenomai task */
    if(td){ /* Xenomai task */
      read_ret = rt_dev_read(dyn_param->fd,p_msg,length_to_read);
    }
    else{ /* Linux task */
      RT_TASK task;
      dyn_xeno_param xprm;
      char           bname[DYN_XENO_BNAME_LEN];
      /* Init variable */
      xprm.dyn_param  = dyn_param;
      xprm.data       = p_msg;
      xprm.len        = length_to_read;
      xprm.ret        = 0;
      /* Get proces pid and create base name for rt task */
      read_ret = getpid();
      sprintf(bname,"pid%i_dyn4lin_r",read_ret);
      /* Create and start real time task */
      read_ret = rt_task_spawn(&task,bname,0,DYN_XENO_TASK_PRIO,
			       T_FPU|T_JOINABLE,TSR_read,&xprm);
      if(read_ret < 0){
	error_treat(dyn_param,read_ret,"Real time task failure");
	return DYN_PORT_SOCKET_FAILURE;
      }
      /* Wait on the termination of a real-time task */
      read_ret = rt_task_join(&task);
      if(read_ret < 0){
	error_treat(dyn_param,read_ret,"Real time task join failure");
	return DYN_PORT_SOCKET_FAILURE;
      }
      /* Set return value */
      read_ret = xprm.ret;
    }
    if (read_ret == -ETIMEDOUT) {
      return DYN_TIMEOUT_ERROR;
    }
    if (read_ret <= 0) {
      error_treat(dyn_param, DYN_PORT_SOCKET_FAILURE,
		  "Read port/socket failure");
      return DYN_PORT_SOCKET_FAILURE;
    }
#else
  while (select_ret) {
    read_ret = read(dyn_param->fd, p_msg, length_to_read);
    if (read_ret == 0) {
      /* The only negative possible value is -1 */
      error_treat(dyn_param, DYN_PORT_SOCKET_FAILURE,
          "Read port/socket failure");
      return DYN_PORT_SOCKET_FAILURE;
    }
#endif

    /* Sums bytes received */
    (*p_msg_length) += read_ret;
    /* Display the hex code of each character received */
    if (dyn_param->debug) {
      int i;
      for (i = 0; i < read_ret; ++i)
        printf("<0x%.2X>", p_msg[i]);
    }
    if ((*p_msg_length) < msg_length_computed) {
      /* Message incomplete */
      length_to_read = msg_length_computed - (*p_msg_length);
    } else
      length_to_read = 0;
    /* Moves the pointer to receive other data */
    p_msg = &(p_msg[read_ret]);

#ifndef __XENO__
    if (length_to_read > 0) {
      /* If no character at the buffer wait  DYN_MAX_BYTE_TO_BYTE_TIME_MS ms
       * before to generate an error.
       */
      tv.tv_sec = 0;
      tv.tv_usec = DYN_MAX_BYTE_TO_BYTE_TIME_MS * 1000;

      WAIT_DATA();
    } else {
      /* All chars are received */
      select_ret = FALSE;
    }
#endif

  }
  if (dyn_param->debug)
    printf("\n");
  return check_checksum(msg, (*p_msg_length));
}

/* Receive message
 * Status: complete.
 * Może dodać spradzanie czy odpowiedź nadeszła od dobrego ID?
 */
static int dyn_receive(dyn_param_t *dyn_param, uint8_t *query,
    uint8_t *response) {
  int ret = 0;
  int response_length;
  int response_length_computed;

  response_length_computed = compute_response_length(dyn_param, query);
  if (response_length_computed)
    ret = receive_msg(dyn_param, response_length_computed, response,
        &response_length);

  if (ret == DYN_TIMEOUT_ERROR) {
    error_treat(dyn_param, ret, "Communication time out");
  } else if (ret == DYN_CHECKSUM_ERROR) {
    char s_error[64];
    sprintf(s_error,
        "invalid checksum received 0x%.2X - checksum calculated 0x%.2X",
        response[response_length - 1], checksum(response, response_length - 2));
    error_treat(dyn_param, ret, s_error);
  } else if (ret >= DYN_NO_ERROR && response_length_computed) {
    dyn_servo_t *servo = dyn_get_servo(dyn_param, response[DYN_FRAMEORDER_ID]);
    if (servo)
      servo->error_code = response[DYN_FRAMEORDER_ERROR];
    ret = DYN_NO_ERROR;
  }

  return ret;
}

/* Initializes the dyn_param_t structure:
 * - device:  "/dev/ttyS0"
 * - baud:    9600, 19200, 57600, 115200, etc.
 * Status: complete.
 */
void dyn_init(dyn_param_t *dyn_param, const char *device, int baud,
    dyn_servo_t *dyn_servo, uint8_t length) {
  dyn_param->fd = -1;
  strcpy(dyn_param->device, device);
  dyn_param->baud = baud;
  memset(&dyn_param->old_tios, 0, sizeof(dyn_param->old_tios));
  dyn_param->error_handling = FLUSH_ON_ERROR;
  dyn_param->dyn_servo = dyn_servo;
  dyn_param->dyn_servo_length = length;
  qsort(dyn_servo, length, sizeof(dyn_servo_t), (int(*)(const void *,
      const void *)) compare_dyn_servo_qsort);
}

/* By default, the error handling mode used is FLUSH_ON_ERROR.
 *
 * With FLUSH_ON_ERROR, the library will flush to I/O port.
 *
 * With NOP_ON_ERROR, it is expected that the application will
 * check for error returns and deal with them as necessary.
 *
 * Status: complete.
 */
void dyn_set_error_handling(dyn_param_t *dyn_param,
    error_handling_t error_handling) {
  if (error_handling == FLUSH_ON_ERROR || error_handling == NOP_ON_ERROR) {
    dyn_param->error_handling = error_handling;
  } else {
    printf("Invalid setting for error handling (not changed)");
  }
}

/* Sets up a serial port
 * Status: complete
 */
int dyn_connect(dyn_param_t *dyn_param) {
#ifdef __XENO__
  struct rtser_config dev_config = {
    .config_mask = 0xFFFF,
    .baud_rate = dyn_param->baud,
    .parity = RTSER_DEF_PARITY,
    .data_bits = RTSER_DEF_BITS,
    .stop_bits = RTSER_DEF_STOPB,
    .handshake = RTSER_DEF_HAND,
    .fifo_depth = RTSER_DEF_FIFO_DEPTH,
    .rx_timeout = DYN_MAX_BYTE_TO_BYTE_TIME_MS * 1000000, /* 0.5 s */
    .tx_timeout = RTSER_DEF_TIMEOUT,
    .event_timeout = RTSER_DEF_TIMEOUT,
    .timestamp_history = RTSER_DEF_TIMESTAMP_HISTORY,
    .event_mask = RTSER_DEF_EVENT_MASK,
  };
  int err;
#else
  struct termios tios;
  speed_t speed;
#endif

  if (dyn_param->debug)
    printf("Opening %s at %d bauds\n", dyn_param->device, dyn_param->baud);

#ifdef __XENO__
  /* Open rt serial port*/
  dyn_param->fd = rt_dev_open(dyn_param->device,0);
  if (dyn_param->fd < 0) {
    fprintf(stderr,"%s: Can't open device: %s\n",
        __FUNCTION__,dyn_param->device);
    return -1;
  }
  /* Config port */
  err = rt_dev_ioctl(dyn_param->fd, RTSER_RTIOC_SET_CONFIG, &dev_config);
  if(err) {
    fprintf(stderr,"%s: Can't configure device: %s\n",
        __FUNCTION__,dyn_param->device);
    return -1;
  }
#else
  dyn_param->fd = open(dyn_param->device, O_RDWR | O_NOCTTY | O_NDELAY);
  if (dyn_param->fd < 0) {
    perror("open");
    printf("ERROR Can't open the device %s (errno %d)\n", dyn_param->device,
        errno);
    return -1;
  }

  /* Save */
  tcgetattr(dyn_param->fd, &(dyn_param->old_tios));

  memset(&tios, 0, sizeof(struct termios));

  switch (dyn_param->baud) {
  case 110:
    speed = B110;
    break;
  case 300:
    speed = B300;
    break;
  case 600:
    speed = B600;
    break;
  case 1200:
    speed = B1200;
    break;
  case 2400:
    speed = B2400;
    break;
  case 4800:
    speed = B4800;
    break;
  case 9600:
    speed = B9600;
    break;
  case 19200:
    speed = B19200;
    break;
  case 38400:
    speed = B38400;
    break;
  case 57600:
    speed = B57600;
    break;
  case 115200:
    speed = B115200;
    break;
  default:
    speed = B115200;
    printf("WARNING Unknown baud rate %d for %s (B115200 used)\n",
        dyn_param->baud, dyn_param->device);
  }

  /* Set the baud rate */
  if ((cfsetispeed(&tios, speed) < 0) || (cfsetospeed(&tios, speed) < 0)) {
    perror("cfsetispeed/cfsetospeed\n");
    return -1;
  }

  /* C_CFLAG  Control options
   * CLOCAL  Local line - do not change "owner" of port
   * CREAD  Enable receiver
   */
  tios.c_cflag |= (CREAD | CLOCAL);

  /* CSIZE, HUPCL, CRTSCT (hardware flow control) */
  tios.c_cflag &= ~CSIZE;
  tios.c_cflag |= CS8;
  tios.c_cflag &= ~CSTOPB;
  tios.c_cflag &= ~PARENB;
  tios.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG);
  tios.c_iflag &= ~INPCK;
  tios.c_iflag &= ~(IXON | IXOFF | IXANY);
  tios.c_oflag &= ~OPOST;
  tios.c_cc[VMIN] = 0;
  tios.c_cc[VTIME] = 0;

  if (tcsetattr(dyn_param->fd, TCSANOW, &tios) < 0) {
    perror("tcsetattr\n");
    return -1;
  }
#endif

  return 0;
}

void dyn_close(dyn_param_t *dyn_param) {
#ifdef __XENO__
  rt_dev_close(dyn_param->fd);
#else
  if (tcsetattr(dyn_param->fd, TCSANOW, &(dyn_param->old_tios)) < 0)
    perror("tcsetattr");
  close(dyn_param->fd);
#endif
}

void dyn_set_debug(dyn_param_t *dyn_param, int boolean) {
  dyn_param->debug = boolean;
}

/* Implementacje API */
int dyn_ping(dyn_param_t *dyn_param, uint8_t id) {
  int ret;
  int query_length;
  uint8_t query[DYN_MIN_FRAME_LENGTH];
  uint8_t respone[DYN_MIN_FRAME_LENGTH];

  ret = check_parameters(id, DYN_INS_PING, 0, 0);
  if (ret)
    return ret;

  /* Build query */
  query_length = build_query_basis(id, DYN_INS_PING, 0, query);

  ret = dyn_send(dyn_param, query, query_length);
  if (ret > 0)
    ret = dyn_receive(dyn_param, query, respone);

  return ret;
}

int dyn_read_data(dyn_param_t *dyn_param, uint8_t id, uint8_t start_address,
    uint8_t nb, uint8_t *data) {
  int ret;
  int query_length;
  uint8_t query[DYN_MIN_FRAME_LENGTH + 2];
  uint8_t respone[DYN_MAX_FRAME_LENGTH];

  ret = check_parameters(id, DYN_INS_READ_DATA, start_address, nb);
  if (ret)
    return ret;

  /* Build query */
  query_length = build_query_basis(id, DYN_INS_READ_DATA, 2, query);
  query[query_length++] = start_address;
  query[query_length++] = nb;

  ret = dyn_send(dyn_param, query, query_length);
  if (ret > 0) {
    ret = dyn_receive(dyn_param, query, respone);
    if (ret >= 0)
      memcpy(data, &respone[DYN_FRAMEORDER_PARAMETER], nb);
  }

  return ret;
}

static int write_data(dyn_param_t *dyn_param, uint8_t id,
    uint8_t start_address, uint8_t nb, uint8_t *data, uint8_t reg) {
  int ret;
  int query_length;
  uint8_t query[DYN_MAX_FRAME_LENGTH];
  uint8_t respone[DYN_MIN_FRAME_LENGTH];

  ret = check_parameters(id, (reg ? DYN_INS_REG_WRITE : DYN_INS_WRITE_DATA),
      start_address, nb);
  if (ret)
    return ret;

  /* Build query */
  query_length = build_query_basis(id, (reg ? DYN_INS_REG_WRITE
      : DYN_INS_WRITE_DATA), nb + 1, query);
  query[query_length++] = start_address;
  memcpy(&query[query_length], data, nb);
  query_length += nb;

  ret = dyn_send(dyn_param, query, query_length);
  if (ret > 0) {
    ret = dyn_receive(dyn_param, query, respone);
    /* Należy coś zrobić z errorcode */
  }

  return ret;
}

int dyn_write_data(dyn_param_t *dyn_param, uint8_t id, uint8_t start_address,
    uint8_t nb, uint8_t *data) {
  return write_data(dyn_param, id, start_address, nb, data, FALSE);
}

int dyn_reg_write_data(dyn_param_t *dyn_param, uint8_t id,
    uint8_t start_address, uint8_t nb, uint8_t *data) {
  return write_data(dyn_param, id, start_address, nb, data, TRUE);
}

int dyn_action(dyn_param_t *dyn_param, uint8_t id) {
  int ret;
  int query_length;
  uint8_t query[DYN_MIN_FRAME_LENGTH];
  uint8_t respone[DYN_MIN_FRAME_LENGTH];

  ret = check_parameters(id, DYN_INS_ACTION, 0, 0);
  if (ret)
    return ret;

  /* Build query */
  query_length = build_query_basis(id, DYN_INS_ACTION, 0, query);

  ret = dyn_send(dyn_param, query, query_length);
  if (ret > 0)
    ret = dyn_receive(dyn_param, query, respone);

  return ret;
}

int dyn_reset(dyn_param_t *dyn_param, uint8_t id) {
  int ret;
  int query_length;
  uint8_t query[DYN_MIN_FRAME_LENGTH];
  uint8_t respone[DYN_MIN_FRAME_LENGTH];

  ret = check_parameters(id, DYN_INS_RESET, 0, 0);
  if (ret)
    return ret;

  /* Build query */
  query_length = build_query_basis(id, DYN_INS_RESET, 0, query);

  ret = dyn_send(dyn_param, query, query_length);
  if (ret > 0)
    ret = dyn_receive(dyn_param, query, respone);

  return ret;
}

int dyn_sync_write_data(dyn_param_t *dyn_param, uint8_t start_address,
    uint8_t *data, uint8_t n) {
  int ret;
  int query_length;
  uint8_t query[DYN_MAX_FRAME_LENGTH];

  /*ret
   = check_parameters(0, DYN_INS_SYNC_WRITE, start_address, (nb + 1) * n + 2);
   if (ret)
   return ret;*/

  /* Build query */
  query_length = build_query_basis(DYN_BROADCAST, DYN_INS_SYNC_WRITE, n + 1,
      query);
  query[query_length++] = start_address;
  memcpy(&query[query_length], data, n);
  query_length += n;

  ret = dyn_send(dyn_param, query, query_length);

  return ret;
}

int dyn_get_config(dyn_param_t *dyn_param, uint8_t id, dyn_config_t *dyn_config) {
  int ret;
  uint8_t data[DYN_ROM_TABLE_LENGTH];

  ret = dyn_read_data(dyn_param, id, DYN_ADR_MODEL_NUMBER_L,
      DYN_ROM_TABLE_LENGTH, data);
  if (ret)
    return ret;

  dyn_data2config(data, dyn_config);

  return ret;
}

int dyn_set_config(dyn_param_t *dyn_param, uint8_t id,
       const dyn_config_t *dyn_config)
{
  int          ret;
  unsigned int i;
  uint8_t      new_data[DYN_ROM_TABLE_LENGTH];
  uint8_t      old_data[DYN_ROM_TABLE_LENGTH];
  dyn_config_t dyn_config_old;

  ret = dyn_get_config(dyn_param, id, &dyn_config_old);
  if (ret < DYN_NO_ERROR)
    return ret;
  dyn_config2data(&dyn_config_old, old_data);
  dyn_config2data(dyn_config, new_data);
  for (i = 2; i < sizeof(dyn_rom_table_index)/sizeof(dyn_rom_table_index[0]);
       ++i) {
    uint8_t adr;
    uint8_t idx;
    adr = dyn_rom_table_address[i];
    idx = dyn_rom_table_index[i];
    if (adr == DYN_ADR_UNUSED_WORD) {
      continue;
    }
    if (idx == sizeof(uint8_t)) {
      if (old_data[adr] != new_data[adr])
        ret = dyn_write_data(dyn_param, id, adr, 1, &new_data[adr]);
    }
    else if(idx == sizeof(uint16_t)){
      uint16_t old,new;
      old = *(uint16_t*) &old_data[adr];
      new = *(uint16_t*) &new_data[adr];
      if (old != new) {
        uint8_t data[] = {DYN_Word16LSB(new),DYN_Word16MSB(new)};
  ret = dyn_write_data(dyn_param, id, adr, sizeof(uint16_t), data);
      }
    }
    else{
      ret = DYN_OUT_OF_RANGE;
    }
    if (ret < DYN_NO_ERROR) return ret;
  }
  return ret;
}

int dyn_get_return_level_verify_config(dyn_param_t *dyn_param) {
  int ret = 0;
  int i;
  uint8_t data[DYN_ROM_TABLE_LENGTH];

  for (i = 0; i < dyn_param->dyn_servo_length; ++i) {
    /* Check if servo exists */
    ret = dyn_ping(dyn_param, dyn_param->dyn_servo[i].id);
    if (ret < DYN_NO_ERROR)
      return ret;

    /* Check if servo has status return level greater than 0 */
    dyn_param->dyn_servo[i].return_level = 1;
    ret = dyn_read_data(dyn_param, dyn_param->dyn_servo[i].id, DYN_ADR_MODEL_NUMBER_L,
              DYN_ROM_TABLE_LENGTH, data);
    if (ret < DYN_NO_ERROR) {
      dyn_param->dyn_servo[i].return_level = 0;
      continue;
    }

    /* Check if servo has status return level greater than 1 */
    dyn_param->dyn_servo[i].return_level = 2;
    ret = dyn_action(dyn_param, dyn_param->dyn_servo[i].id);
    if (ret < DYN_NO_ERROR) {
      dyn_param->dyn_servo[i].return_level = 1;
      continue;
    }
  }

  return ret;
}

int dyn_set_torque(dyn_param_t *dyn_param, uint8_t id, uint8_t enable) {
  return dyn_write_data(dyn_param, id, DYN_ADR_TORQUE_ENABLE, 1, &enable);
}

int dyn_set_torque_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *enable,
    uint8_t n) {
  uint8_t data[2* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(1), dyn_puint8(id),
      dyn_puint8(enable)) ;

  return dyn_sync_write_data(dyn_param, DYN_ADR_TORQUE_ENABLE, data,
      sizeof(data));
}

int dyn_set_led(dyn_param_t *dyn_param, uint8_t id, uint8_t enable) {
  return dyn_write_data(dyn_param, id, DYN_ADR_LED, 1, &enable);
}

int dyn_set_led_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *enable,
    uint8_t n) {
  uint8_t data[2* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(1), dyn_puint8(id),
    dyn_puint8(enable)) ;

  return dyn_sync_write_data(dyn_param, DYN_ADR_LED, data, sizeof(data));
}

int dyn_set_margin(dyn_param_t *dyn_param, uint8_t id, uint8_t cw, uint8_t ccw) {
  uint8_t data[2] = { cw, ccw };

  return dyn_write_data(dyn_param, id, DYN_ADR_CW_COMPLIANCE_MARGIN, 2, data);
}

int dyn_set_margin_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n) {
  uint8_t data[3* n + 1];

  dyn_param2data(data, 4, n, dyn_uint8(2), dyn_puint8(id),
        dyn_puint8(cw),  dyn_puint8(ccw));

  return dyn_sync_write_data(dyn_param, DYN_ADR_CW_COMPLIANCE_MARGIN, data,
      sizeof(data));
}

int dyn_get_margin(dyn_param_t *dyn_param, uint8_t id, uint8_t *cw,
    uint8_t *ccw) {
  uint8_t data[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_CW_COMPLIANCE_MARGIN, 2, data);
  *cw = data[1];
  *ccw = data[2];

  return ret;
}

int dyn_get_margin_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n) {
  int ret = 0;
  int i;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_margin(dyn_param, id[i], &cw[i], &ccw[i]);
    if (ret < DYN_NO_ERROR)
      return ret;
  }
  return ret;
}

int dyn_set_slope(dyn_param_t *dyn_param, uint8_t id, uint8_t cw, uint8_t ccw) {
  uint8_t data[2] = { cw, ccw };

  return dyn_write_data(dyn_param, id, DYN_ADR_CW_COMPLIANCE_SLOPE, 2, data);
}

int dyn_set_slope_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n) {
  uint8_t data[3* n + 1];

  dyn_param2data(data, 4, n, dyn_uint8(2), dyn_puint8(id),
          dyn_puint8(cw),  dyn_puint8(ccw));

  return dyn_sync_write_data(dyn_param, DYN_ADR_CW_COMPLIANCE_SLOPE, data,
      sizeof(data));
}

int dyn_get_slope(dyn_param_t *dyn_param, uint8_t id, uint8_t *cw, uint8_t *ccw) {
  uint8_t data[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_CW_COMPLIANCE_SLOPE, 2, data);
  *cw = data[1];
  *ccw = data[2];

  return ret;
}

int dyn_get_slope_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *cw,
    uint8_t *ccw, uint8_t n) {
  int ret = 0;
  int i;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_margin(dyn_param, id[i], &cw[i], &ccw[i]);
    if (ret < DYN_NO_ERROR)
      return ret;
  }
  return ret;
}

int dyn_set_position(dyn_param_t *dyn_param, uint8_t id, uint16_t position) {
  uint8_t data[2] = { DYN_Word16LSB(position), DYN_Word16MSB(position) };
  return dyn_write_data(dyn_param, id, DYN_ADR_GOAL_POSITION_L, 2, data);
}

int dyn_set_position_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *position, uint8_t n) {
  uint8_t data[3* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(2), dyn_puint8(id),
            dyn_puint16(position)) ;

  return dyn_sync_write_data(dyn_param, DYN_ADR_GOAL_POSITION_L, data,
      sizeof(data));
}

int dyn_set_speed(dyn_param_t *dyn_param, uint8_t id, uint16_t speed) {
  uint8_t data[2] = { DYN_Word16LSB(speed), DYN_Word16MSB(speed) };
  return dyn_write_data(dyn_param, id, DYN_ADR_MOVING_SPEED_L, 2, data);
}

int dyn_set_speed_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *speed,
    uint8_t n) {
  uint8_t data[3* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(2), dyn_puint8(id),
              dyn_puint16(speed)) ;

  return dyn_sync_write_data(dyn_param, DYN_ADR_MOVING_SPEED_L, data,
      sizeof(data));
}

int dyn_set_movement(dyn_param_t *dyn_param, uint8_t id, uint16_t position,
    uint16_t speed) {
  uint8_t data[4] = { DYN_Word16LSB(position), DYN_Word16MSB(position),
      DYN_Word16LSB(speed), DYN_Word16MSB(speed) };

  return dyn_write_data(dyn_param, id, DYN_ADR_GOAL_POSITION_L, 4, data);
}

int dyn_set_movement_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *position, uint16_t *speed, uint8_t n) {
  uint8_t data[5* n + 1];

  dyn_param2data(data, 4, n, dyn_uint8(4), dyn_puint8(id),
      dyn_puint16(position),  dyn_puint16(speed));

  return dyn_sync_write_data(dyn_param, DYN_ADR_GOAL_POSITION_L, data,
      sizeof(data));
}

int dyn_set_torque_limit(dyn_param_t *dyn_param, uint8_t id, uint16_t torque) {
  uint8_t data[2] = { DYN_Word16LSB(torque), DYN_Word16MSB(torque) };
  return dyn_write_data(dyn_param, id, DYN_ADR_TORQUE_LIMIT_L, 2, data);
}

int dyn_set_torque_limit_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *torque, uint8_t n) {
  uint8_t data[3* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(2), dyn_puint8(id), dyn_puint16(torque));

  return dyn_sync_write_data(dyn_param, DYN_ADR_TORQUE_LIMIT_L, data,
      sizeof(data));
}

int dyn_get_torque_limit(dyn_param_t *dyn_param, uint8_t id, uint16_t *data) {
  uint8_t respone[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_TORQUE_LIMIT_L, 2, respone);
  *data = DYN_MSBLSBWord16(respone[1], respone[0]);

  return ret;
}

int dyn_get_torque_limit_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *data, uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_torque_limit(dyn_param, id[i], &data[i]);
    if (ret < DYN_NO_ERROR)
      return ret;
  }
  return ret;
}

int dyn_get_position(dyn_param_t *dyn_param, uint8_t id, uint16_t *data) {
  uint8_t respone[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_PRESENT_POSITION_L, 2, respone);
  *data = DYN_MSBLSBWord16(respone[1], respone[0]);

  return ret;
}

int dyn_get_position_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *data,
    uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_position(dyn_param, id[i], &data[i]);
    if (ret)
      return ret;
  }
  return ret;
}

int dyn_get_speed(dyn_param_t *dyn_param, uint8_t id, int16_t *data) {
  uint8_t respone[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_PRESENT_SPEED_L, 2, respone);
  *data = DYN_GET_SPEED(respone[1], respone[0]);

  return ret;
}

int dyn_get_speed_multi(dyn_param_t *dyn_param, uint8_t *id, int16_t *data,
    uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_speed(dyn_param, id[i], &data[i]);
    if (ret)
      return ret;
  }
  return ret;
}

int dyn_get_movement(dyn_param_t *dyn_param, uint8_t id, uint16_t *position,
    int16_t *speed) {
  uint8_t respone[4];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_PRESENT_POSITION_L, 4, respone);
  *position = DYN_MSBLSBWord16(respone[1], respone[0]);
  *speed = DYN_GET_SPEED(respone[3], respone[2]);

  return ret;

}

int dyn_get_movement_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint16_t *position, int16_t *speed, uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_movement(dyn_param, id[i], &position[i], &speed[i]);
    if (ret)
      return ret;
  }
  return ret;
}

int dyn_get_load(dyn_param_t *dyn_param, uint8_t id, int16_t *data) {
  uint8_t respone[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_PRESENT_LOAD_L, 2, respone);
  *data = DYN_GET_LOAD(respone[1], respone[0]);

  return ret;
}

int dyn_get_load_multi(dyn_param_t *dyn_param, uint8_t *id, int16_t *data,
    uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_load(dyn_param, id[i], &data[i]);
    if (ret)
      return ret;
  }

  return ret;
}

int dyn_get_voltage(dyn_param_t *dyn_param, uint8_t id, uint8_t *voltage) {
  return dyn_read_data(dyn_param, id, DYN_ADR_PRESENT_VOLTAGE, 1, voltage);
}

int dyn_get_voltage_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint8_t *voltage, uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_voltage(dyn_param, id[i], &voltage[i]);
    if (ret)
      return ret;
  }
  return ret;
}

int dyn_get_temperature(dyn_param_t *dyn_param, uint8_t id,
    uint8_t *temperature) {
  return dyn_read_data(dyn_param, id, DYN_ADR_PRESENT_TEMPERATURE, 1,
      temperature);
}

int dyn_get_temperature_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint8_t *temperature, uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_temperature(dyn_param, id[i], &temperature[i]);
    if (ret)
      return ret;
  }
  return ret;
}

int dyn_get_registered(dyn_param_t *dyn_param, uint8_t id) {
  uint8_t data;
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_REGISTERED_INSTRUCTION, 1, &data);

  if (ret)
    return ret;

  return data;
}

int dyn_get_registered_multi(dyn_param_t *dyn_param, uint8_t *id,
    uint8_t *data, uint8_t n) {
  uint8_t registered = 0;
  int i, ret;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_moving(dyn_param, id[i]);
    if (ret < DYN_NO_ERROR)
      return ret;

    data[i] = (uint8_t) ret;
    registered = MAX(registered, data[i]);
  }

  return registered;
}

int dyn_get_moving(dyn_param_t *dyn_param, uint8_t id) {
  uint8_t data;
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_MOVING, 1, &data);

  if (ret)
    return ret;

  return data;
}

int dyn_get_moving_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *data,
    uint8_t n) {
  uint8_t moving = 0;
  int i;
  int ret;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_moving(dyn_param, id[i]);
    if (ret < DYN_NO_ERROR)
      return ret;

    if (data)
      data[i] = (uint8_t) ret;

    moving = MAX(moving, ret);
  }
  return moving;
}

int dyn_set_lock(dyn_param_t *dyn_param, uint8_t id, uint8_t enable) {
  return dyn_write_data(dyn_param, id, DYN_ADR_LOCK, 1, &enable);
}

int dyn_set_lock_multi(dyn_param_t *dyn_param, uint8_t *id, uint8_t *enable,
    uint8_t n) {
  uint8_t data[2* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(1), dyn_puint8(id),
      dyn_puint8(enable)) ;

  return dyn_sync_write_data(dyn_param, DYN_ADR_LOCK, data, sizeof(data));
}

int dyn_set_punch(dyn_param_t *dyn_param, uint8_t id, uint16_t punch) {
  uint8_t data[2] = { DYN_Word16LSB(punch), DYN_Word16MSB(punch) };
  return dyn_write_data(dyn_param, id, DYN_ADR_PUNCH_L, 2, data);
}

int dyn_set_punch_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *punch,
    uint8_t n) {
  uint8_t data[3* n + 1];

  dyn_param2data(data, 3, n, dyn_uint8(2), dyn_puint8(id),
                dyn_puint16(punch)) ;

  return dyn_sync_write_data(dyn_param, DYN_ADR_PUNCH_L, data, sizeof(data));
}

int dyn_get_punch(dyn_param_t *dyn_param, uint8_t id, uint16_t *data) {
  uint8_t respone[2];
  int ret;

  ret = dyn_read_data(dyn_param, id, DYN_ADR_PUNCH_L, 2, respone);
  *data = DYN_MSBLSBWord16(respone[1], respone[0]);

  return ret;
}

int dyn_get_punch_multi(dyn_param_t *dyn_param, uint8_t *id, uint16_t *data,
    uint8_t n) {
  int i;
  int ret = 0;

  for (i = 0; i < n; ++i) {
    ret = dyn_get_punch(dyn_param, id[i], &data[i]);
    if (ret)
      return ret;
  }
  return ret;
}

void dyn_config2data(const dyn_config_t *dyn_config, uint8_t *data) {
  data[DYN_ADR_ID] = dyn_config->id;
  data[DYN_ADR_BAUD_RATE] = dyn_config->baud;
  data[DYN_ADR_RETURN_DELAY_TIME] = dyn_config->delay;
  data[DYN_ADR_CW_ANGLE_LIMIT_L] = DYN_Word16LSB(dyn_config->CWLim);
  data[DYN_ADR_CW_ANGLE_LIMIT_H] = DYN_Word16MSB(dyn_config->CWLim);
  data[DYN_ADR_CCW_ANGLE_LIMIT_L] = DYN_Word16LSB(dyn_config->CCWLim);
  data[DYN_ADR_CCW_ANGLE_LIMIT_H] = DYN_Word16MSB(dyn_config->CCWLim);
  data[DYN_ADR_UNUSED_WORD] = DYN_WORD8_SKIP;
  data[DYN_ADR_HIGHEST_LIMIT_TEMPERATURE] = dyn_config->hTempLimit;
  data[DYN_ADR_LOWEST_LIMIT_VOLTAGE] = dyn_config->lVoltLimit;
  data[DYN_ADR_HIGHEST_LIMIT_VOLTAGE] = dyn_config->hVoltLimit;
  data[DYN_ADR_MAX_TORQUE_L] = DYN_Word16LSB(dyn_config->torque);
  data[DYN_ADR_MAX_TORQUE_H] = DYN_Word16MSB(dyn_config->torque);
  data[DYN_ADR_STATUS_RETURN_LEVEL] = dyn_config->statLevel;
  data[DYN_ADR_ALARM_LED] = dyn_config->led;
  data[DYN_ADR_ALARM_SHUTDOWN] = dyn_config->shutdown;
}

void dyn_data2config(uint8_t *data, dyn_config_t *dyn_config) {
  dyn_config->model = DYN_MSBLSBWord16(data[DYN_ADR_MODEL_NUMBER_H],
      data[DYN_ADR_MODEL_NUMBER_L]);
  dyn_config->firmware = data[DYN_ADR_VERSION_OF_FIRMWARE];
  dyn_config->id = data[DYN_ADR_ID];
  dyn_config->baud = data[DYN_ADR_BAUD_RATE];
  dyn_config->delay = data[DYN_ADR_RETURN_DELAY_TIME];
  dyn_config->CWLim = DYN_MSBLSBWord16(data[DYN_ADR_CW_ANGLE_LIMIT_H],
      data[DYN_ADR_CW_ANGLE_LIMIT_L]);
  dyn_config->CCWLim = DYN_MSBLSBWord16(data[DYN_ADR_CCW_ANGLE_LIMIT_H],
      data[DYN_ADR_CCW_ANGLE_LIMIT_L]);
  dyn_config->hTempLimit = data[DYN_ADR_HIGHEST_LIMIT_TEMPERATURE];
  dyn_config->lVoltLimit = data[DYN_ADR_LOWEST_LIMIT_VOLTAGE];
  dyn_config->hVoltLimit = data[DYN_ADR_HIGHEST_LIMIT_VOLTAGE];
  dyn_config->torque = DYN_MSBLSBWord16(data[DYN_ADR_MAX_TORQUE_H],
      data[DYN_ADR_MAX_TORQUE_L]);
  dyn_config->statLevel = data[DYN_ADR_STATUS_RETURN_LEVEL];
  dyn_config->led = data[DYN_ADR_ALARM_LED];
  dyn_config->shutdown = data[DYN_ADR_ALARM_SHUTDOWN];
}

dyn_servo_t *dyn_get_servo(dyn_param_t *dyn_param, uint8_t id) {
  return (dyn_servo_t*) bsearch(&id, dyn_param->dyn_servo,
      dyn_param->dyn_servo_length, sizeof(dyn_servo_t), (int(*)(const void *,
          const void *)) compare_dyn_servo_binsearch);
}

uint8_t dyn_get_error_code(dyn_param_t *dyn_param, uint8_t id) {
  dyn_servo_t *servo = dyn_get_servo(dyn_param, id);

  if (servo)
    return servo->error_code;
  else
    return DYN_WORD8_SKIP;
}

uint8_t dyn_get_return_level(dyn_param_t *dyn_param, uint8_t id) {
  dyn_servo_t *servo = dyn_get_servo(dyn_param, id);

  if (servo)
    return servo->return_level;
  else
    return DYN_WORD8_SKIP;
}

int dyn_param2data(uint8_t *data, int n, int length, ...) {
  va_list ap;
  int i, j;
  dyn_parameter_type_t param_types[n];
  dyn_parameter_t param[n];

  va_start(ap,length);
  /* Recognize arguments */
  for (i = 0; i < n; ++i) {
    param_types[i] = va_arg(ap, dyn_parameter_type_t);
    switch (param_types[i]) {
    case DYN_UINT8:
      param[i].uint8 = (uint8_t) va_arg(ap, unsigned int);
      break;
    case DYN_PUINT8:
      param[i].puint8 = va_arg(ap, uint8_t*);
      break;
    case DYN_UINT16:
      param[i].uint16 = (uint16_t) va_arg(ap, unsigned int);
      break;
    case DYN_PUINT16:
    default:
      param[i].puint16 = va_arg(ap, uint16_t*);
      break;
    }
  }
  /* Put values from variables */
  for (i = 0; i < n; ++i) {
    switch (param_types[i]) {
    case DYN_UINT8:
      *data++ = param[i].uint8;
      break;
    case DYN_UINT16:
      *data++ = DYN_Word16LSB(param[i].uint16);
      *data++ = DYN_Word16MSB(param[i].uint16);
      break;
    default:
      break;
    }
  }
  /* Put values from arrays */
  for (i = 0; i < length; ++i) {
    for (j = 0; j < n; ++j) {
      switch (param_types[j]) {
      case DYN_PUINT8:
        *data++ = param[j].puint8[i];
        break;
      case DYN_PUINT16:
        *data++ = DYN_Word16LSB(param[j].puint16[i]);
        *data++ = DYN_Word16MSB(param[j].puint16[i]);
        break;
      default:
        break;
      }
    }
  }
  va_end(ap);

  return 0;
}
